#!/usr/bin/env python3
# need the 'shebang' for linux

#imports
import os.path
from os import walk
import fnmatch
import shutil
import subprocess

# setup some variables
mProjectName='DogFight'
mOS = os.name

if mOS == 'nt': # windows
    mUnrealEngineRoot='C:\\Program Files\\Epic Games\\4.9'
    mUnrealBuild='{}\\Engine\\Build\\BatchFiles\\RunUAT.bat'.format(mUnrealEngineRoot)
    mUnrealPak='{}\\Engine\\Binaries\\Win64\\UnrealPak.exe'.format(mUnrealEngineRoot)
    mProject='C:\\Projects\\DogFight'
    mProjectContent='{}\\Content'.format(mProject)
    mBuild='{}\\Build\\Paks'.format(mProject)
else:
    mUnrealEngineRoot='/home/linux/UnrealEngine/4.9'
    mUnrealBuild='{}/Engine/Build/BatchFiles/RunUAT.sh'.format(mUnrealEngineRoot)
    mUnrealPak='{}/Engine/Binaries/Linux/UnrealPak'.format(mUnrealEngineRoot)
    mProject='/home/linux/Projects/DogFight'
    mProjectContent='{}/Content'.format(mProject)
    mBuild='{}/Build/Paks'.format(mProject)

mUProject=os.path.join(mProject, mProjectName + '.uproject')



# function get list of folders in the project content directory
def ListContent():
    print(mProjectContent)
    exists = os.path.exists(mProjectContent)
    fileList = []

    if exists:
        print('Content List:')
        for f in os.listdir(mProjectContent):
            if os.path.isdir(os.path.join(mProjectContent, f)):
                fileList.append(f)
                print(f)
    else:
        print('Error: content folder not found!')

    #sort the list
    fileList.sort()
    return fileList


# recursive list of files in a directory
def GetContent(path):
    fileList = []

    print('\n-{}-'.format(path))
    for root, dirs, files in walk(os.path.join(mProjectContent, path)):
        for f in files:
            fileList.append(os.path.join(root, f))
            print(os.path.join(root, f))

    return fileList


# function to make the pak file
def MakePak(name, fileList):
    if (len(fileList) > 0):
        # convert the list to string seperated by spaces
        fileString = ' '.join(map(str, fileList))

        # execute/create pak
        package = '{}.pak'.format(os.path.join(mBuild, name))
        args = '{} {} {} -installed'.format(mUnrealPak, package, fileString)
        sp = subprocess.call(args.split(' '))
    else:
        print('Empty, skipping...')

    return


# function to package content by folder name
def PackageContent():
    contentList = ListContent()

    for f in contentList:
        assetList = []
        assetList = GetContent(f)
        MakePak(f, assetList)
    return

# function to package content by user input name
def PackageCustomContent():
    print(' Type the name of the folder which contains the content:')
    x = input(' -> ')
    print('\n')
    contentList = []
    contentList.append(x)

    for f in contentList:
        assetList = []
        assetList = GetContent(f)
        MakePak(f, assetList)
    return

# function to do a shipping build
def BuildShipping():
    # execute
    #RunUAT BuildCookRun -project="full_project_path_and_project_name.uproject" -noP4 -platform=Win64 -clientconfig=Development -serverconfig=Development -cook -allmaps -build -stage -pak -archive -archivedirectory="Output Directory"
    args = '{} BuildCookRun -project="{}" -noP4 -platform=Win64 -clientconfig=Development -serverconfig=Development -cook -allmaps -build -stage -pak -archive -archivedirectory="C:\\Output"'.format(mUnrealBuild, mUProject)
    sp = subprocess.call(args.split(' '))
    return

# display a user menu
def DisplayMenu():
    print ('\nPackage Project:')
    print(' 1) List Contents to Pak')
    print(' 2) Package Content')
    print(' 3) Build - Shipping')
    print(' 4) Package Custom Content')
    print(' 5) Quit')
    x = input(' -> ')
    print('\n')

    # no switch in python, this looks similar :)
    if x == '1':
        ListContent()
        DisplayMenu()
    elif x == '2':
        PackageContent()
        DisplayMenu()
    elif x == '3':
        BuildShipping()
        DisplayMenu()
    elif x == '4':
        PackageCustomContent()
        DisplayMenu()
    else:
        print('exiting')
        return


# main
if __name__ == '__main__':
    DisplayMenu()
