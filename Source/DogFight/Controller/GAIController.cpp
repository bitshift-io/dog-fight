// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "GAIController.h"
#include "Actor/GActor.h"
#include "Actor/HoverFlyState.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

#pragma optimize("", OPTIMISATION)

AGAIController::AGAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)//, IControllerInterface(ObjectInitializer)
{
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));

	bWantsPlayerState = true; // stops controller being cleaned up with the pawn

	//DefaultPawnClassForController = NULL;
}

void AGAIController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	//DOREPLIFETIME(AGAIController, DefaultPawnClassForController);
}

void AGAIController::InitPlayerState()
{
	Super::InitPlayerState();

	// server initialize also
	//PlayerState->ClientInitialize(this);
	check(PlayerState);
	Cast<AGPlayerState>(PlayerState)->Controller = this; // GetPawnController ?
}

void AGAIController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AGAIController::Tick(float deltaTime)
{
	Super::Tick(deltaTime);


	// dont update if dead
	if (!GetPawn()/* || GetPawn().GetHealthPercent() <= 0.0f*/)
		return;

	/*
	// clear all states
	foreach(ControllerAction action in ActionList)
	{
		if (action.Command == ControllerActionType.CAT_ViewX
			|| action.Command == ControllerActionType.CAT_ViewY)
			continue;

		action.m_state = 0.0f;
	}*/

	StateUpdate(deltaTime);

}

void AGAIController::StateUpdate(float deltaTime)
{
	AGActor* actor = Cast<AGActor>(GetPawn());

	m_timeInState += deltaTime; //gGame.mGameUpdate.GetDeltaTime();

	m_turnTimer -= deltaTime;
	if (m_turnTimer <= 0.0f)
	{
		// determine if this move should be strait or turning
		float straitOrTurn = FMath::FRandRange(0.0f, 100.0f);
		if (straitOrTurn <= StraitPercent)
		{
			m_turnTimer = FMath::FRandRange(MinStraitTime, MaxStraitTime);
		}
		else
		{
			m_turnTimer = FMath::FRandRange(MinTurnTime, MaxTurnTime);

			// turn
			bool leftOrRight = FMath::FRandRange(0.0f, 100.0f) > 50.0f;
			float value = leftOrRight ? 1.0f : -1.0f;
			actor->SetHorizontalMovementAxis(FMath::FRandRange(0.0f, 1.0f) * value);

		}
	}

	// now handles by behaviour tree
	/*
	if (ShouldFire())
	{
		actor->SetWeaponTarget(GetEnemy());
		actor->StartWeaponFire();
	}
	else
	{
		actor->StopWeaponFire();
	}*/
}

bool AGAIController::ShouldFire()
{
#if 0
	AGActor* aiActor = Cast<AGActor>(GetPawn());

	for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; ++it)
	{
		AGActor* enemyActor = Cast<AGActor>(*it);
		IControllerInterface* enemyController = Cast<IControllerInterface>(enemyActor->Controller);
		if (!enemyActor || !aiActor || enemyActor->IsDead() || enemyActor == aiActor || (enemyController && aiActor->GetPlayerState()->GetTeam() == enemyActor->GetPlayerState()->GetTeam()))
			continue;

		// calculate dot
		FVector enemyActorPos = enemyActor->GetActorLocation();
		FVector aiActorPos = aiActor->GetActorLocation();
		FVector dirToEnemy = enemyActorPos - aiActorPos;
		float distToEnemy = dirToEnemy.Size() / 100.0f; // cm to meters
		dirToEnemy.Normalize();

		FVector aiActorDir = aiActor->GetActorForwardVector();

		float dot = FVector::DotProduct(aiActorDir, dirToEnemy);

		if (dot > MinFireDot && distToEnemy > MinFireDistance && distToEnemy < MaxFireDistance)
		{
			SetEnemy(enemyActor);
			return true;
		}
	}
#endif
	return false;
}

AGActor* AGAIController::FindEnemy()
{
	if (!GetPawn())
		return NULL;

	const FVector actorPos = GetPawn()->GetActorLocation();
	float bestDistSq = MAX_FLT;
	AGActor* bestActor = NULL;

	for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; ++it)
	{
		AGActor* enemy = Cast<AGActor>(*it);
		if (enemy)
		{
			const float distSq = FVector::Dist(actorPos, enemy->GetActorLocation());
			if (distSq < bestDistSq)
			{
				bestDistSq = distSq;
				bestActor = enemy;
			}
		}
	}

	return bestActor;
}

void AGAIController::BeginInactiveState()
{
	Super::BeginInactiveState();
}

void AGAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	/*
	if (GetActor())
	{
	GetActor()->SetTeam(Team);
	//GetActor()->SetColour(Colour);
	}*/
}

void AGAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	AGActor* Bot = Cast<AGActor>(InPawn);

	// start behavior
	bool bIsAIDisabled = Cast<AGGameMode>(GetWorld()->GetAuthGameMode())->IsAIDisabled();
	if (Bot && Bot->BotBehavior && !bIsAIDisabled)
	{
		if (Bot->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Bot->BotBehavior->BlackboardAsset);
		}

		EnemyKeyID = BlackboardComp->GetKeyID("Enemy");
		NeedAmmoKeyID = BlackboardComp->GetKeyID("NeedAmmo");

		BehaviorComp->StartTree(*(Bot->BotBehavior));
	}
}

void AGAIController::UnPossess()
{
	Super::UnPossess();
}

void AGAIController::CheckAmmo(const class AWeapon* CurrentWeapon)
{
	if (CurrentWeapon && BlackboardComp)
	{
		const int32 Ammo = CurrentWeapon->GetCurrentAmmo();
		const int32 MaxAmmo = CurrentWeapon->GetMaxAmmo();
		const float Ratio = (float)Ammo / (float)MaxAmmo;

		BlackboardComp->SetValue<UBlackboardKeyType_Bool>(NeedAmmoKeyID, (Ratio <= 0.1f));
	}
}

void AGAIController::SetEnemy(class AGActor* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InPawn);
		SetFocus(InPawn);
	}
}

class AGActor* AGAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<AGActor>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID));
	}

	return NULL;
}

void AGAIController::Respawn()
{
	GetWorld()->GetAuthGameMode()->RestartPlayer(GetPawnController()); // untested
}

void AGAIController::FindClosestEnemy()
{
#if 0
	APawn* MyBot = GetPawn();
	if (MyBot == NULL)
	{
		return;
	}

	const FVector MyLoc = MyBot->GetActorLocation();
	float BestDistSq = MAX_FLT;
	AShooterCharacter* BestPawn = NULL;

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AShooterCharacter* TestPawn = Cast<AShooterCharacter>(*It);
		if (TestPawn && TestPawn->IsAlive() && TestPawn->IsEnemyFor(GetPawnController()))
		{
			const float DistSq = (TestPawn->GetActorLocation() - MyLoc).SizeSquared();
			if (DistSq < BestDistSq)
			{
				BestDistSq = DistSq;
				BestPawn = TestPawn;
			}
		}
	}

	if (BestPawn)
	{
		SetEnemy(BestPawn);
	}
#endif
}


bool AGAIController::FindClosestEnemyWithLOS(AGActor* ExcludeEnemy)
{
	AGActor* MyBot = Cast<AGActor>(GetPawn());
	if (!MyBot || !MyBot->GetInventory())
		return false;

	TArray<AWeapon*> WeaponList = MyBot->GetInventory()->WeaponList();
	for (int w = 0; w < WeaponList.Num(); ++w)
	{
		AGActor* BestEnemy = WeaponList[w]->FindClosestEnemyWithLOS(ExcludeEnemy);
		if (BestEnemy)
		{
			SetEnemy(BestEnemy);
			return true;
		}
	}

	return false;


#if 0
	bool bGotEnemy = false;
	APawn* MyBot = GetPawn();
	if (MyBot != NULL)
	{
		const FVector MyLoc = MyBot->GetActorLocation();
		float BestDistSq = MAX_FLT;
		AGActor* BestPawn = NULL;

		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			AGActor* TestPawn = Cast<AGActor>(*It);
			if (TestPawn && TestPawn != ExcludeEnemy && TestPawn->IsAlive() && TestPawn->IsEnemyFor(GetPawnController()))
			{
				if (HasWeaponLOSToEnemy(TestPawn, true) == true)
				{
					const float DistSq = (TestPawn->GetActorLocation() - MyLoc).SizeSquared();
					if (DistSq < BestDistSq)
					{
						BestDistSq = DistSq;
						BestPawn = TestPawn;
					}
				}
			}
		}
		if (BestPawn)
		{
			SetEnemy(BestPawn);
			bGotEnemy = true;
		}
	}
	return bGotEnemy;
#endif
}

#if 0
bool AGAIController::HasWeaponLOSToEnemy(AActor* InEnemyActor, const bool bAnyEnemy) const
{
#if 0
	static FName LosTag = FName(TEXT("AIWeaponLosTrace"));

	AGActor* MyBot = Cast<AGActor>(GetPawn());

	bool bHasLOS = false;
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(LosTag, true, GetPawn());
	TraceParams.bTraceAsyncScene = true;

	TraceParams.bReturnPhysicalMaterial = true;
	FVector StartLocation = MyBot->GetActorLocation();
	StartLocation.Z += GetPawn()->BaseEyeHeight; //look from eyes

	FHitResult Hit(ForceInit);
	const FVector EndLocation = InEnemyActor->GetActorLocation();
	GetWorld()->LineTraceSingle(Hit, StartLocation, EndLocation, COLLISION_WEAPON, TraceParams);
	if (Hit.bBlockingHit == true)
	{
		// Theres a blocking hit - check if its our enemy actor
		AActor* HitActor = Hit.GetActor();
		if (Hit.GetActor() != NULL)
		{
			if (HitActor == InEnemyActor)
			{
				bHasLOS = true;
			}
			else if (bAnyEnemy == true)
			{
				// Its not our actor, maybe its still an enemy ?
				APawn* HitChar = Cast<APawn>(HitActor);
				if (HitChar != NULL)
				{
					AGPlayerState* HitPlayerState = Cast<AGPlayerState>(HitChar->PlayerState);
					AGPlayerState* MyPlayerState = Cast<AGPlayerState>(PlayerState);
					if ((HitPlayerState != NULL) && (MyPlayerState != NULL))
					{
						if (HitPlayerState->GetTeam() != MyPlayerState->GetTeam())
						{
							bHasLOS = true;
						}
					}
				}
			}
		}
	}

	return bHasLOS;
# endif

	// always have line of sight in dogfight
	return true;
}
#endif

void AGAIController::ShootEnemy()
{
	AGActor* MyBot = Cast<AGActor>(GetPawn());
	if (!MyBot || !MyBot->GetInventory())
		return;
	/*
	AWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}*/

	bool bCanShoot = false;
	AGActor* Enemy = GetEnemy();
	TArray<AWeapon*> UseableWeapons = MyBot->GetInventory()->WeaponsThatCanAttackTarget(Enemy);

	AWeapon* MyWeapon = NULL;
	for (int w = 0; w < UseableWeapons.Num(); ++w)
	{
		MyWeapon = UseableWeapons[w];
		if (Enemy && (Enemy->IsAlive()) && (MyWeapon->GetCurrentAmmo() > 0) && (MyWeapon->CanFire() == true))
		{
			// always true in dog fight
			//if (LineOfSightTo(Enemy, MyBot->GetActorLocation()))
			{
				bCanShoot = true;
				break;
			}
		}
	}

	// change weapon
	if (MyBot->GetInventory()->GetCurrentWeapon() != MyWeapon)
	{
		MyBot->StopWeaponFire();
		MyBot->GetInventory()->EquipWeapon(MyWeapon);
	}

	if (bCanShoot)
	{	
		// special AI handling for hover state - would this be a good candidate for a behaviour tree component?
		UHoverFlyState* HoverFlyState = Cast<UHoverFlyState>(MyBot->CurrentState());
		if (HoverFlyState && HoverFlyState->bMustBeStationaryToFire)
		{
			HoverFlyState->StopToFire();

			// wait for the actor to stop before shooting
			if (!HoverFlyState->IsStopped())
			{
				MyBot->StopWeaponFire();
				return;
			}
		}

		MyWeapon->SetTargetPawn(Enemy);
		MyBot->StartWeaponFire();
	}
	else
	{
		MyBot->StopWeaponFire();
	}
}

void AGAIController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
#if 0
	// Stop the behaviour tree/logic
	BehaviorComp->StopTree();

	// Stop any movement we already have
	StopMovement();

	// Cancel the repsawn timer
	GetWorldTimerManager().ClearTimer(TimerHandle_Respawn);

	// Clear any enemy
	SetEnemy(NULL);

	// Finally stop firing
	AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());
	AShooterWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}
	MyBot->StopWeaponFire();
#endif
}

AController* AGAIController::GetPawnController()
{
	// this will return "this" if this pawn is simply just an AI
	// it will return a player controller if this controller is trying to fake a player controller
	return GetPawn()->GetController();
}
