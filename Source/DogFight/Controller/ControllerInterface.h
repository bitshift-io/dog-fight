#pragma once

#include "ControllerInterface.generated.h"

class ATeamInfo;
class AGActor;

/** Class needed to support InterfaceCast<IToStringInterface>(Object) */
UINTERFACE(MinimalAPI)
class UControllerInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IControllerInterface
{
	GENERATED_IINTERFACE_BODY()

	IControllerInterface();
	IControllerInterface(const FObjectInitializer& ObjectInitializer);

	//virtual void SetTeam(ATeamInfo* team);
	//virtual void SetColour(const FLinearColor& colour);

	//virtual ATeamInfo* Team();

	//AGPlayerState*	GetPlayerState() = 0;

	virtual UClass*			GetDefaultPawnClassForController() = 0;
	//ATeamInfo*			Team;
	//FLinearColor	Colour;

	/** Get pawn as AGActor **/
	AGActor*	GetActor();
};
