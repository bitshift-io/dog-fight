// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "ControllerInterface.h"
#include "GAIController.generated.h"

class AGActor;

/**
 *  https://www.youtube.com/watch?v=VxvahnKYB8E
 */
UCLASS()
class DOGFIGHT_API AGAIController : public AAIController//, public IControllerInterface
{
	GENERATED_BODY()
	
public:
	
	AGAIController(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	/** spawns and initializes the PlayerState for this Controller */
	virtual void InitPlayerState();

	virtual void PostInitializeComponents() override;

	virtual void Tick(float deltaTime) override;

	virtual void BeginInactiveState() override;
	virtual void Possess(APawn* InPawn) override;
	virtual void UnPossess() override;
	virtual void SetPawn(APawn* InPawn) override;

	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;

	void Respawn();

	void CheckAmmo(const class AWeapon* CurrentWeapon);

	/* If there is line of sight to current enemy, start firing at them */
	UFUNCTION(BlueprintCallable, Category = Behavior)
	void ShootEnemy();


	/* Finds the closest enemy and sets them as current target */
	UFUNCTION(BlueprintCallable, Category = Behavior)
		void FindClosestEnemy();

	UFUNCTION(BlueprintCallable, Category = Behavior)
	bool FindClosestEnemyWithLOS(AGActor* ExcludeEnemy);

	//bool HasWeaponLOSToEnemy(AActor* InEnemyActor, const bool bAnyEnemy) const;

	AGActor* FindEnemy();

	void SetEnemy(class AGActor* InPawn);

	class AGActor* GetEnemy() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MaxTurnTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MinTurnTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MinStraitTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MaxStraitTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			StraitPercent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MinFireDot;				// fire if dot is greater than this

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MaxFireDistance;		// fire if distance is less than this

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float			MinFireDistance;		// fire if dist further than this

	AController* GetPawnController();

protected:

	void StateUpdate(float deltaTime);

	bool ShouldFire();

	float			m_timeInState;
	float			m_turnTimer;

	AGActor*		m_enemy;

	UPROPERTY(transient)
	UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(transient)
	UBehaviorTreeComponent* BehaviorComp;

	int32 EnemyKeyID;
	int32 NeedAmmoKeyID;

public:
	/** Returns BlackboardComp subobject **/
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }



	//virtual UClass*			GetDefaultPawnClassForController() { return DefaultPawnClassForController; }

	//UPROPERTY(replicated)
	//UClass*	DefaultPawnClassForController;
};
