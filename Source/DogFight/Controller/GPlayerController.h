// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ControllerInterface.h"
#include "Online.h"
#include "GPlayerController.generated.h"

class ATeamInfo;
class AMainMenu;

/**
 * 
 */
UCLASS(config = Game)
class DOGFIGHT_API AGPlayerController : public APlayerController//, public IControllerInterface
{
	GENERATED_BODY()
public:

	AGPlayerController(const FObjectInitializer& ObjectInitializer);

	/** spawns and initializes the PlayerState for this Controller */
	virtual void InitPlayerState();

	virtual void SetPawn(APawn* InPawn) override;
	virtual void Possess(APawn* InPawn) override;
	virtual void UnPossess() override;

	void HorizontalMovement(float value); // left/right
	void VerticalMovement(float value);	// up/down

	float ClampAxis(float value);

	virtual bool InputAxis(FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad);

	void OnFireBegin();
	void OnFireEnd();

	/** Get pawn as AGActor **/
	AGActor*	GetActor();

	/** spawn cameras for servers and owning players */
	virtual void SpawnPlayerCameraManager();

	/** Set the view target
	* @param A - new actor to set as view target
	* @param TransitionParams - parameters to use for controlling the transition
	*/
	virtual void SetViewTarget(class AActor* NewViewTarget, FViewTargetTransitionParams TransitionParams = FViewTargetTransitionParams());

public:
	/** sets spectator location and rotation */
	UFUNCTION(reliable, client)
		void ClientSetSpectatorCamera(FVector CameraLocation, FRotator CameraRotation);

	virtual void ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation);

	/** notify player about started match */
	UFUNCTION(reliable, client)
		void ClientGameStarted();

	virtual void ClientGameStarted_Implementation();

	/** Starts the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
		void ClientStartOnlineGame();

	virtual void ClientStartOnlineGame_Implementation();

	/** Ends the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
		void ClientEndOnlineGame();

	virtual void ClientEndOnlineGame_Implementation();

	/** notify player about finished match */
	virtual void ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner);

	/** Notifies clients to send the end-of-round event */
	UFUNCTION(reliable, client)
		void ClientSendRoundEndEvent(bool bIsWinner, int32 ExpendedTimeInSeconds);

	virtual void ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds);

	/** used for input simulation from blueprint (for automatic perf tests) */
	UFUNCTION(BlueprintCallable, Category = "Input")
		void SimulateInputKey(FKey Key, bool bPressed = true);

	/** sends cheat message */
	UFUNCTION(reliable, server, WithValidation)
		void ServerCheat(const FString& Msg);

	virtual bool ServerCheat_Validate(const FString& Msg);
	virtual void ServerCheat_Implementation(const FString& Msg);

	/* Overriden Message implementation. */
	virtual void ClientTeamMessage_Implementation(APlayerState* SenderPlayerState, const FString& S, FName Type, float MsgLifeTime) override;

	/* Tell the HUD to toggle the chat window. */
	void ToggleChatWindow();

	/** Local function say a string */
	UFUNCTION(exec)
		virtual void Say(const FString& Msg);

	/** RPC for clients to talk to server */
	UFUNCTION(unreliable, server, WithValidation)
		void ServerSay(const FString& Msg);

	virtual bool ServerSay_Validate(const FString& Msg);
	virtual void ServerSay_Implementation(const FString& Msg);

	/** Local function run an emote */
	// 	UFUNCTION(exec)
	// 	virtual void Emote(const FString& Msg);

	/** notify local client about deaths */
	void OnDeathMessage(class AGPlayerState* KillerPlayerState, class AGPlayerState* KilledPlayerState, const UDamageType* KillerDamageType);

	/** toggle InGameMenu handler */
	void OnToggleInGameMenu();

	/** Show the in-game menu if it's not already showing */
	void ShowInGameMenu();

	/** Hides scoreboard if currently diplayed */
	void OnConditionalCloseScoreboard();

	/** Toggles scoreboard */
	void OnToggleScoreboard();

	/** shows scoreboard */
	void OnShowScoreboard();

	/** hides scoreboard */
	void OnHideScoreboard();

	/** set infinite ammo cheat */
	void SetInfiniteAmmo(bool bEnable);

	/** set infinite clip cheat */
	void SetInfiniteClip(bool bEnable);

	/** set health regen cheat */
	void SetHealthRegen(bool bEnable);

	/** set god mode cheat */
	UFUNCTION(exec)
		void SetGodMode(bool bEnable);

	/** get infinite ammo cheat */
	bool HasInfiniteAmmo() const;

	/** get infinite clip cheat */
	bool HasInfiniteClip() const;

	/** get health regen cheat */
	bool HasHealthRegen() const;

	/** get gode mode cheat */
	bool HasGodMode() const;

	/** check if gameplay related actions (movement, weapon usage, etc) are allowed right now */
	bool IsGameInputAllowed() const;

	/** is game menu currently active? */
	bool IsGameMenuVisible() const;

	/** Ends and/or destroys game session */
	void CleanupSessionOnReturnToMenu();

	/**
	* Called when the read achievements request from the server is complete
	*
	* @param PlayerId The player id who is responsible for this delegate being fired
	* @param bWasSuccessful true if the server responded successfully to the request
	*/
	void OnQueryAchievementsComplete(const FUniqueNetId& PlayerId, const bool bWasSuccessful);

	// Begin APlayerController interface

	/** handle weapon visibility */
	virtual void SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning) override;

	/** Returns true if movement input is ignored. Overridden to always allow spectators to move. */
	virtual bool IsMoveInputIgnored() const override;

	/** Returns true if look input is ignored. Overridden to always allow spectators to look around. */
	virtual bool IsLookInputIgnored() const override;

	/** initialize the input system from the player settings */
	virtual void InitInputSystem() override;

	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;

	// End APlayerController interface

	// begin AGPlayerController-specific

	/**
	* Reads achievements to precache them before first use
	*/
	void QueryAchievements();

	UFUNCTION(exec)
	void ResetAchievements();

	/**
	* Writes a single achievement (unless another write is in progress).
	*
	* @param Id achievement id (string)
	* @param Percent number 1 to 100
	*/
	void UpdateAchievementProgress(const FString& Id, float Percent);
#if 0
	/** Returns a pointer to the G game hud. May return NULL. */
	AGHUD* GetGHUD() const;
#endif

	/** Returns the persistent user record associated with this player, or null if there is't one. */
	UFUNCTION()
	class UGPersistentUser* GetPersistentUser() const;

	/** Informs that player fragged someone */
	void OnKill();

	/** Cleans up any resources necessary to return to main menu.  Does not modify GameInstance state. */
	virtual void HandleReturnToMainMenu();

	/** Associate a new UPlayer with this PlayerController. */
	virtual void SetPlayer(UPlayer* Player);

	// end AGPlayerController-specific

	virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;

protected:

	/** infinite ammo cheat */
	UPROPERTY(Transient, Replicated)
		uint8 bInfiniteAmmo : 1;

	/** infinite clip cheat */
	UPROPERTY(Transient, Replicated)
		uint8 bInfiniteClip : 1;

	/** health regen cheat */
	UPROPERTY(Transient, Replicated)
		uint8 bHealthRegen : 1;

	/** god mode cheat */
	UPROPERTY(Transient, Replicated)
		uint8 bGodMode : 1;

	/** if set, gameplay related actions (movement, weapn usage, etc) are allowed */
	uint8 bAllowGameActions : 1;

	/** true for the first frame after the game has ended */
	uint8 bGameEndedFrame : 1;

	/** stores pawn location at last player death, used where player scores a kill after they died **/
	FVector LastDeathLocation;

	/** In-game menu */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	TSubclassOf<class UIngameMenu> IngameMenuClass;

	UPROPERTY(Transient)
	UIngameMenu* IngameMenu;

	/** Achievements write object */
	FOnlineAchievementsWritePtr WriteObject;

	/** try to find spot for death cam */
	bool FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation);

	//Begin AActor interface

	/** after all game elements are created */
	virtual void PostInitializeComponents() override;

	virtual void BeginPlay() override;

	void SpawnAIController();

	virtual void Tick(float deltaTime) override;
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
	//End AActor interface

	//Begin AController interface

	/** transition to dead state, retries spawning later */
	virtual void FailedToSpawnPawn() override;

	/** update camera when pawn dies */
	virtual void PawnPendingDestroy(APawn* P) override;

	//End AController interface

	// Begin APlayerController interface

	/** respawn after dying */
	virtual void UnFreeze() override;

	/** sets up input */
	virtual void SetupInputComponent() override;

	/**
	* Called from game info upon end of the game, used to transition to proper state.
	*
	* @param EndGameFocus Actor to set as the view target on end game
	* @param bIsWinner true if this controller is on winning team
	*/
	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;

	/** Return the client to the main menu gracefully.  ONLY sets GI state. */
	void ClientReturnToMainMenu_Implementation(const FString& ReturnReason) override;

	/** Causes the player to commit suicide */
	UFUNCTION(exec)
		virtual void Suicide();

	/** Notifies the server that the client has suicided */
	UFUNCTION(reliable, server, WithValidation)
		void ServerSuicide();

	virtual bool ServerSuicide_Validate();
	virtual void ServerSuicide_Implementation();

	/** Updates achievements based on the PersistentUser stats at the end of a round */
	void UpdateAchievementsOnGameEnd();

public:
	/** Updates leaderboard stats at the end of a round */
	void UpdateLeaderboardsOnGameEnd();

protected:
	/** Updates the save file at the end of a round */
	void UpdateSaveFileOnGameEnd(bool bIsWinner);

	virtual ACameraActor* GetAutoActivateCameraForPlayer() const;

	// End APlayerController interface

	FName	ServerSayString;

	// Timer used for updating friends in the player tick.
	float GFriendUpdateTimer;

	// For tracking whether or not to send the end event
	bool bHasSentStartEvents;

	/**
	* Default class to use when pawn is controlled by AI.
	*/
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "AI Controller Class"), Category = AI)
	//TSubclassOf<AController> AIControllerClass;

	AController* AIController;

private:

	/** Handle for efficient management of ClientStartOnlineGame timer */
	FTimerHandle TimerHandle_ClientStartOnlineGame;


public:
	// menu/lobby functionality
	// client cant call RPC on the main menu as its owned by the server
	// so we work around it using the PC as a proxy

	/** client wants to change slot */
	UFUNCTION(reliable, server, WithValidation)
	void Server_ClientRequestChangeSlot(AMainMenu* Menu, const FString& ColourName);

	virtual bool Server_ClientRequestChangeSlot_Validate(AMainMenu* Menu, const FString& ColourName);
	virtual void Server_ClientRequestChangeSlot_Implementation(AMainMenu* Menu, const FString& ColourName);


	//UFUNCTION(Server, Reliable, WithValidation)
	//void ServerSetDefaultPawnClassForController(UClass* Class);

	//virtual UClass*			GetDefaultPawnClassForController() { return DefaultPawnClassForController; }

	//UPROPERTY(replicated)
	//UClass*	DefaultPawnClassForController;
};
