// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "ControllerInterface.h"
#include "GameMode/TeamInfo.h"
#include "Actor/GActor.h"
#include "UnrealNetwork.h"

UControllerInterface::UControllerInterface(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

IControllerInterface::IControllerInterface()
{
}

IControllerInterface::IControllerInterface(const FObjectInitializer& ObjectInitializer)
{
}

#if 0
void IControllerInterface::SetTeam(ATeamInfo* team)
{
	// remove from existing team
	if (Team)
		Team->ControllerList.Remove(Cast<AController>(this));

	Team = team;
	if (team)
		Team->ControllerList.Add(Cast<AController>(this));

	/*
	NotifyTeamChangeMsg teamChangeMsg;
	teamChangeMsg.mTeam = team;
	if (GetPawn())
	GetPawn()->HandleMsg(&teamChangeMsg);
	*/

	if (team && GetActor())
		GetActor()->SetTeam(team);
}

void IControllerInterface::SetColour(const FLinearColor& colour)
{
	Colour = colour;

	if (GetActor())
		GetActor()->SetColour(colour);
}
#endif

AGActor* IControllerInterface::GetActor()
{
	AController* controller = Cast<AController>(this);
	check(controller);
	return Cast<AGActor>(controller->GetPawn());
}