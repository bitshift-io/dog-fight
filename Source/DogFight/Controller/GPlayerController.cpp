// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "GPlayerController.h"
#include "Actor/GActor.h"
#include "Game/GGameInstance.h"
#include "Game/GLeaderboards.h"
#include "Game/GLocalPlayer.h"
#include "Game/GPersistentUser.h"
#include "Game/GProfileSettings.h"
#include "Weapon/Weapon.h"
#include "Online.h"
#include "OnlineAchievementsInterface.h"
#include "OnlineEventsInterface.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "UserWidget.h"
#include "UI/MainMenu.h"
#include "UI/IngameMenu.h"
#include "Util/OnlineSubsystem.h"


/*
#include "../../OnlineSubsystemSteam/Private/OnlineSubsystemSteamModule.h"
#include "../../OnlineSubsystemSteam/Private/OnlineSubsystemSteamPrivatePCH.h"
//#include "OnlineLeaderboardInterfaceSteam.h"
//#include "OnlineSubsystemSteam.h"
//#include "OnlineAsyncTaskManagerSteam.h"
#include "SteamUtilities.h"
#include "OnlineAchievementsInterfaceSteam.h"
*/

// Steamworks SDK headers
//#include "../../../ThirdParty/Steamworks/Steamv132/sdk/public/steam/steam_api.h"
//#include "steam/steam_gameserver.h"



#pragma optimize("", OPTIMISATION)

/*

local coop:
https://answers.unrealengine.com/questions/28663/local-mp-second-player-created-how-to-control-it.html

*/
AGPlayerController::AGPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)//, IControllerInterface(ObjectInitializer)
{
	IngameMenu = NULL;
	AIController = NULL;
	//AIControllerClass = ((APawn*)APawn::StaticClass()->GetDefaultObject())->AIControllerClass;

	PlayerCameraManagerClass = APlayerCameraManager::StaticClass();
	//CheatClass = UGCheatManager::StaticClass();
	bAllowGameActions = true;
	bGameEndedFrame = false;
	LastDeathLocation = FVector::ZeroVector;

	ServerSayString = TEXT("Say");
	GFriendUpdateTimer = 0.0f;
	bHasSentStartEvents = false;

	//DefaultPawnClassForController = NULL;
}

void AGPlayerController::InitPlayerState()
{
	Super::InitPlayerState();

	// server initialize also
	//PlayerState->ClientInitialize(this);
	AGPlayerState* PS = Cast<AGPlayerState>(PlayerState);
	if (PS)
		PS->Controller = this;
}

void AGPlayerController::HorizontalMovement(float value)
{
	AGActor* actor = Cast<AGActor>(GetPawn());
	if (actor && !AIController)
		actor->SetHorizontalMovementAxis(ClampAxis(value));
}

void AGPlayerController::VerticalMovement(float value)
{
	AGActor* actor = Cast<AGActor>(GetPawn());
	if (actor && !AIController)
		actor->SetVerticalMovementAxis(ClampAxis(value));
}

/*
	Axis clamping and deadzone
*/
float AGPlayerController::ClampAxis(float value)
{
	float retValue = FMath::Clamp(value, -1.0f, 1.0f);

	const float JOYSTICK_DEAD_ZONE = 0.05;
	if (FMath::Abs(retValue) < JOYSTICK_DEAD_ZONE)
	{
		retValue = 0.0f;
	}

	return retValue;
}

bool AGPlayerController::InputAxis(FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad)
{
	/*
	ULocalPlayer* LP = Cast<ULocalPlayer>(Player);
	if (LP)
	{
		UE_LOG(LogOnline, Log, TEXT("AGPlayerController::InputAxis - ID:%i Key:%s Delta:%f DeltaTime:%f NumSamples:%i bGamepad:%i"), LP->GetControllerId(), *Key.GetDisplayName().ToString(), Delta, DeltaTime, NumSamples, bGamepad);
	}*/
	return Super::InputAxis(Key, Delta, DeltaTime, NumSamples, bGamepad);
}

void AGPlayerController::OnFireBegin()
{
	// testing
	bool isLocal = IsLocalController();

	AGActor* actor = Cast<AGActor>(GetPawn());
	if (actor)
	{
		bool isActorLocal = actor->IsLocallyControlled();
		actor->StartWeaponFire();
	}
}

void AGPlayerController::OnFireEnd()
{
	AGActor* actor = Cast<AGActor>(GetPawn());
	if (actor)
		actor->StopWeaponFire();
}

void AGPlayerController::SpawnAIController()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = GetLevel();
	SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save AI controllers into a map
	APawn* P = GetPawn();
	UWorld* W = GetWorld();
	check(P);
	check(W);
	AIController = W->SpawnActor<AController>(P->AIControllerClass, P->GetActorLocation(), P->GetActorRotation(), SpawnInfo);
	AIController->SetPawn(P);
}

void AGPlayerController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
}

void AGPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	// create an AI controller to control this player if this player is AI Controlled
	// this is done once the pawn is set as the pawn is required by the AI
	UGLocalPlayer* LP = Cast<UGLocalPlayer>(Player);
	if (InPawn && LP && LP->GetProfileSettings()->bAIPlayer && !AIController)
	{
		SpawnAIController();
	}

	if (AIController)
	{
		AIController->SetPawn(InPawn);
	}
}

void AGPlayerController::UnPossess()
{
	Super::UnPossess();
}

void AGPlayerController::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	//AIController->Tick(deltaTime);
}

#define  ACH_FRAG_SOMEONE	TEXT("ACH_FRAG_SOMEONE")
#define  ACH_SOME_KILLS		TEXT("ACH_SOME_KILLS")
#define  ACH_LOTS_KILLS		TEXT("ACH_LOTS_KILLS")
#define  ACH_FINISH_MATCH	TEXT("ACH_FINISH_MATCH")
#define  ACH_LOTS_MATCHES	TEXT("ACH_LOTS_MATCHES")
#define  ACH_FIRST_WIN		TEXT("ACH_FIRST_WIN")
#define  ACH_LOTS_WIN		TEXT("ACH_LOTS_WIN")
#define  ACH_MANY_WIN		TEXT("ACH_MANY_WIN")
#define  ACH_SHOOT_BULLETS	TEXT("ACH_SHOOT_BULLETS")
#define  ACH_SHOOT_ROCKETS	TEXT("ACH_SHOOT_ROCKETS")
#define  ACH_GOOD_SCORE		TEXT("ACH_GOOD_SCORE")
#define  ACH_GREAT_SCORE	TEXT("ACH_GREAT_SCORE")
#define  ACH_PLAY_SANCTUARY	TEXT("ACH_PLAY_SANCTUARY")
#define  ACH_PLAY_HIGHRISE	TEXT("ACH_PLAY_HIGHRISE")

static const int32 SomeKillsCount = 10;
static const int32 LotsKillsCount = 20;
static const int32 LotsMatchesCount = 5;
static const int32 LotsWinsCount = 3;
static const int32 ManyWinsCount = 5;
static const int32 LotsBulletsCount = 100;
static const int32 LotsRocketsCount = 10;
static const int32 GoodScoreCount = 10;
static const int32 GreatScoreCount = 15;

void AGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();


	InputComponent->BindAxis("HorizontalMovement", this, &AGPlayerController::HorizontalMovement);
	InputComponent->BindAxis("VerticalMovement", this, &AGPlayerController::VerticalMovement);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AGPlayerController::OnFireBegin);
	InputComponent->BindAction("Fire", IE_Released, this, &AGPlayerController::OnFireEnd);

	// UI input
	InputComponent->BindAction("InGameMenu", IE_Pressed, this, &AGPlayerController::OnToggleInGameMenu);
#if 0
	InputComponent->BindAction("Scoreboard", IE_Pressed, this, &AGPlayerController::OnShowScoreboard);
	InputComponent->BindAction("Scoreboard", IE_Released, this, &AGPlayerController::OnHideScoreboard);
	InputComponent->BindAction("ConditionalCloseScoreboard", IE_Pressed, this, &AGPlayerController::OnConditionalCloseScoreboard);
	InputComponent->BindAction("ToggleScoreboard", IE_Pressed, this, &AGPlayerController::OnToggleScoreboard);

	// voice chat
	InputComponent->BindAction("PushToTalk", IE_Pressed, this, &APlayerController::StartTalking);
	InputComponent->BindAction("PushToTalk", IE_Released, this, &APlayerController::StopTalking);

	InputComponent->BindAction("ToggleChat", IE_Pressed, this, &AGPlayerController::ToggleChatWindow);
#endif
}


void AGPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
#if 0
	FGStyle::Initialize();
#endif
	GFriendUpdateTimer = 0;
}

void AGPlayerController::BeginPlay()
{
	Super::BeginPlay();
/*
	//if (GetNetMode() != NM_Client)
	//{
	// apply DefaultPawnClassForController this player controller now we have a local player assigned
	UGLocalPlayer* LP = Cast<UGLocalPlayer>(Player);
	if (LP)
	{
		AGPlayerState* PS = Cast<AGPlayerState>(PlayerState);
		UGProfileSettings* Settings = LP->GetProfileSettings();
		DefaultPawnClassForController = Settings->DefaultPawnClassForController;
		ServerSetDefaultPawnClassForController(DefaultPawnClassForController);

		//PS->ServerSetPlayerColour(Settings->PlayerColour);
		//PS->SetPlayerName(Settings->PlayerName);
	}
	//}*/
}

void AGPlayerController::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);
#if 0
	if (IsGameMenuVisible())
	{
		if (GFriendUpdateTimer > 0)
		{
			GFriendUpdateTimer -= DeltaTime;
		}
		else
		{
			TSharedPtr<class FGFriends> GFriends = GIngameMenu->GetGFriends();
			ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
			if (GFriends.IsValid() && LocalPlayer && LocalPlayer->GetControllerId() >= 0)
			{
				GFriends->UpdateFriends(LocalPlayer->GetControllerId());
			}
			GFriendUpdateTimer = 4; //make sure the time between calls is long enough that we won't trigger (0x80552C81) and not exceed the web api rate limit
		}
	}
#endif
	// Is this the first frame after the game has ended
	if (bGameEndedFrame)
	{
		bGameEndedFrame = false;

		// ONLY PUT CODE HERE WHICH YOU DON'T WANT TO BE DONE DUE TO HOST LOSS

		// Do we need to show the end of round scoreboard?
		if (IsPrimaryPlayer())
		{
#if 0
			AGHUD* GHUD = GetGHUD();
			if (GHUD)
			{
				GHUD->ShowScoreboard(true, true);
			}
#endif
		}
	}
}

/*
bool AGPlayerController::ServerSetDefaultPawnClassForController_Validate(UClass* Class)
{
	return true;
}

void AGPlayerController::ServerSetDefaultPawnClassForController_Implementation(UClass* Class)
{
	if (DefaultPawnClassForController == Class)
	{
		return;
	}

	DefaultPawnClassForController = Class;

	// replace existing pawn with the new class pawn
	{
		AGGameMode* const GameMode = Cast<AGGameMode>(GetWorld()->GetAuthGameMode());
		if (GetPawn() != NULL)
		{
			GameMode->ReplacePlayerPawn(this, DefaultPawnClassForController);
		}
	}

	/  *
	//ServerRestartPlayer();
	{
		AGameMode* const GameMode = GetWorld()->GetAuthGameMode();
		if (!GetWorld()->GetAuthGameMode()->PlayerCanRestart(this))
		{
			return;
		}

		// If we're still attached to a Pawn, leave it
		if (GetPawn() != NULL)
		{
			APawn* P = GetPawn();
			UnPossess();
			P->Destroy(); // client still sees the spawn ring.....BUG 
		}

		GameMode->RestartPlayer(this);
	}* /
}*/

void AGPlayerController::SetPlayer(UPlayer* InPlayer)
{
	Super::SetPlayer(InPlayer);

	//Build menu only after game is initialized
	if (IngameMenuClass)
	{
		IngameMenu = CreateWidget<UIngameMenu>(this, IngameMenuClass); //MakeShareable();
		if (IngameMenu) //.IsValid())
		{
			IngameMenu->Construct(Cast<ULocalPlayer>(Player));
		}
	}
}

void AGPlayerController::QueryAchievements()
{
	// precache achievements
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer && LocalPlayer->GetControllerId() != -1)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
			if (Identity.IsValid())
			{
				TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalPlayer->GetControllerId());

				if (UserId.IsValid())
				{
					IOnlineAchievementsPtr Achievements = OnlineSub->GetAchievementsInterface();

					if (Achievements.IsValid())
					{
						Achievements->QueryAchievements(*UserId.Get(), FOnQueryAchievementsCompleteDelegate::CreateUObject(this, &AGPlayerController::OnQueryAchievementsComplete));
					}
				}
				else
				{
					UE_LOG(LogOnline, Warning, TEXT("No valid user id for this controller."));
				}
			}
			else
			{
				UE_LOG(LogOnline, Warning, TEXT("No valid identity interface."));
			}
		}
		else
		{
			UE_LOG(LogOnline, Warning, TEXT("No default online subsystem."));
		}
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("No local player, cannot read achievements."));
	}
}

void AGPlayerController::OnQueryAchievementsComplete(const FUniqueNetId& PlayerId, const bool bWasSuccessful)
{
	UE_LOG(LogOnline, Display, TEXT("AGPlayerController::OnQueryAchievementsComplete(bWasSuccessful = %s)"), bWasSuccessful ? TEXT("TRUE") : TEXT("FALSE"));
}


void AGPlayerController::ResetAchievements()
{
#if !UE_BUILD_SHIPPING
	// precache achievements
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer && LocalPlayer->GetControllerId() != -1)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
			if (Identity.IsValid())
			{
				TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalPlayer->GetControllerId());

				if (UserId.IsValid())
				{
					IOnlineAchievementsPtr Achievements = OnlineSub->GetAchievementsInterface();

					if (Achievements.IsValid())
					{
						Achievements->ResetAchievements(*UserId.Get());
					}
				}
				else
				{
					UE_LOG(LogOnline, Warning, TEXT("No valid user id for this controller."));
				}
			}
			else
			{
				UE_LOG(LogOnline, Warning, TEXT("No valid identity interface."));
			}
		}
		else
		{
			UE_LOG(LogOnline, Warning, TEXT("No default online subsystem."));
		}
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("No local player, cannot read achievements."));
	}
#endif
}

void AGPlayerController::UnFreeze()
{
	/*
	ServerRestartPlayer();*/
}

void AGPlayerController::FailedToSpawnPawn()
{
	if (StateName == NAME_Inactive)
	{
		BeginInactiveState();
	}
	Super::FailedToSpawnPawn();
}

void AGPlayerController::PawnPendingDestroy(APawn* P)
{
#if 0
	LastDeathLocation = P->GetActorLocation();
	FVector CameraLocation = LastDeathLocation + FVector(0, 0, 300.0f);
	FRotator CameraRotation(-90.0f, 0.0f, 0.0f);
	FindDeathCameraSpot(CameraLocation, CameraRotation);
#endif
	Super::PawnPendingDestroy(P);
#if 0
	ClientSetSpectatorCamera(CameraLocation, CameraRotation);
#endif
}

void AGPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	UpdateSaveFileOnGameEnd(bIsWinner);
	UpdateAchievementsOnGameEnd();
	UpdateLeaderboardsOnGameEnd();

	// dont call Super::GameHasEnded as this resets the camera
	// to first person!
	//Super::GameHasEnded(EndGameFocus, bIsWinner);
}

void AGPlayerController::ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation)
{
	SetInitialLocationAndRotation(CameraLocation, CameraRotation);
	SetViewTarget(this);
}

bool AGPlayerController::FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation)
{
#if 0
	const FVector PawnLocation = GetPawn()->GetActorLocation();
	FRotator ViewDir = GetControlRotation();
	ViewDir.Pitch = -45.0f;

	const float YawOffsets[] = { 0.0f, -180.0f, 90.0f, -90.0f, 45.0f, -45.0f, 135.0f, -135.0f };
	const float CameraOffset = 600.0f;
	FCollisionQueryParams TraceParams(TEXT("DeathCamera"), true, GetPawn());

	for (int32 i = 0; i < ARRAY_COUNT(YawOffsets); i++)
	{
		FRotator CameraDir = ViewDir;
		CameraDir.Yaw += YawOffsets[i];
		CameraDir.Normalize();

		const FVector TestLocation = PawnLocation - CameraDir.Vector() * CameraOffset;
		const bool bBlocked = GetWorld()->LineTraceTest(PawnLocation, TestLocation, ECC_Camera, TraceParams);

		if (!bBlocked)
		{
			CameraLocation = TestLocation;
			CameraRotation = CameraDir;
			return true;
		}
	}
#endif
	return false;
}

bool AGPlayerController::ServerCheat_Validate(const FString& Msg)
{
	return true;
}

void AGPlayerController::ServerCheat_Implementation(const FString& Msg)
{
	if (CheatManager)
	{
		ClientMessage(ConsoleCommand(Msg));
	}
}

void AGPlayerController::SimulateInputKey(FKey Key, bool bPressed)
{
	InputKey(Key, bPressed ? IE_Pressed : IE_Released, 1, false);
}

void AGPlayerController::OnKill()
{
	UpdateAchievementProgress(ACH_FRAG_SOMEONE, 100.0f);

	const auto Events = Online::GetEventsInterface();
	const auto Identity = Online::GetIdentityInterface();

	if (Events.IsValid() && Identity.IsValid())
	{
		ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
		if (LocalPlayer)
		{
			int32 UserIndex = LocalPlayer->GetControllerId();
			TSharedPtr<const FUniqueNetId> UniqueID = Identity->GetUniquePlayerId(UserIndex);
			if (UniqueID.IsValid())
			{
				AGActor* Pawn = Cast<AGActor>(GetPawn());
				// If player is dead, use location stored during pawn cleanup.
				FVector Location = Pawn ? Pawn->GetActorLocation() : LastDeathLocation;
				AWeapon* Weapon = Pawn ? Pawn->GetWeapon() : 0;
				int32 WeaponType = Weapon ? (int32)Weapon->GetAmmoType() : 0;

				FOnlineEventParms Params;

				Params.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
				Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
				Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused

				Params.Add(TEXT("PlayerRoleId"), FVariantData((int32)0)); // unused
				Params.Add(TEXT("PlayerWeaponId"), FVariantData((int32)WeaponType));
				Params.Add(TEXT("EnemyRoleId"), FVariantData((int32)0)); // unused
				Params.Add(TEXT("EnemyWeaponId"), FVariantData((int32)0)); // untracked			
				Params.Add(TEXT("KillTypeId"), FVariantData((int32)0)); // unused
				Params.Add(TEXT("LocationX"), FVariantData(Location.X));
				Params.Add(TEXT("LocationY"), FVariantData(Location.Y));
				Params.Add(TEXT("LocationZ"), FVariantData(Location.Z));

				Events->TriggerEvent(*UniqueID, TEXT("KillOponent"), Params);
			}
		}
	}
}

void AGPlayerController::OnDeathMessage(class AGPlayerState* KillerPlayerState, class AGPlayerState* KilledPlayerState, const UDamageType* KillerDamageType)
{
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD)
	{
		GHUD->ShowDeathMessage(KillerPlayerState, KilledPlayerState, KillerDamageType);
	}
#endif
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer && LocalPlayer->GetCachedUniqueNetId().IsValid() && KilledPlayerState->UniqueId.IsValid())
	{
		// if this controller is the player who died, update the hero stat.
		if (*LocalPlayer->GetCachedUniqueNetId() == *KilledPlayerState->UniqueId)
		{
			const auto Events = Online::GetEventsInterface();
			const auto Identity = Online::GetIdentityInterface();

			if (Events.IsValid() && Identity.IsValid())
			{
				const int32 UserIndex = LocalPlayer->GetControllerId();
				TSharedPtr<const FUniqueNetId> UniqueID = Identity->GetUniquePlayerId(UserIndex);
				if (UniqueID.IsValid())
				{
					AGActor* Pawn = Cast<AGActor>(GetPawn());
					AWeapon* Weapon = Pawn ? Pawn->GetWeapon() : NULL;

					FVector Location = Pawn ? Pawn->GetActorLocation() : FVector::ZeroVector;
					int32 WeaponType = Weapon ? (int32)Weapon->GetAmmoType() : 0;

					FOnlineEventParms Params;
					Params.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
					Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
					Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused

					Params.Add(TEXT("PlayerRoleId"), FVariantData((int32)0)); // unused
					Params.Add(TEXT("PlayerWeaponId"), FVariantData((int32)WeaponType));
					Params.Add(TEXT("EnemyRoleId"), FVariantData((int32)0)); // unused
					Params.Add(TEXT("EnemyWeaponId"), FVariantData((int32)0)); // untracked

					Params.Add(TEXT("LocationX"), FVariantData(Location.X));
					Params.Add(TEXT("LocationY"), FVariantData(Location.Y));
					Params.Add(TEXT("LocationZ"), FVariantData(Location.Z));

					Events->TriggerEvent(*UniqueID, TEXT("PlayerDeath"), Params);
				}
			}
		}
	}
}

void AGPlayerController::UpdateAchievementProgress(const FString& Id, float Percent)
{
	// hack around steam stats only having boolean - given or not
	if (Percent < 1.0f)
	{
		Percent = 0.0f;
	}
	else
	{
		Percent = 1.0f;
	}

	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
			if (Identity.IsValid())
			{
				TSharedPtr<const FUniqueNetId> UserId = LocalPlayer->GetPreferredUniqueNetId(); // GetCachedUniqueNetId();

				if (UserId.IsValid())
				{

					IOnlineAchievementsPtr Achievements = OnlineSub->GetAchievementsInterface();
					if (Achievements.IsValid() && (!WriteObject.IsValid() || WriteObject->WriteState != EOnlineAsyncTaskState::InProgress))
					{
						WriteObject = MakeShareable(new FOnlineAchievementsWrite());
						WriteObject->SetFloatStat(*Id, Percent);

						FOnlineAchievementsWriteRef WriteObjectRef = WriteObject.ToSharedRef();
						Achievements->WriteAchievements(*UserId, WriteObjectRef);
					}
					else
					{
						UE_LOG(LogOnline, Warning, TEXT("No valid achievement interface or another write is in progress."));
					}
				}
				else
				{
					UE_LOG(LogOnline, Warning, TEXT("No valid user id for this controller."));
				}
			}
			else
			{
				UE_LOG(LogOnline, Warning, TEXT("No valid identity interface."));
			}
		}
		else
		{
			UE_LOG(LogOnline, Warning, TEXT("No default online subsystem."));
		}
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("No local player, cannot update achievements."));
	}
}

void AGPlayerController::OnToggleInGameMenu()
{
	// this is not ideal, but necessary to prevent both players from pausing at the same time on the same frame
	UWorld* GameWorld = GEngine->GameViewport->GetWorld();

	for (auto It = GameWorld->GetControllerIterator(); It; ++It)
	{
		AGPlayerController* Controller = Cast<AGPlayerController>(*It);
		if (Controller && Controller->IsPaused())
		{
			return;
		}
	}

	// if no one's paused, pause
	if (IngameMenu)//.IsValid())
	{
		IngameMenu->ToggleGameMenu();
	}
}

void AGPlayerController::OnConditionalCloseScoreboard()
{
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD && (GHUD->IsMatchOver() == false))
	{
		GHUD->ConditionalCloseScoreboard();
	}
#endif
}

void AGPlayerController::OnToggleScoreboard()
{
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD && (GHUD->IsMatchOver() == false))
	{
		GHUD->ToggleScoreboard();
	}
#endif
}

void AGPlayerController::OnShowScoreboard()
{
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD)
	{
		GHUD->ShowScoreboard(true);
	}
#endif
}

void AGPlayerController::OnHideScoreboard()
{
#if 0
	AGHUD* GHUD = GetGHUD();
	// If have a valid match and the match is over - hide the scoreboard
	if ((GHUD != NULL) && (GHUD->IsMatchOver() == false))
	{
		GHUD->ShowScoreboard(false);
	}
#endif
}

bool AGPlayerController::IsGameMenuVisible() const
{
	bool Result = false;
#if 0
	if (GIngameMenu.IsValid())
	{
		Result = GIngameMenu->GetIsGameMenuUp();
	}
#endif
	return Result;
}

void AGPlayerController::SetInfiniteAmmo(bool bEnable)
{
	bInfiniteAmmo = bEnable;
}

void AGPlayerController::SetInfiniteClip(bool bEnable)
{
	bInfiniteClip = bEnable;
}

void AGPlayerController::SetHealthRegen(bool bEnable)
{
	bHealthRegen = bEnable;
}

void AGPlayerController::SetGodMode(bool bEnable)
{
	bGodMode = bEnable;
}

void AGPlayerController::ClientGameStarted_Implementation()
{
	bAllowGameActions = true;

	// Enable controls mode now the game has started
	SetIgnoreMoveInput(false);
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD)
	{
		GHUD->SetMatchState(EGMatchState::Playing);
		GHUD->ShowScoreboard(false);
	}
#endif
	bGameEndedFrame = false;

	QueryAchievements();

	// Send round start event
	const auto Events = Online::GetEventsInterface();
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);

	if (LocalPlayer != nullptr && Events.IsValid())
	{
		auto UniqueId = LocalPlayer->GetPreferredUniqueNetId();

		if (UniqueId.IsValid())
		{
			// Generate a new session id
			Events->SetPlayerSessionId(*UniqueId, FGuid::NewGuid());

			FString MapName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName());

			// Fire session start event for all cases
			FOnlineEventParms Params;
			Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
			Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("MapName"), FVariantData(MapName));

			Events->TriggerEvent(*UniqueId, TEXT("PlayerSessionStart"), Params);

			// Online matches require the MultiplayerRoundStart event as well
			UGGameInstance* SGI = GetWorld() != NULL ? Cast<UGGameInstance>(GetWorld()->GetGameInstance()) : NULL;

			if (SGI->GetIsOnline())
			{
				FOnlineEventParms MultiplayerParams;

				// @todo: fill in with real values
				MultiplayerParams.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
				MultiplayerParams.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
				MultiplayerParams.Add(TEXT("MatchTypeId"), FVariantData((int32)1)); // @todo abstract the specific meaning of this value across platforms
				MultiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused

				Events->TriggerEvent(*UniqueId, TEXT("MultiplayerRoundStart"), MultiplayerParams);
			}

			bHasSentStartEvents = true;
		}
	}
}

/** Starts the online game using the session name in the PlayerState */
void AGPlayerController::ClientStartOnlineGame_Implementation()
{
	if (!IsPrimaryPlayer())
		return;

	AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
	if (GPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid())
			{
				UE_LOG(LogOnline, Log, TEXT("Starting session %s on client"), *GPlayerState->SessionName.ToString());
				Sessions->StartSession(GPlayerState->SessionName);
			}
		}
	}
	else
	{
		// Keep retrying until player state is replicated
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ClientStartOnlineGame, this, &AGPlayerController::ClientStartOnlineGame_Implementation, 0.2f, false);
	}
}

/** Ends the online game using the session name in the PlayerState */
void AGPlayerController::ClientEndOnlineGame_Implementation()
{
	if (!IsPrimaryPlayer())
		return;

	AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
	if (GPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid())
			{
				UE_LOG(LogOnline, Log, TEXT("Ending session %s on client"), *GPlayerState->SessionName.ToString());
				Sessions->EndSession(GPlayerState->SessionName);
			}
		}
	}
}

void AGPlayerController::HandleReturnToMainMenu()
{
	OnHideScoreboard();
	CleanupSessionOnReturnToMenu();
}

void AGPlayerController::ClientReturnToMainMenu_Implementation(const FString& AReturnReason)
{
	UGGameInstance* SGI = GetWorld() != NULL ? Cast<UGGameInstance>(GetWorld()->GetGameInstance()) : NULL;

	if (!ensure(SGI != NULL))
	{
		return;
	}

	if (GetNetMode() == NM_Client)
	{
		const FText ReturnReason = NSLOCTEXT("NetworkErrors", "HostQuit", "The host has quit the match.");
		const FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");

		SGI->ShowMessageThenGotoState(ReturnReason, OKButton, FText::GetEmpty(), GGameInstanceState::MainMenu);
	}
	else
	{
		SGI->GotoState(GGameInstanceState::MainMenu);
	}

	// Clear the flag so we don't do normal end of round stuff next
	bGameEndedFrame = false;
}

/** Ends and/or destroys game session */
void AGPlayerController::CleanupSessionOnReturnToMenu()
{
	UGGameInstance * SGI = GetWorld() != NULL ? Cast<UGGameInstance>(GetWorld()->GetGameInstance()) : NULL;

	if (ensure(SGI != NULL))
	{
		SGI->CleanupSessionOnReturnToMenu();
	}
}

void AGPlayerController::ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::ClientGameEnded_Implementation(EndGameFocus, bIsWinner);

	// Disable controls now the game has ended
	SetIgnoreMoveInput(true);

	bAllowGameActions = false;

	// Make sure that we still have valid view target
	SetViewTarget(GetPawn());
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GHUD)
	{
		GHUD->SetMatchState(bIsWinner ? EGMatchState::Won : EGMatchState::Lost);
	}
#endif
	UpdateSaveFileOnGameEnd(bIsWinner);
	UpdateAchievementsOnGameEnd();
	UpdateLeaderboardsOnGameEnd();

	// Flag that the game has just ended (if it's ended due to host loss we want to wait for ClientReturnToMainMenu_Implementation first, incase we don't want to process)
	bGameEndedFrame = true;
}

void AGPlayerController::ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds)
{
	const auto Events = Online::GetEventsInterface();
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);

	if (bHasSentStartEvents && LocalPlayer != nullptr && Events.IsValid())
	{
		auto UniqueId = LocalPlayer->GetPreferredUniqueNetId();

		if (UniqueId.IsValid())
		{
			FString MapName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName());
			AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
			int32 PlayerScore = GPlayerState ? GPlayerState->GetScore() : 0;

			// Fire session end event for all cases
			FOnlineEventParms Params;
			Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
			Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("ExitStatusId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("PlayerScore"), FVariantData((int32)PlayerScore));
			Params.Add(TEXT("PlayerWon"), FVariantData((bool)bIsWinner));
			Params.Add(TEXT("MapName"), FVariantData(MapName));
			Params.Add(TEXT("MapNameString"), FVariantData(MapName)); // @todo workaround for a bug in backend service, remove when fixed

			Events->TriggerEvent(*UniqueId, TEXT("PlayerSessionEnd"), Params);

			// Online matches require the MultiplayerRoundEnd event as well
			UGGameInstance* SGI = GetWorld() != NULL ? Cast<UGGameInstance>(GetWorld()->GetGameInstance()) : NULL;
			if (SGI->GetIsOnline())
			{
				FOnlineEventParms MultiplayerParams;

				AGGameState* const MyGameState = GetWorld() != NULL ? GetWorld()->GetGameState<AGGameState>() : NULL;
				if (ensure(MyGameState != nullptr))
				{
					MultiplayerParams.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
					MultiplayerParams.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
					MultiplayerParams.Add(TEXT("MatchTypeId"), FVariantData((int32)1)); // @todo abstract the specific meaning of this value across platforms
					MultiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
					MultiplayerParams.Add(TEXT("TimeInSeconds"), FVariantData((float)ExpendedTimeInSeconds));
					MultiplayerParams.Add(TEXT("ExitStatusId"), FVariantData((int32)0)); // unused

					Events->TriggerEvent(*UniqueId, TEXT("MultiplayerRoundEnd"), MultiplayerParams);
				}
			}
		}

		bHasSentStartEvents = false;
	}
}

void AGPlayerController::SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning)
{
	Super::SetCinematicMode(bInCinematicMode, bHidePlayer, bAffectsHUD, bAffectsMovement, bAffectsTurning);

	// If we have a pawn we need to determine if we should show/hide the weapon
	AGActor* MyPawn = Cast<AGActor>(GetPawn());
	AWeapon* MyWeapon = MyPawn ? MyPawn->GetWeapon() : NULL;
	if (MyWeapon)
	{
		if (bInCinematicMode && bHidePlayer)
		{
			MyWeapon->SetActorHiddenInGame(true);
		}
		else if (!bCinematicMode)
		{
			MyWeapon->SetActorHiddenInGame(false);
		}
	}
}

bool AGPlayerController::IsMoveInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		return false;
	}
	else
	{
		return Super::IsMoveInputIgnored();
	}
}

bool AGPlayerController::IsLookInputIgnored() const
{
	if (IsInState(NAME_Spectating))
	{
		return false;
	}
	else
	{
		return Super::IsLookInputIgnored();
	}
}

void AGPlayerController::InitInputSystem()
{
	Super::InitInputSystem();

	UGPersistentUser* PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		PersistentUser->TellInputAboutKeybindings();
	}
}

void AGPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	//DOREPLIFETIME(AGPlayerController, DefaultPawnClassForController);

	DOREPLIFETIME_CONDITION(AGPlayerController, bInfiniteAmmo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AGPlayerController, bInfiniteClip, COND_OwnerOnly);
}

void AGPlayerController::Suicide()
{
	if (IsInState(NAME_Playing))
	{
		ServerSuicide();
	}
}

bool AGPlayerController::ServerSuicide_Validate()
{
	return true;
}

void AGPlayerController::ServerSuicide_Implementation()
{
#if 0
	if ((GetPawn() != NULL) && ((GetWorld()->TimeSeconds - GetPawn()->CreationTime > 1) || (GetNetMode() == NM_Standalone)))
	{
		AGActor* MyPawn = Cast<AGActor>(GetPawn());
		if (MyPawn)
		{
			MyPawn->Suicide();
		}
	}
#endif
}

bool AGPlayerController::HasInfiniteAmmo() const
{
	return bInfiniteAmmo;
}

bool AGPlayerController::HasInfiniteClip() const
{
	return bInfiniteClip;
}

bool AGPlayerController::HasHealthRegen() const
{
	return bHealthRegen;
}

bool AGPlayerController::HasGodMode() const
{
	return bGodMode;
}

bool AGPlayerController::IsGameInputAllowed() const
{
	return bAllowGameActions && !bCinematicMode;
}

void AGPlayerController::ToggleChatWindow()
{
#if 0
	AGHUD* GHUD = Cast<AGHUD>(GetHUD());
	if (GHUD)
	{
		GHUD->ToggleChat();
	}
#endif
}

void AGPlayerController::ClientTeamMessage_Implementation(APlayerState* SenderPlayerState, const FString& S, FName Type, float MsgLifeTime)
{
#if 0
	AGHUD* GHUD = Cast<AGHUD>(GetHUD());
	if (GHUD)
	{
		if (Type == ServerSayString)
		{
			if (SenderPlayerState != PlayerState)
			{
				GHUD->AddChatLine(S, false);
			}
		}
	}
#endif
}

void AGPlayerController::Say(const FString& Msg)
{
	ServerSay(Msg.Left(128));
}

bool AGPlayerController::ServerSay_Validate(const FString& Msg)
{
	return true;
}

void AGPlayerController::ServerSay_Implementation(const FString& Msg)
{
	GetWorld()->GetAuthGameMode()->Broadcast(this, Msg, ServerSayString);
}

#if 0
AGHUD* AGPlayerController::GetGHUD() const
{
	return Cast<AGHUD>(GetHUD());
}
#endif

UGPersistentUser* AGPlayerController::GetPersistentUser() const
{
	UGLocalPlayer* const GLP = Cast<UGLocalPlayer>(Player);
	return GLP ? GLP->GetPersistentUser() : nullptr;
}

bool AGPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate /*= FCanUnpause()*/)
{
	const bool Result = APlayerController::SetPause(bPause, CanUnpauseDelegate);

	// Update rich presence.
	const auto PresenceInterface = Online::GetPresenceInterface();
	const auto Events = Online::GetEventsInterface();
	const auto LocalPlayer = Cast<ULocalPlayer>(Player);
	TSharedPtr<const FUniqueNetId> UserId = LocalPlayer ? LocalPlayer->GetCachedUniqueNetId() : nullptr;

	if (PresenceInterface.IsValid() && UserId.IsValid())
	{
		FOnlineUserPresenceStatus PresenceStatus;
		if (Result && bPause)
		{
			PresenceStatus.Properties.Add(DefaultPresenceKey, FString("Paused"));
		}
		else
		{
			PresenceStatus.Properties.Add(DefaultPresenceKey, FString("InGame"));
		}
		PresenceInterface->SetPresence(*UserId, PresenceStatus);

	}

	// Don't send pause events while online since the game doesn't actually pause
	if (GetNetMode() == NM_Standalone && Events.IsValid() && PlayerState->UniqueId.IsValid())
	{
		FOnlineEventParms Params;
		Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
		Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
		if (Result && bPause)
		{
			Events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionPause"), Params);
		}
		else
		{
			Events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionResume"), Params);
		}
	}

	return Result;
}

void AGPlayerController::ShowInGameMenu()
{
#if 0
	AGHUD* GHUD = GetGHUD();
	if (GIngameMenu.IsValid() && !GIngameMenu->GetIsGameMenuUp() && GHUD && (GHUD->IsMatchOver() == false))
	{
		GIngameMenu->ToggleGameMenu();
	}
#endif
}
void AGPlayerController::UpdateAchievementsOnGameEnd()
{
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer)
	{
		AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
		if (GPlayerState)
		{
			UGPersistentUser*  PersistentUser = GetPersistentUser();
			if (PersistentUser)
			{
				AGGameState* GameState = Cast<AGGameState>(GetWorld()->GetGameState());
				if (GameState && !GameState->Achieve10GamesName.IsEmpty())
				{
					const float ACH_10_GAMES = 10.0f;
					float Progress = PersistentUser->GetFloatValue(GameState->Achieve10GamesName);
					Progress += 1.0f;
					PersistentUser->SetFloatValue(GameState->Achieve10GamesName, Progress);
					float fPct = ((float)Progress / (float)ACH_10_GAMES);
					UpdateAchievementProgress(GameState->Achieve10GamesName, fPct);
					PersistentUser->SaveIfDirty();
				}

#if 0
				const int32 Wins = PersistentUser->GetWins();
				const int32 Losses = PersistentUser->GetLosses();
				const int32 Matches = Wins + Losses;

				const int32 TotalKills = PersistentUser->GetKills();
				const int32 MatchScore = (int32)GPlayerState->GetScore();

				const int32 TotalBulletsFired = PersistentUser->GetBulletsFired();
				const int32 TotalRocketsFired = PersistentUser->GetRocketsFired();

				float TotalGameAchievement = 0;
				float CurrentGameAchievement = 0;

				///////////////////////////////////////
				// Kill achievements
				if (TotalKills >= 1)
				{
					CurrentGameAchievement += 100.0f;
				}
				TotalGameAchievement += 100;

				{
					float fSomeKillPct = ((float)TotalKills / (float)SomeKillsCount) * 100.0f;
					fSomeKillPct = FMath::RoundToFloat(fSomeKillPct);
					UpdateAchievementProgress(ACH_SOME_KILLS, fSomeKillPct);

					CurrentGameAchievement += FMath::Min(fSomeKillPct, 100.0f);
					TotalGameAchievement += 100;
				}

				{
					float fLotsKillPct = ((float)TotalKills / (float)LotsKillsCount) * 100.0f;
					fLotsKillPct = FMath::RoundToFloat(fLotsKillPct);
					UpdateAchievementProgress(ACH_LOTS_KILLS, fLotsKillPct);

					CurrentGameAchievement += FMath::Min(fLotsKillPct, 100.0f);
					TotalGameAchievement += 100;
				}
				///////////////////////////////////////

				///////////////////////////////////////
				// Match Achievements
				{
					UpdateAchievementProgress(ACH_FINISH_MATCH, 100.0f);

					CurrentGameAchievement += 100;
					TotalGameAchievement += 100;
				}

				{
					float fLotsRoundsPct = ((float)Matches / (float)LotsMatchesCount) * 100.0f;
					fLotsRoundsPct = FMath::RoundToFloat(fLotsRoundsPct);
					UpdateAchievementProgress(ACH_LOTS_MATCHES, fLotsRoundsPct);

					CurrentGameAchievement += FMath::Min(fLotsRoundsPct, 100.0f);
					TotalGameAchievement += 100;
				}
				///////////////////////////////////////

				///////////////////////////////////////
				// Win Achievements
				if (Wins >= 1)
				{
					UpdateAchievementProgress(ACH_FIRST_WIN, 100.0f);

					CurrentGameAchievement += 100.0f;
				}
				TotalGameAchievement += 100;

				{
					float fLotsWinPct = ((float)Wins / (float)LotsWinsCount) * 100.0f;
					fLotsWinPct = FMath::RoundToInt(fLotsWinPct);
					UpdateAchievementProgress(ACH_LOTS_WIN, fLotsWinPct);

					CurrentGameAchievement += FMath::Min(fLotsWinPct, 100.0f);
					TotalGameAchievement += 100;
				}

				{
					float fManyWinPct = ((float)Wins / (float)ManyWinsCount) * 100.0f;
					fManyWinPct = FMath::RoundToInt(fManyWinPct);
					UpdateAchievementProgress(ACH_MANY_WIN, fManyWinPct);

					CurrentGameAchievement += FMath::Min(fManyWinPct, 100.0f);
					TotalGameAchievement += 100;
				}
				///////////////////////////////////////

				///////////////////////////////////////
				// Ammo Achievements
				{
					float fLotsBulletsPct = ((float)TotalBulletsFired / (float)LotsBulletsCount) * 100.0f;
					fLotsBulletsPct = FMath::RoundToFloat(fLotsBulletsPct);
					UpdateAchievementProgress(ACH_SHOOT_BULLETS, fLotsBulletsPct);

					CurrentGameAchievement += FMath::Min(fLotsBulletsPct, 100.0f);
					TotalGameAchievement += 100;
				}

				{
					float fLotsRocketsPct = ((float)TotalRocketsFired / (float)LotsRocketsCount) * 100.0f;
					fLotsRocketsPct = FMath::RoundToFloat(fLotsRocketsPct);
					UpdateAchievementProgress(ACH_SHOOT_ROCKETS, fLotsRocketsPct);

					CurrentGameAchievement += FMath::Min(fLotsRocketsPct, 100.0f);
					TotalGameAchievement += 100;
				}
				///////////////////////////////////////

				///////////////////////////////////////
				// Score Achievements
				{
					float fGoodScorePct = ((float)MatchScore / (float)GoodScoreCount) * 100.0f;
					fGoodScorePct = FMath::RoundToFloat(fGoodScorePct);
					UpdateAchievementProgress(ACH_GOOD_SCORE, fGoodScorePct);
				}

				{
					float fGreatScorePct = ((float)MatchScore / (float)GreatScoreCount) * 100.0f;
					fGreatScorePct = FMath::RoundToFloat(fGreatScorePct);
					UpdateAchievementProgress(ACH_GREAT_SCORE, fGreatScorePct);
				}
				///////////////////////////////////////

				///////////////////////////////////////
				// Map Play Achievements
				UWorld* World = GetWorld();
				if (World)
				{
					FString MapName = *FPackageName::GetShortName(World->PersistentLevel->GetOutermost()->GetName());
					if (MapName.Find(TEXT("Highrise")) != -1)
					{
						UpdateAchievementProgress(ACH_PLAY_HIGHRISE, 100.0f);
					}
					else if (MapName.Find(TEXT("Sanctuary")) != -1)
					{
						UpdateAchievementProgress(ACH_PLAY_SANCTUARY, 100.0f);
					}
				}
				///////////////////////////////////////			

				const auto Events = Online::GetEventsInterface();
				const auto Identity = Online::GetIdentityInterface();

				if (Events.IsValid() && Identity.IsValid())
				{
					const int32 UserIndex = LocalPlayer->GetControllerId();
					TSharedPtr<const FUniqueNetId> UniqueID = Identity->GetUniquePlayerId(UserIndex);
					if (UniqueID.IsValid())
					{
						FOnlineEventParms Params;

						float fGamePct = (CurrentGameAchievement / TotalGameAchievement) * 100.0f;
						fGamePct = FMath::RoundToFloat(fGamePct);
						Params.Add(TEXT("CompletionPercent"), FVariantData((float)fGamePct));
						if (UniqueID.IsValid())
						{
							Events->TriggerEvent(*UniqueID, TEXT("GameProgress"), Params);
						}
					}
				}
#endif
			}
		}
	}
}

void AGPlayerController::UpdateLeaderboardsOnGameEnd()
{
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer)
	{
		// update leaderboards
		IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineIdentityPtr Identity = OnlineSub->GetIdentityInterface();
			if (Identity.IsValid())
			{
				TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalPlayer->GetControllerId());
				if (UserId.IsValid())
				{
					IOnlineLeaderboardsPtr Leaderboards = OnlineSub->GetLeaderboardsInterface();
					if (Leaderboards.IsValid())
					{
						AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
						if (GPlayerState)
						{
							AGGameState* GameState = Cast<AGGameState>(GetWorld()->GetGameState());
							if (GameState && GameState->WaveCount > 0)
							{

								FGAllTimeMatchResultsWrite AWriteObject;

								AWriteObject.SetIntStat(LEADERBOARD_STAT_SCORE, GPlayerState->GetScore());
								//AWriteObject.SetIntStat(LEADERBOARD_STAT_KILLS, GPlayerState->GetKills());
								//AWriteObject.SetIntStat(LEADERBOARD_STAT_DEATHS, GPlayerState->GetDeaths());
								//AWriteObject.SetIntStat(LEADERBOARD_STAT_MATCHESPLAYED, 1);
								AWriteObject.SetIntStat(LEADERBOARD_STAT_WAVE, GameState->WaveCount);

								// the call will copy the user id and write object to its own memory
								//Leaderboards->WriteLeaderboards(GPlayerState->SessionName, *UserId, AWriteObject);
								WriteLeaderboards_IfGreater(Leaderboards, GPlayerState->SessionName, *UserId, AWriteObject, LEADERBOARD_STAT_SCORE, GPlayerState->GetScore());

								Leaderboards->FlushLeaderboards(TEXT("DOGFIGHT"));
							}
						}
					}
				}
			}
		}
	}
}

void AGPlayerController::UpdateSaveFileOnGameEnd(bool bIsWinner)
{
	AGPlayerState* GPlayerState = Cast<AGPlayerState>(PlayerState);
	if (GPlayerState)
	{
		// update local saved profile
		UGPersistentUser* const PersistentUser = GetPersistentUser();
		if (PersistentUser)
		{
			PersistentUser->AddMatchResult(GPlayerState->GetKills(), GPlayerState->GetDeaths(), GPlayerState->GetNumBulletsFired(), GPlayerState->GetNumRocketsFired(), bIsWinner);
			PersistentUser->SaveIfDirty();
		}
	}
}

void AGPlayerController::PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
	Super::PreClientTravel(PendingURL, TravelType, bIsSeamlessTravel);
#if 0
	if (GetWorld() != NULL)
	{
		UGGameViewportClient * GViewport = Cast<UGGameViewportClient>(GetWorld()->GetGameViewport());

		if (GViewport != NULL)
		{
			GViewport->ShowLoadingScreen();
		}

		AGHUD* GHUD = Cast<AGHUD>(GetHUD());
		if (GHUD != nullptr)
		{
			// Passing true to bFocus here ensures that focus is returned to the game viewport.
			GHUD->ShowScoreboard(false, true);
		}
	}
#endif
}

void AGPlayerController::SetViewTarget(class AActor* NewViewTarget, FViewTargetTransitionParams TransitionParams)
{
	if (bAutoManageActiveCameraTarget)
	{
		// See if there is a CameraActor in the level that auto-activates for this PC.
		ACameraActor* AutoCameraTarget = GetAutoActivateCameraForPlayer();
		if (AutoCameraTarget)
		{
			Super::SetViewTarget(AutoCameraTarget);
			return;
		}
	}
}

void AGPlayerController::SpawnPlayerCameraManager()
{
	Super::SpawnPlayerCameraManager();

	// fix camera having any chance to have incorrect view when map first loads
	if (bAutoManageActiveCameraTarget)
	{
		AutoManageActiveCameraTarget(this);
	}
}

/*
	Modified version of APlayerController::GetAutoActivateCameraForPlayer
	to ensure we just return the first registered camera, all players use the same fixed camera
*/
ACameraActor* AGPlayerController::GetAutoActivateCameraForPlayer() const
{
	/*
	if (GetNetMode() == NM_Client)
	{
		// Clients get their view target replicated, they don't use placed cameras because they don't know their own index.
		return NULL;
	}*/

	UWorld* CurWorld = GetWorld();
	if (!CurWorld)
	{
		return NULL;
	}

	// Only bother if there are any registered cameras.
	FConstCameraActorIterator CameraIterator = CurWorld->GetAutoActivateCameraIterator();
	if (!CameraIterator)
	{
		return NULL;
	}

	// Find our player index
	int32 IterIndex = 0;
	int32 PlayerIndex = INDEX_NONE;
	for (FConstPlayerControllerIterator Iterator = CurWorld->GetPlayerControllerIterator(); Iterator; ++Iterator, ++IterIndex)
	{
		const APlayerController* PlayerController = *Iterator;
		if (PlayerController == this)
		{
			PlayerIndex = IterIndex;
			break;
		}
	}

	//if (PlayerIndex != INDEX_NONE)
	{
		// Find the matching camera
		for ( /*CameraIterater initialized above*/; CameraIterator; ++CameraIterator)
		{
			ACameraActor* CameraActor = *CameraIterator;
			if (CameraActor /*&& CameraActor->GetAutoActivatePlayerIndex() == PlayerIndex*/)
			{
				return CameraActor;
			}
		}
	}

	return NULL;
}


bool AGPlayerController::Server_ClientRequestChangeSlot_Validate(AMainMenu* Menu, const FString& ColourName)
{
	return true;
}

void AGPlayerController::Server_ClientRequestChangeSlot_Implementation(AMainMenu* Menu, const FString& ColourName)
{
	if (Menu)
	{
		Menu->ClientRequestChangeSlot(this, ColourName);
	}
}

AGActor* AGPlayerController::GetActor()
{
	AController* controller = Cast<AController>(this);
	check(controller);
	return Cast<AGActor>(controller->GetPawn());
}
