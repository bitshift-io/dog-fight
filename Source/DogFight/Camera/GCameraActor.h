
#pragma once

#include "Camera/CameraActor.h"
#include "GCameraActor.generated.h"

/*
	Specially modified camera to make all player controllers use this camera
	as their view

	this works inconjunction with a modified AGPlayerController::GetAutoActivateCameraForPlayer
	really, 
	CameraActor->GetAutoActivatePlayerIndex() == PlayerIndex
	should be changed to something like CameraActor->IsAutoActivatePlayerIndex(PlayerIndex)

*/
UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API AGCameraActor : public ACameraActor
{
	GENERATED_BODY()
public:

	AGCameraActor(const FObjectInitializer& ObjectInitializer);

	virtual void PostInitializeComponents() override;

	// Begin AActor interface
	virtual void BeginPlay() override;
	// End AActor interface

	/** Specifies if all player controllers, should automatically use this Camera when the controller is active. */
	UPROPERTY(Category = "AutoPlayerActivation", EditAnywhere)
	bool bAutoActivateForAllPlayers;

	/** Register for controllers from 0 through to PlayerControllerCount. */
	UPROPERTY(Category = "AutoPlayerActivation", EditAnywhere)
	int32 PlayerControllerCount;
};
