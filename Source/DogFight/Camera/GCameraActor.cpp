// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "GCameraActor.h"

#pragma optimize("", OPTIMISATION)

AGCameraActor::AGCameraActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAutoActivateForAllPlayers = true;
	PlayerControllerCount = 16; // todo: have some max player count as a #define somewhere!
}

void AGCameraActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (bAutoActivateForAllPlayers/* && GetNetMode() != NM_Client*/)
	{
		for (int PlayerIndex = 0; PlayerIndex < PlayerControllerCount; ++PlayerIndex)
		{
			// Always put it in the pool of available auto-activate cameras.
			GetWorld()->RegisterAutoActivateCamera(this, PlayerIndex);
		}
	}

	if (GetAutoActivatePlayerIndex() != INDEX_NONE/* && GetNetMode() != NM_Client*/)
	{
		const int32 PlayerIndex = GetAutoActivatePlayerIndex();

		// Always put it in the pool of available auto-activate cameras.
		GetWorld()->RegisterAutoActivateCamera(this, PlayerIndex);
	}
}

void AGCameraActor::BeginPlay()
{
	if (bAutoActivateForAllPlayers/* && GetNetMode() != NM_Client*/)
	{
		for (int PlayerIndex = 0; PlayerIndex < PlayerControllerCount; ++PlayerIndex)
		{
			// If we find a matching PC, bind to it immediately.
			APlayerController* PC = UGameplayStatics::GetPlayerController(this, PlayerIndex);
			if (PC)
			{
				PC->SetViewTarget(this);
			}
		}
	}

	Super::BeginPlay();
}