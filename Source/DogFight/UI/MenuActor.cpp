// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "MenuActor.h"
#include "UserWidget.h"

// move actor:
// https://answers.unrealengine.com/questions/26573/move-a-character-in-direction-c.html

AMenuActor::AMenuActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bReplicates = true;
	bAlwaysRelevant = true;
}

void AMenuActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	MenuWidget = CreateWidget<UMenuWidget>(GetWorld()->GetGameInstance(), MenuWidgetClass);
	if (MenuWidget)
	{
		UGameInstance* ge = GetGameInstance();
		MenuWidget->Construct(ge->GetFirstGamePlayer(), this);
		MenuWidget->AddToViewport(); // this calls OnConstruct for the UI
		SetWidgetToFocus();
	}
}

void AMenuActor::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AMenuActor::SetWidgetToFocus()
{
	if (!MenuWidget)
	{
		return;
	}

	MenuWidget->SetWidgetToFocus();
}