// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "Util/EngineUtil.h"
#include "BPFunctionLibrary.h"
#include "Game/GGameUserSettings.h"
#include "AudioDevice.h"
#include "Engine/GameInstance.h"
#include "PanelWidget.h"

#pragma optimize("", OPTIMISATION)

UBPFunctionLibrary::UBPFunctionLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

bool UBPFunctionLibrary::GetScreenResolution(FString& resolution)
{
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	FIntPoint ScreenRes = UserSettings->GetScreenResolution();
	
	FString Str = "";
	Str += FString::FromInt(ScreenRes.X);
	Str += "x";
	Str += FString::FromInt(ScreenRes.Y);

	resolution = Str;

	return true;
}

bool UBPFunctionLibrary::GetScreenResolutions(TArray<FString>& resolutionList)
{
	resolutionList.Empty();

	FScreenResolutionArray Resolutions;
	if (RHIGetAvailableResolutions(Resolutions, false))
	{
		for (int i = Resolutions.Num() - 1; i; --i)
		{
			const FScreenResolutionRHI& EachResolution = Resolutions[i];

			FString Str = "";
			Str += FString::FromInt(EachResolution.Width);
			Str += "x";
			Str += FString::FromInt(EachResolution.Height);

			if (resolutionList.Contains(Str))
			{
				//Skip! This is duplicate!
				continue;
			}
			else
			{
				//Add to Unique List!
				resolutionList.AddUnique(Str);
			}
		}

		return true;
	}
	return false;
}

bool UBPFunctionLibrary::SetScreenResolution(const FString& resolution)
{
	if (resolution.IsEmpty())
	{
		return false;
	}

	FString strWidth;
	FString strHeight;
	check(resolution.Split("x", &strWidth, &strHeight));

	// dont bother trying to change to the current reolution if it is unchanged
	FString currentResolution;
	GetScreenResolution(currentResolution);
	if (currentResolution.Equals(resolution))
	{
		return true;
	}

	FIntPoint ResolutionOpt;
	ResolutionOpt.X = FCString::Atoi(*strWidth);
	ResolutionOpt.Y = FCString::Atoi(*strHeight);

	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	UserSettings->SetScreenResolution(ResolutionOpt);
	UserSettings->ApplySettings(false);
	return true;
}

bool UBPFunctionLibrary::SetFullScreen(bool fullscreen)
{
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	UserSettings->SetFullscreenMode(fullscreen ? EWindowMode::Fullscreen : EWindowMode::Windowed);
	UserSettings->ApplySettings(false);
	return true;
}

bool UBPFunctionLibrary::GetFullscreenMode(bool& fullscreen)
{
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	fullscreen = (UserSettings->GetFullscreenMode() == EWindowMode::Fullscreen);
	return true;
}

void UBPFunctionLibrary::GetAudioClassVolume(const FString& ClassName, float& Volume)
{
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();
	if (AudioDevice != NULL)
	{
		for (TMap<USoundClass*, FSoundClassProperties>::TIterator It(AudioDevice->SoundClasses); It; ++It)
		{
			USoundClass* ThisSoundClass = It.Key();
			if (ThisSoundClass != NULL && ThisSoundClass->GetFullName().Find(ClassName) != INDEX_NONE)
			{
				// the audiodevice function logspams for some reason
				//AudioDevice->SetClassVolume(ThisSoundClass, NewVolume);
				Volume = ThisSoundClass->Properties.Volume;
			}
		}
	}
}

void UBPFunctionLibrary::SetAudioClassVolume(const FString& ClassName, float Volume)
{
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();
	if (AudioDevice != NULL)
	{
		for (TMap<USoundClass*, FSoundClassProperties>::TIterator It(AudioDevice->SoundClasses); It; ++It)
		{
			USoundClass* ThisSoundClass = It.Key();
			if (ThisSoundClass != NULL && ThisSoundClass->GetFullName().Find(ClassName) != INDEX_NONE)
			{
				// the audiodevice function logspams for some reason
				//AudioDevice->SetClassVolume(ThisSoundClass, NewVolume);
				ThisSoundClass->Properties.Volume = Volume;
			}
		}
	}
}

void UBPFunctionLibrary::GetPostProcessingEnabled(bool& enabled)
{
	UGameViewportClient* gvc = GetGameInstance()->GetGameViewportClient();
	if (!gvc)
		return;

	enabled = gvc->EngineShowFlags.PostProcessing;
}

void UBPFunctionLibrary::SetPostProcessingEnabled(bool enabled)
{
	UGameViewportClient* gvc = GetGameInstance()->GetGameViewportClient();
	if (!gvc)
		return;

	gvc->EngineShowFlags.PostProcessing = enabled;
}

void UBPFunctionLibrary::SetScalabilityQuality(int32 quality)
{
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	UserSettings->ScalabilityQuality.SetFromSingleQualityLevel(quality);
	UserSettings->ScalabilityQuality.PostProcessQuality = FMath::Max(3, quality); // this needs to remain high otherwise the game looks like crap
	UserSettings->ApplySettings(false);

	UGameViewportClient* gvc = GetGameInstance()->GetGameViewportClient();
	if (!gvc)
		return;

	bool lowQuality = (quality == 0);
	gvc->EngineShowFlags.SetBloom(!lowQuality);
	gvc->EngineShowFlags.SetAntiAliasing(!lowQuality);
	gvc->EngineShowFlags.SetAmbientOcclusion(!lowQuality);
	gvc->EngineShowFlags.SetAmbientCubemap(!lowQuality);
	gvc->EngineShowFlags.SetDepthOfField(!lowQuality);
	gvc->EngineShowFlags.SetMotionBlur(!lowQuality);
	gvc->EngineShowFlags.SetReflectionEnvironment(!lowQuality);
	gvc->EngineShowFlags.SetRefraction(!lowQuality);
	gvc->EngineShowFlags.SetSceneColorFringe(!lowQuality);
	gvc->EngineShowFlags.SetScreenSpaceReflections(!lowQuality);
	gvc->EngineShowFlags.SetVignette(!lowQuality);
}

void UBPFunctionLibrary::GetScalabilityQuality(int32& quality)
{
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	quality = UserSettings->ScalabilityQuality.EffectsQuality;
}

void UBPFunctionLibrary::GetGameViewportClient(UGGameViewportClient*& GameViewport)
{
	// because we dont want to have a compile dependency on the joystick plugin
	// we have to let blueprints set up the joysticks and then tell us about it
	// if the plugin is missing, the blueprints should still function hopefully!
	// blueprints is doing the following:

	/*
	int32 gamepadCount = UJoystickFunctions::JoystickCount();
	UJoystickFunctions::IgnoreGameControllers(false);
	UJoystickFunctions::IgnoreXInputControllers(true);
	int32 nonXInputGamepadCount = UJoystickFunctions::JoystickCount();
	int32 xinputGamepadCount = gamepadCount - nonXInputGamepadCount;
	*/

	if (!GetGameInstance())
	{
		return;
	}

	UGGameViewportClient* gvc = Cast<UGGameViewportClient>(GetGameInstance()->GetGameViewportClient());
	GameViewport = gvc;
}


UWidget* FindFirstChildWidgetByObjectNameInternal(class UWidget* Widget, FString& Name)
{
	if (!Widget)
		return NULL;

	if (Widget->GetName() == Name)
	{
		return Widget;
	}

	UPanelWidget* PanelWidget = Cast<UPanelWidget>(Widget);
	if (!PanelWidget)
		return NULL;

	for (int i = 0; i < PanelWidget->GetChildrenCount(); ++i)
	{
		UWidget* Child = PanelWidget->GetChildAt(i);

		UWidget* Result = FindFirstChildWidgetByObjectNameInternal(Child, Name);
		if (Result)
			return Result;
	}

	return NULL;
}

void UBPFunctionLibrary::FindFirstChildWidgetByObjectName(class UWidget* Parent, FString Name, class UWidget*& Child)
{
	Child = FindFirstChildWidgetByObjectNameInternal(Parent, Name);
}
