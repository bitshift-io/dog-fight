// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "OnlineBlueprintCallProxyBase.h"
#include "FindSessionsCallbackProxy.h"
#include "Game/GLeaderboards.h"
#include "Scoreboard.generated.h"

class AGGameState;

// based loosely on SShooterLeaderboard

/** leaderboard row display information */
USTRUCT(BlueprintType)
struct FLeaderboardRow
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bValid;

	/** player rank on the leaderboard */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Rank;

	/** player name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString PlayerName;


	/** player total kills to display */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Kills;

	/** player total deaths to display */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Deaths;


	/** wave reached */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Wave;

	/** score */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Score;
	
	/** Unique Id for the player at this rank */
	TSharedPtr<const FUniqueNetId> PlayerId;

	/** Default Constructor */
	void Populate(const FOnlineStatsRow& Row, UScoreboard* Scoreboard);

	inline static bool ConstPredicate(const TSharedPtr<FLeaderboardRow>& a, const TSharedPtr<FLeaderboardRow>& b)
	{
		return (a->Score > b->Score);
	}
};

UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API UScoreboard : public UIngameMenu
{
	GENERATED_BODY()
public:

	UScoreboard(const FObjectInitializer& ObjectInitializer);
	virtual ~UScoreboard();

	virtual void PostInitProperties();

	UFUNCTION(BlueprintImplementableEvent, Category = "Scoreboard")
	void InsertPlayerScore(int32 Index, const FString& Name, const FString& ActorName, const FLinearColor& Color, int32 Score, const FString& Details);

	UFUNCTION(BlueprintImplementableEvent, Category = "Scoreboard")
	void SetWave(int32 Wave);

	UFUNCTION(BlueprintImplementableEvent, Category = "Scoreboard")
	void SetMessage(const FString& Message);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Scoreboard")
	void InsertLeaderboardRow(const FLeaderboardRow& LeaderboardRow);

	UFUNCTION(BlueprintImplementableEvent, Category = "Scoreboard")
	void EnableLeaderboard();

	virtual void Show(const FString& WinMessage);
	virtual void ToggleGameMenu();

	virtual void AddToViewport();

	AGGameState* GetGameState();

	//int32 CalculateScore(AGPlayerState* PlayerState);
	void PopulateScores();

	void ReadStats();
	void OnStatsRead(bool bWasSuccessful);

	/** Removes the bound LeaderboardReadCompleteDelegate if possible*/
	void ClearOnLeaderboardReadCompleteDelegate();

	/** Returns true if a leaderboard read request is in progress or scheduled */
	bool IsLeaderboardReadInProgress();

	/** action bindings array */
	TArray< TSharedPtr<FLeaderboardRow> > StatRows;

	/** Leaderboard read object */
	FOnlineLeaderboardReadPtr ReadObject;

	/** Indicates that a stats read operation has been initiated */
	bool bReadingStats;

	/** Delegate called when a leaderboard has been successfully read */
	FOnLeaderboardReadCompleteDelegate LeaderboardReadCompleteDelegate;

	/** Handle to the registered LeaderboardReadComplete delegate */
	FDelegateHandle LeaderboardReadCompleteDelegateHandle;
};

