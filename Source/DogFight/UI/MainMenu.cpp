// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "MainMenu.h"
#include "MenuWidget.h"
#include "MessageMenu.h"
#include "AudioDevice.h"
#include "Game/GGameInstance.h"
#include "Game/GGameViewportClient.h"
#include "Game/GProfileSettings.h"
#include "IPlatformFilePak.h"
#include "BPFunctionLibrary.h"
#include "Widgets/MapDesc.h"
#include "Util/OnlineSubsystem.h"
#include "JoystickFunctions.h"

#if PLATFORM_WINDOWS
#include "WindowsApplication.h"
#endif

/*
#include "UnrealEd.h"
#include "Runtime/PakFile/Public/IPlatformFilePak.h"
#include "GenericPlatformChunkInstall.h"
*/
// move actor:
// https://answers.unrealengine.com/questions/26573/move-a-character-in-direction-c.html

#pragma optimize("", OPTIMISATION)


const int32 MAX_CONTROLLER_ID = 8; // ue4 currently only allows up to 8 controller ID's

const FString MoveSlotString = "Move to this slot";
const FString NoneString = "None";
const FString AIString = "AI";

// sort by device type, then by index
bool FInputNameStruct::operator<(const FInputNameStruct& Other) const
{
	if (DeviceType == Other.DeviceType)
	{
		return Index < Other.Index;
	}

	return DeviceType < Other.DeviceType;
}

// sort for assigning controller id's
bool FPlayerStruct::operator<(const FPlayerStruct& Other) const
{
	FInputNameStruct* iThis = MainMenu->GetInputNameStruct(InputName);
	FInputNameStruct* iOther = MainMenu->GetInputNameStruct(Other.InputName);

	// network players, dont care about sorting here so just abort
	if (!iThis || !iOther)
	{
		return false;
	}

	return iThis->operator<(*iOther);
}

void FPlayerStruct::ResolveWidgets(AMainMenu* Menu)
{
	MainMenu = Menu;

	// resolve comboboxes and populate and add delegates/callbacks
	UMenuWidget* MenuWidget = MainMenu->MenuWidget;
	if (!MenuWidget)
	{
		return;
	}

	UWidget* widget = NULL;

	InputName = NoneString;

	/** @returns The uobject widget corresponding to a given name */
	widget = MenuWidget->GetWidgetFromName(FName(*InputComboName));
	//UBPFunctionLibrary::FindFirstChildWidgetByObjectName(MenuWidget, InputComboName, widget);
	InputCombo = Cast<UGComboBoxString>(widget);
	if (InputCombo)
	{
		InputCombo->AddOptionUnique(NoneString);
		InputCombo->SetSelectedOption(NoneString);

		InputCombo->OnOpening.AddDynamic(MainMenu, &AMainMenu::OnInputOpening);
		InputCombo->OnSelectionChanged.AddDynamic(MainMenu, &AMainMenu::OnInputSelectionChanged);
	}

	widget = NULL;
	widget = MenuWidget->GetWidgetFromName(FName(*ActorComboName));
	//UBPFunctionLibrary::FindFirstChildWidgetByObjectName(MenuWidget, ActorComboName, widget);
	ActorCombo = Cast<UGComboBoxString>(widget);
	if (ActorCombo)
	{
		TArray<FString> ActorNamesList;
		MainMenu->GetActorNames(ActorNamesList);
		ActorCombo->SetOptions(ActorNamesList);
		ActorCombo->SetSelectedOption(ActorNamesList[0]);
		
		ActorCombo->OnSelectionChanged.AddDynamic(MainMenu, &AMainMenu::OnActorSelectionChanged);
	}
}

void FPlayerStruct::SetSlotOwner(APlayerController* PC)
{
	if (!PC || !PC->PlayerState)
		return;

	AGPlayerState* PS = Cast<AGPlayerState>(PC->PlayerState);
	if (!PS)
		return;

	UniquePlayerId = PS->UniqueId;
	PlayerName = PS->PlayerName;
}

bool FPlayerStruct::IsSlotOwner(APlayerController* PC)
{
	if (!PC || !PC->PlayerState)
		return false;

	return UniquePlayerId == PC->PlayerState->UniqueId;
}

bool FPlayerStruct::IsOccupiedByPlayer()
{
	return InputName != AIString && InputName != NoneString;
}

bool FPlayerStruct::IsLocalPlayer()
{
	if (InputName == NoneString)
	{
		return false;
	}

	FInputNameStruct* i = MainMenu->GetInputNameStruct(InputName);
	FActorNameStruct* a = MainMenu->GetActorNameStruct(ActorName);
	FColourNameStruct* c = MainMenu->GetColourNameStruct(ColourName);

	if (!i)
	{
		return false;
	}

	return true;
}

bool FPlayerStruct::IsAIPlayer()
{
	return InputName == AIString;
}

AMainMenu::AMainMenu(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NumKeyboardDevices = 2;
	GamepadCount = 0;
	//NetConnection = NULL;
	bIgnoreOnInputSelectionChangedEvent = false;
}

AMainMenu::~AMainMenu()
{
}

void AMainMenu::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//if (!UserHasLicenseForApp())
	//{
		/*
		// if we load the menu and there is a message box active, change the z-order
		UMessageMenu* MsgBox = UMessageMenu::Construct(EMessageMenuType::AcceptableMessage);
		if (MsgBox)
		{
			// hide menu until user closes message box
			MenuWidget->RemoveFromViewport();
			MsgBox->OnOk.AddDynamic(this, &AMainMenu::MessageBoxClosed);
		}
		*/
	//	return;
	//}

	

	//
	// configure the input name list
	//
	ConfigureDevices();

	//
	// resolve and populate widgets
	//
	UGameInstance* ge = GetGameInstance();
	AGPlayerController* PC = ge ? Cast<AGPlayerController>(ge->GetFirstLocalPlayerController()) : NULL;
	bool isClient = (Role < ROLE_Authority);

	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		PlayerList[i].ResolveWidgets(this);

		if (!isClient)
		{
			PlayerList[i].SetSlotOwner(PC);
		}
	}

	// enable/disable appropriate slots by default on client
	if (isClient)
	{
		OnRep_PlayerList();
	}

	if (MenuWidget)
	{
		for (int i = 0; i < ClientSyncComboBoxNameList.Num(); ++i)
		{
			UGComboBoxString* Widget = Cast<UGComboBoxString>(MenuWidget->GetWidgetFromName(FName(*ClientSyncComboBoxNameList[i])));
			Widget->OnSelectionChanged.AddDynamic(this, &AMainMenu::OnClientSyncComboBoxSelectionChanged);
			ClientSyncComboBoxList.Add(Widget);
			ClientSyncComboBoxValueList.Add(FString());
		}
	}

	// if we load the menu and there is a message box active, change the z-order
	UMessageMenu* MsgBox = UMessageMenu::Active();
	if (MsgBox)
	{
		// hide menu until user closes message box
		if (MenuWidget)
		{
			MenuWidget->RemoveFromViewport();
		}
		MsgBox->OnOk.AddDynamic(this, &AMainMenu::MessageBoxClosed);
		MsgBox->OnCancel.AddDynamic(this, &AMainMenu::MessageBoxClosed);
	}
}


void AMainMenu::ConfigureDevices()
{	
	GamepadCount = UJoystickFunctions::JoystickCount();
	UGGameViewportClient* GVC = Cast<UGGameViewportClient>(GEngine->GameViewport);
	if (GVC)
	{
		GVC->SetGamepadCount(GamepadCount);
	}

	UE_LOG(LogClass, Log, TEXT("-------------------------------"));
	UE_LOG(LogClass, Log, TEXT("SDL2 detected %i joysticks"), GamepadCount);

	InputNameList.Empty();
	AddInputName(NoneString, None, -1, -1);
	AddInputName(AIString, AI, -1, -1);

	UE4DeviceList.Empty(); // devices UE4 will handle
	SDL2DeviceList.Empty(); // devices SDL2 will need to handle


	//
	// In EngineSourceChanges directory are some changes that need to be applied to engine for this to compile
	// SetXInputEnabled
	//
#if PLATFORM_WINDOWS
	TSharedPtr<FWindowsApplication> WindowsApp = StaticCastSharedPtr<FWindowsApplication>(FSlateApplication::Get().GetPlatformApplication());
	WindowsApp->SetXInputEnabled(true);

	const int32 MAX_XINPUT_DEVICES = 4;
#else
	const int32 MAX_XINPUT_DEVICES = 8; // only windows has the xinput issue
#endif

	// work out how many XInput devices in total there are
	int32 TotalXInputDeviceCount = 0;
	for (int i = 0; i < GamepadCount; ++i)
	{
		bool bIsXInput = UJoystickFunctions::IsXInputController(i);
		if (bIsXInput)
		{
			++TotalXInputDeviceCount;
		}

		if (UE4DeviceList.Num() < MAX_XINPUT_DEVICES && bIsXInput)
		{
			UE4DeviceList.Add(i);
		}
		else
		{
			SDL2DeviceList.Add(i);
		}

		// unmap all devices in SDL2
		UJoystickFunctions::MapJoystickDeviceToPlayer(i, -1);
	}

	int32 MaxNumNonXInputDevices = (MAX_CONTROLLER_ID - UE4DeviceList.Num()); 
	int32 NumNonXInputDevices = 2 + SDL2DeviceList.Num(); // keyboard left and right 

	int32 SDL2StartPlayerIndex = 0;
	bool bMoreXInputDevicesThanUE4CanHandle = (TotalXInputDeviceCount > MAX_XINPUT_DEVICES);
	bool bMoreNonXInputDevicesThanUE4CanHandle = (NumNonXInputDevices > MaxNumNonXInputDevices);

	// more devices than ue4 can handle, so disable XInput system from UE4 and let SDL2 take over
	if (bMoreXInputDevicesThanUE4CanHandle || bMoreNonXInputDevicesThanUE4CanHandle) 
	{
		UE_LOG(LogClass, Log, TEXT("Disabling XInput system in UE4 engine"));
#if PLATFORM_WINDOWS
		WindowsApp->SetXInputEnabled(false);
#endif

		SDL2DeviceList.Append(UE4DeviceList);
		UE4DeviceList.Empty();
	}

	// sort by device ID
	UE4DeviceList.Sort();
	SDL2DeviceList.Sort();	

	SDL2StartPlayerIndex += UE4DeviceList.Num();
	for (int i = 0; i < UE4DeviceList.Num(); ++i)
	{
		UE_LOG(LogClass, Log, TEXT("Joystick %i handled by UE4 (XInput)"), UE4DeviceList[i]);

		//uint32 alt = GamepadCount + i;
		check(i == UE4DeviceList[i]);
		AddInputName(FString::Printf(TEXT("Gamepad %i (Xbox 360)"), UE4DeviceList[i] + 1), XInputGamepad, i, UE4DeviceList[i]); // i, alt); // tries to map to keyboard devices
	}

	// map SDL2 devices to player controller ID's
	for (int i = 0; i < SDL2DeviceList.Num(); ++i)
	{
		UE_LOG(LogClass, Log, TEXT("Joystick %i handled by SDL2"), SDL2DeviceList[i]);
		//UJoystickFunctions::MapJoystickDeviceToPlayer(SDL2DeviceList[i], i + SDL2StartPlayerIndex);
		FJoystickInfo info = UJoystickFunctions::GetJoystick(SDL2DeviceList[i]);

		//uint32 alt = GamepadCount + i;
		AddInputName(FString::Printf(TEXT("Gamepad %i (%s)"), SDL2DeviceList[i] + 1, *info.DeviceName), SDL2Gamepad, i + SDL2StartPlayerIndex, SDL2DeviceList[i]); // i + SDL2StartPlayerIndex, alt); // tries to map to keyboard devices
	}

	AddInputName("Right Keyboard", Keyboard, 0, -1);	// right keyboard will try to map to the primary gamepad controller
	AddInputName("Left Keyboard", Keyboard, 1, -1); // left keyboard will try to map to the secondary gamepad controller

	UE_LOG(LogClass, Log, TEXT(""));
	UE_LOG(LogClass, Log, TEXT("Total XInput joysticks: %i"), TotalXInputDeviceCount);
	UE_LOG(LogClass, Log, TEXT("Joysticks handled by UE4: %i"), UE4DeviceList.Num());
	UE_LOG(LogClass, Log, TEXT("Joysticks handled by SDL2: %i"), SDL2DeviceList.Num());
	UE_LOG(LogClass, Log, TEXT("-------------------------------"));
}

void AMainMenu::MessageBoxClosed(class UMessageMenu* MsgBox)
{
	//if (!UserHasLicenseForApp())
	//{
	//	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
	//}

	MenuWidget->AddToViewport();
	SetWidgetToFocus();
}

void AMainMenu::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMainMenu, PlayerList);
	DOREPLIFETIME(AMainMenu, ClientSyncComboBoxValueList);
}

void AMainMenu::SetGamepadCount(int32 Count)
{
	// legacy.. called by blue prints some where

	/*
	GamepadCount = Count;

	/ *
	// this bit of code relies on the blueprints to configure the joysticks using the joystick plugin
	for (int i = 0; i < Count; ++i)
	{
		uint32 alt = (i < NumKeyboardDevices) ? i : -1;
		AddInputName(FString::Printf(TEXT("Gamepad %i"), i + 1), i + NumKeyboardDevices, alt); // tries to map to keyboard devices
	}
	*/
}

//void AMainMenu::AddInputName(const FString& name, uint32 controllerId, uint32 alternateControllerId)
void AMainMenu::AddInputName(const FString& Name, DeviceType DeviceType, int32 Index, int32 SDL2DeviceIndex)
{
	FInputNameStruct s;
	s.Name = Name;
	s.DeviceType = DeviceType;
	s.Index = Index;
	s.SDL2DeviceIndex = SDL2DeviceIndex;
	//s.ControllerId = controllerId;
	//s.AlternateControllerId = alternateControllerId;
	InputNameList.Add(s);
}

/*
FString AMainMenu::GetInputNameFromControllerId(int32 ControllerId)
{

	for (int i = 0; i < InputNameList.Num(); ++i)
	{
		if (InputNameList[i].ControllerId == ControllerId)
		{
			return InputNameList[i].Name;
		}
	}

	return "";
}
*/

/*
int32 AMainMenu::GetControllerIdFromInputName(const FString& InputName)
{
	for (int i = 0; i < InputNameList.Num(); ++i)
	{
		if (InputNameList[i].Name == InputName)
		{
			return InputNameList[i].ControllerId;
		}
	}

	return -1;
}*/

void AMainMenu::CreateLocalPlayers()
{
	RemoveLocalPlayers();

	//*const_cast<int32*>(&SlateApplicationDefs::MaxUsers) = 32; // allow up to 32 controller id's to be assigned

	UGameInstance* ge = GetGameInstance();
	//int32 aiControllerId = -1000;

	UGGameViewportClient* GVC = Cast<UGGameViewportClient>(GEngine->GameViewport);
	if (GVC)
	{
		GVC->ClearControllerToControllerMap();
	}

	//APlayerController* CreatePlayer(UObject* WorldContextObject, int32 ControllerId = -1, bool bSpawnPawn = true);
	TArray<int32> ControllerIdInUseList;
	TArray<FInputNameStruct*> UsedDeviceList;

	PlayerList.Sort();


	int XInputDeviceCount = UE4DeviceList.Num();
	int Idx = 0;
	int NonXInputControllerIdx = 0;

	bool bFirst = true;
	for (FPlayerStruct& p : PlayerList)
	{
		if (!p.IsLocalPlayer())
		{
			continue;
		}

		FInputNameStruct* i = GetInputNameStruct(p.InputName);
		FActorNameStruct* a = GetActorNameStruct(p.ActorName);
		FColourNameStruct* c = GetColourNameStruct(p.ColourName);

		UsedDeviceList.Add(i);

		uint32 ControllerId = -1;
		switch (i->DeviceType)
		{
			// we cant change controller ID's for XInput devices they are hard bound by ue4 to 0-4
		case XInputGamepad:
			ControllerId = Idx;
			break;

			// assign an ID that is not used by any XInput device
		default:
			ControllerId = NonXInputControllerIdx + XInputDeviceCount;
			++NonXInputControllerIdx;
			break;
		}

		if (ControllerId < 0)
		{
			UE_LOG(LogClass, Log, TEXT("AMainMenu::CreateLocalPlayers - Error: ControllerId < 0"));
			continue;
		}

		if (ControllerId >= MAX_CONTROLLER_ID)
		{
			UE_LOG(LogClass, Log, TEXT("AMainMenu::CreateLocalPlayers - Error: ControllerId >= 8"));
			continue;
		}
		
		// SDL2 gamepad mapping
		if (i->DeviceType == SDL2Gamepad)
		{
			UJoystickFunctions::MapJoystickDeviceToPlayer(i->SDL2DeviceIndex, ControllerId);
		}

		// keyboard mapping
		if (i->DeviceType == Keyboard)
		{
			TArray<FKey> KeyArray;

			// Right keyboard
			if (i->Index == 0) 
			{
				KeyArray.Add(EKeys::Up);
				KeyArray.Add(EKeys::Down);
				KeyArray.Add(EKeys::Left);
				KeyArray.Add(EKeys::Right);
				KeyArray.Add(EKeys::RightControl);
			}
			// Left keyboard
			else
			{
				KeyArray.Add(EKeys::W);
				KeyArray.Add(EKeys::S);
				KeyArray.Add(EKeys::A);
				KeyArray.Add(EKeys::D);
				KeyArray.Add(EKeys::LeftControl);
			}

			GVC->MapKeysToController(ControllerId, KeyArray);
		}



		// controllerId == 0 refers to the first local player which is never created or destroyed
		UGLocalPlayer* LocalPlayer = NULL;
		if (bFirst)
		{
			LocalPlayer = Cast<UGLocalPlayer>(ge->GetFirstGamePlayer());
			bFirst = false;
		}
		else
		{
			FString Error;
			LocalPlayer = Cast<UGLocalPlayer>(ge->CreateLocalPlayer(ControllerId, Error, true));
			if (Error.Len())
			{
				UE_LOG(LogClass, Log, TEXT("%s"), *Error);
			}
		}

		if (!LocalPlayer)
		{
			// some error
			continue;
		}

		LocalPlayer->SetControllerId(ControllerId);
		UGProfileSettings* ProfileSettings = LocalPlayer->GetProfileSettings();
		ProfileSettings->PlayerColourName = p.ColourName;
		ProfileSettings->PlayerColour = c->Colour;
		ProfileSettings->DefaultPawnClassForController = a ? a->ActorClass : ActorNameList[0].ActorClass;
		ProfileSettings->bAIPlayer = p.IsAIPlayer();


		// keep last
		++Idx;
	}

	// attempt to map all unused input devices to first player - to allow single player to not have to configure any devices
	for (FInputNameStruct& i : InputNameList)
	{
		bool bUsed = false;
		for (FInputNameStruct* usedI : UsedDeviceList)
		{
			if (usedI->Name == i.Name)
			{
				bUsed = true;
				break;
			}
		}

		if (bUsed)
		{
			continue;
		}

		UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(ge->GetFirstGamePlayer());

		// SDL2 gamepad mapping
		if (i.DeviceType == SDL2Gamepad)
		{
			UJoystickFunctions::MapJoystickDeviceToPlayer(i.SDL2DeviceIndex, LocalPlayer->GetControllerId());
		}
		// XInput mapping
		if (i.DeviceType == XInputGamepad)
		{
			GVC->MapControllerToController(i.Index, LocalPlayer->GetControllerId());
		}

	}


#if 0
	{
		/*
		if (!i)
			continue;

		// AI controllers get negative ID's
		bool bAIPlayer = (p.InputName == AIString);
		if (bAIPlayer)
		{
			++aiControllerId;
		}*/

		UE_LOG(LogClass, Log, TEXT("Player: %s, InputName: %s, ActorName: %s, ControllerId: %i"), *p.ColourName, *i->Name, *p.ActorName, i->ControllerId);

		int32 controllerId = /*p.IsAIPlayer() ? aiControllerId :*/ i->ControllerId;

		// controllerId == 0 refers to the first local player which is never created or destroyed
		UGLocalPlayer* LocalPlayer = NULL;
		if (bFirst)
		{
			LocalPlayer = Cast<UGLocalPlayer>(ge->GetFirstGamePlayer());
			bFirst = false;
		}
		else
		{
			FString Error;
			LocalPlayer = Cast<UGLocalPlayer>(ge->CreateLocalPlayer(controllerId, Error, true));
			if (Error.Len())
			{
				UE_LOG(LogClass, Log, TEXT("%s"), *Error);
			}
		}

		if (!LocalPlayer)
		{
			// some error
			continue;
		}
		
		// apply menu options to player profile
		ControllerIdInUseList.Add(controllerId);
		LocalPlayer->SetControllerId(controllerId);
		UGProfileSettings* ProfileSettings = LocalPlayer->GetProfileSettings();
		ProfileSettings->PlayerColourName = p.ColourName;
		ProfileSettings->PlayerColour = c->Colour;
		ProfileSettings->DefaultPawnClassForController = a ? a->ActorClass : ActorNameList[0].ActorClass;
		ProfileSettings->bAIPlayer = false; // bAIPlayer;

		// set up AlternateControllerId
		if (/*!bAIPlayer &&*/ i->AlternateControllerId != -1)
		{
			bool bAlternateControllerIdInUse = false;
			for (FPlayerStruct& p2 : PlayerList)
			{
				if (!p2.IsLocalPlayer() || p2.IsAIPlayer())
				{
					continue;
				}

				FInputNameStruct* i2 = GetInputNameStruct(p2.InputName);

				/*
				FString Error2;

				if (p2.InputName == NoneString)
					continue;

				FInputNameStruct* i2 = GetInputNameStruct(p2.InputName);
				FActorNameStruct* a2 = GetActorNameStruct(p2.ActorName);
				FColourNameStruct* c2 = GetColourNameStruct(p2.ColourName);

				if (!i2)
					continue;

				// AI controllers get negative ID's
				bool bAIPlayer2 = (p2.InputName == AIString);
				if (bAIPlayer2)
				{
					continue;
				}*/

				// if this check passes, the alternate id is being used for another
				// player, so do nothing
				if (i->AlternateControllerId == i2->ControllerId)
				{
					bAlternateControllerIdInUse = true;
					break;
				}
			}

			// now do some controller mapping
			if (!bAlternateControllerIdInUse && GVC)
			{
				ControllerIdInUseList.Add(i->AlternateControllerId);

				// special case that if we have a controller mapped to non-zero,
				// but its alternate is zero, make the alternate the non-zero and make its id zero
				// this fixes issues
				if (i->AlternateControllerId == 0)
				{
					LocalPlayer->SetControllerId(0);
					GVC->MapControllerToController(controllerId, i->AlternateControllerId);
				}
				else
				{
					
					GVC->MapControllerToController(i->AlternateControllerId, controllerId);
				}
			}
		}
	}

	// ensure controller id 0 is always in use otherwise the menu system wont work!
	// just map it to the first device
	int32 idxOfZero = ControllerIdInUseList.Find(0);
	if (idxOfZero == INDEX_NONE && GVC && ControllerIdInUseList.Num())
	{
		GVC->MapControllerToController(0, ControllerIdInUseList[0]);
	}

	//
	// now add AI players
	//
	for (FPlayerStruct& p : PlayerList)
	{
		if (!p.IsAIPlayer())
		{
			continue;
		}

		// find an unused contorller id
		int32 controllerId = -1;
		for (int i = 0; i < SlateApplicationDefs::MaxUsers; ++i)
		{
			if (!ControllerIdInUseList.Contains(i))
			{
				controllerId = i;
				break;
			}
		}

		if (controllerId == -1)
		{
			// ran out of controller slots!
			// this means we are using more than 
			// SlateApplicationDefs::MaxUsers controller ids (8)
			// we just have to leave out some AI because I dont want to crash the engine!
			continue;
		}

		FString Error;
		UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(ge->CreateLocalPlayer(controllerId, Error, true));
		if (Error.Len())
		{
			UE_LOG(LogClass, Log, TEXT("%s"), *Error);
		}

		if (!LocalPlayer)
		{
			// some error
			continue;
		}

		FActorNameStruct* a = GetActorNameStruct(p.ActorName);
		FColourNameStruct* c = GetColourNameStruct(p.ColourName);

		// apply menu options to player profile
		ControllerIdInUseList.Add(controllerId);
		LocalPlayer->SetControllerId(controllerId);
		UGProfileSettings* ProfileSettings = LocalPlayer->GetProfileSettings();
		ProfileSettings->PlayerColourName = p.ColourName;
		ProfileSettings->PlayerColour = c->Colour;
		ProfileSettings->DefaultPawnClassForController = a ? a->ActorClass : ActorNameList[0].ActorClass;
		ProfileSettings->bAIPlayer = true; // bAIPlayer;
	}
#endif
}

void AMainMenu::RemoveLocalPlayers()
{
	UGameInstance* ge = GetGameInstance();

	// restore the controller id of the first player
	ge->GetFirstGamePlayer()->SetControllerId(0);

	TArray< class ULocalPlayer * >::TConstIterator it = ge->GetLocalPlayerIterator();
	for (; it; ++it)
	{
		// do not delete the first local player
		if (*it == ge->GetFirstGamePlayer())
			continue;

		ge->RemoveLocalPlayer(*it);
	}
}

void AMainMenu::ResetPlayerList()
{
	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		PlayerList[i].InputName = NoneString;
	}
}

bool AMainMenu::CheckCanHostGame()
{
	// check we can start a game, ie. is there at least one player?

	bool bHumanPlayerFound = false;
	for (FPlayerStruct& p : PlayerList)
	{
		FString Error;

		if (p.InputName == NoneString)
			continue;

		FInputNameStruct* i = GetInputNameStruct(p.InputName);
		FActorNameStruct* a = GetActorNameStruct(p.ActorName);
		FColourNameStruct* c = GetColourNameStruct(p.ColourName);

		if (!i)
			continue;

		// AI controllers get negative ID's
		bool bAIPlayer = (p.InputName == AIString);
		if (bAIPlayer)
		{
			continue;
		}

		bHumanPlayerFound = true;
		break;
	}

	if (!bHumanPlayerFound)
	{
		//MenuWidget->RemoveFromViewport();
		UMessageMenu* MsgBox = UMessageMenu::ConstructDefault(EMessageMenuType::AcceptableMessage);
		//MsgBox->OnOk.AddDynamic(this, &AMainMenu::MessageBoxClosed);
		MsgBox->SetMessage("No local input has been assigned to a slot");
		return false;
	}

	// check all remote network players
	// are assigned a slot
	UWorld *CurrentWorld = GetGGameInstance()->GetWorld();
	for (FConstPlayerControllerIterator Iterator = CurrentWorld->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PC = *Iterator;
		if (!PC->IsLocalController())
		{
			bool bRemotePlayerAssignedSlot = false;
			for (FPlayerStruct& p : PlayerList)
			{
				if (p.IsSlotOwner(PC))
				{
					bRemotePlayerAssignedSlot = true;
					break;
				}
			}
			
			if (!bRemotePlayerAssignedSlot)
			{
				//MenuWidget->RemoveFromViewport();
				UMessageMenu* MsgBox = UMessageMenu::ConstructDefault(EMessageMenuType::AcceptableMessage);
				//MsgBox->OnOk.AddDynamic(this, &AMainMenu::MessageBoxClosed);

				AGPlayerState* PS = Cast<AGPlayerState>(PC->PlayerState);
				FString msg("Not all clients have been assigned a slot");
				if (PS)
				{
					msg = FString::Printf(TEXT("%s has not been assigned a slot"), *PS->PlayerName);
				}
				MsgBox->SetMessage(msg);
				return false;
			}
		}
	}

	return true;
}

void AMainMenu::InstantAction()
{
	SetNetworkVisibility("None");
	ResetPlayerList();
	SetPlayerInputName("Green", "Right Keyboard");

	// pick a random map
	TArray<UMapDesc*> Maps;
	UMapDesc::GetMapDescList(Maps);
	MapDesc = Maps[FMath::RandRange(0, Maps.Num() - 1)];

	HostGame();
}

void AMainMenu::HostGame()
{
	if (!UserHasLicenseForApp())
	{
		GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
	}

	// only authority can host a game
	if (Role < ROLE_Authority)
	{
		return;
	}

	if (!CheckCanHostGame())
	{
		return;
	}

	NotifyHostGame();

	UGameInstance* ge = GetGameInstance();

	if (!MapDesc)
	{
		UE_LOG(LogClass, Log, TEXT("No MapDesc is set - Call GetMaps"));
		return;
	}

	// clean up the map name
	FString formattedMapName = MapDesc->LongPackageName;

	// clean up the game mode name
	FString fudgedGameModeName = GameModeName.Replace(TEXT(" "), TEXT(""));
	FString formattedGameModeName = FString::Printf(TEXT("/Game/Blueprint/BP_%sGameMode.BP_%sGameMode_C"), *fudgedGameModeName, *fudgedGameModeName);

	// /Game/Map/
	uint32 BotsCountOpt = 1;
	FString const StartURL = FString::Printf(TEXT("%s?game=%s%s%s?%s=%d%s"), *formattedMapName, *formattedGameModeName, bIsOnline ? TEXT("?listen") : TEXT(""), bIsLanMatch ? TEXT("?bIsLanMatch") : TEXT(""), *AGGameMode::GetBotsCountOptionName(), BotsCountOpt, DemoName.IsEmpty() ? TEXT("") : *DemoName);

	UE_LOG(LogClass, Log, TEXT("%s"), *StartURL);

	// Game instance will handle success, failure and dialogs
	Cast<UGGameInstance>(ge)->SetIsOnline(bIsOnline);
	Cast<UGGameInstance>(ge)->HostGame(ge->GetFirstGamePlayer(), formattedGameModeName, StartURL);

	// stop the session blocking other players joining
	SetNetworkVisibility("None");
}

void AMainMenu::NotifyHostGame_Implementation()
{
	UGameInstance* ge = GetGameInstance();

	// clients only need to configure thier own players
	if (Role < ROLE_Authority)
	{
		// find what colour and actor the player is using
		// back these up, then clear the player list and configure as if we are setting up single player
		// this is similar to the way solo play is setup, so we only configure ourself and do not try to 
		// add AI players etc...

		//FString ColourName;
		//FString ActorName;

		for (int i = 0; i < PlayerList.Num(); ++i)
		{
			if (PlayerList[i].IsSlotOwner(ge->GetFirstLocalPlayerController()))
			{
				//SetPlayerInputName(ColourName, GetInputNameFromControllerId(0));
				PlayerList[i].InputName = InputNameList[2].Name; // GetInputNameFromControllerId(0); // skip none=0 and AI=1
			}
			else
			{
				PlayerList[i].InputName = NoneString;
			}
		}
	}

	CreateLocalPlayers();
	MenuWidget->RemoveFromViewport();

	UMessageMenu* MsgBox = UMessageMenu::ConstructDefault(EMessageMenuType::Progress);
	if (MsgBox)
	{
		MsgBox->SetMessage("Loading");
	}
}

void AMainMenu::JoinGame(const FBlueprintSessionResult& SearchResult)
{
	/*
	if (bSearchingForServers)
	{
		// unsafe
		return;
	}
#if WITH_EDITOR
	if (GIsEditor == true)
	{
		return;
	}
#endif
	if (SelectedItem.IsValid())
	{
		int ServerToJoin = SelectedItem->SearchResultsIndex;

		if (GEngine && GEngine->GameViewport)
		{
			GEngine->GameViewport->RemoveAllViewportWidgets();
		}

		UShooterGameInstance* const GI = Cast<UShooterGameInstance>(PlayerOwner->GetGameInstance());
		if (GI)
		{
			GI->JoinSession(PlayerOwner.Get(), ServerToJoin);
		}
	}*/

	
	UGGameInstance* ge = Cast<UGGameInstance>(::GetGameInstance());
	if (!ge->JoinSession(ge->GetFirstGamePlayer(), SearchResult.OnlineResult))
	{
		return;
	}

	// TODO: this is null because its called by something outside of main menu!
	if (MenuWidget)
	{
		MenuWidget->RemoveFromViewport();
	}

	UMessageMenu* MsgBox = UMessageMenu::ConstructDefault(EMessageMenuType::Progress);
	if (MsgBox)
	{
		MsgBox->SetMessage("Joining Game");
	}


	/*
	// Joins a remote session with the default online subsystem
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "Online|Session")
	UJoinSessionCallbackProxy* proxy = UJoinSessionCallbackProxy::JoinSession(UObject* WorldContextObject, class APlayerController* PlayerController, const FBlueprintSessionResult& SearchResult);



	UGGameInstance* const GI = Cast<UGGameInstance>(GetGameInstance());
	if (GI)
	{
		GI->JoinSession(ge->GetFirstGamePlayer(), ServerToJoin);
	}*/
}

/*
void UMainMenu::FindSessions()
{
	UGameInstance* ge = GetGameInstance();
	Cast<UGGameInstance>(ge)->FindSessions(ge->GetFirstGamePlayer(), bIsLanMatch);
}*/

void AMainMenu::GetActorNames(TArray<FString>& list)
{
	list.Empty();

	for (FActorNameStruct s : ActorNameList)
	{
		list.AddUnique(s.Name);
	}
}


void AMainMenu::GetColourNames(TArray<FString>& list)
{
	list.Empty();

	for (FColourNameStruct s : ColourNameList)
	{
		list.AddUnique(s.Name);
	}
}

void AMainMenu::GetAvailableInputNames(const FString& ColourName, TArray<FString>& list)
{
	list.Empty();

	// always allow AI and none
	list.AddUnique(NoneString);
	list.AddUnique(AIString);

	for (FInputNameStruct i : InputNameList)
	{
		bool iInUse = false;
		for (FPlayerStruct p : PlayerList)
		{
			if (p.InputName == i.Name && p.ColourName != ColourName)
			{
				iInUse = true;
				break;
			}
		}

		if (iInUse)
			continue;

		list.AddUnique(i.Name);
	}


	// add remote network players
	UWorld *CurrentWorld = GetGGameInstance()->GetWorld();
	for (FConstPlayerControllerIterator Iterator = CurrentWorld->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PC = *Iterator;
		if (!PC->IsLocalController())
		{
			AGPlayerState* PS = Cast<AGPlayerState>(PC->PlayerState);
			if (PS)
			{
				list.AddUnique(PS->PlayerName);
			}
		}
	}
}

void AMainMenu::SetPlayerInputName(const FString& ColourName, const FString& InputName)
{
	bIgnoreOnInputSelectionChangedEvent = true;

	UGameInstance* ge = GetGameInstance();
	FPlayerStruct& p = GetPlayerStruct(ColourName);
	p.InputName = InputName;
	p.SetSlotOwner(ge->GetFirstLocalPlayerController());
	p.InputCombo->AddOptionUnique(InputName);
	p.InputCombo->SetSelectedOption(InputName);

	// set user id
	UWorld *CurrentWorld = GetGGameInstance()->GetWorld();
	for (FConstPlayerControllerIterator Iterator = CurrentWorld->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PC = *Iterator;
		AGPlayerState* PS = Cast<AGPlayerState>(PC->PlayerState);
		if (PC && PS && !PC->IsLocalController() && PS->PlayerName == InputName)
		{
			// clear previous slot(s) this user may be assigned
			for (FPlayerStruct& p2 : PlayerList)
			{
				if (p2.IsSlotOwner(PC))
				{
					p2.SetSlotOwner(ge->GetFirstLocalPlayerController());
					p2.InputName = NoneString;
					p2.InputCombo->AddOptionUnique(NoneString);
					p2.InputCombo->SetSelectedOption(NoneString);
				}
			}

			p.SetSlotOwner(PC);
			break;
		}
	}

	bIgnoreOnInputSelectionChangedEvent = false;
}

void AMainMenu::SetPlayerActorName(const FString& ColourName, const FString& ActorName)
{
	FPlayerStruct& p = GetPlayerStruct(ColourName);
	p.ActorName = ActorName;
}

FPlayerStruct& AMainMenu::GetPlayerStruct(const FString& ColourName)
{
	for (FPlayerStruct& p : PlayerList)
	{
		if (p.ColourName == ColourName)
		{
			return p;
		}
	}

	FPlayerStruct p;
	p.ColourName = ColourName;
	PlayerList.Add(p);
	return GetPlayerStruct(ColourName);
}

FPlayerStruct* AMainMenu::GetPlayerStruct(FInputNameStruct* InputNameStruct)
{
	for (FPlayerStruct& p : PlayerList)
	{
		if (p.InputName == InputNameStruct->Name)
			return &p;
	}

	return NULL;
}

FInputNameStruct* AMainMenu::GetInputNameStruct(const FString& InputName)
{
	for (int i = 0; i < InputNameList.Num(); ++i)
	{
		FInputNameStruct& inputNameStruct = InputNameList[i];
		if (inputNameStruct.Name == InputName)
		{
			return &inputNameStruct;
		}
	}

	return NULL;
}

FInputNameStruct* AMainMenu::GetInputNameStruct(int32 ControllerId)
{
	/*
	for (int i = 0; i < InputNameList.Num(); ++i)
	{
		FInputNameStruct& inputNameStruct = InputNameList[i];
		if (inputNameStruct.ControllerId == ControllerId)
		{
			return &inputNameStruct;
		}
	}*/

	return NULL;
}

FActorNameStruct* AMainMenu::GetActorNameStruct(const FString& ActorName)
{
	for (FActorNameStruct& a : ActorNameList)
	{
		if (a.Name == ActorName)
		{
			return &a;
		}
	}

	return NULL;
}


FColourNameStruct* AMainMenu::GetColourNameStruct(const FString& ColourName)
{
	for (FColourNameStruct& c : ColourNameList)
	{
		if (c.Name == ColourName)
		{
			return &c;
		}
	}

	return NULL;
}

void AMainMenu::ClientRequestLeaveLobby()
{
	check(MenuWidget);
	MenuWidget->RemoveFromViewport();
	UMessageMenu* MsgBox = UMessageMenu::ConstructDefault(EMessageMenuType::OkCancelMessage);
	MsgBox->SetMessage("Disconnect from multiplayer game?");
	MsgBox->OnOk.AddDynamic(this, &AMainMenu::OnClientRequestLeaveLobby);
	MsgBox->OnCancel.AddDynamic(this, &AMainMenu::MessageBoxClosed);
}

void AMainMenu::OnClientRequestLeaveLobby(class UMessageMenu* MsgBox)
{
	//GEngine->HandleDisconnect(GetWorld(), GetWorld()->GetNetDriver());

	Cast<UGGameInstance>(GetGameInstance())->GotoState(GGameInstanceState::MainMenu);

	// reload this current level without connecting to a server
	if (FWorldContext* WorldContext = GEngine->GetWorldContextFromWorld(GetWorld()))
	{
		// Remove ?Listen parameter, if it exists
		//WorldContext->LastURL.RemoveOption(TEXT("Listen"));

		UGameplayStatics::OpenLevel(GetWorld(), *WorldContext->LastURL.Map);
	}
}

// this code reconfigures the UWorld network driver as well as setup the session
void AMainMenu::SetNetworkVisibility(const FString& NetworkVisibility)
{
	// client cannot do anything here
	if (Role < ROLE_Authority)
	{
		if (NetworkVisibility.Equals(TEXT("None"), ESearchCase::IgnoreCase))
		{
			ClientRequestLeaveLobby();
		}
		return;
	}


	AGGameSession* const GameSession = GetGGameInstance()->GetGameSession();

	if (NetworkVisibility.Equals(TEXT("None"), ESearchCase::IgnoreCase))
	{
		bIsLanMatch = false;
		bIsOnline = false;
		GameSession->DestroySession(GetGGameInstance()->GetFirstGamePlayer()->GetPreferredUniqueNetId(), GameSessionName);
	}

	if (NetworkVisibility.Equals(TEXT("LAN"), ESearchCase::IgnoreCase))
	{
		bIsLanMatch = true;
		bIsOnline = true;
		GameSession->UpdateSession(GetGGameInstance()->GetFirstGamePlayer()->GetPreferredUniqueNetId(), GameSessionName, "GameType", "MapName", bIsLanMatch, true, AGGameSession::DEFAULT_NUM_PLAYERS);
		FURL InURL(NULL, TEXT("unreal:/Game/Map/Menu?Listen?bIsLanMatch"), TRAVEL_Absolute);
		if (!GetGGameInstance()->GetWorld()->GetNetDriver())
		{
			GetGGameInstance()->GetWorld()->Listen(InURL);
		}
		else
		{
			FString Error;
			GetGGameInstance()->GetWorld()->GetNetDriver()->InitListen(GetGGameInstance()->GetWorld(), InURL, false, Error);
		}
	}

	if (NetworkVisibility.Equals(TEXT("Internet"), ESearchCase::IgnoreCase))
	{
		bIsLanMatch = false;
		bIsOnline = true;
		GameSession->UpdateSession(GetGGameInstance()->GetFirstGamePlayer()->GetPreferredUniqueNetId(), GameSessionName, "GameType", "MapName", bIsLanMatch, true, AGGameSession::DEFAULT_NUM_PLAYERS);
		FURL InURL(NULL, TEXT("unreal:/Game/Map/Menu?Listen"), TRAVEL_Absolute);
		if (!GetGGameInstance()->GetWorld()->GetNetDriver())
		{
			GetGGameInstance()->GetWorld()->Listen(InURL);
		}
		else
		{
			FString Error;
			GetGGameInstance()->GetWorld()->GetNetDriver()->InitListen(GetGGameInstance()->GetWorld(), InURL, false, Error);
		}
	}
}

UGGameInstance* AMainMenu::GetGGameInstance()
{
	UGameInstance* ge = GetGameInstance();
	return Cast<UGGameInstance>(ge);
}

void AMainMenu::OnClientSyncComboBoxSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	// find the index for the combobox and set the value to replicate
	for (int i = 0; i < ClientSyncComboBoxList.Num(); ++i)
	{
		if (ClientSyncComboBoxList[i] == ComboBox)
		{
			ClientSyncComboBoxValueList[i] = SelectedItem;
		}
	}
}

void AMainMenu::OnRep_ClientSyncComboBoxValueList()
{
	// client now simply sets the option in the combobox
	for (int i = 0; i < ClientSyncComboBoxList.Num(); ++i)
	{
		ClientSyncComboBoxList[i]->SetSelectedOption(ClientSyncComboBoxValueList[i]);
	}
}

void AMainMenu::OnRep_PlayerList()
{
	UGameInstance* ge = GetGameInstance();

	// this is only called on clients
	// server has changed a setting, update comboboxes to reflect change
	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		bool currentSlotThisClients = PlayerList[i].IsSlotOwner(ge->GetFirstLocalPlayerController());
		/*
		IOnlineIdentityPtr IdentityPtr = Online::GetIdentityInterface();
		if (IdentityPtr.IsValid())
		{
			currentSlotThisClients = (PlayerList[i].PlayerUniqueId == IdentityPtr->GetUniquePlayerId(0));
		}*/

		if (PlayerList[i].ActorCombo)
		{
			PlayerList[i].ActorCombo->SetSelectedOption(PlayerList[i].ActorName);
			PlayerList[i].ActorCombo->SetIsEnabled(currentSlotThisClients);
		}

		if (PlayerList[i].InputCombo)
		{
			// on clients, we simply want to show the player who owns the slot,
			// OR we want to show if the slot is AI or None
			// as we don't care what inputs the server is using

			FString DisplayString = PlayerList[i].InputName;

			if (PlayerList[i].IsOccupiedByPlayer())
			{
				DisplayString = PlayerList[i].PlayerName;
			}

			bIgnoreOnInputSelectionChangedEvent = true;
			PlayerList[i].InputCombo->ClearOptions();
			PlayerList[i].InputCombo->AddOptionUnique(DisplayString);
			PlayerList[i].InputCombo->SetSelectedOption(DisplayString);
			bIgnoreOnInputSelectionChangedEvent = false;
		}		
	}
}

void AMainMenu::OnActorSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		if (PlayerList[i].ActorCombo == ComboBox)
		{
			SetPlayerActorName(PlayerList[i].ColourName, SelectedItem);
			break;
		}
	}
}

void AMainMenu::OnInputOpening(class UGComboBoxString* ComboBox)
{
	UGameInstance* ge = GetGameInstance();

	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		if (PlayerList[i].InputCombo == ComboBox)
		{
			FString Selected = ComboBox->GetSelectedOption();
			TArray<FString> InputNamesList;

			bool isClient = (Role < ROLE_Authority);

			bool currentSlotThisClients = PlayerList[i].IsSlotOwner(ge->GetFirstLocalPlayerController()); 
			/*false;
			IOnlineIdentityPtr IdentityPtr = Online::GetIdentityInterface();
			if (IdentityPtr.IsValid())
			{
				currentSlotThisClients = (PlayerList[i].UserId == IdentityPtr->GetUniquePlayerId(0));
			}
			*/

			// client cannot do anything except request move to slots they do not have control over
			if (isClient)
			{
				if (!currentSlotThisClients)
				{
					InputNamesList.Add(Selected);
					InputNamesList.Add(MoveSlotString);
				}
				else
				{
					// client can do nothing, they automatically get right keyboard and gamecontroller as if they hit instant action
				}
			}
			else
			{
				GetAvailableInputNames(PlayerList[i].ColourName, InputNamesList);
			}

			bIgnoreOnInputSelectionChangedEvent = true;
			ComboBox->SetOptions(InputNamesList);
			ComboBox->SetSelectedOption(Selected);
			bIgnoreOnInputSelectionChangedEvent = false;
			break;
		}
	}
}

void AMainMenu::OnInputSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	if (bIgnoreOnInputSelectionChangedEvent)
	{
		return;
	}

	for (int i = 0; i < PlayerList.Num(); ++i)
	{
		if (PlayerList[i].InputCombo == ComboBox)
		{
			bool isClient = (Role < ROLE_Authority);

			// client must make a request on the server to be able to change slot
			if (isClient)
			{
				if (SelectedItem.Equals(MoveSlotString))
				{
					UGameInstance* ge = GetGameInstance();
					AGPlayerController* PC = Cast<AGPlayerController>(ge->GetFirstLocalPlayerController());
					PC->Server_ClientRequestChangeSlot(this, PlayerList[i].ColourName);
				}
			}
			else
			{
				SetPlayerInputName(PlayerList[i].ColourName, SelectedItem);
			}
			break;
		}
	}
}

/*
UNetConnection* AMainMenu::GetNetConnection() const
{
	if (NetConnection)
	{
		return NetConnection;
	}

	return Super::GetNetConnection();
}*/

/*
bool AMainMenu::ServerClientRequestChangeSlot_Validate(const FUniqueNetIdRepl& UserId, const FString& ColourName)
{
	return true;
}

void AMainMenu::ServerClientRequestChangeSlot_Implementation(const FUniqueNetIdRepl& UserId, const FString& ColourName)
{
}*/

void AMainMenu::ClientRequestChangeSlot(APlayerController* PC, const FString& ColourName)
{
	FPlayerStruct& PlayerStruct = GetPlayerStruct(ColourName);
	if (PlayerStruct.InputName == AIString || PlayerStruct.InputName == NoneString || PlayerStruct.InputName.IsEmpty())
	{
		AGPlayerState* PS = Cast<AGPlayerState>(PC->PlayerState);
		if (PS)
		{
			SetPlayerInputName(ColourName, PS->PlayerName);
		}
	}	
}

void AMainMenu::OnActorChannelOpen(class FInBunch& InBunch, class UNetConnection* Connection)
{
	//NetConnection = Connection;
}

void AMainMenu::OnSerializeNewActor(class FOutBunch& OutBunch)
{
	int nothng = 0;
	++nothng;
}
