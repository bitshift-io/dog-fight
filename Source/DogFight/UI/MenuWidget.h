// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

class AMenuActor;

/*
 Base class for UMG menu blueprints, provides functionality to allow the UMG menu to communicate with the UMG Actor
 which contains all the c++ code to drive the menu system
*/
UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

	//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnConstructEventMenu, class UGComboBoxString*, ComboBox);

public:

	UMenuWidget(const FObjectInitializer& ObjectInitializer);
	virtual ~UMenuWidget();

	virtual void PostInitProperties();

	/** sets owning player controller */
	void Construct(ULocalPlayer* PlayerOwner, AMenuActor* MenuActor);
	
	virtual TSharedRef<SWidget> RebuildWidget() override;
	virtual void OnWidgetRebuilt() override;

	void SetWidgetToFocus();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCosmetic, Category = "User Interface", meta = (Keywords = "SetWidgetToFocus"))
	void OnSetWidgetToFocus();

	/**
	* Called before the underlying slate widget is constructed.  Depending on how the slate object is used
	* this event may be called multiple times due to adding and removing from the hierarchy.
	* This occurs before any other child widgets are constructed.
	* MenuActor and PlayerOwner should be valid at this point.
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCosmetic, Category = "User Interface", meta = (Keywords = "PreConstruct"))
	void PreConstruct();

	/** Owning player controller */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULocalPlayer* PlayerOwner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AMenuActor* MenuActor;

	/** Called when the combobox is constructed */
	//UPROPERTY(BlueprintAssignable, Category = "Events")
	//FOnConstructEventCombo OnPreConstruct;
};

