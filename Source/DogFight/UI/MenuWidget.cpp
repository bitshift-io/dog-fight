// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "MenuWidget.h"

#pragma optimize( "", OPTIMISATION )

UMenuWidget::UMenuWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerOwner = NULL;
	MenuActor = NULL;
}

UMenuWidget::~UMenuWidget()
{
	int nothing = 0;
	++nothing;
}

void UMenuWidget::PostInitProperties()
{
	Super::PostInitProperties();
}

void UMenuWidget::Construct(ULocalPlayer* _PlayerOwner, AMenuActor* _MenuActor)
{
	PlayerOwner = _PlayerOwner;
	MenuActor = _MenuActor;
	//SetFlags(RF_RootSet); // stop garbage collection failing!
}

TSharedRef<SWidget> UMenuWidget::RebuildWidget()
{
	PreConstruct();
	return Super::RebuildWidget();
}

void UMenuWidget::OnWidgetRebuilt()
{
	PreConstruct();
	Super::OnWidgetRebuilt();
}

void UMenuWidget::SetWidgetToFocus()
{
	AGPlayerController* const PCOwner = Cast<AGPlayerController>(GetOwningPlayer());
	FInputModeUIOnly Mode;
	Mode.SetWidgetToFocus(GetCachedWidget());
	if (PCOwner)
	{
		PCOwner->SetInputMode(Mode);
	}

	// allow UMG to have custom widget focus
	OnSetWidgetToFocus();
}