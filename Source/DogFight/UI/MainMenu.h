// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "OnlineBlueprintCallProxyBase.h"
#include "FindSessionsCallbackProxy.h"
#include "Widgets/MapDesc.h"
#include "MenuActor.h"
#include "Widgets/GComboBoxString.h"
#include "Widgets/ScreenResolutionComboBox.h"
#include "SharedPointer.h"
#include "MainMenu.generated.h"

class AGActor;
class AGAIController;
class GComboBoxString;
class AMainMenu;
class UNetConnection;

USTRUCT()
struct FActorNameStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AGActor>		ActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AGAIController> AIControllerClass;
};

USTRUCT()
struct FColourNameStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor				Colour;
};

enum DeviceType
{
	XInputGamepad,	// must be first as XInput devices occupy 0-4 controller index
	Keyboard,
	SDL2Gamepad,
	AI,
	None,
};

USTRUCT()
struct FInputNameStruct
{
	GENERATED_USTRUCT_BODY()

	//FInputNameStruct(const FString& Name, DeviceType DeviceType, int32 Index);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						Name;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//int32						ControllerId;

	/** If AlternateControllerId is unmapped when creating local players, then this alternate id will be mapped to ControllerId  to allow
	multiple inputs for a single player **/
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//int32						AlternateControllerId;

	DeviceType	DeviceType;
	int32		Index;
	int32		SDL2DeviceIndex;

	//FActorNameStruct* ActorNameStruct;
	//FColourNameStruct* ColourNameStruct;

	// sort by device type, then by index
	bool operator<(const FInputNameStruct& Other) const;
};

USTRUCT()
struct FPlayerStruct
{
	GENERATED_USTRUCT_BODY()

	void ResolveWidgets(AMainMenu* MainMenu);
	void SetSlotOwner(APlayerController* PC);
	bool IsSlotOwner(APlayerController* PC);
	bool IsOccupiedByPlayer();

	bool IsLocalPlayer();
	bool IsAIPlayer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						ActorName;

	/** This is really the players name **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						ColourName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						InputName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						InputComboName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString						ActorComboName;

	// which player is assigned to this slot
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUniqueNetIdRepl UniquePlayerId;

	// name of player who owns this slot
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString			PlayerName;

	UGComboBoxString*			InputCombo;
	UGComboBoxString*			ActorCombo;

	AMainMenu*	MainMenu;

	// sort for assigning controller id's
	bool operator<(const FPlayerStruct& Other) const;
};

UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API AMainMenu : public AMenuActor //UUserWidget
{
	GENERATED_BODY()
public:

	//static UMainMenu* instance();

	AMainMenu(const FObjectInitializer& ObjectInitializer);
	~AMainMenu();

	/** debug joystick stuff **/
	UFUNCTION(exec, BlueprintImplementableEvent)
	void GamepadConfigureSDL();

	UFUNCTION(exec, BlueprintImplementableEvent)
	void GamepadConfigureDefault();

	UFUNCTION(exec, BlueprintImplementableEvent)
	void GamepadDebug();
	
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
	virtual void PostInitializeComponents();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	UGGameInstance* GetGGameInstance();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void SetNetworkVisibility(const FString& NetworkVisibility);

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void SetGamepadCount(int32 Count);

	/** Call this to clear any player settings **/
	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void ResetPlayerList();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void InstantAction();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void HostGame();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void JoinGame(const FBlueprintSessionResult& SearchResult);

	/** client hit the back button and wants to leave the lobby **/
	void ClientRequestLeaveLobby();

	UFUNCTION()
	void OnClientRequestLeaveLobby(class UMessageMenu* MsgBox);

	UFUNCTION()
	void MessageBoxClosed(class UMessageMenu* MsgBox);

	void ConfigureDevices();

	/** Initiates the session searching */
	//UFUNCTION(BlueprintCallable, Category = "MainMenu")
	//void FindSessions();

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void SetPlayerInputName(const FString& ColourName, const FString& InputName);

	UFUNCTION(BlueprintCallable, Category = "MainMenu")
	void SetPlayerActorName(const FString& ColourName, const FString& ActorName);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "MainMenu")
	void GetActorNames(TArray<FString>& list);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "MainMenu")
	void GetColourNames(TArray<FString>& list);

	/** Get the list of unused input names and include the player's current selected input name **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "MainMenu")
	void GetAvailableInputNames(const FString& ColourName, TArray<FString>& list);

	void AddInputName(const FString& Name, DeviceType DeviceType, int32 Index, int32 SDL2DeviceIndex);
	//void AddInputName(const FString& name, uint32 controllerId, uint32 alternateControllerId);

	bool CheckCanHostGame();

	void CreateLocalPlayers();
	void RemoveLocalPlayers();
	

	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = "MainMenu")
	//FString GetInputNameFromControllerId(int32 ControllerId);

	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = "MainMenu")
	//int32 GetControllerIdFromInputName(const FString& InputName);

	/** Find the Player struct with the given name **/
	FPlayerStruct& GetPlayerStruct(const FString& ColourName);

	/** Find the Player struct assigned the Input Name Struct **/
	FPlayerStruct* GetPlayerStruct(FInputNameStruct* InputNameStruct);

	/** Find the InputName struct with the given name **/
	FInputNameStruct* GetInputNameStruct(const FString& InputName);

	/** Find the InputName struct with the given controllerId **/
	FInputNameStruct* GetInputNameStruct(int32 ControllerId);
	
	/** Find the ActorName struct with the given name **/
	FActorNameStruct* GetActorNameStruct(const FString& ActorName);

	/** Find the ColourName struct with the given name **/
	FColourNameStruct* GetColourNameStruct(const FString& ColourName);

	UFUNCTION()
	void OnActorSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);

	UFUNCTION()
	void OnInputOpening(class UGComboBoxString* ComboBox);

	UFUNCTION()
	void OnInputSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);

	/** replicate player list */
	UFUNCTION()
	void OnRep_PlayerList();

	/* This allows the client to RPC back to the server - avoids the "No Owning connection for actor" issue */
	//virtual UNetConnection* GetNetConnection() const override;

	/** client wants to change slot */
	/*
	UFUNCTION(reliable, server, WithValidation)
	void ServerClientRequestChangeSlot(const FUniqueNetIdRepl& UserId, const FString& ColourName);

	virtual bool ServerClientRequestChangeSlot_Validate(const FUniqueNetIdRepl& UserId, const FString& ColourName);
	virtual void ServerClientRequestChangeSlot_Implementation(const FUniqueNetIdRepl& UserId, const FString& ColourName);
	*/


	UFUNCTION(reliable, NetMulticast)
	void NotifyHostGame();
	virtual void NotifyHostGame_Implementation();

	void ClientRequestChangeSlot(APlayerController* PC, const FString& ColourName);

	/**
	* Allows for a specific response from the actor when the actor channel is opened (client side)
	* @param InBunch Bunch received at time of open
	* @param Connection the connection associated with this actor
	*/
	virtual void OnActorChannelOpen(class FInBunch& InBunch, class UNetConnection* Connection);

	/**
	* SerializeNewActor has just been called on the actor before network replication (server side)
	* @param OutBunch Bunch containing serialized contents of actor prior to replication
	*/
	virtual void OnSerializeNewActor(class FOutBunch& OutBunch);

	/** Map to load **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMapDesc*	MapDesc;

	/** Name of game mode to play **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString GameModeName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsLanMatch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsOnline;

	/** Name of file record too? **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString DemoName;

	/** Player setup data **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_PlayerList)
	TArray<FPlayerStruct>	PlayerList;

	/** Colour data **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FColourNameStruct>	ColourNameList;

	/** Actor data **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FActorNameStruct>	ActorNameList;

	/** Input data **/
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FInputNameStruct>	InputNameList;
	
	/** How many keyboard devices are there **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumKeyboardDevices;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 GamepadCount;

	//UNetConnection* NetConnection;

	bool bIgnoreOnInputSelectionChangedEvent;

	/** replicate combo box values to clients */
	UFUNCTION()
	void OnRep_ClientSyncComboBoxValueList();

	UFUNCTION()
	void OnClientSyncComboBoxSelectionChanged(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);

	/** List of map names to by synced across the network **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString>			ClientSyncComboBoxNameList;

	TArray<UGComboBoxString*>	ClientSyncComboBoxList;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_ClientSyncComboBoxValueList)
	TArray<FString>	ClientSyncComboBoxValueList;

	TArray<int32>	UE4DeviceList;		// devices UE4 will handle
	TArray<int32>	SDL2DeviceList;		// devices SDL2 will need to handle
};

