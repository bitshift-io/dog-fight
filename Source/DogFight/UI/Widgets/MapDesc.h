// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MapDesc.generated.h"

/*
	Descriptor for describing the map in terms of what can be displayed about the map
	without having to load it up
*/
UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API UMapDesc : public UObject
{
	GENERATED_BODY()
public:

	UMapDesc(const FObjectInitializer& ObjectInitializer);
	~UMapDesc();

	/* Populated by the game */
	FString LongPackageName;

	/* Name to display for the user */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapDesc)
	FString DisplayName;

	/* Image of the map */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapDesc)
	//UMaterial* DisplayImage;

	/* Image of the map */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapDesc)
	UTexture2D* DisplayImage;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Map Desc")
	static void GetMapDescList(TArray<UMapDesc*>& Maps);
};

