// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GComboBoxString.h"
#include "WindowModeComboBox.generated.h"

/**
* The combobox allows you set window mode
*/
UCLASS(meta = (DisplayName = "WindowModeComboBox"), ClassGroup = UserInterface)
class DOGFIGHT_API UWindowModeComboBox : public UGComboBoxString
{
	GENERATED_UCLASS_BODY()

protected:

	UFUNCTION()
	virtual void SetWindowMode(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);

	virtual void HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType);

	// UWidget interface
	virtual TSharedRef<SWidget> RebuildWidget() override;


	FString MapEnumToString(EWindowMode::Type Mode);
	EWindowMode::Type MapStringToEnum(FString& Mode);

	TMap<EWindowMode::Type, FString> WindowModeMap;
};
