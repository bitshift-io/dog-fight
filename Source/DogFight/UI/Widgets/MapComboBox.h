// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GComboBoxString.h"
#include "MapComboBox.generated.h"

class UImage;
class UMapDesc;

/**
* The combobox populated with maps
*/
UCLASS(meta = (DisplayName = "MapComboBox"), ClassGroup = UserInterface)
class DOGFIGHT_API UMapComboBox : public UGComboBoxString
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "MapComboBox")
	UMapDesc* GetSelectedMap();

	UFUNCTION()
	virtual void SetSelectMap(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);
	
	virtual void HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content", AdvancedDisplay)
	FString ImageWidgetName;

	/**
	* Add an entry to random map, if blank, will not be used
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content", AdvancedDisplay)
	FString RandomMapDisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content", AdvancedDisplay)
	UTexture2D*	UnknownMapDisplayImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content", AdvancedDisplay)
	UTexture2D*	RandomMapDisplayImage;

protected:

	bool IsRandomMapSelected();

	// UWidget interface
	virtual TSharedRef<SWidget> RebuildWidget() override;

	int32 SelectedIndex;

	UImage*	ImageWidget;

	// UProperty stops garbage collection
	UPROPERTY(BlueprintReadOnly, Category = "Content", AdvancedDisplay)
	TArray<UMapDesc*> MapDescList;
};
