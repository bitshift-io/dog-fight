// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "GComboBoxString.h"
#include "UMGStyle.h"

#pragma optimize("", OPTIMISATION)

#define LOCTEXT_NAMESPACE "UMG"

/////////////////////////////////////////////////////
// UGComboBoxString

UGComboBoxString::UGComboBoxString(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SComboBox< TSharedPtr<FString> >::FArguments SlateDefaults;
	WidgetStyle = *SlateDefaults._ComboBoxStyle;

	ContentPadding = FMargin(4.0, 2.0);
	MaxListHeight = 450.0f;
	HasDownArrow = true;

	static ConstructorHelpers::FObjectFinder<UFont> RobotoFontObj(TEXT("/Engine/EngineFonts/Roboto"));
	Font = FSlateFontInfo(RobotoFontObj.Object, 24, FName("Bold"));
}

void UGComboBoxString::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyComboBox.Reset();
	ComoboBoxContent.Reset();
}

TSharedRef<SWidget> UGComboBoxString::RebuildWidget()
{
	for (FString& DefaultOption : DefaultOptions)
	{
		AddOption(DefaultOption);
	}

	int32 InitialIndex = FindOptionIndex(SelectedOption);
	if (InitialIndex != -1)
	{
		CurrentOptionPtr = Options[InitialIndex];
	}

	MyComboBox =
		SNew(SComboBox< TSharedPtr<FString> >)
		.ComboBoxStyle(&WidgetStyle)
		.OptionsSource(&Options)
		.InitiallySelectedItem(CurrentOptionPtr)
		.ContentPadding(ContentPadding)
		.MaxListHeight(MaxListHeight)
		.HasDownArrow(HasDownArrow)
		.OnGenerateWidget(BIND_UOBJECT_DELEGATE(SComboBox< TSharedPtr<FString> >::FOnGenerateWidget, HandleGenerateWidget))
		.OnSelectionChanged(BIND_UOBJECT_DELEGATE(SComboBox< TSharedPtr<FString> >::FOnSelectionChanged, HandleSelectionChanged))
		.OnComboBoxOpening(BIND_UOBJECT_DELEGATE(FOnComboBoxOpening, HandleOpening))
		[
			SAssignNew(ComoboBoxContent, SBox)
		];

	if (InitialIndex != -1)
	{
		// Generate the widget for the initially selected widget if needed
		ComoboBoxContent->SetContent(HandleGenerateWidget(CurrentOptionPtr));
	}

	OnConstruct.Broadcast(this);

	return MyComboBox.ToSharedRef();
}

void UGComboBoxString::SetOptions(const TArray<FString>& OptionList)
{
	ClearOptions();

	for (int32 i = 0; i < OptionList.Num(); ++i)
	{
		Options.Add(MakeShareable(new FString(OptionList[i])));
	}

	RefreshOptions();
}

void UGComboBoxString::AddOption(const FString& Option)
{
	Options.Add(MakeShareable(new FString(Option)));

	RefreshOptions();
}

void UGComboBoxString::AddOptionUnique(const FString& Option)
{
	int32 OptionIndex = FindOptionIndex(Option);
	if (OptionIndex == -1)
	{
		AddOption(Option);
	}
}

bool UGComboBoxString::RemoveOption(const FString& Option)
{
	int32 OptionIndex = FindOptionIndex(Option);

	if (OptionIndex != -1)
	{
		if (Options[OptionIndex] == CurrentOptionPtr)
		{
			ClearSelection();
		}

		Options.RemoveAt(OptionIndex);

		RefreshOptions();

		return true;
	}

	return false;
}

int32 UGComboBoxString::FindOptionIndex(const FString& Option) const
{
	for (int32 OptionIndex = 0; OptionIndex < Options.Num(); OptionIndex++)
	{
		const TSharedPtr<FString>& OptionAtIndex = Options[OptionIndex];

		if ((*OptionAtIndex) == Option)
		{
			return OptionIndex;
		}
	}

	return -1;
}

FString UGComboBoxString::GetOptionAtIndex(int32 Index) const
{
	if (Index >= 0 && Index < Options.Num())
	{
		return *(Options[Index]);
	}
	return FString();
}

void UGComboBoxString::ClearOptions()
{
	ClearSelection();

	Options.Empty();

	if (MyComboBox.IsValid())
	{
		MyComboBox->RefreshOptions();
	}
}

void UGComboBoxString::ClearSelection()
{
	CurrentOptionPtr.Reset();

	if (MyComboBox.IsValid())
	{
		MyComboBox->ClearSelection();
	}

	if (ComoboBoxContent.IsValid())
	{
		ComoboBoxContent->SetContent(SNullWidget::NullWidget);
	}
}

void UGComboBoxString::RefreshOptions()
{
	if (MyComboBox.IsValid())
	{
		MyComboBox->RefreshOptions();
	}
}

void UGComboBoxString::SetSelectedOption(FString Option)
{
	int32 InitialIndex = FindOptionIndex(Option);
	if (InitialIndex != -1)
	{
		CurrentOptionPtr = Options[InitialIndex];

		if (ComoboBoxContent.IsValid())
		{
			MyComboBox->SetSelectedItem(CurrentOptionPtr);
			ComoboBoxContent->SetContent(HandleGenerateWidget(CurrentOptionPtr));
		}
		else
		{
			SelectedOption = Option;
		}
	}
}

FString UGComboBoxString::GetSelectedOption() const
{
	if (CurrentOptionPtr.IsValid())
	{
		return *CurrentOptionPtr;
	}
	return FString();
}

int32 UGComboBoxString::GetOptionCount() const
{
	return Options.Num();
}

TSharedRef<SWidget> UGComboBoxString::HandleGenerateWidget(TSharedPtr<FString> Item) const
{
	FString StringItem = Item.IsValid() ? *Item : FString();

	// Call the user's delegate to see if they want to generate a custom widget bound to the data source.
	if (!IsDesignTime() && OnGenerateWidgetEvent.IsBound())
	{
		UWidget* Widget = OnGenerateWidgetEvent.Execute(StringItem);
		if (Widget != NULL)
		{
			return Widget->TakeWidget();
		}
	}

	// If a row wasn't generated just create the default one, a simple text block of the item's name.
	return SNew(STextBlock).Text(FText::FromString(StringItem))
		.Font(Font);
}

void UGComboBoxString::HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType)
{
	CurrentOptionPtr = Item;

	if (!IsDesignTime())
	{
		FString option = Item.IsValid() ? *Item : FString();
		OnSelectionChanged.Broadcast(this, option, FindOptionIndex(option), SelectionType);
	}

	// When the selection changes we always generate another widget to represent the content area of the comobox.
	ComoboBoxContent->SetContent(HandleGenerateWidget(Item));
}

void UGComboBoxString::HandleOpening()
{
	OnOpening.Broadcast(this);
}

#if WITH_EDITOR

const FSlateBrush* UGComboBoxString::GetEditorIcon()
{
	return FUMGStyle::Get().GetBrush("Widget.ComboBox");
}

const FText UGComboBoxString::GetPaletteCategory()
{
	return LOCTEXT("Game", "Game");
}

#endif

/////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE
