#include "DogFight.h"
#include "WindowModeComboBox.h"
#include "UI/BPFunctionLibrary.h"

#pragma optimize("", OPTIMISATION)

#define LOCTEXT_NAMESPACE "UMG"

UWindowModeComboBox::UWindowModeComboBox(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	WindowModeMap.Add(EWindowMode::Fullscreen, "Fullscreen");
	WindowModeMap.Add(EWindowMode::WindowedFullscreen, "Windowed Fullscreen");
	WindowModeMap.Add(EWindowMode::Windowed, "Windowed");
}

void UWindowModeComboBox::SetWindowMode(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, *SelectedItem);
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *SelectedItem);

	/*
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	UserSettings->SetFullscreenMode(MapStringToEnum(SelectedItem));
	UserSettings->ApplySettings(false);
	*/

	FString currentRes;
	UBPFunctionLibrary::GetScreenResolution(currentRes);

	FString WindowModeSuffix;
	switch (MapStringToEnum(SelectedItem))
	{
		case EWindowMode::Windowed:
		{
			WindowModeSuffix = TEXT("w");
		} break;
		case EWindowMode::WindowedMirror:
		{
			WindowModeSuffix = TEXT("wm");
		} break;
		case EWindowMode::WindowedFullscreen:
		{
			WindowModeSuffix = TEXT("wf");
		} break;
		case EWindowMode::Fullscreen:
		{
			WindowModeSuffix = TEXT("f");
		} break;
	}

	// using gamesettings doesnt work properly, so we just enter it as a console command that works great!
	FString Cmd = FString::Printf(TEXT("r.SetRes %s%s"), *currentRes, *WindowModeSuffix);
	APlayerController* PController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PController)
	{
		PController->ConsoleCommand(*Cmd, true);
	}


	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	UserSettings->SetFullscreenMode(MapStringToEnum(SelectedItem));
	//UserSettings->ApplySettings(false);
	UserSettings->SaveSettings();
}

void UWindowModeComboBox::HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType)
{
	// SetSelect map needs to not occur with a delegate as order is important
	if (!IsDesignTime())
	{
		FString option = Item.IsValid() ? *Item : FString();
		SetWindowMode(this, option, FindOptionIndex(option), SelectionType);
	}

	Super::HandleSelectionChanged(Item, SelectionType);
}

TSharedRef<SWidget> UWindowModeComboBox::RebuildWidget()
{
	TSharedRef<SWidget> result = Super::RebuildWidget();

	TArray<FString> modeList;
	for (auto& Elem : WindowModeMap)
	{
		modeList.Add(Elem.Value);
	}
	
	UGGameUserSettings* UserSettings = CastChecked<UGGameUserSettings>(GEngine->GetGameUserSettings());
	FString currentMode = MapEnumToString(UserSettings->GetFullscreenMode());

	SetOptions(modeList);
	SetSelectedOption(currentMode);
	
	return result;
}

FString UWindowModeComboBox::MapEnumToString(EWindowMode::Type Mode)
{
	return WindowModeMap[Mode];
}

EWindowMode::Type UWindowModeComboBox::MapStringToEnum(FString& Mode)
{
	for (auto& Elem : WindowModeMap)
	{
		if (Elem.Value == Mode)
		{
			return Elem.Key;
		}
	}

	// error
	return EWindowMode::NumWindowModes;
}

#undef LOCTEXT_NAMESPACE