#include "DogFight.h"
#include "MapComboBox.h"
#include "UI/BPFunctionLibrary.h"
#include "Image.h"

#pragma optimize("", OPTIMISATION)

#define LOCTEXT_NAMESPACE "UMG"



UMapComboBox::UMapComboBox(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	RandomMapDisplayName = "Random";
	SelectedIndex = 0;
}

void UMapComboBox::HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType)
{
	// SetSelect map needs to not occur with a delegate as order is important
	if (!IsDesignTime())
	{
		FString option = Item.IsValid() ? *Item : FString();
		SetSelectMap(this, option, FindOptionIndex(option), SelectionType);
	}

	Super::HandleSelectionChanged(Item, SelectionType);
}

void UMapComboBox::SetSelectMap(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, *SelectedItem);
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *SelectedItem);
	SelectedIndex = Index;

	UMapDesc* SelectedMap = GetSelectedMap();
	if (ImageWidget)
	{
		if (IsRandomMapSelected())
		{
			ImageWidget->SetBrushFromTexture(RandomMapDisplayImage);
		}
		else if (SelectedMap)
		{
			ImageWidget->SetBrushFromTexture(SelectedMap->DisplayImage);
		}
		else
		{
			ImageWidget->SetBrushFromTexture(UnknownMapDisplayImage);
		}
	}
}

TSharedRef<SWidget> UMapComboBox::RebuildWidget()
{
	TSharedRef<SWidget> result = Super::RebuildWidget();

	//OnSelectionChanged.AddDynamic(this, &UMapComboBox::SetSelectMap);

	// get parent root, then search for the image widget
	UPanelWidget* Parent = GetParent();
	if (Parent)
	{
		while (Parent->GetParent())
		{
			Parent = Parent->GetParent();
		}
		UWidget* Widget = NULL;
		UBPFunctionLibrary::FindFirstChildWidgetByObjectName(Parent, ImageWidgetName, Widget);
		ImageWidget = Cast<UImage>(Widget);
	}

	TArray<FString> Options;

	if (!RandomMapDisplayName.IsEmpty())
	{
		Options.Add(RandomMapDisplayName);
	}

	MapDescList.Empty();
	UMapDesc::GetMapDescList(MapDescList);
	for (int i = 0; i < MapDescList.Num(); ++i)
	{
		Options.Add(MapDescList[i]->DisplayName);
	}

	SetOptions(Options);

	if (!RandomMapDisplayName.IsEmpty())
	{
		SetSelectedOption(RandomMapDisplayName);
	}
	else if (MapDescList.Num())
	{
		SetSelectedOption(MapDescList[0]->DisplayName);
	}

	return result;
}

bool UMapComboBox::IsRandomMapSelected()
{
	if (!RandomMapDisplayName.IsEmpty() && SelectedIndex <= 0)
	{
		return true;
	}

	return false;
}

UMapDesc* UMapComboBox::GetSelectedMap()
{
	if (MapDescList.Num() <= 0)
	{
		return NULL;
	}

	int32 Idx = SelectedIndex;
	if (!RandomMapDisplayName.IsEmpty())
	{
		--Idx;
	}

	// pick a random map is the first entry
	if (IsRandomMapSelected())
	{
		Idx = FMath::RandRange(0, MapDescList.Num() - 1);
	}

	return MapDescList[Idx];
}

#undef LOCTEXT_NAMESPACE