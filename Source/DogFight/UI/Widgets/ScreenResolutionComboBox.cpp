#include "DogFight.h"
#include "ScreenResolutionComboBox.h"
#include "UI/BPFunctionLibrary.h"

#pragma optimize("", OPTIMISATION)

#define LOCTEXT_NAMESPACE "UMG"

UScreenResolutionComboBox::UScreenResolutionComboBox(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UScreenResolutionComboBox::SetScreenRes(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, *SelectedItem);
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *SelectedItem);

	UBPFunctionLibrary::SetScreenResolution(SelectedItem);
}

void UScreenResolutionComboBox::HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType)
{
	// SetSelect map needs to not occur with a delegate as order is important
	if (!IsDesignTime())
	{
		FString option = Item.IsValid() ? *Item : FString();
		SetScreenRes(this, option, FindOptionIndex(option), SelectionType);
	}

	Super::HandleSelectionChanged(Item, SelectionType);
}

TSharedRef<SWidget> UScreenResolutionComboBox::RebuildWidget()
{
	TSharedRef<SWidget> result = Super::RebuildWidget();

	TArray<FString> resList;
	UBPFunctionLibrary::GetScreenResolutions(resList);

	FString currentRes;
	UBPFunctionLibrary::GetScreenResolution(currentRes);
	resList.AddUnique(currentRes);

	SetOptions(resList);
	SetSelectedOption(currentRes);

	//UE_LOG(LogTemp, Warning, TEXT("TESTING!"));
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, "TESTING!");

	return result;
}

#undef LOCTEXT_NAMESPACE