// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GComboBoxString.h"
#include "ScreenResolutionComboBox.generated.h"

/**
* The combobox allows you set resolution
*/
UCLASS(meta = (DisplayName = "ScreenResolutionComboBox"), ClassGroup = UserInterface)
class DOGFIGHT_API UScreenResolutionComboBox : public UGComboBoxString
{
	GENERATED_UCLASS_BODY()

protected:

	UFUNCTION()
	virtual void SetScreenRes(class UGComboBoxString* ComboBox, FString SelectedItem, int32 Index, ESelectInfo::Type SelectionType);

	virtual void HandleSelectionChanged(TSharedPtr<FString> Item, ESelectInfo::Type SelectionType);

	// UWidget interface
	virtual TSharedRef<SWidget> RebuildWidget() override;
};
