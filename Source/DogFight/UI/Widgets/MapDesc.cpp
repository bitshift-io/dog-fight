// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "UObjectGlobals.h"
#include "MapDesc.h"
#include "IPlatformFilePak.h"

#pragma optimize("", OPTIMISATION)

//static TArray<UMapDesc*> MapDescList;

UMapDesc::UMapDesc(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

UMapDesc::~UMapDesc()
{
	int nothing = 0;
	++nothing;
}

#if 0
class Temp : public FPakPlatformFile
{
public:

	using FPakPlatformFile::FPakListEntry; // make this public in the headerfile
										   /*
										   static void FindAllPakFiles(IPlatformFile* LowLevelFile, const TArray<FString>& PakFolders, TArray<FString>& OutPakFiles)
										   {
										   // copied from:
										   //FPakPlatformFile::FindAllPakFiles(LowLevelFile, PakFolders, OutPakFiles);

										   // Find pak files from the specified directories.
										   for (int32 FolderIndex = 0; FolderIndex < PakFolders.Num(); ++FolderIndex)
										   {
										   FindPakFilesInDirectory(LowLevelFile, *PakFolders[FolderIndex], OutPakFiles);
										   }

										   }


										   static void FindPakFilesInDirectory(IPlatformFile* LowLevelFile, const TCHAR* Directory, TArray<FString>& OutPakFiles)
										   {
										   // Helper class to find all pak files.
										   class FPakSearchVisitor : public IPlatformFile::FDirectoryVisitor
										   {
										   TArray<FString>& FoundPakFiles;
										   IPlatformChunkInstall* ChunkInstall;
										   public:
										   FPakSearchVisitor(TArray<FString>& InFoundPakFiles, IPlatformChunkInstall* InChunkInstall)
										   : FoundPakFiles(InFoundPakFiles)
										   , ChunkInstall(InChunkInstall)
										   {}
										   virtual bool Visit(const TCHAR* FilenameOrDirectory, bool bIsDirectory)
										   {
										   if (bIsDirectory == false)
										   {
										   FString Filename(FilenameOrDirectory);
										   if (FPaths::GetExtension(Filename) == TEXT("pak"))
										   {
										   // if a platform supports chunk style installs, make sure that the chunk a pak file resides in is actually fully installed before accepting pak files from it
										   if (ChunkInstall)
										   {
										   FString ChunkIdentifier(TEXT("pakchunk"));
										   FString BaseFilename = FPaths::GetBaseFilename(Filename);
										   if (BaseFilename.StartsWith(ChunkIdentifier))
										   {
										   int32 DelimiterIndex = 0;
										   int32 StartOfChunkIndex = ChunkIdentifier.Len();

										   BaseFilename.FindChar(TEXT('-'), DelimiterIndex);
										   FString ChunkNumberString = BaseFilename.Mid(StartOfChunkIndex, DelimiterIndex - StartOfChunkIndex);
										   int32 ChunkNumber = 0;
										   TTypeFromString<int32>::FromString(ChunkNumber, *ChunkNumberString);
										   if (ChunkInstall->GetChunkLocation(ChunkNumber) == EChunkLocation::NotAvailable)
										   {
										   return true;
										   }
										   }
										   }
										   FoundPakFiles.Add(Filename);
										   }
										   }
										   return true;
										   }
										   };
										   // Find all pak files.
										   FPakSearchVisitor Visitor(OutPakFiles, FPlatformMisc::GetPlatformChunkInstall());
										   LowLevelFile->IterateDirectoryRecursively(Directory, Visitor);
										   }
										   */


										   //using FPakPlatformFile::FPakListEntry;
};
#endif

/*
Get map data
*/
void UMapDesc::GetMapDescList(TArray<UMapDesc*>& Maps)
{
	TArray<UMapDesc*> MapDescList;
	/*
	// GetMaps already cached
	if (MapDescList.Num() > 0)
	{
		Maps = MapDescList;
		return;
	}*/


	TArray<FString> MapNames;

	// from FeaturePackContentSource.cpp:152
	FPakPlatformFile PakPlatformFile;
	PakPlatformFile.Initialize(&FPlatformFileManager::Get().GetPlatformFile(), TEXT(""));

	//FPakPlatformFile* PakPlatformFile = new FPakPlatformFile();
	//PakPlatformFile->Initialize(&InnerPlatformFile, TEXT(""));
	//PakPlatformFile->Mount(*PakToLoad, 0, *(ModDir /*+ TEXT("/") + ModPakFileName*/));


	// to get this working I had to edit:
	// IPlatformFilePak.h
	/*
	class PAKFILE_API FPakPlatformFile : public IPlatformFile
	{
	public: // Added by FM!!!
	*/


	// editor doesnt use pak files, but we have a backup plan!
	TArray<FString> AllFiles;
	IFileManager::Get().FindFilesRecursive(AllFiles, *FPaths::GameContentDir(), *FString("*.umap"), true, false);
	AllFiles.Sort([](const FString& A, const FString& B) { return FPaths::GetBaseFilename(A) < FPaths::GetBaseFilename(B); });
	for (int i = 0; i < AllFiles.Num(); ++i)
	{
		FString FullFilename = AllFiles[i];

		FString Error;
		FString LongPackageName;
		FPackageName::TryConvertFilenameToLongPackageName(FullFilename, LongPackageName, &Error);
		MapNames.AddUnique(LongPackageName);
	}


	TArray<FPakPlatformFile::FPakListEntry> MountedPaks;
	PakPlatformFile.GetMountedPaks(MountedPaks);
	for (int32 i = 0; i < MountedPaks.Num(); i++)
	{
		const FPakPlatformFile::FPakListEntry& mountedPak = MountedPaks[i];
		const TMap<FString, FPakDirectory>& pakDirectoryMap = mountedPak.PakFile->GetIndex();

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, *mountedPak.PakFile->GetFilename());
		//UE_LOG(LogClass, Log, TEXT("PAK File: %s"), *mountedPak.PakFile->GetFilename());

		FString filename = mountedPak.PakFile->GetFilename();
		FString mountPoint = mountedPak.PakFile->GetMountPoint();

		for (TMap<FString, FPakDirectory>::TConstIterator It(pakDirectoryMap); It; ++It)
		{
			FString Key = It.Key();
			const FPakDirectory& Val = It.Value();

			for (FPakDirectory::TConstIterator It2(Val); It2; ++It2)
			{
				FString Filename = It2.Key();
				FPakEntry* pakEntry = It2.Value();

				if (Filename.Contains(TEXT(".umap")))
				{
					FString FullFilename = mountPoint + Filename;

					FString Error;
					FString LongPackageName;
					FPackageName::TryConvertFilenameToLongPackageName(FullFilename, LongPackageName, &Error);
					MapNames.AddUnique(LongPackageName);
					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, *Key2);
					//UE_LOG(LogClass, Log, TEXT("UMAP File: %s"), *Key2);
				}
			}
		}
	}

	// sort
	MapNames.Sort([](const FString& A, const FString& B) { return FPaths::GetBaseFilename(A) < FPaths::GetBaseFilename(B); });


	/*
	// ganked from FFeaturePackContentSource::FFeaturePackContentSource
	//
	// Find and mount pak files from the specified directories.
	TArray<FString> PakFolders;
	FPakPlatformFile::GetPakFolders(L"", PakFolders);
	TArray<FString> FoundPakFiles;
	Temp::FindAllPakFiles(&FPlatformFileManager::Get().GetPlatformFile(), PakFolders, FoundPakFiles);

	for (int i = 0; i < PakFolders.Num(); ++i)
	{
	UE_LOG(LogClass, Log, TEXT("Pak Folder: %s"), *PakFolders[i]);
	}

	for (int i = 0; i < FoundPakFiles.Num(); ++i)
	{
	UE_LOG(LogClass, Log, TEXT("Pak File: %s"), *FoundPakFiles[i]);
	}

	TArray<FString> AllFiles;
	IFileManager::Get().FindFilesRecursive(AllFiles, *FPaths::GameContentDir(), *FString("*"), true, false);
	AllFiles.Sort([](const FString& A, const FString& B) { return FPaths::GetBaseFilename(A) < FPaths::GetBaseFilename(B); });
	for (int i = 0; i < AllFiles.Num(); ++i)
	{
	UE_LOG(LogClass, Log, TEXT("File: %s"), *AllFiles[i]);
	}

	/ *
	const FString FilePath = FPackageName::LongPackageNameToFilename(TEXT("/Content/"));

	TArray< FString > OutPackages;
	FPackageName::FindPackagesInDirectory(OutPackages, FilePath);

	TArray<FString> AllFiles;
	IFileManager::Get().FindFilesRecursive(AllFiles, *FilePath, TEXT("*.*"), true, false);
	* /





	// ganked from GameMapsSettingsCustomization.h
	const FString MapFileWildCard = FString::Printf(TEXT("*%s"), *FPackageName::GetMapPackageExtension());
	//TArray<FString> MapNames;
	IFileManager::Get().FindFilesRecursive(MapNames, *FPaths::GameContentDir(), *MapFileWildCard, true, false);
	MapNames.Sort([](const FString& A, const FString& B) { return FPaths::GetBaseFilename(A) < FPaths::GetBaseFilename(B); });


	for (int i = 0; i < MapNames.Num(); ++i)
	{
	UE_LOG(LogClass, Log, TEXT("GameContentDir map: %s"), *MapNames[i]);
	}


	MapNames.Empty();
	IFileManager::Get().FindFilesRecursive(MapNames, *FPaths::EngineContentDir(), *MapFileWildCard, true, false);

	for (int i = 0; i < MapNames.Num(); ++i)
	{
	UE_LOG(LogClass, Log, TEXT("EngineContentDir map: %s"), *MapNames[i]);
	}

	int nothing = 0;
	++nothing;
	*/


	// https://forums.unrealengine.com/showthread.php?5749-Extract-*-umap-file



	/*
	ConstructorHelpers::FObjectFinder<UGPersistentUser> OpaqueMaterialRef(*descriptorBlueprintName);
	UGPersistentUser* AlternateMaterial = OpaqueMaterialRef.Object;
	*/

	/*
	FObjectFinder<UClass> bpClassFinder(TEXT("Class'/Game/dice/D4_Dice_BP.D4_Dice_BP_C'"));
	if (bpClassFinder.Object)
	{
	UClass* bpClass = bpClassFinder.Object;
	}*/

	// Don't overwrite the map's world settings if we failed to load the value off the command line parameter
	//UClass* GameModeParamClass = StaticLoadClass(AGameMode::StaticClass(), NULL, *GameClassName, NULL, LOAD_None, NULL);
	//UClass* DescriptorClass = StaticLoadClass(UGPersistentUser::StaticClass(), NULL, *descriptorBlueprintName, NULL, LOAD_None, NULL);
	//UGPersistentUser* PU = Cast<UGPersistentUser>(StaticLoadObject(DescriptorClass, NULL, *descriptorBlueprintName));

	for (int i = 0; i < MapNames.Num(); ++i)
	{
		FString mapName = *MapNames[i];

		FString baseFilename = FPaths::GetBaseFilename(mapName);
		FString mapDescriptor = FString::Printf(TEXT("BP_%s.BP_%s_C"), *baseFilename, *baseFilename);
		FString descriptorBlueprintName = mapName.Replace(*baseFilename, *mapDescriptor);

		// Load localized part of level first in case it exists.
		FString LocalizedMapFilename;
		if (!FPackageName::DoesPackageExist(*descriptorBlueprintName, NULL, &LocalizedMapFilename))
		{
			continue;
		}

		//UClass* DescriptorClass = FindObject<UClass>(ANY_PACKAGE, *descriptorBlueprintName);
		UClass* DescriptorClass = StaticLoadClass(UMapDesc::StaticClass(), NULL, *descriptorBlueprintName, NULL, LOAD_None, NULL);
		if (!DescriptorClass)
		{
			continue;
		}

		//UMapDesc* Descriptor = NewObject<UMapDesc>(Owner/*GetTransientPackage()*/, DescriptorClass);
		UMapDesc* Descriptor = NewObject<UMapDesc>(GetTransientPackage(), DescriptorClass, FName(*baseFilename), (EObjectFlags)RF_Transactional | RF_Transient);
		if (!Descriptor)
		{
			continue;
		}
		
		Descriptor->LongPackageName = mapName;
		MapDescList.Add(Descriptor);
	}

	Maps = MapDescList;

	/*
	//UGPersistentUser* PU = LoadObject<UGPersistentUser>(NULL, *descriptorBlueprintName);


	UPackage *Package = Cast<UPackage>(LoadPackage(NULL, *descriptorBlueprintName, LOAD_None));
	if (!Package)
	{
	UE_LOG(LogClass, Log, TEXT("Error loading package: %s"), *MapNames[0]);
	}
	else
	{
	TArray<UObject *> LoadedObjects;
	for (TObjectIterator<UObject> It; It; ++It)
	{
	if (It->GetOuter() == Package)
	{
	LoadedObjects.Add(*It);
	}
	}
	UE_LOG(LogClass, Log, TEXT("Successfully loaded package: %s, %d"), *MapNames[0], LoadedObjects.Num());

	for (int32 i = 0; i < LoadedObjects.Num(); ++i)
	{
	const UObject *obj = LoadedObjects[i];
	const FString name = obj->GetName();
	/ *
	for (int32 j = 0; j < name.GetCharArray().Num() - 1; ++j)
	{
	fprintf(fptr, "%c", name.GetCharArray()[j]);
	}
	fprintf(fptr, "\n");* /

	int nothing = 0;
	++nothing;


	}
	}*/
}
