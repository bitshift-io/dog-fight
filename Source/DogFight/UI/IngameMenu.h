// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "OnlineBlueprintCallProxyBase.h"
#include "FindSessionsCallbackProxy.h"
#include "IngameMenu.generated.h"

UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API UIngameMenu : public UUserWidget
{
	GENERATED_BODY()
public:

	UIngameMenu(const FObjectInitializer& ObjectInitializer);
	virtual ~UIngameMenu();

	virtual void PostInitProperties();

	/** sets owning player controller */
	void Construct(ULocalPlayer* PlayerOwner);

	/** toggles in game menu */
	UFUNCTION(BlueprintCallable, Category = "Menu")
	virtual void ToggleGameMenu();

	UFUNCTION(BlueprintCallable, Category = "Menu")
	void ExitToMain();

	/** Name of map to load **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString	MainMenuMapName;

	virtual void AddToViewport();

	/*
	* Implemented in blueprint to avoid compile time binding to joystick plugin
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCosmetic, Category = "User Interface", meta = (Keywords = "OnShow"))
	void OnShow();

	UFUNCTION(BlueprintCallable, Category = "Menu")
	AGPlayerController* GetPlayerController();

protected:

	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld);

	/** Owning player controller */
	ULocalPlayer* PlayerOwner;

	/** if game menu is currently opened*/
	bool bIsGameMenuUp;
};

