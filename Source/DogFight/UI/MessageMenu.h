// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MessageMenu.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOkSignature, UMessageMenu*, MessageMenu);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCancelSignature, UMessageMenu*, MessageMenu);

UENUM()
enum class EMessageMenuType
{
	/** Progress + Text + Cancel button */
	CancelableProgress,
	/** Message with OK button */
	AcceptableMessage,
	/** Progress + Text */
	Progress,
	/** Option to continue or cancel */
	OkCancelMessage,
};

UENUM()
enum class EMessageWorldFlag
{
	/** Progress + Text + Cancel button */
	DisplayAcrossWorlds,

	/** Message with OK button */
	DestroyOnWorldChange,
};

UCLASS(BlueprintType, Blueprintable, config = Game)
class DOGFIGHT_API UMessageMenu : public UUserWidget
{
	GENERATED_BODY()
public:

	UMessageMenu(const FObjectInitializer& ObjectInitializer);
	virtual ~UMessageMenu();

	virtual void PostInitProperties();

	void SetWorldFlag(EMessageWorldFlag Flag);



	/*
	* Implemented in blueprint to avoid compile time binding to joystick plugin
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "User Interface", meta = (Keywords = "Message", DisplayName = "Set Message"))
	void SetMessage(const FString& Message);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "User Interface")
	void SetMessageVisible(bool bVisible);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "User Interface")
	void SetProgressIndicatorVisible(bool bVisible);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "User Interface")
	void SetOkButtonVisible(bool bVisible);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "User Interface")
	void SetCancelButtonVisible(bool bVisible);

	void Construct(EMessageMenuType Type);

	UFUNCTION(BlueprintCallable, Category = "Message Menu|User Interface")
	static void ConstructDefaultMessageMenu(EMessageMenuType Type, UMessageMenu*& Menu);

	UFUNCTION(BlueprintCallable, Category = "Message Menu|User Interface")
	static void ActiveMessageMenu(UMessageMenu*& Menu);


	static UMessageMenu* Active();

	static UMessageMenu* ConstructDefault(EMessageMenuType Type);

	/** default class class */
	UPROPERTY(config)
	TSubclassOf<class UMessageMenu> DefaultClass;

	virtual void AddToViewport();
	virtual void RemoveFromViewport();

	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld);

	UFUNCTION(BlueprintCallable, Category = "Message Menu|User Interface")
	virtual void CallOnOk();

	UFUNCTION(BlueprintCallable, Category = "Message Menu|User Interface")
	virtual void CallOnCancel();

	/** Called when the actor is damaged in any way. */
	UPROPERTY(BlueprintAssignable, Category = "Message Menu|User Interface")
	FOnOkSignature OnOk;

	/** Called when the actor is damaged in any way. */
	UPROPERTY(BlueprintAssignable, Category = "Message Menu|User Interface")
	FOnCancelSignature OnCancel;
};
