// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "Scoreboard.h"
#include "Game/GGameInstance.h"
#include "GameMode/GGameState.h"

#pragma optimize("", OPTIMISATION)


void FLeaderboardRow::Populate(const FOnlineStatsRow& Row, UScoreboard* Scoreboard)
{
	bValid = (Row.Rank != -1);
	Rank = Row.Rank;
	PlayerName = Row.NickName;
	PlayerId = Row.PlayerId;

	/*
	if (const FVariantData* KillData = Row.Columns.Find(LEADERBOARD_STAT_KILLS))
	{
		int32 Val;
		KillData->GetValue(Val);
		Kills = FString::FromInt(Val);
	}

	if (const FVariantData* DeathData = Row.Columns.Find(LEADERBOARD_STAT_DEATHS))
	{
		int32 Val;
		DeathData->GetValue(Val);
		Deaths = FString::FromInt(Val);
	}*/
	
	if (const FVariantData* WaveData = Row.Columns.Find(LEADERBOARD_STAT_SCORE))
	{
		int32 Val;
		WaveData->GetValue(Val);

		// the highscore we pulled from steam might not actually be the players best score
		// if they achived the best score on this round, then show the score for this round
		for (FConstControllerIterator It = Scoreboard->GetWorld()->GetControllerIterator(); It; ++It)
		{
			AGPlayerController* PC = Cast<AGPlayerController>(*It);
			if (!PC)
				continue;

			AGPlayerState* PlayerState = Cast<AGPlayerState>(PC->PlayerState);
			AGGameState* GameState = Cast<AGGameState>(Scoreboard->GetWorld()->GameState);

			if (PlayerState && GameState && PlayerState->UniqueId == PlayerId)
			{
				if (PlayerState->GetScore() >= Val)
				{
					Score = PlayerState->GetScore();
					Wave = GameState->WaveCount;
					return;
				}
			}
		}

		Score = Val;
	}

	if (const FVariantData* WaveData = Row.Columns.Find(LEADERBOARD_STAT_WAVE))
	{
		int32 Val;
		WaveData->GetValue(Val);
		Wave = Val;
	}
}

UScoreboard::UScoreboard(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

UScoreboard::~UScoreboard()
{
	int nothing = 0;
	++nothing;
}

void UScoreboard::PostInitProperties()
{
	Super::PostInitProperties();
	LeaderboardReadCompleteDelegate = FOnLeaderboardReadCompleteDelegate::CreateUObject(this, &UScoreboard::OnStatsRead);
	//SetFlags(RF_RootSet); // stop garbage collection failing! - causes a crash thought!
}

/** Starts reading leaderboards for the game */
void UScoreboard::ReadStats()
{
	StatRows.Reset();

	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineLeaderboardsPtr Leaderboards = OnlineSub->GetLeaderboardsInterface();
		if (Leaderboards.IsValid())
		{
			// We are about to read the stats. The delegate will set this to false once the read is complete.
			LeaderboardReadCompleteDelegateHandle = Leaderboards->AddOnLeaderboardReadCompleteDelegate_Handle(LeaderboardReadCompleteDelegate);
			bReadingStats = true;

			// There's no reason to request leaderboard requests while one is in progress, so only do it if there isn't one active.
			if (!IsLeaderboardReadInProgress())
			{
				ReadObject = MakeShareable(new FGAllTimeMatchResultsRead());
				FOnlineLeaderboardReadRef ReadObjectRef = ReadObject.ToSharedRef();
				bReadingStats = Leaderboards->ReadLeaderboardsForFriends(0, ReadObjectRef);
			}
		}
		else
		{
			// TODO: message the user?
		}
	}
}

/** Called on a particular leaderboard read */
void UScoreboard::OnStatsRead(bool bWasSuccessful)
{
	// It's possible for another read request to happen while another one is ongoing (such as when the player leaves the menu and
	// re-enters quickly). We only want to process a leaderboard read if our read object is done.
	if (!IsLeaderboardReadInProgress())
	{
		ClearOnLeaderboardReadCompleteDelegate();

		if (bWasSuccessful)
		{
			StatRows.Empty();

			if (ReadObject.IsValid())
			{
				for (int Idx = 0; Idx < ReadObject->Rows.Num(); ++Idx)
				{
					TSharedPtr<FLeaderboardRow> NewLeaderboardRow = MakeShareable(new FLeaderboardRow());
					NewLeaderboardRow->Populate(ReadObject->Rows[Idx], this);
					StatRows.Add(NewLeaderboardRow);
				}
			}

			// sort by wave
			StatRows.Sort(FLeaderboardRow::ConstPredicate);

			// rank the players and insert into the UI
			for (int Idx = 0; Idx < StatRows.Num(); ++Idx)
			{
				if (StatRows[Idx]->bValid)
				{
					StatRows[Idx]->Rank = Idx + 1;
					if (bIsGameMenuUp)
					{
						InsertLeaderboardRow(*StatRows[Idx].Get());
					}
				}
			}

			//RowListWidget->RequestListRefresh();
		}

		bReadingStats = false;
	}
}

void UScoreboard::ClearOnLeaderboardReadCompleteDelegate()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineLeaderboardsPtr Leaderboards = OnlineSub->GetLeaderboardsInterface();
		if (Leaderboards.IsValid())
		{
			Leaderboards->ClearOnLeaderboardReadCompleteDelegate_Handle(LeaderboardReadCompleteDelegateHandle);
		}
	}
}

bool UScoreboard::IsLeaderboardReadInProgress()
{
	return ReadObject.IsValid() && (ReadObject->ReadState == EOnlineAsyncTaskState::InProgress || ReadObject->ReadState == EOnlineAsyncTaskState::NotStarted);
}

/*
int32 UScoreboard::CalculateScore(AGPlayerState* PlayerState)
{
	if (!PlayerState)
		return 0;
	
	// factor modifier to score for each wave
	// up to 100 points for high accuracy
	// up to 100 points for no deaths
	// up to 100 points for pickups
	int accuracyScore = float(PlayerState->GetNumBulletsHit()) / FMath::Max(1.f, float(PlayerState->GetNumBulletsFired())) * 100.f;
	int pickupScore = ((1.f - (1.f / FMath::Max(1.f, PlayerState->GetNumPickups() * 0.3f))) * 100.f);
	int deathsScore = 100.f / FMath::Max(1.f, float(PlayerState->GetDeaths()));
	int score = (accuracyScore + pickupScore + deathsScore);


	// apply "wave" multiplier
	score = score * FMath::Max(1, GetGameState()->WaveCount);


	PlayerState->SetScore(score);


	return score;
}*/

void UScoreboard::PopulateScores()
{
	AGGameState* GS = GetGameState();

	TArray<AGPlayerState*> PlayerStateList;

	// compute scores
	for (int i = 0; i < GS->PlayerArray.Num(); ++i)
	{
		AGPlayerState* PS = Cast<AGPlayerState>(GS->PlayerArray[i]);
		if (PS->bIsABot)
		{
			continue;
		}

		PlayerStateList.Add(PS);
		//CalculateScore(PS);
	}

	// sort PlayerControllerList by score
	for (int i = 0; i < PlayerStateList.Num(); ++i)
	{
		for (int j = i; j < PlayerStateList.Num(); ++j)
		{
			AGPlayerState* PlayerStateI = Cast<AGPlayerState>(PlayerStateList[i]); // Cast<AGPlayerState>(PlayerControllerList[i]->PlayerState);
			AGPlayerState* PlayerStateJ = Cast<AGPlayerState>(PlayerStateList[j]); // Cast<AGPlayerState>(PlayerControllerList[j]->PlayerState);

			if (PlayerStateJ->GetScore() > PlayerStateI->GetScore())
			{
				AGPlayerState* temp = PlayerStateList[j];
				PlayerStateList[j] = PlayerStateList[i];
				PlayerStateList[i] = temp;
			}
		}
	}

	// insert scores 
	for (int i = 0; i < PlayerStateList.Num(); ++i)
	{
		AGPlayerState* PlayerState = PlayerStateList[i];

		float accuracy = (PlayerState->GetNumBulletsFired() == 0) ? 0.f : (float(PlayerState->GetNumBulletsHit()) / float(PlayerState->GetNumBulletsFired())) * 100.f;
		//char details[2048];
		//sprintf(details, "Pickups: %i\nDeaths: %i\nAccuracy: %.1f%% (%i/%i)", PlayerState->GetNumPickups(), PlayerState->GetDeaths(), accuracy, PlayerState->GetNumBulletsHit(), PlayerState->GetNumBulletsFired());
		FString details;
		details.Printf(TEXT("Pickups: %i\nDeaths: %i\nAccuracy: %.1f%% (%i/%i)"), PlayerState->GetNumPickups(), PlayerState->GetDeaths(), accuracy, PlayerState->GetNumBulletsHit(), PlayerState->GetNumBulletsFired());

		//IControllerInterface* CI = Cast<IControllerInterface>(PlayerState->Controller);
		FString name = PlayerState->PawnClass ? PlayerState->PawnClass->GetName() : "";
		InsertPlayerScore(i, PlayerState->PlayerColourName, name, PlayerState->PlayerColour, PlayerState->GetScore(), details);
	}
}

void UScoreboard::Show(const FString& WinMessage)
{
	if (!bIsGameMenuUp)
	{
		ToggleGameMenu();
	}

	if (WinMessage.IsEmpty())
	{
		SetMessage(FString::Printf(TEXT("%s"), *GetGameState()->GameModeName));
	}
	else
	{
		SetMessage(FString::Printf(TEXT("%s - %s"), *GetGameState()->GameModeName, *WinMessage));
	}

	// show wave count in title if its valid
	if (GetGameState()->WaveCount > 0)
	{
		// show leaderboard for invasion
		EnableLeaderboard();
		if (!bReadingStats)
		{
			OnStatsRead(true);
		}
	}

	PopulateScores();
}

void UScoreboard::ToggleGameMenu()
{
	Super::ToggleGameMenu();
}

void UScoreboard::AddToViewport()
{
	Super::AddToViewport();
}

AGGameState* UScoreboard::GetGameState()
{
	check(GetWorld() && GetWorld()->GameState)
	return Cast<AGGameState>(GetWorld()->GameState);
}
