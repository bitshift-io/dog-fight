// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "MessageMenu.h"

#pragma optimize("", OPTIMISATION)

static UMessageMenu* ActiveMenu = NULL;

UMessageMenu::UMessageMenu(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

UMessageMenu::~UMessageMenu()
{
	if (ActiveMenu == this)
	{
		ActiveMenu = NULL;
	}
}

void UMessageMenu::Construct(EMessageMenuType Type)
{
	switch (Type)
	{
	case EMessageMenuType::CancelableProgress:
		SetOkButtonVisible(false);
		break;

	case EMessageMenuType::AcceptableMessage:
		SetCancelButtonVisible(false);
		SetProgressIndicatorVisible(false);
		break;

	case EMessageMenuType::Progress:
		SetOkButtonVisible(false);
		SetCancelButtonVisible(false);
		break;

	case EMessageMenuType::OkCancelMessage:
		SetProgressIndicatorVisible(false);
		break;
	}

	AddToViewport(); // message box should always be on top of other widgets
}

void UMessageMenu::PostInitProperties()
{
	Super::PostInitProperties();
}

void UMessageMenu::SetWorldFlag(EMessageWorldFlag Flag)
{
	if (Flag == EMessageWorldFlag::DisplayAcrossWorlds)
	{
		SetFlags(RF_MarkAsRootSet); // stop garbage collection failing! - allow to survive a level transition
	}
}

void UMessageMenu::ConstructDefaultMessageMenu(EMessageMenuType Type, UMessageMenu*& Menu)
{
	Menu = ConstructDefault(Type);
}

UMessageMenu* UMessageMenu::ConstructDefault(EMessageMenuType Type)
{
	// create a temporary widget just to get the DefaultClass from Config/DefaultGame.ini
	UMessageMenu* tempWidget = CreateWidget<UMessageMenu>(GetGameInstance(), UMessageMenu::StaticClass());
	TSubclassOf<class UMessageMenu> DefaultClass = tempWidget->DefaultClass;
	//tempWidget->Destroy();

	if (!DefaultClass)
	{
		// BAD!
		return NULL;
	}

	UMessageMenu* widget = CreateWidget<UMessageMenu>(GetGameInstance(), DefaultClass);
	widget->Construct(Type);
	return widget;
}

void UMessageMenu::AddToViewport()
{
	Super::AddToViewport();

	// stop UUserWidget::OnLevelRemovedFromWorld being called
	// and instead call UIngameMenu::OnLevelRemovedFromWorld

	FWorldDelegates::LevelRemovedFromWorld.RemoveAll(this);

	// Widgets added to the viewport are automatically removed if the persistent level is unloaded.
	FWorldDelegates::LevelRemovedFromWorld.AddUObject(this, &UMessageMenu::OnLevelRemovedFromWorld);

	ActiveMenu = this;
}

void UMessageMenu::RemoveFromViewport()
{
	if (ActiveMenu == this)
	{
		ActiveMenu = NULL;
	}

	Super::RemoveFromViewport();
}

void UMessageMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	if (!HasAnyFlags(RF_MarkAsRootSet))
	{
		Super::OnLevelRemovedFromWorld(InLevel, InWorld);
	}

	/*
	// if level changes while the menu is up  "SetFlags(RF_RootSet);" causes a crash
	// in Super::OnLevelRemovedFromWorld
	if (bIsGameMenuUp)
	{
		ToggleGameMenu();
		ClearFlags(RF_RootSet);
	}

	Super::OnLevelRemovedFromWorld(InLevel, InWorld);*/
}

/*
	Return the active menu
*/
UMessageMenu* UMessageMenu::Active()
{
	return ActiveMenu;
}

void UMessageMenu::ActiveMessageMenu(UMessageMenu*& Menu)
{
	Menu = ActiveMenu;
}

void UMessageMenu::CallOnOk()
{
	RemoveFromViewport();
	OnOk.Broadcast(this);
}

void UMessageMenu::CallOnCancel()
{
	RemoveFromViewport();
	OnCancel.Broadcast(this);
}