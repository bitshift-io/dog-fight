// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BPFunctionLibrary.generated.h"

UCLASS()
class DOGFIGHT_API UBPFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get screen resolution"))
	static bool GetScreenResolution(FString& resolution);

	/** Returns three arrays containing all of the resolutions and refresh rates for the current computer's current display adapter. You can loop over just 1 of the arrays and use the current index for the other two arrays, as all 3 arrays will always have the same length. Returns false if operation could not occur. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "screen resolutions display adapater"))
	static bool GetScreenResolutions(TArray<FString>& resolutionList);

	/** Set screen resolution. */
	UFUNCTION(BlueprintCallable, Category = "BPLibrary", meta = (Keywords = "screen resolutions display adapater"))
	static bool SetScreenResolution(const FString& resolution);


	/** Toggle full screen. */
	UFUNCTION(BlueprintCallable, Category = "BPLibrary", meta = (Keywords = "set full screen mode"))
	static bool SetFullScreen(bool fullscreen);

	/** Is full screen. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get full screen mode"))
	static bool GetFullscreenMode(bool& fullscreen);

	/** Get audio volume **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get audio volume"))
	static void GetAudioClassVolume(const FString& ClassName, float& Volume);

	/** Set audio volume **/
	UFUNCTION(BlueprintCallable, Category = "BPLibrary", meta = (Keywords = "set audio volume"))
	static void SetAudioClassVolume(const FString& ClassName, float Volume);

	UFUNCTION(BlueprintCallable, Category = "BPLibrary", meta = (Keywords = "set post processing enabled"))
	static void SetPostProcessingEnabled(bool enabled);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get post processing enabled"))
	static void GetPostProcessingEnabled(bool& enabled);

	UFUNCTION(BlueprintCallable, Category = "BPLibrary", meta = (Keywords = "set scalability quality"))
	static void SetScalabilityQuality(int32 quality);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get scalability quality"))
	static void GetScalabilityQuality(int32& quality);

	/** Use this to configure keyboard input mapping as well as joysticks **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "get game viewport client"))
	static void GetGameViewportClient(class UGGameViewportClient*& GameViewport);



	/** Find a child object by name **/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "BPLibrary", meta = (Keywords = "find child actor by object name"))
	static void FindFirstChildWidgetByObjectName(class UWidget* Parent, FString Name, class UWidget*& Child);
};
