// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "IngameMenu.h"
#include "Game/GGameInstance.h"
#include "Controller/GPlayerController.h"

#pragma optimize("", OPTIMISATION)

UIngameMenu::UIngameMenu(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerOwner = NULL;
}

UIngameMenu::~UIngameMenu()
{
	int nothing = 0;
	++nothing;
}

void UIngameMenu::PostInitProperties()
{
	Super::PostInitProperties();
}

void UIngameMenu::Construct(ULocalPlayer* _PlayerOwner)
{
	PlayerOwner = _PlayerOwner;
	bIsGameMenuUp = false;
	//SetFlags(RF_MarkAsRootSet); // stop garbage collection failing!


	if (!PlayerOwner)
	{
		PlayerOwner = GetOwningLocalPlayer();
	}

	// AddToViewport will set input mode to UI, but we hide the menu to stop it being cleaned up
	// so need to set input mode back to game
	AddToViewport();

	SetVisibility(ESlateVisibility::Collapsed);
	AGPlayerController* const PCOwner = PlayerOwner ? Cast<AGPlayerController>(PlayerOwner->PlayerController) : nullptr;
	FInputModeGameOnly Mode;
	if (PCOwner)
	{
		PCOwner->SetInputMode(Mode);
	}



/*
	if (!GEngine || !GEngine->GameViewport)
	{
		return;
	}

	//todo:  don't create ingame menus for remote players.
	const UShooterGameInstance* GI = nullptr;
	if (PlayerOwner)
	{
		GI = Cast<UShooterGameInstance>(PlayerOwner->GetGameInstance());
	}

	if (!GameMenuWidget.IsValid())
	{
		SAssignNew(GameMenuWidget, SShooterMenuWidget)
			.PlayerOwner(TWeakObjectPtr<ULocalPlayer>(PlayerOwner))
			.Cursor(EMouseCursor::Default)
			.IsGameMenu(true);


		int32 const OwnerUserIndex = GetOwnerUserIndex();

		// setup the exit to main menu submenu.  We wanted a confirmation to avoid a potential TRC violation.
		// fixes TTP: 322267
		TSharedPtr<FShooterMenuItem> MainMenuRoot = FShooterMenuItem::CreateRoot();
		MainMenuItem = MenuHelper::AddMenuItem(MainMenuRoot, LOCTEXT("Main Menu", "MAIN MENU"));
		MenuHelper::AddMenuItemSP(MainMenuItem, LOCTEXT("No", "NO"), this, &FShooterIngameMenu::OnCancelExitToMain);
		MenuHelper::AddMenuItemSP(MainMenuItem, LOCTEXT("Yes", "YES"), this, &FShooterIngameMenu::OnConfirmExitToMain);

		ShooterOptions = MakeShareable(new FShooterOptions());
		ShooterOptions->Construct(PlayerOwner);
		ShooterOptions->TellInputAboutKeybindings();
		ShooterOptions->OnApplyChanges.BindSP(this, &FShooterIngameMenu::CloseSubMenu);

		MenuHelper::AddExistingMenuItem(RootMenuItem, ShooterOptions->CheatsItem.ToSharedRef());
		MenuHelper::AddExistingMenuItem(RootMenuItem, ShooterOptions->OptionsItem.ToSharedRef());
		if (GI && GI->GetIsOnline())
		{
#if !PLATFORM_XBOXONE
			ShooterFriends = MakeShareable(new FShooterFriends());
			ShooterFriends->Construct(PlayerOwner, OwnerUserIndex);
			ShooterFriends->TellInputAboutKeybindings();
			ShooterFriends->OnApplyChanges.BindSP(this, &FShooterIngameMenu::CloseSubMenu);

			MenuHelper::AddExistingMenuItem(RootMenuItem, ShooterFriends->FriendsItem.ToSharedRef());

			ShooterRecentlyMet = MakeShareable(new FShooterRecentlyMet());
			ShooterRecentlyMet->Construct(PlayerOwner, OwnerUserIndex);
			ShooterRecentlyMet->TellInputAboutKeybindings();
			ShooterRecentlyMet->OnApplyChanges.BindSP(this, &FShooterIngameMenu::CloseSubMenu);

			MenuHelper::AddExistingMenuItem(RootMenuItem, ShooterRecentlyMet->RecentlyMetItem.ToSharedRef());
#endif		

#if SHOOTER_CONSOLE_UI			
			TSharedPtr<FShooterMenuItem> ShowInvitesItem = MenuHelper::AddMenuItem(RootMenuItem, LOCTEXT("Invite Players", "INVITE PLAYERS"));
			ShowInvitesItem->OnConfirmMenuItem.BindRaw(this, &FShooterIngameMenu::OnShowInviteUI);
#endif
		}

		if (FSlateApplication::Get().SupportsSystemHelp())
		{
			TSharedPtr<FShooterMenuItem> HelpSubMenu = MenuHelper::AddMenuItem(RootMenuItem, LOCTEXT("Help", "HELP"));
			HelpSubMenu->OnConfirmMenuItem.BindStatic([]() { FSlateApplication::Get().ShowSystemHelp(); });
		}

		MenuHelper::AddExistingMenuItem(RootMenuItem, MainMenuItem.ToSharedRef());

#if !SHOOTER_CONSOLE_UI
		MenuHelper::AddMenuItemSP(RootMenuItem, LOCTEXT("Quit", "QUIT"), this, &FShooterIngameMenu::OnUIQuit);
#endif

		GameMenuWidget->MainMenu = GameMenuWidget->CurrentMenu = RootMenuItem->SubMenu;
		GameMenuWidget->OnMenuHidden.BindSP(this, &FShooterIngameMenu::DetachGameMenu);
		GameMenuWidget->OnToggleMenu.BindSP(this, &FShooterIngameMenu::ToggleGameMenu);
		GameMenuWidget->OnGoBack.BindSP(this, &FShooterIngameMenu::OnMenuGoBack);
	}*/
}

void UIngameMenu::ExitToMain()
{
	// detatch menu
	if (bIsGameMenuUp)
	{
		ToggleGameMenu();
	}

	UGGameInstance* const GI = Cast<UGGameInstance>(GetGameInstance());
	if (GI)
	{
		GI->LabelPlayerAsQuitter(GI->GetFirstGamePlayer());

		// tell game instance to go back to main menu state
		GI->GotoState(GGameInstanceState::MainMenu);
	}

	UGameplayStatics::OpenLevel(GetWorld(), *MainMenuMapName);
}

AGPlayerController* UIngameMenu::GetPlayerController()
{
	AGPlayerController* const PCOwner = PlayerOwner ? Cast<AGPlayerController>(PlayerOwner->PlayerController) : nullptr;
	return PCOwner;
}

void UIngameMenu::ToggleGameMenu()
{
	AGPlayerController* const PCOwner = GetPlayerController();
	if (!bIsGameMenuUp)
	{
		bIsGameMenuUp = true;
		//AddToViewport();
		SetVisibility(ESlateVisibility::Visible);

		if (PCOwner)
		{
			// Disable controls while paused
			PCOwner->SetCinematicMode(true, false, false, true, true);

			PCOwner->SetPause(true);

			FInputModeUIOnly Mode;
			Mode.SetWidgetToFocus(GetCachedWidget());
			PCOwner->SetInputMode(Mode);
			PCOwner->bShowMouseCursor = true;
		}

		OnShow();
	}
	else
	{
		if (PCOwner)
		{
			// Make sure viewport has focus
			FSlateApplication::Get().SetAllUserFocusToGameViewport();
			/*
			// Don't renable controls if the match is over
			AShooterHUD* const ShooterHUD = PCOwner->GetShooterHUD();
			if ((ShooterHUD != NULL) && (ShooterHUD->IsMatchOver() == false))
			{
				
			}*/

			PCOwner->SetCinematicMode(false, false, false, true, true);

			PCOwner->SetPause(false);

			FInputModeGameOnly Mode;
			PCOwner->SetInputMode(Mode);
			PCOwner->bShowMouseCursor = false;
		}

		bIsGameMenuUp = false;
		//RemoveFromViewport();
		SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UIngameMenu::AddToViewport()
{
	Super::AddToViewport();

	// stop UUserWidget::OnLevelRemovedFromWorld being called
	// and instead call UIngameMenu::OnLevelRemovedFromWorld

	FWorldDelegates::LevelRemovedFromWorld.RemoveAll(this);

	// Widgets added to the viewport are automatically removed if the persistent level is unloaded.
	FWorldDelegates::LevelRemovedFromWorld.AddUObject(this, &UIngameMenu::OnLevelRemovedFromWorld);
}

void UIngameMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	// if level changes while the menu is up  "SetFlags(RF_RootSet);" causes a crash
	// in Super::OnLevelRemovedFromWorld
	if (bIsGameMenuUp)
	{
		ToggleGameMenu();
		RemoveFromViewport();
		//ClearFlags(RF_MarkAsRootSet);
	}

	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
}