// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MenuActor.generated.h"

class UMenuWidget;

/*
	Class to deisgned to allow UMG widgets to be replicated
	- use this for games with a multiplayer lobby

	The MenuActor spawns a MenuWidget, the MenuWidget communicates with the
	MenuActor, this allows the actor to replicate data over the network
*/
UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API AMenuActor : public AActor
{
	GENERATED_BODY()
public:

	AMenuActor(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
	virtual void PostInitializeComponents() override;

	void SetWidgetToFocus();

	// The UMG class of the created blueprint
	UPROPERTY(EditAnywhere, Category = WidgetBlueprintFactory, meta = (AllowAbstract = ""))
	TSubclassOf<UMenuWidget> MenuWidgetClass;

	// instance of UI widget
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMenuWidget* MenuWidget;

};

