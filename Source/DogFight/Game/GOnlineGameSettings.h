// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

/**
 * General session settings for a G game
 */
class FGOnlineSessionSettings : public FOnlineSessionSettings
{
public:

	FGOnlineSessionSettings(bool bIsLAN = false, bool bIsPresence = false, int32 MaxNumPlayers = 4);
	virtual ~FGOnlineSessionSettings() {}
};

/**
 * General search setting for a G game
 */
class FGOnlineSearchSettings : public FOnlineSessionSearch
{
public:
	FGOnlineSearchSettings(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FGOnlineSearchSettings() {}
};

/**
 * Search settings for an empty dedicated server to host a match
 */
class FGOnlineSearchSettingsEmptyDedicated : public FGOnlineSearchSettings
{
public:
	FGOnlineSearchSettingsEmptyDedicated(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FGOnlineSearchSettingsEmptyDedicated() {}
};
