// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "GGameUserSettings.h"

UGGameUserSettings::UGGameUserSettings(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetToDefaults();
}

void UGGameUserSettings::SetToDefaults()
{
	Super::SetToDefaults();
	bIsLanMatch = true;
}

void UGGameUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{	
	Super::ApplySettings(bCheckForCommandLineOverrides);
}