// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "GPersistentUser.h"
#include "GLocalPlayer.h"
#include "OnlineSubsystem.h"

UGPersistentUser::UGPersistentUser(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetToDefaults();
}

void UGPersistentUser::SetToDefaults()
{
	bIsDirty = false;

	bInvertedYAxis = false;
	AimSensitivity = 1.0f;
	Gamma = 2.2f;
	BotsCount = 1;
	bIsRecordingDemos = false;
}

bool UGPersistentUser::IsAimSensitivityDirty() const
{
	bool bDirty = false;

	// Fixme: UGPersistentUser is not setup to work with multiple worlds.
	// For now, user settings are global to all world instances.
	if (GEngine)
	{
		TArray<APlayerController*> PlayerList;
		GEngine->GetAllLocalPlayerControllers(PlayerList);

		for (auto It = PlayerList.CreateIterator(); It; ++It)
		{
			APlayerController* PC = *It;
			if (!PC || !PC->Player || !PC->PlayerInput)
			{
				continue;
			}

			// Update key bindings for the current user only
			UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(PC->Player);
			if(!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
			{
				continue;
			}

			// check if the aim sensitivity is off anywhere
			for (int32 Idx = 0; Idx < PC->PlayerInput->AxisMappings.Num(); Idx++)
			{
				FInputAxisKeyMapping &AxisMapping = PC->PlayerInput->AxisMappings[Idx];
if (AxisMapping.AxisName == "Lookup" || AxisMapping.AxisName == "LookupRate" || AxisMapping.AxisName == "Turn" || AxisMapping.AxisName == "TurnRate")
{
	if (FMath::Abs(AxisMapping.Scale) != GetAimSensitivity())
	{
		bDirty = true;
		break;
	}
}
			}
		}
	}

	return bDirty;
}

bool UGPersistentUser::IsInvertedYAxisDirty() const
{
	bool bDirty = false;
	if (GEngine)
	{
		TArray<APlayerController*> PlayerList;
		GEngine->GetAllLocalPlayerControllers(PlayerList);

		for (auto It = PlayerList.CreateIterator(); It; ++It)
		{
			APlayerController* PC = *It;
			if (!PC || !PC->Player || !PC->PlayerInput)
			{
				continue;
			}

			// Update key bindings for the current user only
			UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(PC->Player);
			if (!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
			{
				continue;
			}

			bDirty |= PC->PlayerInput->GetInvertAxis("Lookup") != GetInvertedYAxis();
			bDirty |= PC->PlayerInput->GetInvertAxis("LookupRate") != GetInvertedYAxis();
		}
	}

	return bDirty;
}

void UGPersistentUser::SavePersistentUser()
{
	UGameplayStatics::SaveGameToSlot(this, SlotName, UserIndex);
	bIsDirty = false;

	// just use steam auto cloud save, its far easier!
#if 0
	// save to steam cloud
	// https://answers.unrealengine.com/questions/208989/steam-user-cloud-not-saving.html
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineUserCloudPtr UserCloud = OnlineSub->GetUserCloudInterface();
		if (UserCloud.IsValid())
		{
			FString path = TEXT("SaveGames/" + SlotName);
			FString savePath = FPaths::GameSavedDir();
			FString fullSavePath = FPaths::Combine(*(FPaths::GameSavedDir()), *path);
			//FFileHelper::SaveStringToFile(TEXT("testi"), *fullSavePath);
			TArray<uint8> FileContents;
			if (FFileHelper::LoadFileToArray(FileContents, *fullSavePath))
			{
				TArray<APlayerController*> PlayerList;
				GEngine->GetAllLocalPlayerControllers(PlayerList);

				for (auto It = PlayerList.CreateIterator(); It; ++It)
				{
					APlayerController* PC = *It;
					if (!PC || !PC->Player || !PC->PlayerInput)
					{
						continue;
					}

					// Update key bindings for the current user only
					UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(PC->Player);
					if (!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
					{
						continue;
					}

					TSharedPtr<const FUniqueNetId> UniqueNetId = LocalPlayer->GetPreferredUniqueNetId();
					//UserCloud->AddOnWriteUserFileCompleteDelegate(OnWriteUserFileCompleteDelegate);
					if (!OnWriteUserFileCompleteDelegateHandle.IsValid())
					{
						OnWriteUserFileCompleteDelegateHandle = UserCloud->AddOnWriteUserFileCompleteDelegate_Handle(FOnWriteUserFileCompleteDelegate::CreateUObject(this, &UGPersistentUser::OnWriteUserFileCompleteDelegate));
					}

					UserCloud->WriteUserFile(*UniqueNetId, SlotName, FileContents);
				}

			}
			else
			{
				//log error
			}
		}

	}
#endif
}

#if 0
void UGPersistentUser::OnWriteUserFileCompleteDelegate(bool bWasSuccessful, const FUniqueNetId& InUserId, const FString& FileName)
{
	UE_LOG(LogOnline, Warning, TEXT("Uploaded to steam cloud: %s"), *FileName);
}
#endif

UGPersistentUser* UGPersistentUser::LoadPersistentUser(FString SlotName, const int32 UserIndex)
{
	UGPersistentUser* Result = nullptr;
	
	// first set of player signins can happen before the UWorld exists, which means no OSS, which means no user names, which means no slotnames.
	// Persistent users aren't valid in this state.
	if (SlotName.Len() > 0)
	{	
		Result = Cast<UGPersistentUser>(UGameplayStatics::LoadGameFromSlot(SlotName, UserIndex));
		if (Result == NULL)
		{
			// if failed to load, create a new one
			Result = Cast<UGPersistentUser>( UGameplayStatics::CreateSaveGameObject(UGPersistentUser::StaticClass()) );
		}
		check(Result != NULL);
	
		Result->SlotName = SlotName;
		Result->UserIndex = UserIndex;
	}

	return Result;
}

void UGPersistentUser::SaveIfDirty()
{
	if (bIsDirty || IsInvertedYAxisDirty() || IsAimSensitivityDirty())
	{
		SavePersistentUser();
	}
}

void UGPersistentUser::AddMatchResult(int32 MatchKills, int32 MatchDeaths, int32 MatchBulletsFired, int32 MatchRocketsFired, bool bIsMatchWinner)
{
	Kills += MatchKills;
	Deaths += MatchDeaths;
	BulletsFired += MatchBulletsFired;
	RocketsFired += MatchRocketsFired;
	
	if (bIsMatchWinner)
	{
		Wins++;
	}
	else
	{
		Losses++;
	}

	bIsDirty = true;
}

void UGPersistentUser::TellInputAboutKeybindings()
{
	TArray<APlayerController*> PlayerList;
	GEngine->GetAllLocalPlayerControllers(PlayerList);

	for (auto It = PlayerList.CreateIterator(); It; ++It)
	{
		APlayerController* PC = *It;
		if (!PC || !PC->Player || !PC->PlayerInput)
		{
			continue;
		}

		// Update key bindings for the current user only
		UGLocalPlayer* LocalPlayer = Cast<UGLocalPlayer>(PC->Player);
		if(!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
		{
			continue;
		}

		//set the aim sensitivity
		for (int32 Idx = 0; Idx < PC->PlayerInput->AxisMappings.Num(); Idx++)
		{
			FInputAxisKeyMapping &AxisMapping = PC->PlayerInput->AxisMappings[Idx];
			if (AxisMapping.AxisName == "Lookup" || AxisMapping.AxisName == "LookupRate" || AxisMapping.AxisName == "Turn" || AxisMapping.AxisName == "TurnRate")
			{
				AxisMapping.Scale = (AxisMapping.Scale < 0.0f) ? -GetAimSensitivity() : +GetAimSensitivity();
			}
		}
		PC->PlayerInput->ForceRebuildingKeyMaps();

		//invert it, and if does not equal our bool, invert it again
		if (PC->PlayerInput->GetInvertAxis("LookupRate") != GetInvertedYAxis())
		{
			PC->PlayerInput->InvertAxis("LookupRate");
		}

		if (PC->PlayerInput->GetInvertAxis("Lookup") != GetInvertedYAxis())
		{
			PC->PlayerInput->InvertAxis("Lookup");
		}
	}
}

int32 UGPersistentUser::GetUserIndex() const
{
	return UserIndex;
}

void UGPersistentUser::SetInvertedYAxis(bool bInvert)
{
	bIsDirty |= bInvertedYAxis != bInvert;

	bInvertedYAxis = bInvert;
}

void UGPersistentUser::SetAimSensitivity(float InSensitivity)
{
	bIsDirty |= AimSensitivity != InSensitivity;

	AimSensitivity = InSensitivity;
}

void UGPersistentUser::SetGamma(float InGamma)
{
	bIsDirty |= Gamma != InGamma;

	Gamma = InGamma;
}

void UGPersistentUser::SetBotsCount(int32 InCount)
{
	bIsDirty |= BotsCount != InCount;

	BotsCount = InCount;
}

void UGPersistentUser::SetIsRecordingDemos(const bool InbIsRecordingDemos)
{
	bIsDirty |= bIsRecordingDemos != InbIsRecordingDemos;

	bIsRecordingDemos = InbIsRecordingDemos;
}

float UGPersistentUser::GetFloatValue(const FString& Name)
{
	for (int i = 0; i < NameValueArray.Num(); ++i)
	{
		if (NameValueArray[i].Name == Name)
		{
			return NameValueArray[i].FloatValue;
		}
	}

	FValueData NameValue;
	return NameValue.FloatValue;
}

void UGPersistentUser::SetFloatValue(const FString& Name, float Value)
{
	bIsDirty = true;

	for (int i = 0; i < NameValueArray.Num(); ++i)
	{
		if (NameValueArray[i].Name == Name)
		{
			NameValueArray[i].FloatValue = Value;
			return;
		}
	}

	FValueData NameValue;
	NameValue.Name = Name;
	NameValue.FloatValue = Value;
	NameValueArray.Add(NameValue);
}
