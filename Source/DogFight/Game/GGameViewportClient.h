// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "DogFight.h"
#include "GGameViewportClient.generated.h"

#if 0
class SShooterConfirmationDialog;

struct FShooterGameLoadingScreenBrush : public FSlateDynamicImageBrush, public FGCObject
{
	FShooterGameLoadingScreenBrush( const FName InTextureName, const FVector2D& InImageSize )
		: FSlateDynamicImageBrush( InTextureName, InImageSize )
	{
		ResourceObject = LoadObject<UObject>( NULL, *InTextureName.ToString() );
	}

	virtual void AddReferencedObjects(FReferenceCollector& Collector)
	{
		if( ResourceObject )
		{
			Collector.AddReferencedObject(ResourceObject);
		}
	}
};

class SShooterLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SShooterLoadingScreen) {}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

private:
	EVisibility GetLoadIndicatorVisibility() const
	{
		return EVisibility::Visible;
	}

	/** loading screen image brush */
	TSharedPtr<FSlateDynamicImageBrush> LoadingScreenBrush;
};
#endif

UCLASS(Within=Engine, transient, config=Engine, BlueprintType)
class UGGameViewportClient : public UGameViewportClient
{
	GENERATED_UCLASS_BODY()

public:

	virtual void PostInitProperties();


	void SetGamepadCount(int32 Count);

	void SetGamepadDisabled(int32 ControllerId);

	/** Gamecontrollers start with 0 and go upwards, but we can shoft them, so it is not handled by the keyboard players for example */
	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	void SetFirstGamepadControllerId(int32 ControllerId);

	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	int32 GetFirstGamepadControllerId();

	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	void MapKeyToController(int32 ControllerId, FKey Key);

	/** Allows us to map keyboard keys to different controllers */
	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	void MapKeysToController(int32 ControllerId, TArray<FKey> KeyArray);

	/** Remap all controls from one device to another **/
	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	void MapControllerToController(int32 FromControllerId, int32 ToControllerId);

	UFUNCTION(BlueprintCallable, Category = "UGGameViewportClient")
	void ClearControllerToControllerMap();
	
	virtual bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
	virtual bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples = 1, bool bGamepad = false) override;

 	// start UGameViewportClient interface
 	void NotifyPlayerAdded( int32 PlayerIndex, ULocalPlayer* AddedPlayer ) override;

	void AddViewportWidgetContent( TSharedRef<class SWidget> ViewportContent, const int32 ZOrder = 0 ) override;
	void RemoveViewportWidgetContent( TSharedRef<class SWidget> ViewportContent ) override;
#if 0
	void ShowDialog(TWeakObjectPtr<ULocalPlayer> PlayerOwner, EShooterDialogType::Type DialogType, const FText& Message, const FText& Confirm, const FText& Cancel, const FOnClicked& OnConfirm, const FOnClicked& OnCancel);
	void HideDialog();

	void ShowLoadingScreen();
	void HideLoadingScreen();

	bool IsShowingDialog() const { return DialogWidget.IsValid(); }

	EShooterDialogType::Type GetDialogType() const;
	TWeakObjectPtr<ULocalPlayer> GetDialogOwner() const;

	TSharedPtr<SShooterConfirmationDialog> GetDialogWidget() { return DialogWidget; }

	//FTicker Funcs
	virtual void Tick(float DeltaSeconds) override;	

#if WITH_EDITOR
	virtual void DrawTransition(class UCanvas* Canvas) override;
#endif //WITH_EDITOR
	// end UGameViewportClient interface

protected:
	void HideExistingWidgets();
	void ShowExistingWidgets();

	/** List of viewport content that the viewport is tracking */
	TArray<TSharedRef<class SWidget>>				ViewportContentStack;

	TArray<TSharedRef<class SWidget>>				HiddenViewportContentStack;

	TSharedPtr<class SWidget>						OldFocusWidget;

	/** Dialog widget to show temporary messages ("Controller disconnected", "Parental Controls don't allow you to play online", etc) */
	TSharedPtr<SShooterConfirmationDialog>			DialogWidget;

	TSharedPtr<SShooterLoadingScreen>				LoadingScreenWidget;
#endif


	int32 AdjustControllerId(int32 ControllerId, FKey& key, bool bGamepad);

	int32 GamepadCount;
	int32 FirstGamepadControllerId;
	TMap<FKey, int32> KeyControllerIdMap;
	TMap<int32, int32> ControllerIdControllerIdMap;
	TMap<int32, int32> DisabledGamepadMap;
};