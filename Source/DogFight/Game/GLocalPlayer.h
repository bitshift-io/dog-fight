// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "DogFight.h"
#include "GLocalPlayer.generated.h"

class UGProfileSettings;

UCLASS(config=Engine, transient)
class UGLocalPlayer : public ULocalPlayer
{
	GENERATED_UCLASS_BODY()

public:

	virtual void SetControllerId(int32 NewControllerId) override;

	class UGPersistentUser* GetPersistentUser() const;

	/** Initializes the PersistentUser */
	void LoadPersistentUser();
	
	using ULocalPlayer::GetViewPoint;
	
#if 0
	virtual void LoadProfileSettings();
	UFUNCTION()
	virtual void SaveProfileSettings();
	virtual void ClearProfileSettings();
#endif

	virtual UGProfileSettings* GetProfileSettings() { return CurrentProfileSettings; };


#if 0
	virtual void SetNickname(FString NewName);

	// Returns the filename for stats.
	static FString GetStatsFilename() { return TEXT("stats.json"); }
#endif

protected:

	/** Persistent user data stored between sessions (i.e. the user's savegame) */
	UPROPERTY()
	class UGPersistentUser* PersistentUser;

	// Holds the current profile settings.  
	UPROPERTY()
	UGProfileSettings* CurrentProfileSettings;

#if 0
	virtual FString GetProfileFilename();
	virtual void ClearProfileWarnResults(TSharedPtr<SCompoundWidget> Widget, uint16 ButtonID);
	virtual void OnReadUserFileComplete(bool bWasSuccessful, const FUniqueNetId& InUserId, const FString& FileName);
	virtual void OnWriteUserFileComplete(bool bWasSuccessful, const FUniqueNetId& InUserId, const FString& FileName);
	virtual void OnDeleteUserFileComplete(bool bWasSuccessful, const FUniqueNetId& InUserId, const FString& FileName);
	virtual void OnEnumerateUserFilesComplete(bool bWasSuccessful, const FUniqueNetId& InUserId);
#endif
};



