// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GGameMode.h"
#include "TeamFreeForAllGameMode.generated.h"

class AGActor;
class ATeamInfo;
class AGAIController;

enum TeamFreeForAllGameState
{
	TFFA_None,
	TFFA_EndRound,
	TFFA_RoundEnded,
	TFFA_ResetRound,
};

/**
 * First player to score MaxKillCount wins. Each player is on his or her own team
 * so its everyone vs everyone
 */
UCLASS()
class DOGFIGHT_API ATeamFreeForAllGameMode : public AGGameMode
{
	GENERATED_BODY()

public:

	ATeamFreeForAllGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);

	virtual void StartNewPlayer(APlayerController* NewPlayer);

	virtual void HandleMatchHasStarted() override;

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType) override;
	
	void SetState(TeamFreeForAllGameState state)	{ m_gameState = state; }

	virtual void Tick(float deltaTime);

	void AutoAddPlayerToTeam(APlayerController * Player);

	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;

	ATeamInfo* GetLeadingTeam() const;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32					MaxKillCount;
	
	TeamFreeForAllGameState		m_gameState;
};
