// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GGameMode.h"
#include "FreeForAllGameMode.generated.h"

class AGActor;
class ATeamInfo;
class AGAIController;

enum FreeForAllGameState
{
	FFA_None,
	FFA_EndRound,
	FFA_RoundEnded,
	FFA_ResetRound,
};

/**
 * First player to score MaxKillCount wins. Each player is on his or her own team
 * so its everyone vs everyone
 */
UCLASS()
class DOGFIGHT_API AFreeForAllGameMode : public AGGameMode
{
	GENERATED_BODY()

public:

	AFreeForAllGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);
	
	/** start match, or let player enter, immediately */
	virtual void StartNewPlayer(APlayerController* NewPlayer);

	virtual void HandleMatchHasStarted() override;

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType) override;
	
	void SetState(FreeForAllGameState state)	{ m_gameState = state; }

	virtual void Tick(float deltaTime);

	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32					MaxKillCount;
	
	FreeForAllGameState		m_gameState;
};
