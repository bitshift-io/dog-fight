// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "GPlayerState.h"

#pragma optimize("", OPTIMISATION)

AGPlayerState::AGPlayerState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	TeamNumber = 0;
	NumKills = 0;
	NumDeaths = 0;
	NumBulletsFired = 0;
	NumRocketsFired = 0;
	bQuitter = false;
	Team = NULL;
	Controller = NULL;
	RabbitTime = 0.f;
	PawnClass = NULL;
}

void AGPlayerState::BeginPlay()
{
	Super::BeginPlay();

	// this seems to be where UT source applies profile settings
	AGPlayerController* PC = Cast<AGPlayerController>(GetOwner());
	if (PC)
	{
		UGLocalPlayer* LP = Cast<UGLocalPlayer>(PC->Player);
		if (LP)
		{
			UGProfileSettings* Settings = LP->GetProfileSettings();
			if (Settings)
			{
				PlayerColour = Settings->PlayerColour;
				ServerSetPlayerColour(Settings->PlayerColour);

				PlayerColourName = Settings->PlayerColourName;
				ServerSetPlayerColourName(Settings->PlayerColourName);
				
				ServerSetPawnClass(Settings->DefaultPawnClassForController);
				PawnClass = Settings->DefaultPawnClassForController;
			}
		}
	}
}

void AGPlayerState::Reset()
{
	Super::Reset();
	
	//PlayerStates persist across seamless travel.  Keep the same teams as previous match.
	//SetTeamNum(0);
	NumKills = 0;
	NumDeaths = 0;
	NumBulletsFired = 0;
	NumRocketsFired = 0;
	bQuitter = false;
	Team = NULL;
	RabbitTime = 0.f;
	PawnClass = NULL;
}

void AGPlayerState::UnregisterPlayerWithSession()
{
	if (!bFromPreviousLevel)
	{
		Super::UnregisterPlayerWithSession();
	}
}

void AGPlayerState::ClientInitialize(class AController* InController)
{
	Super::ClientInitialize(InController);

	Controller = InController;
	UpdateColors();
}

#if 0
void AGPlayerState::SetTeam(ATeamInfo* team)
{
	// remove from existing team
	if (Team)
		Team->TeamMembers.Remove(Controller);

	Team = team;

	if (Team)
		Team->TeamMembers.Add(Controller);

	UpdateColors();
}
#endif

bool AGPlayerState::ServerSetPlayerColour_Validate(const FLinearColor& colour)
{
	return true;
}

void AGPlayerState::ServerSetPlayerColour_Implementation(const FLinearColor& colour)
{
	PlayerColour  = colour;

	UpdateColors();
}

void AGPlayerState::OnRep_PlayerColour()
{
	UpdateColors();
}

bool AGPlayerState::ServerSetPlayerName_Validate(const FString& Name)
{
	return true;
}

void AGPlayerState::ServerSetPlayerName_Implementation(const FString& Name)
{
	SetPlayerName(Name);
}

bool AGPlayerState::ServerSetPlayerColourName_Validate(const FString& Name)
{
	return true;
}

void AGPlayerState::ServerSetPlayerColourName_Implementation(const FString& Name)
{
	PlayerColourName = Name;
}

void AGPlayerState::SetTeamNum(int32 NewTeamNumber)
{
	TeamNumber = NewTeamNumber;

	UpdateColors();
}

void AGPlayerState::OnRep_TeamColor()
{
	UpdateColors();
}

void AGPlayerState::AddBulletsFired(int32 NumBullets)
{
	NumBulletsFired += NumBullets;
}

void AGPlayerState::AddBulletsHit(int32 NumHit)
{
	NumBulletsHit += NumHit;
}

void AGPlayerState::AddRocketsFired(int32 NumRockets)
{
	NumRocketsFired += NumRockets;
}

void AGPlayerState::SetQuitter(bool bInQuitter)
{
	bQuitter = bInQuitter;
}

void AGPlayerState::CopyProperties(class APlayerState* PlayerState)
{	
	Super::CopyProperties(PlayerState);

	AGPlayerState* GPlayer = Cast<AGPlayerState>(PlayerState);
	if (GPlayer)
	{
		GPlayer->TeamNumber = TeamNumber;
	}	
}

void AGPlayerState::UpdateColors()
{
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AGActor* P = Cast<AGActor>(*It);
		if (P != NULL && P->PlayerState == this)
		{
			P->UpdateColors();
		}
	}
}

int32 AGPlayerState::GetTeamNum() const
{
	return TeamNumber;
}

int32 AGPlayerState::GetKills() const
{
	return NumKills;
}

int32 AGPlayerState::GetNumBulletsHit() const
{
	return NumBulletsHit;
}

int32 AGPlayerState::GetDeaths() const
{
	return NumDeaths;
}

float AGPlayerState::GetScore() const
{
	return Score;
	/*

	AGGameState* GS = Cast<AGGameState>(GetWorld()->GameState);

	// factor modifier to score for each wave
	// up to 100 points for high accuracy
	// up to 100 points for no deaths
	// up to 100 points for pickups
	// 1000 points per wave

	int accuracyScore = GetNumBulletsFired() <= 0 ? 100.0f :  (float(GetNumBulletsHit()) / FMath::Max<float>(1.f, float(GetNumBulletsFired())) * 100.f);
	int pickupScore = ((1.f - (1.f / FMath::Max<float>(1.f, float(GetNumPickups()) * 0.3f))) * 100.f);
	int deathsScore = 100.f / FMath::Max<float>(1.f, float(GetDeaths()));
	int waveScore = GS->WaveCount * 1000.0f;
	int32 score = (accuracyScore + pickupScore + deathsScore + waveScore);
	return (float)score;*/
}

int32 AGPlayerState::GetNumBulletsFired() const
{
	return NumBulletsFired;
}

int32 AGPlayerState::GetNumRocketsFired() const
{
	return NumRocketsFired;
}

int32 AGPlayerState::GetNumPickups() const
{
	return NumPickups;
}


bool AGPlayerState::IsQuitter() const
{
	return bQuitter;
}

void AGPlayerState::ScoreKill(AGPlayerState* Victim, int32 Points)
{
	NumKills++;
	ScorePoints(Points);
}

void AGPlayerState::ScoreDeath(AGPlayerState* KilledBy, int32 Points)
{
	NumDeaths++;
	ScorePoints(Points);
}

void AGPlayerState::ScorePoints(int32 Points)
{
	/*
	AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
	if (MyGameState && TeamNumber >= 0)
	{
		if (TeamNumber >= MyGameState->TeamScores.Num())
		{
			MyGameState->TeamScores.AddZeroed(TeamNumber - MyGameState->TeamScores.Num() + 1);
		}

		MyGameState->TeamScores[TeamNumber] += Points;
	}

	Score += Points;*/
}

/*
void AGPlayerState::SetScore(float score)
{
	Score = score;
}*/

void AGPlayerState::InformAboutKill_Implementation(class AGPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AGPlayerState* KilledPlayerState)
{
#if 0
	//id can be null for bots
	if (KillerPlayerState->UniqueId.IsValid())
	{	
		//search for the actual killer before calling OnKill()	
		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{		
			AGPlayerController* TestPC = Cast<AGPlayerController>(*It);
			if (TestPC && TestPC->IsLocalController())
			{
				// a local player might not have an ID if it was created with CreateDebugPlayer.
				ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(TestPC->Player);
				TSharedPtr<FUniqueNetId> LocalID = LocalPlayer->GetCachedUniqueNetId();
				if (LocalID.IsValid() &&  *LocalPlayer->GetCachedUniqueNetId() == *KillerPlayerState->UniqueId)
				{			
					TestPC->OnKill();
				}
			}
		}
	}
#endif
}

void AGPlayerState::BroadcastDeath_Implementation(class AGPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AGPlayerState* KilledPlayerState)
{	
#if 0
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		// all local players get death messages so they can update their huds.
		AGPlayerController* TestPC = Cast<AGPlayerController>(*It);
		if (TestPC && TestPC->IsLocalController())
		{
			TestPC->OnDeathMessage(KillerPlayerState, this, KillerDamageType);				
		}
	}	
#endif
}

void AGPlayerState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(AGPlayerState, TeamNumber );
	DOREPLIFETIME(AGPlayerState, NumKills );
	DOREPLIFETIME(AGPlayerState, NumDeaths );
	DOREPLIFETIME(AGPlayerState, Team);
	DOREPLIFETIME(AGPlayerState, NumPickups);
	DOREPLIFETIME(AGPlayerState, PlayerColour);
	DOREPLIFETIME(AGPlayerState, PawnClass);
	DOREPLIFETIME(AGPlayerState, PlayerColourName);	
	DOREPLIFETIME(AGPlayerState, NumBulletsFired);
	DOREPLIFETIME(AGPlayerState, NumBulletsHit);
	DOREPLIFETIME(AGPlayerState, RabbitTime);	
}

FString AGPlayerState::GetShortPlayerName() const
{
#if 0
	if( PlayerName.Len() > MAX_PLAYER_NAME_LENGTH )
	{
		return PlayerName.Left(MAX_PLAYER_NAME_LENGTH) + "...";
	}
#endif
	return PlayerName;
}

#if 0
void AGPlayerState::OnRep_Team(/*ATeamInfo* oldTeam*/)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("AGPlayerState::OnRep_Team"));
	/*
	// undo the change to the variable and apply it correctly
	ATeamInfo* team = Team;
	Team = oldTeam;
	SetTeam(team);*/
}
#endif

ATeamInfo* AGPlayerState::GetTeam() const
{
	return Team;
}

void AGPlayerState::GetTeam(ATeamInfo*& ATeam)
{
	ATeam = Team;
}

void AGPlayerState::NotifyTeamChanged_Implementation()
{
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AGActor* P = Cast<AGActor>(*It);
		if (P != NULL && P->PlayerState == this)
		{
			P->NotifyTeamChanged();
		}
	}
}

FLinearColor AGPlayerState::GetTeamColour() const
{
	if (!Team)
		return GetPlayerColour();

	return Team->TeamColor;
}

FLinearColor AGPlayerState::GetPlayerColour() const
{
	return PlayerColour;
}



bool AGPlayerState::ServerSetPawnClass_Validate(UClass* Class)
{
	return true;
}

void AGPlayerState::ServerSetPawnClass_Implementation(UClass* Class)
{
	if (PawnClass == Class)
	{
		return;
	}

	PawnClass = Class;

	// replace existing pawn with the new class pawn
	{
		AGGameMode* const GameMode = Cast<AGGameMode>(GetWorld()->GetAuthGameMode());

		AController* OwningController = Cast<AController>(GetOwner());
		if (OwningController != NULL && OwningController->GetPawn() != NULL)
		{
			GameMode->ReplacePlayerPawn(OwningController, PawnClass);
		}
	}

	/*
	//ServerRestartPlayer();
	{
	AGameMode* const GameMode = GetWorld()->GetAuthGameMode();
	if (!GetWorld()->GetAuthGameMode()->PlayerCanRestart(this))
	{
	return;
	}

	// If we're still attached to a Pawn, leave it
	if (GetPawn() != NULL)
	{
	APawn* P = GetPawn();
	UnPossess();
	P->Destroy(); // client still sees the spawn ring.....BUG
	}

	GameMode->RestartPlayer(this);
	}*/
}

void AGPlayerState::SetScore(float S)
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	Score = S;
	SendScoreToAllClients(Score);
}

bool AGPlayerState::SendScoreToAllClients_Validate(float S)
{
	return true;
}

void AGPlayerState::SendScoreToAllClients_Implementation(float S)
{
	Score = S;
}