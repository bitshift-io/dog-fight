// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "MenuGameMode.h"

AMenuGameMode::AMenuGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//PlayerControllerClass = NULL; // AShooterPlayerController_Menu::StaticClass();
}

void AMenuGameMode::RestartPlayer(class AController* NewPlayer)
{
	// don't restart
}

/** Returns game session class to use */
TSubclassOf<AGameSession> AMenuGameMode::GetGameSessionClass() const
{
	return AGGameSession::StaticClass();
}
