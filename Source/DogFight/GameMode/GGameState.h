// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GGameState.generated.h"

class AGPlayerState;

/** ranked PlayerState map, created from the GameState */
typedef TMap<int32, TWeakObjectPtr<AGPlayerState> > RankedPlayerMap;

UCLASS()
class AGGameState : public AGameState
{
	GENERATED_UCLASS_BODY()

public:

	/** TODO: make a single instance visible at once only! */
	UFUNCTION(reliable, NetMulticast)
	void ShowWave(int32 wave);

	/** TODO: make a single instance visible at once only! */
	UFUNCTION(reliable, NetMulticast)
	void ShowMessage(const FString& message);

	/** TODO: make a single instance visible at once only! */
	UFUNCTION(reliable, NetMulticast)
	void ShowScoreboard(const FString& WinMessage);

	/** replicate team colors. Updated the players mesh colors appropriately */
	UFUNCTION()
	void OnRep_Teams();

	UFUNCTION()
	void OnRep_TeamTagsVisible();

	UFUNCTION()
	void OnRep_ScoreboardClass();

	/** time left for warmup / match */
	UPROPERTY(Transient, Replicated)
	int32 RemainingTime;

	UPROPERTY(Transient, Replicated)
	FString Achieve10GamesName;

	/** is timer paused? */
	UPROPERTY(Transient, Replicated)
	bool bTimerPaused;
	
	// ut doesnt even replicate this team list
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Teams)
	TArray<ATeamInfo*>			Teams;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_TeamTagsVisible)
	bool bTeamTagsVisible;

	/** message class */
	UPROPERTY(Transient, Replicated)
	TSubclassOf<class AActor> MessageClass;

	/** scoreboard class */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_ScoreboardClass)
	TSubclassOf<class UScoreboard> ScoreboardClass;

	UScoreboard* ScoreboardWidget;

	/** wave class */
	UPROPERTY(Transient, Replicated)
	TSubclassOf<class AActor> WaveClass;

	UPROPERTY(Transient, Replicated)
	int32	WaveCount;

	UPROPERTY(Transient, Replicated)
	FString GameModeName;

	/** Used for parachute and pickups that want to move down the screen, provides a quaterion
	that can be used to give actors moving down the screen a rotation on the actor plane **/
	virtual FQuat	ActorViewRotation() const;

	/** Return true if the position it outside the camera frustum, will then modify the position to wrap around and return true in this case **/
	virtual bool WrapAround(FVector& worldPosition, float radius);

	/** gets ranked PlayerState map for specific team */
	void GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const;

	void RequestFinishAndExitToMainMenu();

	/**
	Find a random location on the actor  play area
	worldPostion.Z will not be modified
	**/
	virtual bool	ActorRandomLocation(FVector& worldPosition);
};
