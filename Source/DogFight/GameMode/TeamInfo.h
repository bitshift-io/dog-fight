#pragma once

#include "TeamInfo.generated.h"

UCLASS() //EditInlineNew, BlueprintType, Blueprintable)
class ATeamInfo : public AInfo
{
	GENERATED_BODY()
public:

	ATeamInfo(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = Team)
	virtual void AddToTeam(AController* C);

	UFUNCTION(BlueprintCallable, Category = Team)
	virtual void RemoveFromTeam(AController* C);

	UFUNCTION(BlueprintCallable, Category = Team)
	virtual void RemoveAllFromTeam();

	/** get number of kills */
	int32 GetKills() const;

	UFUNCTION()
	virtual void ReceivedTeamIndex();

	UFUNCTION()
	virtual void OnRep_TeamColor();

	int					GetPawnAliveCount();

	//int32				PickupCount;
	//int32				KillCount;

	/** Team lives **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Team)
	int32				RemainingLives;

	/** list of players on this team currently (server only) */
	UPROPERTY(BlueprintReadOnly, Category = Team)
	TArray<AController*> TeamMembers;

	//list<CController*>	mController;
	//MaterialBase*		mMaterialBase;

	/** Team colour **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_TeamColor, Category = Team)
	FLinearColor		TeamColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Team)
	FText TeamName;

	/** team ID, set by UTTeamGameMode */
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = ReceivedTeamIndex, Category = Team)
	uint8 TeamIndex;

	/** internal flag used to prevent the wrong TeamInfo from getting hooked up on clients during seamless travel, because it is possible for two sets to be on the client temporarily */
	UPROPERTY(Replicated)
	bool bFromPreviousLevel;
};
