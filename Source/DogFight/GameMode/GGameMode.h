// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "GGameMode.generated.h"

class UScoreboard;

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FTakeDamageSignature, class AActor*, Receiver, float, Damage, struct FDamageEvent const&, DamageEvent, class AController*, EventInstigator, class AActor*, DamageCauser);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FKilledSignature, AController*, Killer, AController*, KilledPlayer, APawn*, KilledPawn, const UDamageType*, DamageType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInitGameSignature);

/**
* https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AGameMode/index.html
*/
UCLASS(config = Game)
class DOGFIGHT_API AGGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()

public:

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);

	virtual void EndRound();

	/** check if PlayerState is a winner */
	virtual bool IsWinner(class AGPlayerState* PlayerState) const;
	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;

	/** Cheat to kill AI */
	UFUNCTION(exec)
	void KillAI();

	/** Cheat to disable AI */
	UFUNCTION(exec)
	void DisableAI();

	bool IsAIDisabled();

	virtual AActor* ChoosePlayerStart_Implementation(AController* Player);

	/**
	* Initialize the GameState actor with default settings
	* called during PreInitializeComponents() of the GameMode after a GameState has been spawned
	* as well as during Reset()
	*/
	virtual void InitGameState();
	virtual void InitTeams();

	UFUNCTION(exec)
	void ShowMessage(const FString& message);

	UFUNCTION(exec)
	void ShowScoreboard();
	
	//virtual int32 CalculateScore(AController* Player);
	//virtual void PopulateScores(UScoreboard* Scoreboard, TArray<AController*>& PlayerControllerList);

	virtual APlayerController* SpawnPlayerController(ENetRole RemoteRole, FVector const & SpawnLocation, FRotator const & SpawnRotation) override;

	/** Called when a Controller with a PlayerState leaves the match. */
	virtual void Logout(AController* Exiting);

	/** control pickups */
	virtual bool CanBePickedUp(class APickup* Pickup, class AGActor* Pawn) const;

	/** prevents friendly fire */
	virtual float ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const;

	/** notify about kills */
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType);

	/** can players damage each other? */
	virtual bool CanDealDamage(class AGPlayerState* DamageInstigator, class AGPlayerState* DamagedPlayer) const;

	using AGameMode::TakeDamage;

	/** Does end of game handling for the online layer */
	virtual void RestartPlayer(AController* NewPlayer) override;
	//virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	virtual APawn* SpawnDefaultPawnFor_Implementation(AController* NewPlayer, AActor* StartSpot) override;
	
	/** Deletes NewPlayer's Pawn and replaces it with a new pawn of type PawnClass */
	void ReplacePlayerPawn(AController* NewPlayer, UClass* PawnClass);

	/**
	* Make sure pawn properties are back to default
	* Also a good place to modify them on spawn
	*/
	virtual void SetPlayerDefaults(APawn* PlayerPawn);

	/** get the name of the bots count option used in server travel URL */
	static FString GetBotsCountOptionName();

	/** Returns game session class to use */
	virtual TSubclassOf<AGameSession> GetGameSessionClass() const override;

	/**
	* Return the 'best' player start for this player to start from.
	* Default implementation just returns the first PlayerStart found.
	* @param Player is the controller for whom we are choosing a playerstart
	* @returns AActor chosen as player start (usually a PlayerStart)
	*/
	//virtual class AActor* ChoosePlayerStart_ImplementationInternal(AController* Player);

	class AGGameState* GetGameState() const;

	/*Finishes the match and bumps everyone to main menu.*/
	/*Only GameInstance should call this function */
	void RequestFinishAndExitToMainMenu();


	APlayerStart* FirstPlayerStart();

	void RestartPlayerControllersWithNoPawn();

	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController);


	/** SetViewTarget of player control on server change */
	//virtual void SetSeamlessTravelViewTarget(APlayerController* PC);

	//UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Killed", FriendlyName = "On Killed"))
	//virtual void OnKilled(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType);

	UPROPERTY(BlueprintAssignable)
	FKilledSignature OnKilled;

	UPROPERTY(BlueprintAssignable)
	FInitGameSignature OnInitGame;

	UPROPERTY()
	TArray<class APickup*> LevelPickups;

	//UPROPERTY(BlueprintAssignable)
	//FTakeDamageSignature OnTakeDamage;


	UPROPERTY(BlueprintReadOnly, Category = TeamGame)
	TArray<class ATeamInfo*> Teams;

	/** colors assigned to the teams */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = TeamGame)
	TArray<FLinearColor> TeamColors;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = TeamGame)
	TArray<FText> TeamNames;

	/** number of teams to create - set either in defaults or via InitGame() */
	UPROPERTY(EditDefaultsOnly, Category = TeamGame)
	uint8 NumTeams;

	/** class of TeamInfo to spawn */
	UPROPERTY(EditDefaultsOnly, Category = TeamGame)
	TSubclassOf<ATeamInfo> TeamClass;

	UPROPERTY(EditDefaultsOnly, Category = TeamGame)
	bool bTeamTagsVisible;

	UPROPERTY(EditDefaultsOnly, Category = GameMode)
	FString GameModeName;

	/** message class */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	TSubclassOf<class AActor> MessageClass;

	/** scoreboard class */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	TSubclassOf<class UScoreboard> ScoreboardClass;

	UPROPERTY(EditDefaultsOnly, Category = Score)
	uint32 KillScore;

	UPROPERTY(EditDefaultsOnly, Category = Score)
	uint32 DeathScore;

	UPROPERTY(EditDefaultsOnly, Category = Achivement)
	FString Achieve10GamesName;

	/** wave class - for invasion gamemode */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	TSubclassOf<class AActor> WaveClass;
};