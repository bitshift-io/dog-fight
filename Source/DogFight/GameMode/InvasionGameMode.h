// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GGameMode.h"
#include "InvasionGameMode.generated.h"

class AGActor;
class ATeam;
class AGAIController;

enum InvasionTeam
{
	IT_Player,
	IT_Enemy,
};

enum InvasionGameState
{
	IGS_None,
	IGS_ResetNextWave,
	IGS_EndRound,
	IGS_RoundEnded,
};

USTRUCT()
struct FEnemyStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AGAIController> AIControllerClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AGActor>		ActorClass;
};

/**
 * https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AGameMode/index.html
 */
UCLASS()
class DOGFIGHT_API AInvasionGameMode : public AGGameMode
{
	GENERATED_BODY()

public:

	AInvasionGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);

	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;

	/**
	* Initialize the GameState actor with default settings
	* called during PreInitializeComponents() of the GameMode after a GameState has been spawned
	* as well as during Reset()
	*/
	virtual void InitGameState();

	virtual void StartNewPlayer(APlayerController* NewPlayer);
	
	virtual void HandleMatchHasStarted() override;

	//void SubmitHighscore();
	//virtual int32 CalculateScore(AController* Player);

	UFUNCTION(exec)
	void ResetNextWave();

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void StartWave(int32 Wave);

	/** Cheat to skip to certain wave **/
	UFUNCTION(exec)
	void GoWave(int32 wave);

	/** Cheat to change remaining player lives **/
	UFUNCTION(exec)
	void SetPlayerLivesRemaining(int32 lives);

	/** Cheat to end the round **/
	UFUNCTION(exec)
	void GoEndRound();

	/** Take damage, handle death */
	//virtual void TakeDamage(class AActor* Receiver, float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType) override;

	void SetState(InvasionGameState state)	{ m_gameState = state;  }

	virtual void Tick(float deltaTime);

	//void EndRound();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FEnemyStruct>	EnemyClassList;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32					InitialTeamLives;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float					EnemyRespawnDelay;


	float					m_curEnemyRespawnDelay;
	FDateTime				m_startTime;

	InvasionGameState		m_gameState;
};
