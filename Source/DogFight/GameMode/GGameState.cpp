// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "GGameState.h"
#include "GPlayerState.h"
#include "Kismet/KismetMathLibrary.h"
#include "Actor/GActor.h"

#pragma optimize("", OPTIMISATION)

AGGameState::AGGameState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RemainingTime = 0;
	bTimerPaused = false;
	bTeamTagsVisible = false;
	ScoreboardWidget = NULL;
	WaveCount = 0;
}

void AGGameState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(AGGameState, RemainingTime );
	DOREPLIFETIME(AGGameState, bTimerPaused );
	DOREPLIFETIME(AGGameState, Teams);
	DOREPLIFETIME(AGGameState, bTeamTagsVisible);
	DOREPLIFETIME(AGGameState, MessageClass);
	DOREPLIFETIME(AGGameState, ScoreboardClass);
	DOREPLIFETIME(AGGameState, WaveCount);	
	DOREPLIFETIME(AGGameState, GameModeName);	
	DOREPLIFETIME(AGGameState, WaveClass);
	DOREPLIFETIME(AGGameState, Achieve10GamesName);
}

void AGGameState::GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const
{
	OutRankedMap.Empty();

	//first, we need to go over all the PlayerStates, grab their score, and rank them
	TMultiMap<int32, AGPlayerState*> SortedMap;
	for(int32 i = 0; i < PlayerArray.Num(); ++i)
	{
		int32 Score = 0;
		AGPlayerState* CurPlayerState = Cast<AGPlayerState>(PlayerArray[i]);
		if (CurPlayerState && (CurPlayerState->GetTeamNum() == TeamIndex))
		{
			SortedMap.Add(FMath::TruncToInt(CurPlayerState->Score), CurPlayerState);
		}
	}

	//sort by the keys
	SortedMap.KeySort(TGreater<int32>());

	//now, add them back to the ranked map
	OutRankedMap.Empty();

	int32 Rank = 0;
	for(TMultiMap<int32, AGPlayerState*>::TIterator It(SortedMap); It; ++It)
	{
		OutRankedMap.Add(Rank++, It.Value());
	}
	
}


void AGGameState::RequestFinishAndExitToMainMenu()
{
	if (AuthorityGameMode)
	{
		// we are server, tell the gamemode
		AGGameMode* const GameMode = Cast<AGGameMode>(AuthorityGameMode);
		if (GameMode)
		{
			GameMode->RequestFinishAndExitToMainMenu();
		}
	}
	else
	{
		// we are client, handle our own business
		UGGameInstance* GI = Cast<UGGameInstance>(GetGameInstance());
		if (GI)
		{
			GI->RemoveSplitScreenPlayers();
		}

		AGPlayerController* const PrimaryPC = Cast<AGPlayerController>(GetGameInstance()->GetFirstLocalPlayerController());
		if (PrimaryPC)
		{
			check(PrimaryPC->GetNetMode() == ENetMode::NM_Client);
			PrimaryPC->HandleReturnToMainMenu();
		}
	}
}

FQuat AGGameState::ActorViewRotation() const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	FSceneView* SceneView = NULL;

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = *Iterator;
		if (PlayerController)
		{
			ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PlayerController->Player);
			if (LocalPlayer && LocalPlayer->ViewportClient && LocalPlayer->ViewportClient->Viewport)
			{
				// Create a view family for the game viewport
				FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
					LocalPlayer->ViewportClient->Viewport,
					LocalPlayer->GetWorld()->Scene,
					LocalPlayer->ViewportClient->EngineShowFlags)
					.SetRealtimeUpdate(true));

				SceneView = LocalPlayer->CalcSceneView(&ViewFamily, /*out*/ ViewLocation, /*out*/ ViewRotation, LocalPlayer->ViewportClient->Viewport);
				if (SceneView)
				{
					break;
				}
			}
		}
	}

	// causes parachute sideways issues
	if (!SceneView)
	{
		return FQuat::Identity;
	}


	// x = forward
	// y = right
	// z = up

	FQuat q = ViewRotation.Quaternion();

	// just set this to up, coming out of the actor plane
	FVector x = FVector::UpVector;

	// get the camera up axis, and remove the Z component squashing it onto the actor plane
	FVector z2 = q.GetAxisZ();
	FVector z = z2;
	z.Z = 0.0f;
	z.Normalize();

	// get the camera right axis, and remove the Z component squashing it onto the actor plane
	FVector y2 = q.GetAxisY();
	FVector y = y2;
	y.Z = 0.0f;
	y.Normalize();

	FMatrix m(z, y, x, FVector::ZeroVector);
	return FQuat(m);
}



bool TestPlaneWrap(FSceneView* sceneView, FPlane& plane, FPlane& oppositePlane, FVector& worldPosition, float radius, bool horizontalMirror, const FPlane& actorPlane)
{
	float worldPosZ = worldPosition.Z;

	// see what side of the plane we are on
	float distToPlane = plane.PlaneDot(worldPosition) - radius;
	if (distToPlane < 0)
	{
		return false;
	}


	FVector worldPosWorldSpace;
	if (horizontalMirror)
	{
		// mirror the horizontal component
		worldPosWorldSpace = worldPosition.MirrorByPlane(FPlane(sceneView->ViewLocation, sceneView->ViewRotation.Quaternion().GetAxisY()));
	}
	else
	{
		// mirror the vertical component
		worldPosWorldSpace = worldPosition.MirrorByPlane(FPlane(sceneView->ViewLocation, sceneView->ViewRotation.Quaternion().GetAxisZ()));
	}

	// push this position down to ensure plane intersection test will occur
	worldPosWorldSpace += (worldPosWorldSpace - sceneView->ViewLocation);

	// find intersection with the actor movement plane
	FVector intersectPos;
	bool insersect = FMath::SegmentPlaneIntersection(sceneView->ViewLocation, worldPosWorldSpace, actorPlane, intersectPos);

	worldPosition = intersectPos;
	worldPosition.Z = worldPosZ; // intersectPos may not be calculated perfectly

	// bring back the world position into the constrained play area
	float distToOppositePlane = oppositePlane.PlaneDot(worldPosition) - radius;
	FVector oppositePlaneNormal = oppositePlane.GetSafeNormal();
	check(oppositePlaneNormal.Z == 0.0f);

	worldPosition = worldPosition - (oppositePlaneNormal * distToOppositePlane);
	check(worldPosition.Z == worldPosZ);
	return true;
}

/*
Because the planes in sceneView and FConvexVolume are camera planes, they are not square (perpendicular) to the actor playing field which they need to be for
TestPlaneWrap to work correctly

This method squares up the camera planes
*/
void SquarePlanes(FConvexVolume& convexVolume, const FPlane& actorPlane)
{
	for (int i = 0; i < 4; ++i)
	{
		// find a point on the acotor plane that is also on the camera plane
		FVector point;
		FVector dir;
		bool ok = FMath::IntersectPlanes2(point, dir, convexVolume.Planes[i], actorPlane);

		// project the camera plane normal onto the actor plane
		FVector projectedNormal = FVector::VectorPlaneProject(convexVolume.Planes[i].GetSafeNormal(), actorPlane);
		projectedNormal.Normalize();
		check(projectedNormal.Z == 0.0f);

		// set up the squared up plane
		convexVolume.Planes[i] = FPlane(point, projectedNormal);
	}
}

void DebugDrawPlayArea(const UWorld* world, FConvexVolume& convexVolume, const FPlane& actorPlane)
{
	// top left
	FVector topLeft;
	bool ok = FMath::IntersectPlanes3(topLeft, actorPlane, convexVolume.Planes[0], convexVolume.Planes[2]);

	// top right
	FVector topRight;
	ok = FMath::IntersectPlanes3(topRight, actorPlane, convexVolume.Planes[1], convexVolume.Planes[2]);

	// bottom left
	FVector bottomLeft;
	ok = FMath::IntersectPlanes3(bottomLeft, actorPlane, convexVolume.Planes[0], convexVolume.Planes[3]);

	// bottom right
	FVector bottomRight;
	ok = FMath::IntersectPlanes3(bottomRight, actorPlane, convexVolume.Planes[1], convexVolume.Planes[3]);

	DrawDebugLine(world, topLeft, topRight, FColor(255, 0, 0), false, -1, 0, 100);
	DrawDebugLine(world, topRight, bottomRight, FColor(255, 0, 0), false, -1, 0, 100);
	DrawDebugLine(world, bottomLeft, bottomRight, FColor(255, 0, 0), false, -1, 0, 100);
	DrawDebugLine(world, bottomRight, topRight, FColor(255, 0, 0), false, -1, 0, 100);
}

bool AGGameState::WrapAround(FVector& worldPosition, float radius)
{
	UGameInstance* ge = GetGameInstance();
	check(ge);
	UGLocalPlayer * LocalPlayer = Cast<UGLocalPlayer>(ge->GetFirstGamePlayer());
	if (!LocalPlayer)
	{
		return false;
	}

	// http://blakelivingston.github.io/blog/2014/10/11/snapping-points-into-screen-boundaries-in-unreal-engine-4/
	check(LocalPlayer->ViewportClient);
	check(LocalPlayer->ViewportClient->Viewport);


	// This sceneview initialization should probably be moved into another function.
	// Create a view family for the game viewport
	FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
		LocalPlayer->ViewportClient->Viewport,
		LocalPlayer->GetWorld()->Scene,
		LocalPlayer->ViewportClient->EngineShowFlags)
		.SetRealtimeUpdate(true));
	FVector ViewLocation;
	FRotator ViewRotation;
	FSceneView* SceneView = LocalPlayer->CalcSceneView(&ViewFamily, /*out*/ ViewLocation, /*out*/ ViewRotation, LocalPlayer->ViewportClient->Viewport);
	if (!SceneView)
		return false;

	FPlane actorPlane(FVector(0, 0, worldPosition.Z), FVector::UpVector); // use actor Z

	// draw actor before warp
	//DrawDebugSphere(LocalPlayer->GetWorld(), worldPosition, radius, 12, FColor(255, 0, 0));

	// TODO: cache this?
	FConvexVolume result;
	bool bUseNearPlane = false;
	GetViewFrustumBounds(result, SceneView->ViewProjectionMatrix, bUseNearPlane);
	SquarePlanes(result, actorPlane);
	//DebugDrawPlayArea(LocalPlayer->GetWorld(), result);

	// left plane
	if (TestPlaneWrap(SceneView, result.Planes[0], result.Planes[1], worldPosition, radius, true, actorPlane))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Wrap left plane"));
		return true;
	}

	// right plane
	if (TestPlaneWrap(SceneView, result.Planes[1], result.Planes[0], worldPosition, radius, true, actorPlane))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Wrap right plane"));
		return true;
	}

	// top plane
	if (TestPlaneWrap(SceneView, result.Planes[2], result.Planes[3], worldPosition, radius, false, actorPlane))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Wrap top plane"));
		return true;
	}

	// bottom plane
	if (TestPlaneWrap(SceneView, result.Planes[3], result.Planes[2], worldPosition, radius, false, actorPlane))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Wrap bottom plane"));
		return true;
	}

	return false;
}

bool AGGameState::ActorRandomLocation(FVector& worldPosition)
{
	FPlane actorPlane(FVector(0, 0, worldPosition.Z), FVector::UpVector); // use actor Z

	UGameInstance* ge = GetGameInstance();
	check(ge);
	UGLocalPlayer * LocalPlayer = Cast<UGLocalPlayer>(ge->GetFirstGamePlayer());
	if (!LocalPlayer)
	{
		return false;
	}

	// This sceneview initialization should probably be moved into another function.
	// Create a view family for the game viewport
	FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
		LocalPlayer->ViewportClient->Viewport,
		LocalPlayer->GetWorld()->Scene,
		LocalPlayer->ViewportClient->EngineShowFlags)
		.SetRealtimeUpdate(true));
	FVector ViewLocation;
	FRotator ViewRotation;
	FSceneView* SceneView = LocalPlayer->CalcSceneView(&ViewFamily, /*out*/ ViewLocation, /*out*/ ViewRotation, LocalPlayer->ViewportClient->Viewport);
	if (!SceneView)
	{
		return false;
	}

	check(SceneView);


	// generate a vector between 0 to 1 then convert to screen space (0 -> screen width, 0 -> screen height)
	// convert from screen space to world space

	float x = FMath::FRandRange(0.0f, 1.0f);
	float y = FMath::FRandRange(0.0f, 1.0f);
	
	FVector worldOrigin;
	FVector worldDirection;
	FVector2D screenPosition(SceneView->ViewRect.Min.X + (x * SceneView->ViewRect.Width()), SceneView->ViewRect.Min.Y + (y * SceneView->ViewRect.Height()));
	SceneView->DeprojectFVector2D(screenPosition, worldOrigin, worldDirection);
	

	float farClipPlane = 1000000.0f; // TODO: dont know how to get this atm
	//float time;
	//FVector worldPosition;

	// Find intersection
	FVector intersection = FMath::LinePlaneIntersection(worldOrigin, worldOrigin + (worldDirection * farClipPlane), actorPlane);
	if (intersection.ContainsNaN())
		return false;

	worldPosition = FVector(intersection.X, intersection.Y, worldPosition.Z);

	return true;
}

void AGGameState::OnRep_Teams()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AGGameState::OnRep_Teams"));
}

void AGGameState::OnRep_TeamTagsVisible()
{
	for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
	{
		AGActor* Actor = Cast<AGActor>(*Iterator);
		if (Actor)
		{
			Actor->SetTeamTagVisible(bTeamTagsVisible);
		}
	}
}

void AGGameState::OnRep_ScoreboardClass()
{
	// same type of thing as done in the player controller for the ingame menu,
	// this code should probably be in the player controller itself
	// so PC->SetupScoreboard(blah)

	/*
	UGameInstance* ge = GetGameInstance();
	if (!ge)
	{
		return;
	}*/

	//AGPlayerController* PC = ge ? Cast<AGPlayerController>(ge->GetFirstLocalPlayerController()) : NULL;
	//if (PC)
	{
		ScoreboardWidget = CreateWidget<UScoreboard>(/*PC*/ GetWorld()->GetGameInstance(), ScoreboardClass);
		if (ScoreboardWidget)
		{
			ScoreboardWidget->Construct(NULL/*Cast<ULocalPlayer>(PC->Player)*/);
			ScoreboardWidget->ReadStats(); // do it now instead of when displaying the scoreboard!
		}
	}
}

void AGGameState::ShowMessage_Implementation(const FString& message)
{
	// show remaining team lives
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = nullptr;

	UWorld* World = GetWorld();
	AActor* HUD = GetWorld()->SpawnActor(MessageClass, NULL, NULL, SpawnInfo);
	SetStringPropertyByName(HUD, TEXT("message"), message);
}

void AGGameState::ShowScoreboard_Implementation(const FString& WinMessage)
{
	check(ScoreboardWidget);
	if (ScoreboardWidget)
	{
		ScoreboardWidget->Show(WinMessage);
	}
}

void AGGameState::ShowWave_Implementation(int32 wave)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = nullptr;

	// spawn the wave hud
	UWorld* World = GetWorld();
	AActor* waveHUD = GetWorld()->SpawnActor(WaveClass, NULL, NULL, SpawnInfo);
	SetIntPropertyByName(waveHUD, TEXT("wave"), wave);
}