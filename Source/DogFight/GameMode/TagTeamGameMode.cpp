// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "TagTeamGameMode.h"
#include "Actor/GActor.h"
#include "Controller/GPlayerController.h"
#include "Controller/GAIController.h"
#include "TeamInfo.h"

#pragma optimize("", OPTIMISATION)

ATagTeamGameMode::ATagTeamGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void ATagTeamGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
}

void ATagTeamGameMode::StartNewPlayer(APlayerController* NewPlayer)
{
	// create a team for each player
	ATeamInfo* NewTeam = GetWorld()->SpawnActor<ATeamInfo>(TeamClass);
	NewTeam->TeamIndex = Teams.Num();
	Teams.Add(NewTeam);
	GetGameState()->Teams.Add(NewTeam);
	NewTeam->AddToTeam(NewPlayer);

	Super::StartNewPlayer(NewPlayer);
}

void ATagTeamGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	m_gameState = TTGS_None;
}

void ATagTeamGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	// find the leading team
	ATeamInfo* leadingTeam = NULL;
	for (int t = 0; t < Teams.Num(); ++t)
	{
		if (!leadingTeam || Teams[t]->TeamMembers.Num() < leadingTeam->TeamMembers.Num())
		{
			leadingTeam = Teams[t];
		}
	}

	// updates player stats and broadcasts kill
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	// put the killed on the killers team
	ATeamInfo* killerTeam = Cast<AGPlayerState>(Killer->PlayerState)->GetTeam();
	killerTeam->AddToTeam(KilledPlayer);

	// check if Killer's team just took the lead
	if (leadingTeam != killerTeam && leadingTeam->TeamMembers.Num() == (killerTeam->TeamMembers.Num() + 1))
	{
		ShowMessage(FString::Printf(TEXT("%s team has taken the lead"), *killerTeam->TeamName.ToString()));
	}

	// check for end game... only 1 team with any players
	int numOfTeamsWithPlayers = 0;
	for (int t = 0; t < Teams.Num(); ++t)
	{
		if (Teams[t]->TeamMembers.Num())
		{
			++numOfTeamsWithPlayers;
		}
	}

	if (numOfTeamsWithPlayers <= 1)
	{
		SetState(TTGS_EndRound);
	}
}

void ATagTeamGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (m_gameState == TTGS_EndRound)
	{
		EndRound();
		m_gameState = TTGS_RoundEnded;
	}
	else if (m_gameState == TTGS_RoundEnded)
	{
	}
	else
	{
		// update team names - we dont have access to the team name stuff when the player is logging in
		for (int t = 0; t < Teams.Num(); ++t)
		{
			ATeamInfo* Team = Teams[t];
			if (Team->TeamMembers.Num() <= 0)
				continue;

			AController* PrimaryMember = Team->TeamMembers[0];
			AGPlayerState* PlayerState = Cast<AGPlayerState>(PrimaryMember->PlayerState);

			if (Team->TeamColor != PlayerState->GetPlayerColour())
			{
				Team->TeamName = FText::FromString(PlayerState->PlayerColourName);
				Team->TeamColor = PlayerState->GetPlayerColour();

				// simply readd the primary player to the team to fix any tag colour issue
				Team->AddToTeam(PrimaryMember);
			}
		}

		// respawn dead players
		RestartPlayerControllersWithNoPawn();
	}
}

void ATagTeamGameMode::ComputeScore(class AGPlayerState* PS) const
{
	PS->SetScore((float)PS->GetKills());
}

FString ATagTeamGameMode::WinMessage(AGPlayerState* Winner) const
{
	return FString::Printf(TEXT("%s team wins"), *Winner->GetTeam()->TeamName.ToString());
}