#include "DogFight.h"
#include "TeamInfo.h"
#include "Actor/GActor.h"

ATeamInfo::ATeamInfo(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	//PickupCount(0),
	//KillCount(0),
	RemainingLives(10)
{
	SetReplicates(true);
	bAlwaysRelevant = true;
	NetUpdateFrequency = 1.0f;

	TeamIndex = 255; // invalid so we will always get ReceivedTeamIndex() on clients
	TeamColor = FLinearColor::White;
}

void ATeamInfo::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATeamInfo, TeamName);
	DOREPLIFETIME(ATeamInfo, TeamIndex);
	DOREPLIFETIME(ATeamInfo, TeamColor);
	DOREPLIFETIME(ATeamInfo, bFromPreviousLevel);
	//DOREPLIFETIME(AUTTeamInfo, Score);
}

int ATeamInfo::GetPawnAliveCount()
{
	int pawnAliveCount = 0;

	for (int32 s = 0; s < TeamMembers.Num(); ++s)
	{
		AGActor* actor = Cast<AGActor>(TeamMembers[s]->GetPawn());
		if (actor && !actor->IsDead())
			++pawnAliveCount;
	}
	
	return pawnAliveCount;
}

int32 ATeamInfo::GetKills() const
{
	int32 numKills = 0;
	for (int32 s = 0; s < TeamMembers.Num(); ++s)
	{
		AGPlayerState* state = Cast<AGPlayerState>(TeamMembers[s]->PlayerState);
		numKills += state->GetKills();
	}

	return numKills;
}


void ATeamInfo::AddToTeam(AController* C)
{
	if (C != NULL)
	{
		AGPlayerState* PS = Cast<AGPlayerState>(C->PlayerState);
		if (PS != NULL)
		{
			if (PS->Team != NULL)
			{
				PS->Team->RemoveFromTeam(C);
			}
			PS->Team = this;
			PS->NotifyTeamChanged();
			TeamMembers.Add(C);
		}
	}
}

void ATeamInfo::RemoveFromTeam(AController* C)
{
	if (C != NULL && TeamMembers.Contains(C))
	{
		TeamMembers.Remove(C);
#if 0
		// remove from squad
		AUTBot* B = Cast<AUTBot>(C);
		if (B != NULL && B->GetSquad() != NULL)
		{
			B->GetSquad()->RemoveMember(B);
		}
#endif
		// TODO: human player squads
		AGPlayerState* PS = Cast<AGPlayerState>(C->PlayerState);
		if (PS != NULL)
		{
			PS->Team = NULL;
			PS->NotifyTeamChanged();
		}
	}
}

void ATeamInfo::RemoveAllFromTeam()
{
	while (TeamMembers.Num())
	{
		RemoveFromTeam(TeamMembers[0]);
	}
}

void ATeamInfo::ReceivedTeamIndex()
{
	if (!bFromPreviousLevel && TeamIndex != 255)
	{
		AGGameState* GS = GetWorld()->GetGameState<AGGameState>();
		if (GS != NULL)
		{
			if (GS->Teams.Num() <= TeamIndex)
			{
				GS->Teams.SetNum(TeamIndex + 1);
			}
			GS->Teams[TeamIndex] = this;
		}
	}
}

void ATeamInfo::OnRep_TeamColor()
{
	for (int32 s = 0; s < TeamMembers.Num(); ++s)
	{
		AGPlayerState* PS = Cast<AGPlayerState>(TeamMembers[s]->PlayerState);
		if (PS)
		{
			PS->UpdateColors();
		}
	}
}