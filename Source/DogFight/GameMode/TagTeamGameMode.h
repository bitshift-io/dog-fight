// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GGameMode.h"
#include "TagTeamGameMode.generated.h"

class AGActor;
class ATeamInfo;
class AGAIController;

enum TagTeamGameState
{
	TTGS_None,
	TTGS_EndRound,
	TTGS_RoundEnded,
};

/**
 * First player to score MaxKillCount wins. Each player is on his or her own team
 * so its everyone vs everyone
 */
UCLASS()
class DOGFIGHT_API ATagTeamGameMode : public AGGameMode
{
	GENERATED_BODY()

public:

	ATagTeamGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);

	virtual void StartNewPlayer(APlayerController* NewPlayer);

	virtual void HandleMatchHasStarted() override;

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType) override;
	
	void SetState(TagTeamGameState state)	{ m_gameState = state; }

	virtual void Tick(float deltaTime);

	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;
	
	TagTeamGameState		m_gameState;
};
