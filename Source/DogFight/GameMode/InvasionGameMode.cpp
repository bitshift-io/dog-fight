// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "InvasionGameMode.h"
#include "Actor/GActor.h"
#include "Controller/GPlayerController.h"
#include "Controller/GAIController.h"
#include "TeamInfo.h"
#include "Util/BlueprintUtil.h"
#include "UI/Scoreboard.h"

// http://gamedev.stackexchange.com/questions/92593/setting-up-local-players-in-unreal-engine

#pragma optimize("", OPTIMISATION)

AInvasionGameMode::AInvasionGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AInvasionGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
}

void AInvasionGameMode::InitGameState()
{
	Super::InitGameState();

	// copy any settings over to the game state for replication
	//GetGameState()->Teams = Teams;
}

void AInvasionGameMode::StartNewPlayer(APlayerController* NewPlayer)
{
	check(Teams.Num() == 2);

	// assign connecting players to a team
	AGPlayerState* NewPlayerState = CastChecked<AGPlayerState>(NewPlayer->PlayerState);
	Teams[IT_Player]->AddToTeam(NewPlayer);

	Super::StartNewPlayer(NewPlayer);
}

void AInvasionGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	m_curEnemyRespawnDelay = 0;
	GetGameState()->WaveCount = 0;
	m_startTime = FDateTime::Now();
	m_gameState = IGS_None;

	// player team followed by enemy team is required and assumed
	check(Teams.Num() == 2);

	ResetNextWave();	
}

void AInvasionGameMode::GoWave(int32 wave)
{
#if WITH_EDITOR
	while (GetGameState()->WaveCount < wave)
	{
		ResetNextWave();
	}
#endif
}

void AInvasionGameMode::ResetNextWave()
{
	m_curEnemyRespawnDelay = EnemyRespawnDelay;
	
	// lets spawn an enemy
	// https://answers.unrealengine.com/questions/4319/questioncai-spawning.html
	int enemyToSpawn = GetGameState()->WaveCount % EnemyClassList.Num();

	APlayerStart* playerStart = FirstPlayerStart();
	if (!playerStart)
		return;

	check(playerStart);
	float height = playerStart->GetActorLocation().Z;


	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = nullptr;
	UWorld* World = GetWorld();

	GetGameState()->ShowWave(GetGameState()->WaveCount + 1);

	AGAIController* AIC = Cast<AGAIController>(World->SpawnActor(EnemyClassList[enemyToSpawn].AIControllerClass, NULL, NULL, SpawnInfo));
	check(AIC);
	check(Teams[IT_Enemy]);
	Teams[IT_Enemy]->AddToTeam(AIC);

	// set player colour to team colour = red for enemies
	AGPlayerState* PS = Cast<AGPlayerState>(AIC->PlayerState);
	check(PS);
	PS->ServerSetPlayerColour(Teams[IT_Enemy]->TeamColor);
	PS->ServerSetPawnClass(EnemyClassList[enemyToSpawn].ActorClass);

	++GetGameState()->WaveCount;
	StartWave(GetGameState()->WaveCount);

	// handle achivements
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AGPlayerController* PC = Cast<AGPlayerController>(*It);
		if (!PC)
			continue;

		UGPersistentUser* PersistentUser = PC->GetPersistentUser();
		if (!PersistentUser)
			continue;

		const FString ACH_INVASION_REACHED_WAVE_1("ACH_INVASION_REACHED_WAVE_1");
		const FString ACH_INVASION_REACHED_WAVE_5("ACH_INVASION_REACHED_WAVE_5");
		const FString ACH_INVASION_REACHED_WAVE_10("ACH_INVASION_REACHED_WAVE_10");
		const FString ACH_INVASION_REACHED_WAVE_15("ACH_INVASION_REACHED_WAVE_15");
		const FString ACH_INVASION_REACHED_WAVE_20("ACH_INVASION_REACHED_WAVE_20");

		if (GetGameState()->WaveCount >= 1)
		{
			PC->UpdateAchievementProgress(ACH_INVASION_REACHED_WAVE_1, 1.0f);
		}
		if (GetGameState()->WaveCount >= 5)
		{
			PC->UpdateAchievementProgress(ACH_INVASION_REACHED_WAVE_5, 1.0f);
		}
		if (GetGameState()->WaveCount >= 10)
		{
			PC->UpdateAchievementProgress(ACH_INVASION_REACHED_WAVE_10, 1.0f);
		}
		if (GetGameState()->WaveCount >= 15)
		{
			PC->UpdateAchievementProgress(ACH_INVASION_REACHED_WAVE_15, 1.0f);
		}
		if (GetGameState()->WaveCount >= 20)
		{
			PC->UpdateAchievementProgress(ACH_INVASION_REACHED_WAVE_20, 1.0f);
		}			
	}
}

void AInvasionGameMode::GoEndRound()
{
	SetState(IGS_EndRound);
}

void AInvasionGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	AGActor* killedActor = Cast<AGActor>(KilledPawn);

	// if all humans are dead, end the round
	if (killedActor->GetPlayerState() && killedActor->GetPlayerState()->GetTeam() == Teams[IT_Player])
	{
		if (Teams[IT_Player]->RemainingLives <= 0 && Teams[IT_Player]->GetPawnAliveCount() <= 0)
		{
			SetState(IGS_EndRound);
			return;
		}
	}
	else
	{
		// check for end of wave
		if (Teams[IT_Enemy]->GetPawnAliveCount() <= 0)
		{
			SetState(IGS_ResetNextWave);
		}
	}
}

void AInvasionGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (m_gameState == IGS_ResetNextWave)
	{
		ResetNextWave();
		m_gameState = IGS_None;
	}
	else if (m_gameState == IGS_EndRound)
	{
		EndRound();
		m_gameState = IGS_RoundEnded;
	}
	else if (m_gameState == IGS_RoundEnded)
	{
	}
	else
	{
		float prevCurenemyRespawnDelay = m_curEnemyRespawnDelay;
		m_curEnemyRespawnDelay -= deltaTime;

		// respawn dead players
		bool bAtLeastOneDeadPlayer = false;
		ATeamInfo* playerTeam = Teams[IT_Player];
		ATeamInfo* enemyTeam = Teams[IT_Enemy];

		// game level changing mid game?
		if (!playerTeam || !enemyTeam)
		{
			return;
		}

		for (int i = 0; i < playerTeam->TeamMembers.Num(); ++i)
		{
			check(playerTeam->TeamMembers[i]); // removed from the game?
			AGActor* actor = Cast<AGActor>(playerTeam->TeamMembers[i]->GetPawn());
			if (!actor && playerTeam->RemainingLives > 0)
			{
				bAtLeastOneDeadPlayer = true;

				// spawn player
				RestartPlayer(playerTeam->TeamMembers[i]);
			}
		}

		if (bAtLeastOneDeadPlayer)
		{
			playerTeam->RemainingLives--;

			// show remaining team lives
			if (playerTeam->RemainingLives == 0)
			{
				ShowMessage(TEXT("No lives remain"));
			}
			else if (playerTeam->RemainingLives == 1)
			{
				ShowMessage(TEXT("1 life remains"));
			}
			else
			{
				ShowMessage(FString::Printf(TEXT("%i lives remain"), playerTeam->RemainingLives));
			}
		}

		// respawn enemies after delay
		for (int i = 0; i < enemyTeam->TeamMembers.Num(); ++i)
		{
			check(enemyTeam->TeamMembers[i]);
			AGActor* actor = Cast<AGActor>(enemyTeam->TeamMembers[i]->GetPawn());
			if (prevCurenemyRespawnDelay >= 0.f && m_curEnemyRespawnDelay < 0.f)
			{
				// spawn enemy
				RestartPlayer(enemyTeam->TeamMembers[i]);
				//check(enemyTeam->TeamMembers[i]->GetPawn());
			}
		}
	}
}

void AInvasionGameMode::SetPlayerLivesRemaining(int32 lives)
{
	Teams[IT_Player]->RemainingLives = lives;
}

void AInvasionGameMode::ComputeScore(class AGPlayerState* PS) const
{
	// factor modifier to score for each wave
	// up to 100 points for high accuracy
	// up to 100 points for no deaths
	// up to 100 points for pickups
	// 1000 points per wave
	int accuracyScore = PS->GetNumBulletsFired() <= 0 ? 100.0f : (float(PS->GetNumBulletsHit()) / FMath::Max<float>(1.f, float(PS->GetNumBulletsFired())) * 100.f);
	int deathsScore = 100.f / FMath::Max<float>(1.f, float(PS->GetDeaths()));
	int waveScore = GetGameState()->WaveCount * 1000.0f;
	int32 score = (accuracyScore + deathsScore + waveScore);
	PS->SetScore((float)score);
}

FString AInvasionGameMode::WinMessage(AGPlayerState* Winner) const
{
	return FString::Printf(TEXT("Reached wave %i"), GetGameState()->WaveCount);
}