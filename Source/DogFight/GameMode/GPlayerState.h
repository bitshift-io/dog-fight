// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GPlayerState.generated.h"

UCLASS()
class AGPlayerState : public APlayerState
{
	GENERATED_UCLASS_BODY()


	virtual void BeginPlay() override;

	// Begin APlayerState interface
	/** clear scores */
	virtual void Reset() override;

	/**
	 * Set the team 
	 *
	 * @param	InController	The controller to initialize state with
	 */
	virtual void ClientInitialize(class AController* InController) override;

	virtual void UnregisterPlayerWithSession() override;

	// End APlayerState interface

	//void SetTeam(ATeamInfo* team);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetPlayerColour(const FLinearColor& colour);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetPlayerColourName(const FString& Name);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetPlayerName(const FString& Name);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetPawnClass(UClass* Class);

	UPROPERTY(Transient, Replicated)
	UClass*	PawnClass;

	// called on server forcing score to be set to all clients
	void SetScore(float S);

	UFUNCTION(Reliable, WithValidation, NetMulticast)
	void SendScoreToAllClients(float S);
	

	/**
	 * Set new team and update pawn. Also updates player character team colors.
	 *
	 * @param	NewTeamNumber	Team we want to be on.
	 */
	void SetTeamNum(int32 NewTeamNumber);

	/** player killed someone */
	void ScoreKill(AGPlayerState* Victim, int32 Points);

	/** player died */
	void ScoreDeath(AGPlayerState* KilledBy, int32 Points);

	/** get current team */
	int32 GetTeamNum() const;

	/** get number of kills */
	int32 GetKills() const;

	/** get number of bullet hits */
	int32 GetNumBulletsHit() const;

	/** get number of deaths */
	int32 GetDeaths() const;

	/** get number of points */
	float GetScore() const;

	/** set number of points */
	//void SetScore(float score);

	/** get number of bullets fired this match */
	int32 GetNumBulletsFired() const;

	/** get number of rockets fired this match */
	int32 GetNumRocketsFired() const;

	/** get number of pickups aquired this match */
	int32 GetNumPickups() const;

	/** get whether the player quit the match */
	bool IsQuitter() const;

	/** gets truncated player name to fit in death log and scoreboards */
	FString GetShortPlayerName() const;

	/** Sends kill (excluding self) to clients */
	UFUNCTION(Reliable, Client)
	void InformAboutKill(class AGPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AGPlayerState* KilledPlayerState);

	/** broadcast death to local clients */
	UFUNCTION(Reliable, NetMulticast)
	void BroadcastDeath(class AGPlayerState* KillerPlayerState, const UDamageType* KillerDamageType, class AGPlayerState* KilledPlayerState);

	/** replicate team colors. Updated the players mesh colors appropriately */
	UFUNCTION()
	void OnRep_TeamColor();

	//We don't need stats about amount of ammo fired to be server authenticated, so just increment these with local functions
	void AddBulletsFired(int32 NumBullets);
	void AddRocketsFired(int32 NumRockets);
	void AddBulletsHit(int32 NumHit);

	/** Set whether the player is a quitter */
	void SetQuitter(bool bInQuitter);

	virtual void CopyProperties(class APlayerState* PlayerState) override;

	/** replicate team colors. Updated the players mesh colors appropriately */
	UFUNCTION(BlueprintNativeEvent)
	void NotifyTeamChanged();

	//UFUNCTION(BlueprintCallable, Category = "PlayerState")
	ATeamInfo* GetTeam() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	void GetTeam(ATeamInfo*& Team);

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	FLinearColor GetTeamColour() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	FLinearColor GetPlayerColour() const;
	
//protected:

	/** Set the mesh colors based on the current teamnum variable */
	void UpdateColors();

	/** team number */
	//UPROPERTY(Transient, ReplicatedUsing = OnRep_Team)
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = NotifyTeamChanged, Category = PlayerState)
	ATeamInfo* Team;

	/** team number */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_TeamColor)
	int32 TeamNumber;

	/** number of kills */
	UPROPERTY(Transient, Replicated)
	int32 NumKills;

	/** number of bullets that hit an enemy */
	UPROPERTY(Transient, Replicated)
	int32 NumBulletsHit;
	
	/** number of deaths */
	UPROPERTY(Transient, Replicated)
	int32 NumDeaths;

	/** number of bullets fired this match */
	UPROPERTY(Transient, Replicated)
	int32 NumBulletsFired;

	/** number of rockets fired this match */
	UPROPERTY()
	int32 NumRocketsFired;

	/** whether the user quit the match */
	UPROPERTY()
	uint8 bQuitter : 1;

	/** time as the rabbit */
	UPROPERTY(Transient, Replicated)
	float RabbitTime;

	/** helper for scoring points */
	void ScorePoints(int32 Points);

	/** number of pickups */
	UPROPERTY(Transient, Replicated)
	int32				NumPickups;

	UFUNCTION()
	void OnRep_PlayerColour();

	/** player colour */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_PlayerColour)
	FLinearColor	PlayerColour;

	/** player colour name */
	UPROPERTY(Transient, Replicated)
	FString			PlayerColourName;

	AController*	Controller;
};
