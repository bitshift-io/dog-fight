// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "RabbitGameMode.h"
#include "Actor/GActor.h"
#include "Controller/GPlayerController.h"
#include "Controller/GAIController.h"
#include "TeamInfo.h"

#pragma optimize("", OPTIMISATION)

ARabbitGameMode::ARabbitGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxRabbitTime = 120; // 2 minutes
	RabbitPawnOrigionalClass = NULL;
	ShowRabbitTimePeriod = 20.f; // 20 seconds
}

void ARabbitGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	Teams.Empty(); // remove the teams that have been init'ed, its free for all until there is a rabbit
}

void ARabbitGameMode::StartNewPlayer(APlayerController* NewPlayer)
{
	if (IsRabbit())
	{
		check(Teams.Num() == 2);

		// assign connecting players to a team
		AGPlayerState* NewPlayerState = CastChecked<AGPlayerState>(NewPlayer->PlayerState);
		Teams[RT_Everyone]->AddToTeam(NewPlayer);
	}
	else
	{ 
		// create a team for each player
		ATeamInfo* NewTeam = GetWorld()->SpawnActor<ATeamInfo>(TeamClass);
		NewTeam->TeamIndex = Teams.Num();
		Teams.Add(NewTeam);
		GetGameState()->Teams.Add(NewTeam);
		NewTeam->AddToTeam(NewPlayer);
	}	
	
	Super::StartNewPlayer(NewPlayer);
}

void ARabbitGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
	m_gameState = RGS_None;
}

void ARabbitGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	AGPlayerState* killerPlayerState = Cast<AGPlayerState>(Killer->PlayerState);

	// first kill, so there is no rabbit!
	if (!IsRabbit())
	{
		InitTeams();

		// put everyone on the every one team
		FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator();
		for (; it; ++it)
		{
			Teams[RT_Everyone]->AddToTeam(*it);
		}

		// add on a fraction of a second so this player is on the lead to stop two messages displaying at the same time
		killerPlayerState->RabbitTime += 0.01f; 
	}
	else
	{
		// swap rabbit back to everyone team
		AController* OldRabbit = Teams[RT_Rabbit]->TeamMembers[0];

		// rabbit killed some one? nothing to do, buisness as usual!
		if (OldRabbit != KilledPlayer)
		{
			return;
		}

		// swap the rabbit back to thier old pawn class
		// dont need to do this, as the player died so will respawn correctly
		//ReplacePlayerPawn(OldRabbit, RabbitPawnOrigionalClass);

		Teams[RT_Everyone]->AddToTeam(OldRabbit);
	}

	// put killer on rabbit team
	Teams[RT_Rabbit]->AddToTeam(Killer);
	Teams[RT_Rabbit]->TeamName = FText::FromString(killerPlayerState->PlayerColourName);

	// backup the Killers pawn class and swap them with something nice
	RabbitPawnOrigionalClass = Killer->GetPawn()->GetClass();
	check(RabbitPawnOrigionalClass != NULL);
	int idx = FMath::RandRange(0, RabbitActorClassList.Num() - 1);
	ReplacePlayerPawn(Killer, RabbitActorClassList[idx]);

	ShowMessage(FString::Printf(TEXT("%s is now the rabbit"), *killerPlayerState->PlayerColourName));
}

void ARabbitGameMode::TickRabbit(AController* Rabbit, float deltaTime)
{
	AGPlayerState* RPS = Cast<AGPlayerState>(Rabbit->PlayerState);
	
	// find the leading player
	AController* leadPlayer = NULL;
	FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator();
	for (; it; ++it)
	{
		AController* pc = *it;
		if (!leadPlayer || Cast<AGPlayerState>(pc->PlayerState)->RabbitTime >= Cast<AGPlayerState>(leadPlayer->PlayerState)->RabbitTime)
		{
			leadPlayer = pc;
		}
	}

	// update time for this player as the rabbit
	float prevRabbitTime = RPS->RabbitTime;
	RPS->RabbitTime += deltaTime;

	// end game, we have a winner!
	if (RPS->RabbitTime >= MaxRabbitTime)
	{
		SetState(RGS_EndRound);
	}
	
	// we just took the lead from the lead player!
	if (leadPlayer != Rabbit && RPS->RabbitTime > Cast<AGPlayerState>(leadPlayer->PlayerState)->RabbitTime)
	{
		ShowMessage(FString::Printf(TEXT("%s takes the lead"), *RPS->PlayerColourName));
	}

	// display score every 20 seconds
	int prevIntTime = int(prevRabbitTime / ShowRabbitTimePeriod);
	int curIntTime = int(RPS->RabbitTime / ShowRabbitTimePeriod);
	if (prevIntTime != curIntTime)
	{
		int rabbitMinutes = RPS->RabbitTime / 60.f;
		int rabbitSeconds = (int)RPS->RabbitTime - (rabbitMinutes * 60);
		if (rabbitSeconds >= 10)
			ShowMessage(FString::Printf(TEXT("%s has been the rabbit for %i:%i"), *RPS->PlayerColourName, rabbitMinutes, rabbitSeconds));
		else
			ShowMessage(FString::Printf(TEXT("%s has been the rabbit for %i:0%i"), *RPS->PlayerColourName, rabbitMinutes, rabbitSeconds));
	}

}

void ARabbitGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (m_gameState == RGS_EndRound)
	{
		EndRound();
		m_gameState = RGS_RoundEnded;
	}
	else if (m_gameState == RGS_RoundEnded)
	{
	}
	else
	{
		if (IsRabbit())
		{
			AController* Rabbit = Teams[RT_Rabbit]->TeamMembers[0];
			TickRabbit(Rabbit, deltaTime);
		}

		// respawn dead players
		RestartPlayerControllersWithNoPawn();
	}
}

void ARabbitGameMode::ComputeScore(class AGPlayerState* PS) const
{
	PS->SetScore(PS->RabbitTime);
}

FString ARabbitGameMode::WinMessage(AGPlayerState* Winner) const
{
	return FString::Printf(TEXT("%s wins"), *Winner->PlayerColourName);
}