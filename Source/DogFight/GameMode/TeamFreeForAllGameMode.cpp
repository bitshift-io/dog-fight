// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "TeamFreeForAllGameMode.h"
#include "Actor/GActor.h"
#include "Controller/GPlayerController.h"
#include "Controller/GAIController.h"
#include "TeamInfo.h"

#pragma optimize("", OPTIMISATION)

ATeamFreeForAllGameMode::ATeamFreeForAllGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxKillCount = 20;
}

void ATeamFreeForAllGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
}

void ATeamFreeForAllGameMode::StartNewPlayer(APlayerController* NewPlayer)
{
	check(Teams.Num() >= 2);

	// assign connecting players to a team
	AutoAddPlayerToTeam(NewPlayer);

	Super::StartNewPlayer(NewPlayer);
}

/*
	Add player to the team with lowest amount of players
	this could be moved to gamemode baseclass
*/
void ATeamFreeForAllGameMode::AutoAddPlayerToTeam(APlayerController * Player)
{
	ATeamInfo* lowestPlayerCountTeam = Teams[0];
	for (int t = 1; t < Teams.Num(); ++t)
	{
		if (Teams[t]->TeamMembers.Num() < lowestPlayerCountTeam->TeamMembers.Num())
		{
			lowestPlayerCountTeam = Teams[t];
		}
	}

	lowestPlayerCountTeam->AddToTeam(Player);
}

void ATeamFreeForAllGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	// player team followed by enemy team is required and assumed
	check(Teams.Num() >= 2);

	m_gameState = TFFA_None;
}

ATeamInfo* ATeamFreeForAllGameMode::GetLeadingTeam() const
{
	// find the leading team
	ATeamInfo* leadingTeam = NULL;
	for (int t = 0; t < Teams.Num(); ++t)
	{
		if (!leadingTeam || Teams[t]->GetKills() < leadingTeam->GetKills())
		{
			leadingTeam = Teams[t];
		}
	}

	return leadingTeam;
}

void ATeamFreeForAllGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	// find the leading team
	ATeamInfo* leadingTeam = GetLeadingTeam();

	// updates player stats and broadcasts kill
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	// check if Killer's team just took the lead
	ATeamInfo* killerTeam = Cast<AGPlayerState>(Killer->PlayerState)->GetTeam();
	if (leadingTeam != killerTeam && leadingTeam->GetKills() == (killerTeam->GetKills() + 1))
	{
		ShowMessage(FString::Printf(TEXT("%s team has taken the lead"), *killerTeam->TeamName.ToString()));
	}

	// check for end game
	if (killerTeam->GetKills() >= MaxKillCount)
	{
		SetState(TFFA_EndRound);
	}
}

void ATeamFreeForAllGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (m_gameState == TFFA_EndRound)
	{
		EndRound();
		m_gameState = TFFA_RoundEnded;
	}
	else if (m_gameState == TFFA_RoundEnded)
	{
	}
	else
	{
		// respawn dead players
		RestartPlayerControllersWithNoPawn();
	}
}

void ATeamFreeForAllGameMode::ComputeScore(class AGPlayerState* PS) const
{
	PS->SetScore((float)PS->GetTeam()->GetKills());
}

FString ATeamFreeForAllGameMode::WinMessage(AGPlayerState* Winner) const
{
	return FString::Printf(TEXT("%s team wins"), *Winner->GetTeam()->TeamName.ToString());
}