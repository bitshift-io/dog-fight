// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "FreeForAllGameMode.h"
#include "Actor/GActor.h"
#include "Controller/GPlayerController.h"
#include "Controller/GAIController.h"
#include "UI/Scoreboard.h"
#include "TeamInfo.h"

AFreeForAllGameMode::AFreeForAllGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxKillCount = 20;
}

void AFreeForAllGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
}

void AFreeForAllGameMode::StartNewPlayer(APlayerController* NewPlayer)
{
	// create a team for each player
	ATeamInfo* NewTeam = GetWorld()->SpawnActor<ATeamInfo>(TeamClass);
	NewTeam->TeamIndex = Teams.Num();
	Teams.Add(NewTeam);
	GetGameState()->Teams.Add(NewTeam);
	NewTeam->AddToTeam(NewPlayer);

	Super::StartNewPlayer(NewPlayer);
}

void AFreeForAllGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	m_gameState = FFA_None;
}

void AFreeForAllGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	// find the leading player
	AController* leadPlayer = NULL;
	FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator();
	for (; it; ++it)
	{
		AController* pc = *it;
		if (!leadPlayer || Cast<AGPlayerState>(pc->PlayerState)->GetKills() >= Cast<AGPlayerState>(leadPlayer->PlayerState)->GetKills())
		{
			leadPlayer = pc;
		}
	}

	// updates player stats and broadcasts kill
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	// check if Killer just took the lead
	AGPlayerState* leadPlayerState = Cast<AGPlayerState>(leadPlayer->PlayerState);
	AGPlayerState* killerPlayerState = Cast<AGPlayerState>(Killer->PlayerState);
	if (leadPlayer != Killer && killerPlayerState->GetKills() == (leadPlayerState->GetKills() + 1))
	{
		ShowMessage(FString::Printf(TEXT("%s has taken the lead"), *killerPlayerState->PlayerColourName));
	}

	// check for end game
	if (killerPlayerState->GetKills() >= MaxKillCount)
	{
		SetState(FFA_EndRound);
	}
}

void AFreeForAllGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	
	if (m_gameState == FFA_EndRound)
	{
		EndRound();
		m_gameState = FFA_RoundEnded;
	}
	else if (m_gameState == FFA_RoundEnded)
	{
	}
	else
	{
		// respawn dead players
		RestartPlayerControllersWithNoPawn();
	}
}

void AFreeForAllGameMode::ComputeScore(class AGPlayerState* PS) const
{
	PS->SetScore((float)PS->GetKills());
}

FString AFreeForAllGameMode::WinMessage(AGPlayerState* Winner) const
{
	return FString::Printf(TEXT("%s wins"), *Winner->PlayerColourName);
}