
#include "DogFight.h"
#include "GGameMode.h"
#include "GPlayerState.h"
#include "Controller/ControllerInterface.h"
#include "Util/BlueprintUtil.h"
#include "TeamInfo.h"
#include "Actor/GActor.h"
#include "UserWidget.h"
#include "UI/Scoreboard.h"

#pragma optimize("", OPTIMISATION)

DEFINE_LOG_CATEGORY_STATIC(LogGameMode, Log, All);

/*
// backup and restore DefaultPawnClass
#define BACKUP_DEFAULT_PAWN_CLASS												\
	TSubclassOf<APawn> OrigDefaultPawnClass = DefaultPawnClass;					\
	IControllerInterface* c = Cast<IControllerInterface>(NewPlayer);			\
	if (c && c->GetDefaultPawnClassForController())								\
	{																			\
		DefaultPawnClass = c->GetDefaultPawnClassForController();				\
	}

#define	RESTORE_DEFAULT_PAWN_CLASS \
	DefaultPawnClass = OrigDefaultPawnClass;
*/


static bool bIsAIDisabled = false;

AGGameMode::AGGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	/*
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnOb(TEXT("/Game/Blueprints/Pawns/PlayerPawn"));
	DefaultPawnClass = PlayerPawnOb.Class;

	static ConstructorHelpers::FClassFinder<APawn> BotPawnOb(TEXT("/Game/Blueprints/Pawns/BotPawn"));
	BotPawnClass = BotPawnOb.Class;
	*/

	//HUDClass = AGHUD::StaticClass();
	PlayerControllerClass = AGPlayerController::StaticClass();
	PlayerStateClass = AGPlayerState::StaticClass();
	//SpectatorClass = AGSpectatorPawn::StaticClass();
	GameStateClass = AGGameState::StaticClass();
	TeamClass = ATeamInfo::StaticClass();

	NumTeams = 0;
	bTeamTagsVisible = false;

	KillScore = 1;
	DeathScore = 0;

	bUseSeamlessTravel = true; // false;
}

void AGGameMode::InitTeams()
{
	//NumTeams = FMath::Max<uint8>(NumTeams, 2);
	if (TeamClass == NULL)
	{
		TeamClass = ATeamInfo::StaticClass();
	}
	
	// clean up any existing teams incase we are inited mid-game
	for (uint8 i = 0; i < Teams.Num(); i++)
	{
		Teams[i]->RemoveAllFromTeam();
		Teams[i]->Destroy();
	}
	Teams.Empty();

	for (uint8 i = 0; i < NumTeams; i++)
	{
		ATeamInfo* NewTeam = GetWorld()->SpawnActor<ATeamInfo>(TeamClass);
		NewTeam->TeamIndex = i;
		if (TeamColors.IsValidIndex(i))
		{
			NewTeam->TeamColor = TeamColors[i];
		}

		if (TeamNames.IsValidIndex(i))
		{
			NewTeam->TeamName = TeamNames[i];
		}

		Teams.Add(NewTeam);
		checkSlow(Teams[i] == NewTeam);
	}
}

void AGGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	InitTeams();	
	OnInitGame.Broadcast();
}

void AGGameMode::InitGameState()
{
	Super::InitGameState();

	// copy any settings over to the game state for replication
	GetGameState()->Teams = Teams;
	GetGameState()->bTeamTagsVisible = bTeamTagsVisible;
	GetGameState()->MessageClass = MessageClass;
	GetGameState()->GameModeName = GameModeName;
	GetGameState()->WaveClass = WaveClass;
	GetGameState()->Achieve10GamesName = Achieve10GamesName;

	GetGameState()->ScoreboardClass = ScoreboardClass;
	GetGameState()->OnRep_ScoreboardClass();
}

void AGGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	AGPlayerState* KillerPlayerState = Killer ? Cast<AGPlayerState>(Killer->PlayerState) : NULL;
	AGPlayerState* VictimPlayerState = KilledPlayer ? Cast<AGPlayerState>(KilledPlayer->PlayerState) : NULL;

	if (KillerPlayerState && KillerPlayerState != VictimPlayerState)
	{
		KillerPlayerState->ScoreKill(VictimPlayerState, KillScore);
		KillerPlayerState->InformAboutKill(KillerPlayerState, DamageType, VictimPlayerState);
	}

	if (VictimPlayerState)
	{
		VictimPlayerState->ScoreDeath(KillerPlayerState, DeathScore);
		VictimPlayerState->BroadcastDeath(KillerPlayerState, DamageType, VictimPlayerState);
	}

	OnKilled.Broadcast(Killer, KilledPlayer, KilledPawn, DamageType);
}

bool AGGameMode::CanBePickedUp(class APickup* Pickup, class AGActor* Pawn) const
{
	// only player controller controlled actors can pickup pickups
	// this stops AI in invasion picking them up
	return Cast<AGPlayerController>(Pawn->GetController()) != NULL;
}

float AGGameMode::ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const
{
	float ActualDamage = Damage;

	AGActor* DamagedPawn = Cast<AGActor>(DamagedActor);
	if (DamagedPawn && EventInstigator)
	{
		AGPlayerState* DamagedPlayerState = Cast<AGPlayerState>(DamagedPawn->PlayerState);
		AGPlayerState* InstigatorPlayerState = Cast<AGPlayerState>(EventInstigator->PlayerState);

		// disable friendly fire
		if (!CanDealDamage(InstigatorPlayerState, DamagedPlayerState))
		{
			ActualDamage = 0.0f;
		}

		// scale self instigated damage
		if (InstigatorPlayerState == DamagedPlayerState)
		{
			ActualDamage = 0.0f; // *= DamageSelfScale;
		}
	}

	return ActualDamage;
}

bool AGGameMode::CanDealDamage(class AGPlayerState* DamageInstigator, class AGPlayerState* DamagedPlayer) const
{
	return DamageInstigator && DamagedPlayer && (DamagedPlayer == DamageInstigator || DamagedPlayer->GetTeam() != DamageInstigator->GetTeam());
	//return true;
}

APlayerController* AGGameMode::SpawnPlayerController(ENetRole RemoteRole, FVector const & SpawnLocation, FRotator const & SpawnRotation)
{
	APlayerController* PC = Super::SpawnPlayerController(RemoteRole, SpawnLocation, SpawnRotation);
	return PC;
}

void AGGameMode::Logout(AController* Exiting)
{
	// remove players who log out
	for (int t = 0; t < Teams.Num(); ++t)
	{
		Teams[t]->RemoveFromTeam(Exiting);
	}

	Super::Logout(Exiting);
}

void AGGameMode::RestartPlayer(AController* NewPlayer)
{
	//BACKUP_DEFAULT_PAWN_CLASS
	check(GetDefaultPawnClassForController(NewPlayer) != NULL);
	Super::RestartPlayer(NewPlayer);
	//RESTORE_DEFAULT_PAWN_CLASS
}

void AGGameMode::SetPlayerDefaults(APawn* PlayerPawn)
{
	Super::SetPlayerDefaults(PlayerPawn);
	/*
	AGActor* Actor = Cast<AGActor>(PlayerPawn);
	if (Actor)
	{
		Actor->ClientSetTeamTagVisible(bTeamTagsVisible); // call RPC on the client
		Actor->SetTeamTagVisible(bTeamTagsVisible);
	}*/
}

void AGGameMode::ReplacePlayerPawn(AController* NewPlayer, UClass* PawnClass)
{
	APawn* OldPawn = NewPlayer->GetPawn();
	FRotator NewControllerRot = OldPawn->GetActorRotation();
	FVector NewControllerLoc = OldPawn->GetActorLocation();

	OldPawn->Destroy();
	check(!NewPlayer->GetPawn());

	APlayerStart* StartSpot = FirstPlayerStart();
	StartSpot->SetActorLocation(NewControllerLoc);
	StartSpot->SetActorRotation(NewControllerRot);

	APawn* NewPawn = NULL;

	// ganked and modified from SpawnDefaultPawnFor_Implementation
	{
		// don't allow pawn to be spawned with any pitch or roll
		FRotator StartRotation(ForceInit);
		StartRotation.Yaw = StartSpot->GetActorRotation().Yaw;
		FVector StartLocation = StartSpot->GetActorLocation();

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnInfo.Instigator = Instigator;
		SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save default player pawns into a map
		NewPawn = GetWorld()->SpawnActor<APawn>(PawnClass, StartLocation, StartRotation, SpawnInfo);
		if (NewPawn == NULL)
		{
			check(NewPawn);
		}
	}
	
	NewPlayer->SetPawn(NewPawn);
	check(NewPlayer->GetPawn());
	NewPlayer->Possess(NewPlayer->GetPawn());
	
	// set initial control rotation to player start's rotation
	NewPlayer->ClientSetRotation(NewControllerRot, true);

	NewControllerRot.Roll = 0.f;
	NewPlayer->SetControlRotation(NewControllerRot);

	SetPlayerDefaults(NewPlayer->GetPawn());

	K2_OnRestartPlayer(NewPlayer);
}


APawn* AGGameMode::SpawnDefaultPawnFor_Implementation(AController* NewPlayer, AActor* StartSpot)
{
	// don't allow pawn to be spawned with any pitch or roll
	FRotator StartRotation(ForceInit);
	StartRotation.Yaw = StartSpot->GetActorRotation().Yaw;
	FVector StartLocation = StartSpot->GetActorLocation();

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save default player pawns into a map
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn; // FM: disable collision check when spawning
	UClass* PawnClass = GetDefaultPawnClassForController(NewPlayer);
	APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(PawnClass, StartLocation, StartRotation, SpawnInfo);
	if (ResultPawn == NULL)
	{
		UE_LOG(LogGameMode, Warning, TEXT("Couldn't spawn Pawn of type %s at %s"), *GetNameSafe(PawnClass), *StartSpot->GetName());
	}
	return ResultPawn;
}

/*
APawn* AGGameMode::SpawnDefaultPawnFor_Implementation(AController* NewPlayer, AActor* StartSpot)
{
	BACKUP_DEFAULT_PAWN_CLASS

	// don't allow pawn to be spawned with any pitch or roll
	FRotator StartRotation(ForceInit);
	StartRotation.Yaw = StartSpot->GetActorRotation().Yaw;
	FVector StartLocation = StartSpot->GetActorLocation();

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save default player pawns into a map
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn; // disable collision check when spawning
	UClass* PawnClass = GetDefaultPawnClassForController(NewPlayer);
	APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(PawnClass, StartLocation, StartRotation, SpawnInfo);
	if (ResultPawn == NULL)
	{
		UE_LOG(LogGameMode, Warning, TEXT("Couldn't spawn Pawn of type %s at %s"), *GetNameSafe(PawnClass), *StartSpot->GetName());
	}


	AGActor* Actor = Cast<AGActor>(ResultPawn);
	if (Actor)
	{
		Actor->SetTeamTagVisible(bTeamTagsVisible);
	}

	RESTORE_DEFAULT_PAWN_CLASS
	return ResultPawn;
}*/

/*
AActor* AGGameMode::ChoosePlayerStart_Implementation(AController* NewPlayer)
{
	BACKUP_DEFAULT_PAWN_CLASS
	AActor* a = ChoosePlayerStart_ImplementationInternal(NewPlayer);
	RESTORE_DEFAULT_PAWN_CLASS
	return a;
}*/

FString AGGameMode::GetBotsCountOptionName()
{
	return FString(TEXT("Bots"));
}

/** Returns game session class to use */
TSubclassOf<AGameSession> AGGameMode::GetGameSessionClass() const
{
	return AGGameSession::StaticClass();
}

APlayerStart* AGGameMode::FirstPlayerStart()
{
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* P = *It;
		return P;
	}

	return NULL;
}

UClass* AGGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	AGPlayerState* PS = Cast<AGPlayerState>(InController->PlayerState);
	if (PS && PS->PawnClass)
	{
		return PS->PawnClass;
	}

		/*
	IControllerInterface* c = Cast<IControllerInterface>(InController);
	if (c && c->GetDefaultPawnClassForController())
	{
		return c->GetDefaultPawnClassForController();
	}*/

	return DefaultPawnClass;
}


/*
	Modified version to skip EncroachingBlockingGeometry which we don't care about for this game
*/
AActor* AGGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	// Choose a player start
	APlayerStart* FoundPlayerStart = NULL;
	UClass* PawnClass = GetDefaultPawnClassForController(Player);
	APawn* PawnToFit = PawnClass ? PawnClass->GetDefaultObject<APawn>() : nullptr;
	TArray<APlayerStart*> UnOccupiedStartPoints;
	TArray<APlayerStart*> OccupiedStartPoints;
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* PlayerStart = *It;

		if (PlayerStart->IsA<APlayerStartPIE>())
		{
			// Always prefer the first "Play from Here" PlayerStart, if we find one while in PIE mode
			FoundPlayerStart = PlayerStart;
			break;
		}
		else
		{
			UnOccupiedStartPoints.Add(PlayerStart);
			/*
			FVector ActorLocation = PlayerStart->GetActorLocation();
			const FRotator ActorRotation = PlayerStart->GetActorRotation();
			if (!GetWorld()->EncroachingBlockingGeometry(PawnToFit, ActorLocation, ActorRotation))
			{
				UnOccupiedStartPoints.Add(PlayerStart);
			}
			else if (GetWorld()->FindTeleportSpot(PawnToFit, ActorLocation, ActorRotation))
			{
				OccupiedStartPoints.Add(PlayerStart);
			}*/
		}
	}
	if (FoundPlayerStart == NULL)
	{
		if (UnOccupiedStartPoints.Num() > 0)
		{
			FoundPlayerStart = UnOccupiedStartPoints[FMath::RandRange(0, UnOccupiedStartPoints.Num() - 1)];
		}
		else if (OccupiedStartPoints.Num() > 0)
		{
			FoundPlayerStart = OccupiedStartPoints[FMath::RandRange(0, OccupiedStartPoints.Num() - 1)];
		}
	}
	return FoundPlayerStart;
}


/*
AActor* AGGameMode::ChoosePlayerStart_ImplementationInternal(AController* Player)
{
	APlayerStart* PlayerStart = FirstPlayerStart();
	//check(PlayerStart);
	if (!PlayerStart)
		return NULL;

	int retryCount = 10;
	for (int r = 0; r < retryCount; ++r)
	{
		AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
		if (MyGameState)
		{
			USceneComponent* comp = PlayerStart->GetRootComponent();
			comp->Mobility = EComponentMobility::Movable;

			// move the spawn spot to a random location on the map
			FVector worldPosition = PlayerStart->GetActorLocation();
			MyGameState->ActorRandomLocation(worldPosition);
			//check(worldPosition.Z == 10000.0f);
			PlayerStart->SetActorLocation(worldPosition, false);

			PlayerStart->SetActorRotation(FRotator(0, FMath::RandRange(0.0f, 360.0f), 0));
		}

		AActor* OkPlayerStart = Super::ChoosePlayerStart_Implementation(Player);
		if (OkPlayerStart)
			return OkPlayerStart;
	}

	// failed to find a free random location on the map, just spawn the best we can
	return PlayerStart;
}*/

AGGameState* AGGameMode::GetGameState() const
{
	return Cast<AGGameState>(GameState);
}


void AGGameMode::RequestFinishAndExitToMainMenu()
{
#if 0
	FinishMatch();
#endif

	UGGameInstance* const GI = Cast<UGGameInstance>(GetGameInstance());
	if (GI)
	{
		GI->RemoveSplitScreenPlayers();
	}

	AGPlayerController* LocalPrimaryController = nullptr;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AGPlayerController* Controller = Cast<AGPlayerController>(*Iterator);

		if (Controller == NULL)
		{
			continue;
		}

		if (!Controller->IsLocalController())
		{
			const FString RemoteReturnReason = NSLOCTEXT("NetworkErrors", "HostHasLeft", "Host has left the game.").ToString();
			Controller->ClientReturnToMainMenu(RemoteReturnReason);
		}
		else
		{
			LocalPrimaryController = Controller;
		}
	}

	// GameInstance should be calling this from an EndState.  So call the PC function that performs cleanup, not the one that sets GI state.
	if (LocalPrimaryController != NULL)
	{
		LocalPrimaryController->HandleReturnToMainMenu();
	}
}

void AGGameMode::ShowMessage(const FString& message)
{
	GetGameState()->ShowMessage(message);
}

void AGGameMode::ShowScoreboard()
{
	AGPlayerState* Winner = NULL;
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AGPlayerController* PC = Cast<AGPlayerController>(*It);
		if (!PC)
			continue;

		AGPlayerState* PlayerState = Cast<AGPlayerState>(PC->PlayerState);
		if (!Winner || PlayerState->GetScore() > Winner->GetScore())
		{
			Winner = PlayerState;
		}
	}

	GetGameState()->ShowScoreboard(WinMessage(Winner));
}

void AGGameMode::RestartPlayerControllersWithNoPawn()
{
	FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator();
	for (; it; ++it)
	{
		AGPlayerController* pc = Cast<AGPlayerController>(*it);
		if (!pc || pc->GetPawn())
			continue;

		RestartPlayer(pc);
	}
}

void AGGameMode::KillAI()
{
#if WITH_EDITOR
	for (int t = 0; t < Teams.Num(); ++t)
	{
		for (int i = 0; i < Teams[t]->TeamMembers.Num(); ++i)
		{
			APlayerController* PC = Cast<APlayerController>(Teams[t]->TeamMembers[i]);
			if (PC)
				continue;

			AGActor* actor = Cast<AGActor>(Teams[t]->TeamMembers[i]->GetPawn());
			if (!actor)
				continue;

			FPointDamageEvent PointDmg;
			PointDmg.Damage = 999999999;

			actor->TakeDamage(PointDmg.Damage, PointDmg, NULL, this);
		}
	}
#endif
}

void AGGameMode::EndRound()
{
	// notify players
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AGPlayerController* PC = Cast<AGPlayerController>(*It);
		if (!PC)
			continue;

		AGPlayerState* PlayerState = Cast<AGPlayerState>(PC->PlayerState);
		const bool bIsWinner = IsWinner(PlayerState);
		ComputeScore(PlayerState);
		(*It)->GameHasEnded(NULL, bIsWinner);
		/*
		UGPersistentUser* PersistentUser = PC->GetPersistentUser();
		if (!Achieve10GamesName.IsEmpty() && PersistentUser)
		{
			const float ACH_10_GAMES = 10.0f;			
			float Progress = PersistentUser->GetFloatValue(Achieve10GamesName);
			Progress += 1.0f;
			PersistentUser->SetFloatValue(Achieve10GamesName, Progress);
			float fPct = ((float)Progress / (float)ACH_10_GAMES);
			PC->UpdateAchievementProgress(Achieve10GamesName, fPct);
			PersistentUser->SaveIfDirty();
		}*/
		/*
		// only submit highscore for invasion
		if (GetGameState()->WaveCount > 0)
		{
			PC->UpdateLeaderboardsOnGameEnd();
		}*/
	}

	ShowScoreboard();
}

void AGGameMode::ComputeScore(class AGPlayerState* PS) const
{
	PS->SetScore(0.0f);
}

bool AGGameMode::IsWinner(class AGPlayerState* PlayerState) const
{
	return false;
}

FString AGGameMode::WinMessage(AGPlayerState* Winner) const
{
	return "";
}



void AGGameMode::DisableAI()
{
	bIsAIDisabled = true;
}

bool AGGameMode::IsAIDisabled()
{
	return bIsAIDisabled;
}