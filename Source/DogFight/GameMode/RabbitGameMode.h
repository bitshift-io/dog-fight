// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GGameMode.h"
#include "RabbitGameMode.generated.h"

class AGActor;
class ATeamInfo;
class AGAIController;

enum RabbitTeam
{
	RT_Rabbit,
	RT_Everyone,
};

enum RabbitGameState
{
	RGS_None,
	RGS_EndRound,
	RGS_RoundEnded,
	RGS_ResetRound,
};

/**
 * First player who gets a kill becomes the rabbit. 
 * Then its all players vs the rabbit.
 * The rabbit gets a special vehicle
 */
UCLASS()
class DOGFIGHT_API ARabbitGameMode : public AGGameMode
{
	GENERATED_BODY()

public:

	ARabbitGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage);

	virtual void StartNewPlayer(APlayerController* NewPlayer);

	virtual void HandleMatchHasStarted() override;

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType) override;
	
	void SetState(RabbitGameState state)	{ m_gameState = state; }

	virtual void Tick(float deltaTime);

	virtual void TickRabbit(AController* Rabbit, float deltaTime);

	virtual void ComputeScore(class AGPlayerState* PlayerState) const;
	virtual FString WinMessage(AGPlayerState* Winner) const;

	// have we got a rabbit yet? once set, this can never be null
	bool IsRabbit()	const { return RabbitPawnOrigionalClass != NULL; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float					MaxRabbitTime;

	// every n secconds show the rabbits time to indicate how close they are to winning
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float					ShowRabbitTimePeriod;
	
	RabbitGameState			m_gameState;

	/** Possible vehicles to spawn the rabbit as */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<AGActor> > RabbitActorClassList;

	UClass* RabbitPawnOrigionalClass;
};
