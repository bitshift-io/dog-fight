// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.IO;

public class DogFight : ModuleRules
{
	public DogFight(TargetInfo Target)
	{
        AddThirdPartyPrivateStaticDependencies(Target, "Steamworks");

        PublicDependencyModuleNames.AddRange(new string[] { 
            "Core", 
            "CoreUObject", 
            "Engine", 
            "InputCore",
            "AIModule", 
            "UMG", 
            "Slate", 
            "SlateCore", 
            "RHI", 
            "RenderCore", 
            //"HTTP", // pulls in libcurl? causes issues on linux
            "OnlineSubsystem",
            "OnlineSubsystemSteam",
            "OnlineSubsystemUtils",
            "PakFile",
            "EngineSettings",
            "Steamworks"
        });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        // joystick plugin
        PrivateDependencyModuleNames.AddRange(new string[] { "JoystickPlugin" });
        PrivateIncludePathModuleNames.AddRange(new string[] { "JoystickPlugin" });
        PrivateIncludePaths.AddRange(new string[] { "JoystickPlugin/Public", "JoystickPlugin/Private" });

        // steam is required!
        /*
        // apparently this only supports LAN only
        DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull");

        if ((Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Linux) || (Target.Platform == UnrealTargetPlatform.Mac))
        {
			if (UEBuildConfiguration.bCompileSteamOSS == true)
			{
				DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
			}
		}
        else if (Target.Platform == UnrealTargetPlatform.PS4)
        {
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemPS4");
        }
        else if (Target.Platform == UnrealTargetPlatform.XboxOne)
        {
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemLive");
        }*/


        // add joystick plugin libraries
        string BaseDirectory = Path.GetFullPath(Path.Combine(ModuleDirectory, "..", ".."));
        string BinDirectory = Path.Combine(BaseDirectory, "Binaries", Target.Platform.ToString());

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "SDL2.dll")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "steam_api64.dll")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "steam_appid.txt")));

            //RuntimeDependencies.Add(new RuntimeDependency("C:\\Apps\\Epic Games\\4.11\\Engine\\Binaries\\ThirdParty\\Steamworks\\Steamv132\\Win64\\steam_api64.dll"));
            
        }

        if (Target.Platform == UnrealTargetPlatform.Mac)
        {
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "libsteam_api.dylib")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "steam_appid.txt")));
        }

	if (Target.Platform == UnrealTargetPlatform.Linux)
        {
		// linux adds this to engine bin dir
            //RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "libsteam_api.so")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDirectory, "steam_appid.txt")));
        }
    }
}
