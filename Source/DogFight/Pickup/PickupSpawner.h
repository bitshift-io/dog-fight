#pragma once

#include "PickupSpawner.generated.h"

USTRUCT()
struct FPickupStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpawnChance;

	float SpawnChanceNormalized;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<APickup>		PickupClass;
};

// Responsible for spawning pickups when actors die
UCLASS(Abstract, Blueprintable)
class APickupSpawner : public AActor
{
	GENERATED_UCLASS_BODY()

	/** initial setup */
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnKilled(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType);

protected:

	virtual void OnActorKilled(class AGActor* Killed, class AGActor* Killer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpawnChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FPickupStruct>	PickupClassList;
};
