// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "PickupSpawner.h"

#pragma optimize("", OPTIMISATION)

APickupSpawner::APickupSpawner(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void APickupSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	// register on pickup list (server only), don't care about unregistering (in FinishDestroy) - no streaming
	AGGameMode* GameMode = GetWorld()->GetAuthGameMode<AGGameMode>();
	if (GameMode)
	{
		GameMode->OnKilled.AddDynamic(this, &APickupSpawner::OnKilled);
	}
}

void APickupSpawner::OnKilled(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	AGActor* killedActor = Cast<AGActor>(KilledPawn);
	AGActor* killerActor = Killer ? Cast<AGActor>(Killer->GetPawn()) : NULL;
	OnActorKilled(killedActor, killerActor);
}

void APickupSpawner::OnActorKilled(class AGActor* Killed, class AGActor* Killer)
{
	bool spawn = FMath::FRandRange(0.0f, 1.0f) <= SpawnChance;
	if (!spawn || !Killed)
		return;

	// TODO: some weighting to modify spawn chance per pickup
	/*
	// normalise spawn chance
	float spawnChanceTotal = 0.0f;
	for (int i = 0; i < PickupClassList.Num(); ++i)
	{
		spawnChanceTotal += PickupClassList[i].SpawnChance;
	}

	for (int i = 0; i < PickupClassList.Num(); ++i)
	{
		PickupClassList[i].SpawnChanceNormalized = PickupClassList[i].SpawnChance / spawnChanceTotal;
	}
	

	float spawnNum = FMath::FRandRange(0.0f, 1.0f);
	*/

	uint32 spawnIdx = FMath::RandRange(0.0, PickupClassList.Num() - 1);

	FRotator r(Killed->ActorViewRotation());
	FVector location = Killed->GetActorLocation();

	//APickup* pickup = Cast<APickup>(GetWorld()->SpawnActor(PickupClassList[spawnIdx].PickupClass, &location, &r, SpawnInfo));

	FTransform SpawnTM(r, location);
	APickup* pickup = Cast<APickup>(UGameplayStatics::BeginDeferredActorSpawnFromClass(Killed, PickupClassList[spawnIdx].PickupClass, SpawnTM));
	if (pickup)
	{
		pickup->Instigator = Instigator;
		pickup->SetOwner(this);

		UGameplayStatics::FinishSpawningActor(pickup, SpawnTM);
	}
}
