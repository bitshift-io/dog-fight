#pragma once

#include "Pickup.generated.h"

// Base class for pickup objects that can be placed in the world
UCLASS(abstract)
class APickup : public AActor
{
	GENERATED_UCLASS_BODY()

	/** pickup on touch */
	virtual void NotifyActorBeginOverlap(class AActor* Other);

	/** check if pawn can use this pickup */
	virtual bool CanBePickedUp(class AGActor* TestPawn);

	/** initial setup */
	virtual void BeginPlay() override;

	/** initial setup */
	virtual void PostInitializeComponents() override;

protected:

	/** collisions */
	UPROPERTY(VisibleDefaultsOnly, Category = Pickup)
	USphereComponent* CollisionComp;

	/** FX component */
	UPROPERTY(VisibleDefaultsOnly, Category=Effects)
	UParticleSystemComponent* PickupPSC;


	/** FX of active pickup */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	UParticleSystem* ActiveFX;

	/** FX of pickup on respawn timer */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	UParticleSystem* RespawningFX;

	/** sound played when player picks it up */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	USoundCue* PickupSound;

	/** sound played on respawn */
	UPROPERTY(EditDefaultsOnly, Category=Effects)
	USoundCue* RespawnSound;

	/** how long it takes to respawn? */
	UPROPERTY(EditDefaultsOnly, Category=Pickup)
	float RespawnTime;

	/** is it ready for interactions? */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_IsActive)
	uint32 bIsActive:1;

	/* The character who has picked up this pickup */
	UPROPERTY(Transient, Replicated)
	AGActor* PickedUpBy;

	/** Handle for efficient management of RespawnPickup timer */
	FTimerHandle TimerHandle_RespawnPickup;

	UFUNCTION()
	void OnRep_IsActive();

	/** give pickup */
	virtual void GivePickupTo(class AGActor* Pawn);

	/** handle touches */
	void PickupOnTouch(class AGActor* Pawn);

	/** show and enable pickup */
	virtual void RespawnPickup();

	/** show effects when pickup disappears */
	UFUNCTION(reliable, NetMulticast)
	virtual void OnPickedUp();

	/** show effects when pickup appears */
	virtual void OnRespawned();

	/** blueprint event: pickup disappears */
	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Picked", DisplayName = "On Picked Up"))
	void OnPickedUpEvent(class AGActor* By);

	/** blueprint event: pickup appears */
	UFUNCTION(BlueprintImplementableEvent)
	void OnRespawnEvent();

	virtual void Tick(float deltaTime) override;

	bool WrapAround(FVector& worldPosition) const;

	FQuat ActorViewRotation() const;

	// Metres per second
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LinearSpeed;

protected:
	/** Returns PickupPSC subobject **/
	FORCEINLINE UParticleSystemComponent* GetPickupPSC() const { return PickupPSC; }
};
