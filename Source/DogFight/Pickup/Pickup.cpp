// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "Pickup.h"
#include "Particles/ParticleSystemComponent.h"

APickup::APickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(100.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PICKUP); // COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap); // ECR_Block);
	RootComponent = CollisionComp;

	/*
	UCapsuleComponent* CollisionComp = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(40.0f, 50.0f);
	CollisionComp->SetCollisionObjectType(COLLISION_PICKUP);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	RootComponent = CollisionComp;
	*/

	PickupPSC = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("PickupFX"));
	PickupPSC->bAutoActivate = false;
	PickupPSC->bAutoDestroy = false;
	PickupPSC->AttachParent = RootComponent;

	RespawnTime = 0.0f;
	bIsActive = false;
	PickedUpBy = NULL;

	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	PrimaryActorTick.bStartWithTickEnabled = true;
	bAlwaysRelevant = true;
}

void APickup::BeginPlay()
{
	Super::BeginPlay();

	RespawnPickup();

	// register on pickup list (server only), don't care about unregistering (in FinishDestroy) - no streaming
	AGGameMode* GameMode = GetWorld()->GetAuthGameMode<AGGameMode>();
	if (GameMode)
	{
		GameMode->LevelPickups.Add(this);
	}
}

void APickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CollisionComp->MoveIgnoreActors.Add(Instigator);
}

void APickup::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);
	PickupOnTouch(Cast<AGActor>(Other));
}

bool APickup::CanBePickedUp(class AGActor* TestPawn)
{
	// Modify based on game rules.
	AGGameMode* const Game = GetWorld()->GetAuthGameMode<AGGameMode>();
	if (!Game)
	{
		return false;
	}

	return TestPawn && TestPawn->IsAlive() && Game->CanBePickedUp(this, TestPawn);
}

void APickup::GivePickupTo(class AGActor* Pawn)
{
	// register that player has picked up an item in their stats
	if (Pawn && Pawn->GetController() && Pawn->GetController()->PlayerState)
	{
		++Cast<AGPlayerState>(Pawn->GetController()->PlayerState)->NumPickups;
	}
}

void APickup::PickupOnTouch(class AGActor* Pawn)
{
	// client cannot do anything here, up to server to call this code
	if (Role < ROLE_Authority)
	{
		return;
	}

	if (bIsActive && Pawn && Pawn->IsAlive() && !IsPendingKill())
	{
		if (CanBePickedUp(Pawn))
		{
			GivePickupTo(Pawn);
			PickedUpBy = Pawn;

			if (!IsPendingKill())
			{
				bIsActive = false;
				OnPickedUp();

				if (RespawnTime > 0.0f)
				{
					GetWorldTimerManager().SetTimer(TimerHandle_RespawnPickup, this, &APickup::RespawnPickup, RespawnTime, false);
				}
				else // added by FM
				{
					PickupPSC->bAutoDestroy = true;
					PickupPSC->DeactivateSystem();
					Destroy();
				}
			}
		}
	}
}

void APickup::RespawnPickup()
{
	bIsActive = true;
	PickedUpBy = NULL;
	OnRespawned();

	TArray<AActor*> OverlappingPawns;
	GetOverlappingActors(OverlappingPawns, AGActor::StaticClass());

	for (int32 i = 0; i < OverlappingPawns.Num(); i++)
	{
		PickupOnTouch(Cast<AGActor>(OverlappingPawns[i]));	
	}
}

void APickup::OnPickedUp_Implementation()
{
	if (RespawningFX)
	{
		PickupPSC->SetTemplate(RespawningFX);
		PickupPSC->ActivateSystem();
	}
	else
	{
		PickupPSC->DeactivateSystem();
	}

	if (PickupSound && PickedUpBy)
	{
		UGameplayStatics::SpawnSoundAttached(PickupSound, PickedUpBy->GetRootComponent());
	}

	OnPickedUpEvent(PickedUpBy);
}

void APickup::OnRespawned()
{
	if (ActiveFX)
	{
		PickupPSC->SetTemplate(ActiveFX);
		PickupPSC->ActivateSystem();
	}
	else
	{
		PickupPSC->DeactivateSystem();
	}

	const bool bJustSpawned = CreationTime <= (GetWorld()->GetTimeSeconds() + 5.0f);
	if (RespawnSound && !bJustSpawned)
	{
		UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
	}

	OnRespawnEvent();
}

void APickup::OnRep_IsActive()
{
	if (bIsActive)
	{
		OnRespawned();
	}
	else
	{
		OnPickedUp();
	}
}

void APickup::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( APickup, bIsActive );
	DOREPLIFETIME( APickup, PickedUpBy );
}

void APickup::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	// move up the screen
	FVector position = GetActorLocation();
	
	FVector fwd = ActorViewRotation().GetAxisX();
	position = position + (fwd * LinearSpeed * 100.0f * deltaTime); // 100 = cm to meters

	if (Role == ROLE_Authority && WrapAround(position))
	{
		// https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AActor/TeleportTo/index.html
		TeleportTo(position, GetActorRotation(), false, true);
	}

	SetActorLocation(position);
}

FQuat APickup::ActorViewRotation() const
{
	AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
	if (!MyGameState)
		return FQuat::Identity;

	return MyGameState->ActorViewRotation();
}

bool APickup::WrapAround(FVector& worldPosition) const
{
	AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
	if (!MyGameState)
		return false;

	return MyGameState->WrapAround(worldPosition, CollisionComp->GetScaledSphereRadius());
}