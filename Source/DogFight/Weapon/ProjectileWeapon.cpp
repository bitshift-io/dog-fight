// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "ProjectileWeapon.h"
#include "Projectile.h"
#include "Actor/GActor.h"

#pragma optimize("", OPTIMISATION)

AProjectileWeapon::AProjectileWeapon(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bInheritActorVelocity = true;
	UpgradeLevel = WUL_SingleBeam;
}

void AProjectileWeapon::BeginPlay()
{
	Super::BeginPlay();
	InitialTimeBetweenShots = WeaponConfig.TimeBetweenShots;
}

//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AProjectileWeapon::FireWeapon()
{
	OnFire();
	if (MyPawn)
	{
		MyPawn->OnFire();
	}

	//Origin = Origin + ShootDir * 2000.0f; // test to get out of actors own collision
	/*
	// trace from camera to check what's under crosshair
	const float ProjectileAdjustRange = 10000.0f;
	const FVector StartTrace = GetCameraDamageStartLocation(ShootDir);
	const FVector EndTrace = StartTrace + ShootDir * ProjectileAdjustRange;
	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	// and adjust directions to hit that actor
	if (Impact.bBlockingHit)
	{
	const FVector AdjustedDir = (Impact.ImpactPoint - Origin).GetSafeNormal();
	bool bWeaponPenetration = false;

	const float DirectionDot = FVector::DotProduct(AdjustedDir, ShootDir);
	if (DirectionDot < 0.0f)
	{
	// shooting backwards = weapon is penetrating
	bWeaponPenetration = true;
	}
	else if (DirectionDot < 0.5f)
	{
	// check for weapon penetration if angle difference is big enough
	// raycast along weapon mesh to check if there's blocking hit

	FVector MuzzleStartTrace = Origin - GetMuzzleDirection() * 150.0f;
	FVector MuzzleEndTrace = Origin;
	FHitResult MuzzleImpact = WeaponTrace(MuzzleStartTrace, MuzzleEndTrace);

	if (MuzzleImpact.bBlockingHit)
	{
	bWeaponPenetration = true;
	}
	}

	if (bWeaponPenetration)
	{
	// spawn at crosshair position
	Origin = Impact.ImpactPoint - ShootDir * 10.0f;
	}
	else
	{
	// adjust direction to hit
	ShootDir = AdjustedDir;
	}
	}*/



	
	ServerFireWeapon();
}

bool AProjectileWeapon::ServerFireWeapon_Validate()
{
	return true;
}

void AProjectileWeapon::ServerFireWeapon_Implementation()
{
	// if we are shooting, try to lock on to a target
	SetTargetPawn(FindClosestEnemyWithLOS(NULL));

	FVector ShootDir = GetAdjustedAim();
	FVector Origin = GetMuzzleLocation();
	
	ShootDir *= ProjectileConfig.ProjectileSpeed;

	// add instigators velocity to the projectile
	if (Instigator && bInheritActorVelocity)
	{
		ShootDir += Instigator->GetVelocity();
	}

	TArray<AProjectile*> projectileList;

	const float DoubleBeamAngle = 5.0f;
	const float TripleBeamAngle = 10.0f;

	switch (UpgradeLevel)
	{
	case WUL_SingleBeam:
		{
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, 0));
		}
		break;

	case WUL_DoubleBeam:
		{
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, DoubleBeamAngle));
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, -DoubleBeamAngle));
		}
		break;

	case WUL_TripleBeam:
		{
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, TripleBeamAngle));
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, 0));
			projectileList.Add(ServerFireProjectileAtAngle(Origin, ShootDir, -TripleBeamAngle));
		}
		break;
	}

	// tell all the projectiles that are fired to ignore each other
	for (int o = 0; o < projectileList.Num(); ++o)
	{
		for (int i = 0; i < projectileList.Num(); ++i)
		{
			if (o == i)
			{
				continue;
			}

			projectileList[o]->AddIgnoreActor(projectileList[i]);
		}
	}
}

AProjectile* AProjectileWeapon::ServerFireProjectileAtAngle(FVector Origin, FVector_NetQuantizeNormal ShootDir, float Angle)
{
	FRotator Rotator(0, Angle, 0);
	FRotationMatrix RotationMatrix(Rotator);
	FVector ShootDirRotated = RotationMatrix.TransformVector(ShootDir);
	ShootDir = ShootDirRotated;

	FTransform SpawnTM(ShootDir.Rotation(), Origin);
	AProjectile* Projectile = Cast<AProjectile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ProjectileConfig.ProjectileClass, SpawnTM));
	if (Projectile)
	{
		Projectile->Instigator = Instigator;
		Projectile->SetOwner(this);
		Projectile->InitVelocity(ShootDir);
		Projectile->SetTargetPawn(TargetPawn);

		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTM);
	}

	return Projectile;
}

void AProjectileWeapon::ApplyWeaponConfig(FProjectileWeaponData& Data)
{
	Data = ProjectileConfig;
}


void AProjectileWeapon::UpgradeWeapon()
{
	unsigned int currentLevel = UpgradeLevel;
	UpgradeLevel = FMath::Min(UpgradeLevel + 1, (unsigned int)WUL_TripleBeam);
	
	// on origional dogfighters this was the code
	// I *think* once you got an upgrade you would keep it
	// so the upgrade path was slow
	// because we get a new pawn after death in ue4, we loose the pickup 
	// so will speed up the upgrade path
	/*
	// pulled from the dogfight metadata it controls firerate changes and upgrade speed
	const float Increment = -0.1f;
	const float MinTimeBetweenShots = 0.5f;

	// what this code does is decrement the reload rate, until min is reached, once min is reached,
	// weapons get upgraded to level 2... rinse and repeat
	if (WeaponConfig.TimeBetweenShots <= MinTimeBetweenShots)
	{
		unsigned int currentLevel = UpgradeLevel;
		UpgradeLevel = FMath::Min(UpgradeLevel + 1, unsigned int(WUL_TripleBeam));

		// have we changed level? if so, reset fire speed
		if (currentLevel != UpgradeLevel)
		{
			WeaponConfig.TimeBetweenShots = InitialTimeBetweenShots;
		}
	}
	else
	{
		if (Increment < 0.f)
			WeaponConfig.TimeBetweenShots = FMath::Max(MinTimeBetweenShots, WeaponConfig.TimeBetweenShots + Increment);
		//else
		//	WeaponConfig.TimeBetweenShots = FMath::Max(MaxTimeBetweenShots, WeaponConfig.TimeBetweenShots + Increment);
	}*/
}
