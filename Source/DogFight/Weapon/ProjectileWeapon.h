// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Weapon.h"
#include "ProjectileWeapon.generated.h"

USTRUCT()
struct FProjectileWeaponData
{
	GENERATED_USTRUCT_BODY()

	/** projectile class */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;

	/** life time */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	float ProjectileLifeMin;

	/** life time */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	float ProjectileLifeMax;

	/** projectile speed */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	float ProjectileSpeed;

	/** damage at impact point */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float ExplosionDamage;

	/** radius of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float ExplosionRadius;

	/** type of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	TSubclassOf<UDamageType> DamageType;

	/** defaults */
	FProjectileWeaponData()
	{
		ProjectileClass = NULL;
		ProjectileLifeMin = 10.0f;
		ProjectileLifeMax = 0.0f;
		ExplosionDamage = 100.0f;
		ExplosionRadius = 300.0f;
		DamageType = UDamageType::StaticClass();
		ProjectileSpeed = 2500.0f;
	}
};


enum WeaponUpgradeLevel
{
	WUL_SingleBeam,
	WUL_DoubleBeam,
	WUL_TripleBeam,
};

// A weapon that fires a visible projectile
// also handles weapon upgrading as per dogfighters ColourWeapon
UCLASS(Abstract)
class AProjectileWeapon : public AWeapon
{
	GENERATED_UCLASS_BODY()

	/** apply config on projectile */
	void ApplyWeaponConfig(FProjectileWeaponData& Data);

	virtual void BeginPlay();
	virtual void UpgradeWeapon();

protected:

	virtual EAmmoType GetAmmoType() const override
	{
		return EAmmoType::ERocket;
	}

	/** weapon config */
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FProjectileWeaponData ProjectileConfig;

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] weapon specific fire implementation */
	virtual void FireWeapon() override;

	void FireProjectileAtAngle(float Angle);

	/** spawn projectile on server */
	UFUNCTION(reliable, server, WithValidation)
	void ServerFireWeapon();

	AProjectile* ServerFireProjectileAtAngle(FVector Origin, FVector_NetQuantizeNormal ShootDir, float Angle);

	/** does projectile inherit the firing actors velocity? */
	UPROPERTY(EditDefaultsOnly, Category = Config)
	uint32 bInheritActorVelocity : 1;


	uint32	UpgradeLevel;
	float	InitialTimeBetweenShots;
};
