#include "DogFight.h"
#include "Projectile.h"
#include "UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Effect/ExplosionEffect.h"
#include "Weapon.h"
#include "GameMode/GGameMode.h"

#pragma optimize("", OPTIMISATION)

UGProjectileMovementComponent::UGProjectileMovementComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UGProjectileMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UGProjectileMovementComponent::SetComponentTickEnabled(bool bEnabled)
{
	Super::SetComponentTickEnabled(bEnabled);
}

void UGProjectileMovementComponent::StopSimulating(const FHitResult& HitResult)
{
	/*
	// only server can stop simulating
	AActor* MyActor = GetOwner();
	if (MyActor && MyActor->Role == ROLE_Authority)
	{
		Super::StopSimulating(HitResult);
	}*/
	Super::StopSimulating(HitResult);
}


AProjectile::AProjectile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetNotifyRigidBodyCollision(true); // "Simulation Generate Hit Events"
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	
	RootComponent = CollisionComp;

	ParticleComp = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ParticleComp"));
	ParticleComp->bAutoActivate = false;
	ParticleComp->bAutoDestroy = false;
	ParticleComp->AttachParent = RootComponent;

	MovementComp = ObjectInitializer.CreateDefaultSubobject<UGProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->InitialSpeed = 2000.0f;
	MovementComp->MaxSpeed = 2000.0f;
	MovementComp->bRotationFollowsVelocity = true;
	MovementComp->ProjectileGravityScale = 0.f;
	MovementComp->bAutoActivate = true;
	MovementComp->PrimaryComponentTick.bStartWithTickEnabled = true;
	MovementComp->PrimaryComponentTick.bCanEverTick = true;
	MovementComp->PrimaryComponentTick.SetTickFunctionEnable(false);


	SetReplicates(true);
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bReplicateMovement = false; // we do this with our own movement code
	//NetPriority = 3.0f;
	bAlwaysRelevant = true;
}

void AProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	MovementComp->OnProjectileStop.AddDynamic(this, &AProjectile::OnProjectileStop);
	AddIgnoreActor(Instigator);
	
	CollisionComp->OnComponentHit.AddDynamic(this, &AProjectile::OnComponentHit);

	AProjectileWeapon* OwnerWeapon = Cast<AProjectileWeapon>(GetOwner());
	if (OwnerWeapon)
	{
		OwnerWeapon->ApplyWeaponConfig(WeaponConfig);
	}

	SetLifeSpan(FMath::FRandRange(WeaponConfig.ProjectileLifeMin, FMath::Max(WeaponConfig.ProjectileLifeMin, WeaponConfig.ProjectileLifeMax)));
	MyController = GetInstigatorController();
	OverrideColourOnAllMaterials();

	// clients dont do any collision
	if (Role < ROLE_Authority)
	{
		CollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void AProjectile::AddIgnoreActor(AActor* IgnoreActor)
{
	CollisionComp->MoveIgnoreActors.Add(IgnoreActor);
}

void AProjectile::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	//MovementComp->SetActive(true)
	check(MovementComp->IsComponentTickEnabled());
}

void AProjectile::OnRep_Instigator()
{
	Super::OnRep_Instigator();
	OverrideColourOnAllMaterials();	
}

void AProjectile::OverrideColourOnAllMaterials()
{
	// tweak the colours of the spawned projectile
	AGActor* Actor = Cast<AGActor>(Instigator);
	if (Actor)
		Actor->OverrideColourOnAllMaterials(GetRootComponent());
}

void AProjectile::InitVelocity(FVector& ShootDirection)
{
	if (MovementComp)
	{
		FVector ShootDir = ShootDirection;
		float ShootSpeed = ShootDir.Size();
		ShootDir.Normalize();
		MovementComp->Velocity = ShootDir * (ShootSpeed + MovementComp->InitialSpeed);
		MovementComp->InitialSpeed = 0.0f; // zero this to stop the movement component changing our velocity
	}
}

void AProjectile::OnComponentHit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// bounce projectiles should not explode until the stop, see OnProjectileStop
	if (!MovementComp->bShouldBounce)
	{
		OnImpact(Hit);
	}
}

void AProjectile::OnProjectileStop(const FHitResult& HitResult)
{
	OnImpact(HitResult);
}

void AProjectile::OnImpact(const FHitResult& HitResult)
{
	if (Role == ROLE_Authority/* && !bExploded*/)
	{
		Explode(HitResult);
		//DisableAndDestroy();
	}
}

void AProjectile::DealDamage(const FHitResult& Impact)
{
	if (!Impact.GetActor())
		return;

	FPointDamageEvent PointDmg;
	PointDmg.DamageTypeClass = WeaponConfig.DamageType;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = MovementComp->Velocity;
	PointDmg.Damage = WeaponConfig.ExplosionDamage;

	// NOTE: this might not work in multiplayer - but should clients be taking damage?! TODO: FIXME!
	// send damage to actor via the gamemode
	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, MyController.Get(), this);
}

float AProjectile::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	float result = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	/*
	// setup a fake impact to avoid duplicating code
	if (!bExploded)
	{
		FHitResult Impact;
		Impact.ImpactNormal = GetActorForwardVector();
		Impact.ImpactPoint = GetActorLocation();
		Explode(Impact);
		DisableAndDestroy();
	}*/

	SetLifeSpan(0.0001f); // something tiny to queue destruction of this projectile

	return result;
}

void AProjectile::LifeSpanExpired()
{
//	Super::LifeSpanExpired();

	// setup a fake impact to avoid duplicating code
	//if (!bExploded)
	{
		FHitResult Impact;
		Impact.ImpactNormal = GetActorForwardVector();
		Impact.ImpactPoint = GetActorLocation();
		Explode(Impact);
	}
}

void AProjectile::Explode_Implementation(const FHitResult& Impact)
{
	/*
	if (bExploded)
	{
		return;
	}*/

	// added by fabian to deal damage
	if (Role == ROLE_Authority)
	{
		DealDamage(Impact);
	}

	if (ParticleComp)
	{
		ParticleComp->Deactivate();
	}

	// effects and damage origin shouldn't be placed inside mesh at impact point
	const FVector NudgedImpactLocation = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;

	if (Role == ROLE_Authority && WeaponConfig.ExplosionDamage > 0 && WeaponConfig.ExplosionRadius > 0 && WeaponConfig.DamageType)
	{
		UGameplayStatics::ApplyRadialDamage(this, WeaponConfig.ExplosionDamage, NudgedImpactLocation, WeaponConfig.ExplosionRadius, WeaponConfig.DamageType, TArray<AActor*>(), this, MyController.Get());
	}

	if (ExplosionTemplate)
	{
		const FRotator SpawnRotation = Impact.ImpactNormal.Rotation();

		AActor* ExplosionActor = GetWorld()->SpawnActorDeferred<AActor>(ExplosionTemplate, FTransform(SpawnRotation, NudgedImpactLocation));
		if (ExplosionActor)
		{
			ExplosionActor->Instigator = Instigator;
			ExplosionActor->SetOwner(this);

			AExplosionEffect* EffectActor = Cast<AExplosionEffect>(ExplosionActor);
			if (EffectActor)
			{
				EffectActor->SurfaceHit = Impact;
			}

			UGameplayStatics::FinishSpawningActor(ExplosionActor, FTransform(SpawnRotation, NudgedImpactLocation));
		}
	}

	//bExploded = true;
	if (Role == ROLE_Authority)
	{
		Destroy();
	}
}

/*
void AProjectile::DisableAndDestroy()
{
	UAudioComponent* ProjAudioComp = FindComponentByClass<UAudioComponent>();
	if (ProjAudioComp && ProjAudioComp->IsPlaying())
	{
		ProjAudioComp->FadeOut(0.1f, 0.f);
	}

	MovementComp->StopMovementImmediately();
	
	// give clients some time to show explosion
	//SetLifeSpan(2.0f);

	Destroy();
}*/

/*
///CODE_SNIPPET_START: AActor::GetActorLocation AActor::GetActorRotation
void AProjectile::OnRep_Exploded()
{
	FVector ProjDirection = GetActorRotation().Vector();

	const FVector StartTrace = GetActorLocation() - ProjDirection * 200;
	const FVector EndTrace = GetActorLocation() + ProjDirection * 150;
	FHitResult Impact;

	if (!GetWorld()->LineTraceSingleByChannel(Impact, StartTrace, EndTrace, COLLISION_PROJECTILE, FCollisionQueryParams(TEXT("ProjClient"), true, Instigator)))
	{
		// failsafe
		Impact.ImpactPoint = GetActorLocation();
		Impact.ImpactNormal = -ProjDirection;
	}

	Explode(Impact);
}
///CODE_SNIPPET_END
*/

void AProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (MovementComp)
	{
		MovementComp->Velocity = NewVelocity;
	}
}

void AProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(AProjectile, bExploded);

	DOREPLIFETIME_CONDITION(AProjectile, UTProjReplicatedMovement, COND_SimulatedOrPhysics);
}

void AProjectile::SetTargetPawn(AGActor* T)
{
	Target = T;
	OnRep_Target();
}

void AProjectile::OnRep_Target()
{
	if (Target)
	{
		MovementComp->HomingTargetComponent = Target->GetRootComponent();
	}
}

void AProjectile::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	if (true) // bForceNextRepMovement || bReplicateUTMovement)
	{
		GatherCurrentMovement();
		//bForceNextRepMovement = false;
	}
}

void AProjectile::GatherCurrentMovement()
{
	/* @TODO FIXMESTEVE support projectiles uing rigid body physics
	UPrimitiveComponent* RootPrimComp = Cast<UPrimitiveComponent>(GetRootComponent());
	if (RootPrimComp && RootPrimComp->IsSimulatingPhysics())
	{
	FRigidBodyState RBState;
	RootPrimComp->GetRigidBodyState(RBState);

	ReplicatedMovement.FillFrom(RBState);
	}
	else
	*/
	if (RootComponent != NULL)
	{
		// If we are attached, don't replicate absolute position
		if (RootComponent->AttachParent != NULL)
		{
			// Networking for attachments assumes the RootComponent of the AttachParent actor. 
			// If that's not the case, we can't update this, as the client wouldn't be able to resolve the Component and would detach as a result.
			FRepAttachment& AttachmentReplication = const_cast<FRepAttachment&>(GetAttachmentReplication()); // test this in 4.11
			if (AttachmentReplication.AttachParent != NULL)
			{
				AttachmentReplication.LocationOffset = RootComponent->RelativeLocation;
				AttachmentReplication.RotationOffset = RootComponent->RelativeRotation;
				AttachmentReplication.RelativeScale3D = RootComponent->RelativeScale3D;
			}
		}
		else
		{
			UTProjReplicatedMovement.Location = RootComponent->GetComponentLocation();
			UTProjReplicatedMovement.Rotation = RootComponent->GetComponentRotation();
			UTProjReplicatedMovement.LinearVelocity = GetVelocity();
		}
	}
}

void AProjectile::OnRep_UTProjReplicatedMovement()
{
	if (Role == ROLE_SimulatedProxy)
	{
		//ReplicatedAccel = UTReplicatedMovement.Acceleration;
		ReplicatedMovement.Location = UTProjReplicatedMovement.Location;
		ReplicatedMovement.Rotation = UTProjReplicatedMovement.Rotation;
		ReplicatedMovement.LinearVelocity = UTProjReplicatedMovement.LinearVelocity;
		ReplicatedMovement.AngularVelocity = FVector(0.f);
		ReplicatedMovement.bSimulatedPhysicSleep = false;
		ReplicatedMovement.bRepPhysics = false;

		OnRep_ReplicatedMovement();
	}
}
