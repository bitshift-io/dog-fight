#pragma once

#include "GameFramework/Actor.h"
#include "ProjectileWeapon.h"
#include "Projectile.generated.h"

class AGActor;

// https://forums.unrealengine.com/showthread.php?56211-How-do-i-code-server-rpc-in-4-7

/** Replicated movement data of our RootComponent.
* More efficient than engine's FRepMovement
*/
USTRUCT()
struct FRepUTProjMovement
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		FVector_NetQuantize LinearVelocity;

	UPROPERTY()
		FVector_NetQuantize Location;

	UPROPERTY()
		FRotator Rotation;

	FRepUTProjMovement()
		: LinearVelocity(ForceInit)
		, Location(ForceInit)
		, Rotation(ForceInit)
	{}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		bOutSuccess = true;

		bool bOutSuccessLocal = true;

		// update location, linear velocity
		Location.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;
		Rotation.SerializeCompressed(Ar);
		LinearVelocity.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;

		return true;
	}

	bool operator==(const FRepUTMovement& Other) const
	{
		if (LinearVelocity != Other.LinearVelocity)
		{
			return false;
		}

		if (Location != Other.Location)
		{
			return false;
		}

		if (Rotation != Other.Rotation)
		{
			return false;
		}
		return true;
	}

	bool operator!=(const FRepUTMovement& Other) const
	{
		return !(*this == Other);
	}
};



UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent), ShowCategories = (Velocity))
class UGProjectileMovementComponent : public UProjectileMovementComponent
{
	GENERATED_UCLASS_BODY()

public:

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction);
	virtual void SetComponentTickEnabled(bool bEnabled);

	virtual void StopSimulating(const FHitResult& HitResult);
};

UCLASS(Abstract, Blueprintable)
class AProjectile : public AActor
{
	GENERATED_UCLASS_BODY()


	/** initial setup */
	virtual void PostInitializeComponents() override;

	/** setup velocity */
	void InitVelocity(FVector& ShootDirection);

	/** handle hit */
	UFUNCTION()
		void OnImpact(const FHitResult& HitResult);

	/** set the projectiles's target pawn */
	void SetTargetPawn(AGActor* AGActor);

	void AddIgnoreActor(AActor* IgnoreActor);

private:
	/** movement component */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
		UGProjectileMovementComponent* MovementComp;

	/** collisions */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
		USphereComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
		UParticleSystemComponent* ParticleComp;
protected:

	/** effects for explosion */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<class AActor> ExplosionTemplate; // was AExplosionEffect

	/** controller that fired me (cache for damage calculations) */
	TWeakObjectPtr<AController> MyController;

	/** projectile data */
	struct FProjectileWeaponData WeaponConfig;

	/** did it explode? */
	//UPROPERTY(Transient, ReplicatedUsing = OnRep_Exploded)
	//	bool bExploded;

	/** [client] explosion happened */
	//UFUNCTION()
	//	void OnRep_Exploded();

	/** trigger explosion */
	UFUNCTION(reliable, NetMulticast)
	void Explode(const FHitResult& Impact);

	/** shutdown projectile and prepare for destruction */
	//void DisableAndDestroy();

	/** update velocity on client */
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

	void DealDamage(const FHitResult& Impact);

	/** Called when the lifespan of an actor expires (if he has one). */
	virtual void LifeSpanExpired();

	UFUNCTION()
	virtual void OnComponentHit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	virtual void OnProjectileStop(const FHitResult& HitResult);

	virtual void OnRep_Instigator();

	void OverrideColourOnAllMaterials();

	virtual void Tick(float deltaTime) override;

	/** UTProjReplicatedMovement struct replication event */
	UFUNCTION()
	virtual void OnRep_UTProjReplicatedMovement();

	virtual void GatherCurrentMovement() override;

	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;

	/** Used for replication of our RootComponent's position and velocity */
	UPROPERTY(ReplicatedUsing = OnRep_UTProjReplicatedMovement)
	struct FRepUTProjMovement UTProjReplicatedMovement;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);
	
	UFUNCTION()
	virtual void OnRep_Target();

	UPROPERTY(ReplicatedUsing = OnRep_Target)
	AGActor* Target;

protected:
	/** Returns MovementComp subobject **/
	FORCEINLINE UProjectileMovementComponent* GetMovementComp() const { return MovementComp; }
	/** Returns CollisionComp subobject **/
	FORCEINLINE USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ParticleComp subobject **/
	FORCEINLINE UParticleSystemComponent* GetParticleComp() const { return ParticleComp; }
};