
#pragma once

void SetIntPropertyByName(UObject* Object, FName PropertyName, int32 Value);
void SetBytePropertyByName(UObject* Object, FName PropertyName, uint8 Value);
void SetFloatPropertyByName(UObject* Object, FName PropertyName, float Value);
void SetBoolPropertyByName(UObject* Object, FName PropertyName, bool Value);
void SetObjectPropertyByName(UObject* Object, FName PropertyName, UObject* Value);
void SetClassPropertyByName(UObject* Object, FName PropertyName, TSubclassOf<UObject> Value);
void SetStringPropertyByName(UObject* Object, FName PropertyName, const FString& Value);
void SetNamePropertyByName(UObject* Object, FName PropertyName, const FName& Value);
void SetTextPropertyByName(UObject* Object, FName PropertyName, const FText& Value);
void SetVectorPropertyByName(UObject* Object, FName PropertyName, const FVector& Value);
void SetRotatorPropertyByName(UObject* Object, FName PropertyName, const FRotator& Value);
void SetLinearColorPropertyByName(UObject* Object, FName PropertyName, const FLinearColor& Value);
void SetTransformPropertyByName(UObject* Object, FName PropertyName, const FTransform& Value);
