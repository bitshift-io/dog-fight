#include "DogFight.h"
#include "EngineUtil.h"

UWorld* GetWorld()
{
	// look for the world that has a valid game instance
	for (int i = 0; i < GEngine->GetWorldContexts().Num(); ++i)
	{
		if (GEngine->GetWorldContexts()[i].World()->GetGameInstance())
		{
			return GEngine->GetWorldContexts()[i].World();
		}
	}

	// fallback to the first!
	return GEngine->GetWorldContexts()[0].World();
}

bool ExecConsoleCommand(const FString& cmd)
{
	return GetWorld()->Exec(GetWorld(), *cmd);
}

UGameInstance* GetGameInstance()
{
	if (!GEngine)
		return NULL;

	//return UGGameInstance::instance();

	for (int i = 0; i < GEngine->GetWorldContexts().Num(); ++i)
	{
		if (GEngine->GetWorldContexts()[i].World() && GEngine->GetWorldContexts()[i].World()->GetGameInstance())
		{
			return GEngine->GetWorldContexts()[i].World()->GetGameInstance();
		}
	}

	return NULL;
}