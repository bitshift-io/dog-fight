#pragma once

// hack to work around steam leaderboard bug being additive
// see: https://answers.unrealengine.com/questions/257107/how-to-use-steam-leaderboards-from-bp.html
//
// need to link against:
// C:\Apps\Epic Games\4.10\Engine\Source\ThirdParty\Steamworks\Steamv132\sdk\redistributable_bin\win64\steam_api64.lib
//bool WriteLeaderboards(IOnlineLeaderboardsPtr Leaderboards, const FName& SessionName, const FUniqueNetId& Player, FOnlineLeaderboardWrite& WriteObject);

bool WriteLeaderboards_IfGreater(IOnlineLeaderboardsPtr Leaderboards, const FName& SessionName, const FUniqueNetId& Player, FOnlineLeaderboardWrite& WriteObject, const FName& HighscoreName, int32 Highscore);

bool UserHasLicenseForApp();
