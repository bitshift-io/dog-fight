
#pragma once

#include "GameFramework/Actor.h"

USceneComponent* GetComponent(FComponentReference& Component, AActor* OwningActor);

/*
	Similar to USceneComponent::GetChildrenComponents except you can filter by class type to find children of specific type
*/
template <typename T>
void GetChildrenComponents(USceneComponent* component, bool bIncludeAllDescendants, TArray< T * > & Children)
{
	TArray< USceneComponent * > tempChildren;
	component->GetChildrenComponents(bIncludeAllDescendants, tempChildren);
	for (int i = 0; i < tempChildren.Num(); ++i)
	{
		if (tempChildren[i]->IsA(T::StaticClass()))
		{
			Children.Add(Cast<T>(tempChildren[i]));
		}
	}
}


/*
	Search through components subobjects looking for static meshes and then override the vector paramter in the material
	This is used to override material colours
*/
void OverrideVectorOnAllMaterials(USceneComponent* component, bool bIncludeAllDescendants, const FName& ParameterName, const FLinearColor & Value);