#include "DogFight.h"
#include "ComponentUtil.h"

#pragma optimize("", OPTIMISATION)

/*!
Work around for FComponentReference.GetComponent not working correctly
https://answers.unrealengine.com/questions/179994/fcomponentreference-works-only-for-uprimitivecompo.html
*/
USceneComponent* GetComponent(FComponentReference& Component, AActor* OwningActor)
{
	USceneComponent* Result = NULL;
	// Component is specified directly, use that
	if (Component.OverrideComponent.IsValid())
	{
		Result = Component.OverrideComponent.Get();
	}
	else
	{
		// Look in Actor if specified, OwningActor if not
		AActor* SearchActor = (Component.OtherActor != NULL) ? Component.OtherActor : OwningActor;
		if (SearchActor)
		{
			if (Component.ComponentProperty != NAME_None)
			{
				UObjectPropertyBase* ObjProp = FindField<UObjectPropertyBase>(SearchActor->GetClass(), Component.ComponentProperty);
				if (ObjProp != NULL)
				{
					// .. and return the component that is there
					Result = Cast<USceneComponent>(ObjProp->GetObjectPropertyValue_InContainer(SearchActor));
				}
			}
			else
			{
				Result = Cast<USceneComponent>(SearchActor->GetRootComponent());
			}
		}
	}

	return Result;
}

void OverrideVectorOnAllMaterials(USceneComponent* component, bool bIncludeAllDescendants, const FName& ParameterName, const FLinearColor & Value)
{
	if (!component)
		return;

	// https://answers.unrealengine.com/questions/11363/best-way-to-modulate-color-at-runtime.html
	TArray< UMeshComponent * > meshList;
	GetChildrenComponents(component, bIncludeAllDescendants, meshList);

	// check if parent is itself a mesh component
	if (component->IsA(UMeshComponent::StaticClass()))
		meshList.Add(Cast<UMeshComponent>(component));

	for (int i = 0; i < meshList.Num(); ++i)
	{
		UMeshComponent* meshComponent = meshList[i];
		if (!meshComponent)
			continue;

		//TArray< UMaterialInterface * >& materials = staticMeshComponent->StaticMesh->Materials;
		for (int m = 0; m < meshComponent->GetNumMaterials() /* materials.Num()*/; ++m)
		{
			UMaterialInstanceDynamic* dynamicMaterial = meshComponent->CreateAndSetMaterialInstanceDynamic(m);
			if (!dynamicMaterial)
				continue;

			//materials[m]->OverrideVectorParameterDefault(ParameterName, Value, true, ERHIFeatureLevel::ES2);
			dynamicMaterial->SetVectorParameterValue(ParameterName, Value);
		}
	}
}
