#include "DogFight.h"
#include "BlueprintUtil.h"


void SetIntPropertyByName(UObject* Object, FName PropertyName, int32 Value)
{
	if (Object != NULL)
	{
		UIntProperty* IntProp = FindField<UIntProperty>(Object->GetClass(), PropertyName);
		if (IntProp != NULL)
		{
			IntProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetBytePropertyByName(UObject* Object, FName PropertyName, uint8 Value)
{
	if (Object != NULL)
	{
		UByteProperty* ByteProp = FindField<UByteProperty>(Object->GetClass(), PropertyName);
		if (ByteProp != NULL)
		{
			ByteProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetFloatPropertyByName(UObject* Object, FName PropertyName, float Value)
{
	if (Object != NULL)
	{
		UFloatProperty* FloatProp = FindField<UFloatProperty>(Object->GetClass(), PropertyName);
		if (FloatProp != NULL)
		{
			FloatProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetBoolPropertyByName(UObject* Object, FName PropertyName, bool Value)
{
	if (Object != NULL)
	{
		UBoolProperty* BoolProp = FindField<UBoolProperty>(Object->GetClass(), PropertyName);
		if (BoolProp != NULL)
		{
			BoolProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetObjectPropertyByName(UObject* Object, FName PropertyName, UObject* Value)
{
	if (Object != NULL && Value != NULL)
	{
		UObjectPropertyBase* ObjectProp = FindField<UObjectPropertyBase>(Object->GetClass(), PropertyName);
		if (ObjectProp != NULL && Value->IsA(ObjectProp->PropertyClass)) // check it's the right type
		{
			ObjectProp->SetObjectPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetClassPropertyByName(UObject* Object, FName PropertyName, TSubclassOf<UObject> Value)
{
	if (Object && *Value)
	{
		auto ClassProp = FindField<UClassProperty>(Object->GetClass(), PropertyName);
		if (ClassProp != NULL && Value->IsChildOf(ClassProp->MetaClass)) // check it's the right type
		{
			ClassProp->SetObjectPropertyValue_InContainer(Object, *Value);
		}
	}
}

void SetStringPropertyByName(UObject* Object, FName PropertyName, const FString& Value)
{
	if (Object != NULL)
	{
		UStrProperty* StringProp = FindField<UStrProperty>(Object->GetClass(), PropertyName);
		if (StringProp != NULL)
		{
			StringProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetNamePropertyByName(UObject* Object, FName PropertyName, const FName& Value)
{
	if (Object != NULL)
	{
		UNameProperty* NameProp = FindField<UNameProperty>(Object->GetClass(), PropertyName);
		if (NameProp != NULL)
		{
			NameProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetTextPropertyByName(UObject* Object, FName PropertyName, const FText& Value)
{
	if (Object != NULL)
	{
		UTextProperty* TextProp = FindField<UTextProperty>(Object->GetClass(), PropertyName);
		if (TextProp != NULL)
		{
			TextProp->SetPropertyValue_InContainer(Object, Value);
		}
	}
}

void SetVectorPropertyByName(UObject* Object, FName PropertyName, const FVector& Value)
{
	if (Object != NULL)
	{
		UScriptStruct* VectorStruct = FindObjectChecked<UScriptStruct>(UObject::StaticClass(), TEXT("Vector"));
		UStructProperty* VectorProp = FindField<UStructProperty>(Object->GetClass(), PropertyName);
		if (VectorProp != NULL && VectorProp->Struct == VectorStruct)
		{
			*VectorProp->ContainerPtrToValuePtr<FVector>(Object) = Value;
		}
	}
}

void SetRotatorPropertyByName(UObject* Object, FName PropertyName, const FRotator& Value)
{
	if (Object != NULL)
	{
		UScriptStruct* RotatorStruct = FindObjectChecked<UScriptStruct>(UObject::StaticClass(), TEXT("Rotator"));
		UStructProperty* RotatorProp = FindField<UStructProperty>(Object->GetClass(), PropertyName);
		if (RotatorProp != NULL && RotatorProp->Struct == RotatorStruct)
		{
			*RotatorProp->ContainerPtrToValuePtr<FRotator>(Object) = Value;
		}
	}
}

void SetLinearColorPropertyByName(UObject* Object, FName PropertyName, const FLinearColor& Value)
{
	if (Object != NULL)
	{
		UScriptStruct* ColorStruct = FindObjectChecked<UScriptStruct>(UObject::StaticClass(), TEXT("LinearColor"));
		UStructProperty* ColorProp = FindField<UStructProperty>(Object->GetClass(), PropertyName);
		if (ColorProp != NULL && ColorProp->Struct == ColorStruct)
		{
			*ColorProp->ContainerPtrToValuePtr<FLinearColor>(Object) = Value;
		}
	}
}

void SetTransformPropertyByName(UObject* Object, FName PropertyName, const FTransform& Value)
{
	if (Object != NULL)
	{
		UScriptStruct* TransformStruct = FindObjectChecked<UScriptStruct>(UObject::StaticClass(), TEXT("Transform"));
		UStructProperty* TransformProp = FindField<UStructProperty>(Object->GetClass(), PropertyName);
		if (TransformProp != NULL && TransformProp->Struct == TransformStruct)
		{
			*TransformProp->ContainerPtrToValuePtr<FTransform>(Object) = Value;
		}
	}
}
