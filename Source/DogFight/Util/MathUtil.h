
#pragma once

#include "Math/Vector.h"

namespace Math
{

	inline float AngleBetween(const FVector& a, const FVector& b)
	{
		return FMath::RadiansToDegrees(acosf(FVector::DotProduct(a, b)));
	}

}