#include "DogFight.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemSteam.h"
#include "OnlineStats.h"
#include "OnlineLeaderboardInterface.h"

//// -------------------------------------------------------------
/// BEGIN - FROM - OnlineSubsystemSteamPrivatePCH.h

#define INVALID_INDEX -1

/** Compile Steam SDK version in use */
#define STEAM_SDK_VER TEXT("1.32")
/** Path of the current Steam SDK version in use */
#define STEAM_SDK_VER_PATH TEXT("Steamv132")
/** Root location of Steam SDK */
#define STEAM_SDK_ROOT_PATH TEXT("Binaries/ThirdParty/Steamworks")

/** FName declaration of Steam subsystem */
#define STEAM_SUBSYSTEM FName(TEXT("STEAM"))
/** URL Prefix when using Steam socket connection */
#define STEAM_URL_PREFIX TEXT("steam.")
/** Filename containing the appid during development */
#define STEAMAPPIDFILENAME TEXT("steam_appid.txt")

/** pre-pended to all steam logging */
#undef ONLINE_LOG_PREFIX
#define ONLINE_LOG_PREFIX TEXT("STEAM: ")

// @todo Steam: Steam headers trigger secure-C-runtime warnings in Visual C++. Rather than mess with _CRT_SECURE_NO_WARNINGS, we'll just
//	disable the warnings locally. Remove when this is fixed in the SDK
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4996)
#endif

#pragma push_macro("ARRAY_COUNT")
#undef ARRAY_COUNT

// Steamworks SDK headers
#include "../../../ThirdParty/Steamworks/Steamv132/sdk/public/steam/steam_api.h"
//#include "steam/steam_gameserver.h"

#pragma pop_macro("ARRAY_COUNT")

// @todo Steam: See above
#ifdef _MSC_VER
#pragma warning(pop)
#endif

//// -------------------------------------------------------------
/// END

#pragma optimize("", OPTIMISATION)

/**
*	Create the proper stat name for a given leaderboard/stat combination
*
* @param LeaderboardName name of leaderboard
* @param StatName name of stat
*/
inline FName GetLeaderboardStatName(const FName& LeaderboardName, const FName& StatName)
{
	return TCHAR_TO_ANSI((*FString::Printf(TEXT("%s_%s"), *LeaderboardName.ToString(), *StatName.ToString())));
}

//
// Similar to the UE code WriteLeaderboards
//
// I reset the steam leaderboard stats to ) as currently epic only increment leaderboard values
// I also check the highscore against the existing highscore to avoid replacing a higherscore with a lower score
//
bool WriteLeaderboards_IfGreater(IOnlineLeaderboardsPtr Leaderboards, const FName& SessionName, const FUniqueNetId& Player, FOnlineLeaderboardWrite& WriteObject, const FName& HighscoreName, int32 Highscore)
{
	// Find or create handles to all requested leaderboards (async)
	int32 NumLeaderboards = WriteObject.LeaderboardNames.Num();
	for (int32 LeaderboardIdx = 0; LeaderboardIdx < NumLeaderboards; LeaderboardIdx++)
	{
		// Will create or retrieve the leaderboards, triggering async calls as appropriate
		//CreateLeaderboard(WriteObject.LeaderboardNames[LeaderboardIdx], WriteObject.SortMethod, WriteObject.DisplayFormat);
	}

	// Update stats columns associated with the leaderboards (before actual leaderboard update so we can retrieve the updated stat)
	FStatPropertyArray LeaderboardStats;
	for (int32 LeaderboardIdx = 0; LeaderboardIdx < NumLeaderboards; LeaderboardIdx++)
	{
		for (FStatPropertyArray::TConstIterator It(WriteObject.Properties); It; ++It)
		{
			const FVariantData& Stat = It.Value();
			FName LeaderboardStatName = GetLeaderboardStatName(WriteObject.LeaderboardNames[LeaderboardIdx], It.Key());
			LeaderboardStats.Add(LeaderboardStatName, Stat);
		}
	}


	const FStatPropertyArray Stats = LeaderboardStats;

	ISteamUserStats* SteamUserStatsPtr = SteamUserStats();
	check(SteamUserStatsPtr);
	for (FStatPropertyArray::TConstIterator It(Stats); It; ++It)
	{
		bool bSuccess = false;
		const FString StatName = It.Key().ToString();
		const FVariantData& Stat = It.Value();

		// Added by FM, this code assumes only a single leaderboard:
		FName HighscoreLeaderboardStatName = GetLeaderboardStatName(WriteObject.LeaderboardNames[0], HighscoreName);
		bool bIsHighscoreStat = (HighscoreLeaderboardStatName.ToString().Equals(StatName));

		switch (Stat.GetType())
		{
		case EOnlineKeyValuePairDataType::Int32:
		{
			// FM:
			// ughly, because we cant retrieve a stat from WriteObject, which is stupid!
			// so we have to pass in highscore as in int32 for now
			if (bIsHighscoreStat)
			{
				int32 existingHighscore;
				SteamUserStatsPtr->GetStat(TCHAR_TO_UTF8(*StatName), &existingHighscore);

				float writeHighscore = Highscore;
				//WriteObject.GetFloatStat(&writeHighscore);
				if (writeHighscore <= existingHighscore)
				{
					return true;
				}
			}

			bSuccess = SteamUserStatsPtr->SetStat(TCHAR_TO_UTF8(*StatName), 0) ? true : false;
		}
		break;
		case EOnlineKeyValuePairDataType::Float:
		{
			bSuccess = SteamUserStatsPtr->SetStat(TCHAR_TO_UTF8(*StatName), 0) ? true : false;
		}
		default:
			UE_LOG_ONLINE(Warning, TEXT("Skipping unsuppported key value pair uploading to Steam %s=%s"), *StatName, *Stat.ToString());
			break;
		}

		int nothing = 0;
		++nothing;
	}

	// the call will copy the user id and write object to its own memory
	return Leaderboards->WriteLeaderboards(SessionName, Player, WriteObject);
}


/*
 Return true if user owns this game
*/
bool UserHasLicenseForApp()
{
    //
    // NOTE: this does not work on mac if running through the editor, it results in a segv
    // probably due to it being a dll when in the editor so loading multiple copies of steam
    //
    
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	IOnlineSubsystem* SteamOnlineSub = IOnlineSubsystem::Get(FName("Steam"));
	FOnlineSubsystemSteam* SteamSub = (OnlineSub == SteamOnlineSub) ? (FOnlineSubsystemSteam*)(SteamOnlineSub) : NULL;
	if (!SteamSub)
	{
		UE_LOG_ONLINE(Log, TEXT("Not using steam subsystem"));
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Not using steam subsystem"));
		}
		return false;
	}

	ISteamUser* SteamUserPtr = SteamUser();
	if (SteamUserPtr == NULL)
	{
		return false;
	}

	FString AppIdStr = OnlineSub->GetAppId();
	int32 AppId = FCString::Atoi(*AppIdStr);

	//FOnlineSubsystemSteam* SteamSubsystem = Cast<FOnlineSubsystemSteam>(OnlineSub);
	//const CGameID GameID(SteamSubsystem->GetSteamAppId());

	CSteamID SteamId = SteamUser()->GetSteamID();

	// ganked from FOnlineIdentitySteam::GetAuthToken
	FString ResultToken;
	uint8 AuthToken[1024];
	uint32 AuthTokenSize = 0;
	if (SteamUserPtr->GetAuthSessionTicket(AuthToken, ARRAY_COUNT(AuthToken), &AuthTokenSize) != k_HAuthTicketInvalid &&
		AuthTokenSize > 0)
	{
		ResultToken = BytesToHex(AuthToken, AuthTokenSize);
		UE_LOG_ONLINE(Log, TEXT("Obtained steam authticket"));
		// In release builds our code checks the authTicket faster than Steam's login server can save it
		// Added a small amount of sleep here so the ResultToken is valid by the time this call returns
		FPlatformProcess::Sleep(0.1f);
	}
	else
	{
		UE_LOG_ONLINE(Warning, TEXT("Failed to acquire Steam auth session ticket"));
		return false;
	}

	//SteamUserPtr->GetAuthSessionTicket()
	EBeginAuthSessionResult BeginResult = SteamUserPtr->BeginAuthSession(AuthToken, ARRAY_COUNT(AuthToken), SteamId);
	if (BeginResult != k_EBeginAuthSessionResultOK)
	{
		return false;
	}

	//EUserHasLicenseForAppResult result = SteamUserPtr->UserHasLicenseForApp(SteamId, AppId); // GameID.AppID());
	SteamUserPtr->EndAuthSession(SteamId);
	/*
	GetAppId();

	
	if (GameID.ToUint64() == CallbackData->m_nGameID)
	{

		FOnlineSubsystemSteam

		//virtual EUserHasLicenseForAppResult UserHasLicenseForApp( CSteamID steamID, AppId_t appID ) = 0;
		*/

	//return result == k_EUserHasLicenseResultHasLicense;
	return true;
}