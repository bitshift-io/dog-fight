
#pragma once

#include "TickInViewportsActor.generated.h"

UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API ATickInViewportsActor : public AActor
{
	GENERATED_BODY()
public:

	virtual bool ShouldTickIfViewportsOnly() const	{ return true; }
};
