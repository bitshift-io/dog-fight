// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"

/** when you modify this, please note that this information can be saved with instances
* also DefaultEngine.ini [/Script/Engine.CollisionProfile] should match with this list **/
#define COLLISION_WEAPON		ECC_GameTraceChannel1
#define COLLISION_PROJECTILE	ECC_GameTraceChannel2
#define COLLISION_PICKUP		ECC_GameTraceChannel3
#define COLLISION_ACTOR			ECC_GameTraceChannel4

// set this to "on" in release, "off" while debugging!
#define OPTIMISATION off

