// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.generated.h"

class AGActor;

/**
 *   FBlueprintDetails::AddEventsCategory
 */
UCLASS(abstract, BlueprintType, Blueprintable, ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UActorState : public USceneComponent
{
	GENERATED_BODY()
public:

	UActorState(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "ActorState")
	virtual void StateInit();

	UFUNCTION(BlueprintCallable, Category = "ActorState")
	virtual void StateEnter();

	UFUNCTION(BlueprintCallable, Category = "ActorState")
	virtual void StateUpdate(float deltaTime);

	UFUNCTION(BlueprintCallable, Category = "ActorState")
	virtual void StateExit();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser, bool bRoot = true);

	/** Should we transition to another state? **/
	virtual void CheckStateTransitions();

	virtual bool CanFire();

	virtual FVector GetVelocity() const;

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite)
	TArray<UActorState*> ActorStateList;

	AGActor* GetOwner() const	{ return Cast<AGActor>(Super::GetOwner()); }

	//AGActor*	m_owner;

	/** Is this state able to be transitioned too? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsEnabled;
};
