// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "FlyDyingState.h"
#include "FlyState.h"
#include "DeadState.h"
#include "GActor.h"
#include "UnrealNetwork.h"

#pragma optimize("", OPTIMISATION)

UFlyDyingState::UFlyDyingState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxTimeInState = 2.0f;
	Acceleration = 100.0f;
}


void UFlyDyingState::StateEnter()
{
	Super::StateEnter();

	m_timeInState = 0.0f;
	m_linearSpeed = 0.0f;
}

void UFlyDyingState::StateUpdate(float deltaTime)
{
	m_timeInState += deltaTime;
	if (!GetOwner())
		return;

	FVector fwd = GetOwner()->GetActorForwardVector();

	// accelerate move down
	m_linearSpeed += deltaTime * Acceleration * 100.0f;
	FVector position = GetOwner()->GetActorLocation();
	position = position - (FVector::UpVector * m_linearSpeed * deltaTime);
	position = position + (fwd * LinearSpeed * 100.0f * deltaTime);

	GetOwner()->SetActorLocation(position);

	Super::StateUpdate(deltaTime);

	if (m_timeInState >= MaxTimeInState)
	{
		UActorState* deadState = GetOwner()->FindState(UDeadState::StaticClass());
		GetOwner()->SetState(deadState);
	}
}
