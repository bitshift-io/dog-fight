// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "DogFight.h"
#include "ActorMovement.h"
#include "GActor.h"

//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UActorMovement::UActorMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


float UActorMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();
	/*
	const AGActor* ShooterCharacterOwner = Cast<AGActor>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsTargeting())
		{
			MaxSpeed *= ShooterCharacterOwner->GetTargetingSpeedModifier();
		}
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
	}*/

	return MaxSpeed;
}
