// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

/**
* Movement component meant for use with Pawns.
*/

#pragma once

#include "ActorMovement.generated.h"

/*
	https://docs.unrealengine.com/latest/INT/Gameplay/Networking/CharacterMovementComponent/index.html
	https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/UCharacterMovementComponent/index.html
*/
UCLASS()
class UActorMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual float GetMaxSpeed() const override;
};

