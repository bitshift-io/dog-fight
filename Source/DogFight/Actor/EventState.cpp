// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "EventState.h"

void UEventState::StateInit(/*AGActor* owner*/)
{
	Super::StateInit(/*owner*/);
	OnStateInit.Broadcast();
}

void UEventState::StateEnter()
{
	Super::StateEnter();
	OnStateEnter.Broadcast();
}

void UEventState::StateUpdate(float deltaTime)
{
	Super::StateUpdate(deltaTime);
	OnStateUpdate.Broadcast(); // deltaTime);
}

void UEventState::StateExit()
{
	Super::StateExit();
	OnStateExit.Broadcast();
}