// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "FlyState.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UFlyState : public UActorState
{
	GENERATED_BODY()
public:

	UFlyState(const FObjectInitializer& ObjectInitializer);

	virtual void StateEnter() override;
	virtual void StateUpdate(float deltaTime) override;

	virtual void CheckStateTransitions() override;

	//virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);
	virtual bool CanFire();

	virtual FVector GetVelocity() const;

	// Metres per second
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	float LinearSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	float RotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxRoll;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RollSpeed;

	/** force feedback effect to play on a player hit */
	UPROPERTY(EditDefaultsOnly)
	UForceFeedbackEffect* DamageForceFeedbackEffect;

protected:

	UPROPERTY(Replicated)
	float	m_roll;

	float	m_timeInState;
};
