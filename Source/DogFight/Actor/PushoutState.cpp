// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "PushoutState.h"
#include "GActor.h"
#include "FlyState.h"
#include "ParachuteState.h"
#include "Controller/ControllerInterface.h"

#pragma optimize("", OPTIMISATION)

void UPushoutState::StateUpdate(float deltaTime)
{
	Super::StateUpdate(deltaTime);

	float actorRadius = GetOwner()->GetCollisionComponent()->GetScaledSphereRadius();
	FVector actorPos = GetOwner()->GetActorLocation();
	actorPos.Z = 10000.f; // hardcode height

	for (FConstPawnIterator it = GetOwner()->GetWorld()->GetPawnIterator(); it; ++it)
	{
		AGActor* otherActor = Cast<AGActor>(*it);
		if (!otherActor || otherActor == GetOwner() || otherActor->IsDead())
			continue;

		
		float otherActorRadius = otherActor->GetCollisionComponent()->GetScaledSphereRadius();
		const FVector otherActorPos = otherActor->GetActorLocation();
		FVector distVector = actorPos - otherActorPos;
		float dist = FVector::Dist(actorPos, otherActorPos);
		distVector.Z = 0.0f;
		distVector.Normalize();

		dist -= (otherActorRadius + actorRadius);
		if (dist < 0)
		{
			// do revive buddies when u run them over
			/*
			IControllerInterface* controller = Cast<IControllerInterface>(m_owner->Controller);
			IControllerInterface* otherController = Cast<IControllerInterface>(otherActor->Controller);
			if (otherController && otherController->Team
				&& otherController->Team == controller->Team)
				*/
			if (GetOwner()->GetPlayerState() && otherActor->GetPlayerState() && GetOwner()->GetPlayerState()->GetTeam() == otherActor->GetPlayerState()->GetTeam())
			{
				if (otherActor->CurrentState()->IsA(UParachuteState::StaticClass()))
				{
					otherActor->Rescue(GetOwner());

					/*
					if (mReviveSound)
					{
						mReviveSound->SetPosition(pawn->GetTransform().GetTranslation());
						mReviveSound->Play();
					}*/
				}
			}

			// https://answers.unrealengine.com/questions/19256/how-to-check-if-an-actor-is-from-a-certain-class.html
			// if im alive and the hit pawn is parachuting, dont pushaway 
			if (GetOwner()->CurrentState()->IsA(UFlyState::StaticClass())
				&& otherActor->CurrentState()->IsA(UParachuteState::StaticClass()))
				continue;

			/*
			// check if we are allowed to cause damage to hit pawn
			// this is only if they are flying though!
			if (CauseDamage && otherActor->GetCurrentState()->GetType() == FlyState::Type)
			{
				TakeDamageMsg damageMsg(1.f, m_owner);
				gGame.GetGameModeMgr()->GetGameMode()->TakeDamage(pawn, &damageMsg);
			}

			if (mForceFeedbackEffect)
			{
				mForceFeedbackEffect->Play();
			}*/

			dist *= 0.5f; // 50% pushoff
			actorPos += distVector * -dist;
			//check(distVector.Z == 0.f);
			actorPos.Z = 10000.f; // hardcode height
		}
	}

	GetOwner()->SetActorLocation(actorPos);
}
