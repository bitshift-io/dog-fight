// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "GActor.h"
#include "ActorMovement.h"
#include "ActorState.h"
#include "Controller/GPlayerController.h"
#include "Util/ComponentUtil.h"
#include "Weapon/Weapon.h"
#include "GameMode/GGameMode.h"
#include "GameMode/GGameState.h"
#include "GameMode/TeamInfo.h"
#include "UnrealNetwork.h"

#pragma optimize("", OPTIMISATION)

FName AGActor::CollisionComponentName(TEXT("CollisionComponent0"));
FName AGActor::InventoryComponentName(TEXT("InventoryComponent0"));

// move actor:
// https://answers.unrealengine.com/questions/26573/move-a-character-in-direction-c.html

AGActor::AGActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) //ObjectInitializer.SetDefaultSubobjectClass<UActorMovement>(ACharacter::CharacterMovementComponentName))
{
	CollisionComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, CollisionComponentName);
	CollisionComponent->InitSphereRadius(35.0f);

	static FName CollisionProfileName(TEXT("Pawn"));
	CollisionComponent->SetCollisionProfileName(CollisionProfileName);

	CollisionComponent->CanCharacterStepUpOn = ECB_No;
	CollisionComponent->bShouldUpdatePhysicsVolume = true;
//	CollisionComponent->bCanEverAffectNavigation = false;

	CollisionComponent->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	CollisionComponent->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	CollisionComponent->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	RootComponent = CollisionComponent;

	Mesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("Mesh"));
	Mesh->bOnlyOwnerSee = false;
	Mesh->bOwnerNoSee = true;
	Mesh->bReceivesDecals = false;
	Mesh->SetCollisionObjectType(ECC_Pawn);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	Mesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh->AttachParent = CollisionComponent;

	//USceneComponent* SceneComponent = ObjectInitializer.CreateOptionalDefaultSubobject<USceneComponent>(this, TEXT("SceneRoot"));
	//RootComponent = SceneComponent;

	// Create movement component
	//MovementComponent = ObjectInitializer.CreateDefaultSubobject<UFloatingPawnMovement>(this, TEXT("Movement"));
	//MovementComponent->UpdatedComponent = RootComponent;

	StateMachine = ObjectInitializer.CreateDefaultSubobject<UActorStateMachine>(this, TEXT("StateMachine"));
	StateMachine->AttachParent = RootComponent;

	Inventory = ObjectInitializer.CreateDefaultSubobject<UInventory>(this, InventoryComponentName);
	Inventory->AttachParent = RootComponent;
	Inventory->SetIsReplicated(true);

	MaxHealth = 1.0f;
	bWantsToFire = false;

	ColourParameterName = "DiffuseColor";
	Colour = FColor(255, 0, 0, 255); // red

	m_horizontalMovementAxis = 0.0f;
	m_verticalMovementAxis = 0.0f;

	m_damageTime = 0.0f;

	// now lets test network replication works here (ie, doesnt crash):
	//TArray< FLifetimeProperty > OutLifetimeProps;
	//GetLifetimeReplicatedProps(OutLifetimeProps);

	bAlwaysRelevant = true;
	bReplicateMovement = true;
}

void AGActor::BeginPlay()
{
	Super::BeginPlay();

	AGGameState* const GameState = Cast<AGGameState>(GetWorld()->GameState);
	if (GameState)
	{
		SetTeamTagVisible(GameState->bTeamTagsVisible);
	}
}

void AGActor::BeginDestroy()
{
	Super::BeginDestroy();
}

/** Returns CollisionComponent subobject **/
USphereComponent* AGActor::GetCollisionComponent() const 
{
	return CollisionComponent; 
}

/*
UPawnMovementComponent* AGActor::GetMovementComponent() const
{
	return MovementComponent;
}*/

void AGActor::SetHorizontalMovementAxis(float value)
{
	if (m_horizontalMovementAxis == value)
		return;

	m_horizontalMovementAxis = value;

	if (Role < ROLE_Authority)
	{
		ServerSetHorizontalMovementAxis(value);
	}
}

void AGActor::SetVerticalMovementAxis(float value)
{
	if (m_verticalMovementAxis == value)
		return;

	m_verticalMovementAxis = value;

	if (Role < ROLE_Authority)
	{
		ServerSetVerticalMovementAxis(value);
	}
}


bool AGActor::ServerSetHorizontalMovementAxis_Validate(float value)
{
	return true;
}

void AGActor::ServerSetHorizontalMovementAxis_Implementation(float value)
{
	SetHorizontalMovementAxis(value);
}

bool AGActor::ServerSetVerticalMovementAxis_Validate(float value)
{
	return true;
}

void AGActor::ServerSetVerticalMovementAxis_Implementation(float value)
{
	SetVerticalMovementAxis(value);
}

void AGActor::SetColour(const FLinearColor& colour)
{
	Colour = colour;
	OverrideColourOnAllMaterials(RootComponent);
	OnSetColour();
}

void AGActor::OverrideColourOnAllMaterials(USceneComponent* component)
{
	OverrideVectorOnAllMaterials(component, true, ColourParameterName, Colour);
}

void AGActor::OverrideColourOnAllMaterialsWithColour(USceneComponent* component, const FLinearColor& colour)
{
	OverrideVectorOnAllMaterials(component, true, ColourParameterName, colour);
}

void AGActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();


	OverrideColourOnAllMaterials(RootComponent);



	//FActorSpawnParameters SpawnInfo;
	//StateMachine = GetWorld()->SpawnActor<UActorStateMachine>(StateMachineClass, GetActorLocation(), GetActorRotation(), SpawnInfo);
	StateMachine->StateInit();
	Inventory->SpawnDefaultInventory();

	/*
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->StateInit(this);
	}

	// enter first state
	if (ActorStateList.Num())
	{
		m_currentState = ActorStateList[0];
		m_currentState->StateEnter();
	}*/

	
	if (Role == ROLE_Authority)
	{
		Health = MaxHealth;
		//SpawnDefaultInventory();
		bCanTakeDamage = true;
	}

	OnSpawn();
}

void AGActor::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	/*
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("SetupPlayerInputComponent"));
	}*/
}

void AGActor::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	m_damageTime += deltaTime;

	// update alive time
	if (!IsDead())
		m_timeAlive += deltaTime;
	else
		m_timeAlive = 0.f;
	
	StateMachine->StateUpdate(deltaTime);
}

void AGActor::BroadcastWarp()
{
	OnWarp();
}

void AGActor::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// This should not be required, as actor states are components
	//StateMachine->GetLifetimeReplicatedProps(OutLifetimeProps);
	
	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	//DOREPLIFETIME_CONDITION(AGActor, Inventory, COND_OwnerOnly);

	// everyone
	//DOREPLIFETIME(AGActor, CurrentWeapon);
	DOREPLIFETIME(AGActor, Health);

	// BUG: for some reason the clients get this actor as autonomous proxy so this doesnt work being conditional
	DOREPLIFETIME(AGActor, UTReplicatedMovement);  //DOREPLIFETIME_CONDITION(AGActor, UTReplicatedMovement, COND_SimulatedOrPhysics);
		//ReplicatedMovement2); // this is also in AActor::GetLifetimeReplicatedProps but its conditional

	DOREPLIFETIME(AGActor, m_horizontalMovementAxis);
	DOREPLIFETIME(AGActor, m_verticalMovementAxis);
}


#if 0

//////////////////////////////////////////////////////////////////////////
// Inventory

void AGActor::SpawnDefaultInventory()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.bNoCollisionFail = true;
			AWeapon* NewWeapon = GetWorld()->SpawnActor<AWeapon>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		EquipWeapon(Inventory[0]);
	}
}

void AGActor::DestroyInventory()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	// remove all weapons from inventory and destroy them
	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AWeapon* Weapon = Inventory[i];
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
		}
	}
}

void AGActor::AddWeapon(AWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
	}
}

void AGActor::RemoveWeapon(AWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		Weapon->OnLeaveInventory();
		Inventory.RemoveSingle(Weapon);
	}
}

AWeapon* AGActor::FindWeapon(TSubclassOf<AWeapon> WeaponClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(WeaponClass))
		{
			return Inventory[i];
		}
	}

	return NULL;
}

void AGActor::EquipWeapon(AWeapon* Weapon)
{
	if (Weapon)
	{
		if (Role == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon);
		}
		else
		{
			ServerEquipWeapon(Weapon);
		}
	}
}

bool AGActor::ServerEquipWeapon_Validate(AWeapon* Weapon)
{
	return true;
}

void AGActor::ServerEquipWeapon_Implementation(AWeapon* Weapon)
{
	EquipWeapon(Weapon);
}

void AGActor::OnRep_CurrentWeapon(AWeapon* LastWeapon)
{
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void AGActor::SetCurrentWeapon(class AWeapon* NewWeapon, class AWeapon* LastWeapon)
{
	AWeapon* LocalLastWeapon = NULL;

	if (LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// unequip previous
	if (LocalLastWeapon)
	{
		LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		NewWeapon->SetOwningPawn(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		NewWeapon->OnEquip();
	}
}

#endif


//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AGActor::SetWeaponTarget(AGActor* Target)
{
	if (GetCurrentWeapon())
	{
		GetCurrentWeapon()->SetTargetPawn(Target);
	}
}

void AGActor::StartWeaponFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->StartFire();
		}
	}
}

void AGActor::StopWeaponFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->StopFire();
		}
	}
}

bool AGActor::CanFire() const
{
	// cant shoot while in spawn protection.. this is time for players to find where
	// they are on screen
	if (GetTimeAlive() <= SpawnProtectionTime)
	{
		return false;
	}

	return IsAlive() && CurrentState()->CanFire();
}

bool AGActor::CanReload() const
{
	return true;
}

AWeapon* AGActor::GetWeapon() const
{
	return GetCurrentWeapon();
}

USkeletalMeshComponent* AGActor::GetPawnMesh() const
{
	return Mesh;
}

void AGActor::SetPawnMesh(class USkeletalMesh* NewMesh)
{
	Mesh->OverrideMaterials.Empty();
	Mesh->SetSkeletalMesh(NewMesh);
	OverrideColourOnAllMaterials(Mesh);
}

#if 0
FName AGActor::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}
#endif

void AGActor::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	int nothing = 0;
	++nothing;
}

void AGActor::Rescue(AGActor* rescuer)
{
	SetHealthPercent(1.f);
	SetState(FindState(UFlyState::StaticClass()));
	OnRescue();
}

void AGActor::Suicide()
{
	KilledBy(this);
}

void AGActor::KilledBy(APawn* EventInstigator)
{
	if (Role == ROLE_Authority/* && !bIsDying*/)
	{
		AController* Killer = NULL;
		if (EventInstigator != NULL)
		{
			Killer = EventInstigator->Controller;
			LastHitBy = NULL;
		}

		Die(Health, FDamageEvent(UDamageType::StaticClass()), Killer, NULL);
	}
}

float AGActor::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	// simply forward to the state to handle
	if (CurrentState())
	{
		return CurrentState()->TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	}

	return 0.0f;
}

float AGActor::TakeDamageInternal(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	AGPlayerController* MyPC = Cast<AGPlayerController>(Controller);
	if (MyPC && MyPC->HasGodMode())
	{
		return 0.f;
	}

	if (Health <= 0.f)
	{
		return 0.f;
	}

	// spawn/damage protection
	if (GetTimeAlive() <= SpawnProtectionTime
		|| m_damageTime < DamageProtectionTime
		|| !bCanTakeDamage)
	{
		OnDamageAverted.Broadcast(DamageCauser);
		return false;
	}

	m_damageTime = 0.f;

	// Modify based on game rules.
	AGGameMode* const Game = GetWorld()->GetAuthGameMode<AGGameMode>();
	Damage = Game ? Game->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : 0.f;

	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;
		OnTakeDamage();

		if (Health <= 0)
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(ActualDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : NULL, DamageCauser);
		}

		MakeNoise(1.0f, EventInstigator ? EventInstigator->GetPawn() : this);

		// register the bullet hit with the enemy player stats
		AGPlayerController* PlayerController = Cast<AGPlayerController>(EventInstigator);
		if (PlayerController)
		{
			AGPlayerState* PlayerState = Cast<AGPlayerState>(PlayerController->PlayerState);
			if (PlayerState)
				PlayerState->AddBulletsHit(1);
		}
	}

	return ActualDamage;

#if 0
	AGPlayerController* MyPC = Cast<AGPlayerController>(Controller);
	/*
	if (MyPC && MyPC->HasGodMode())
	{
		return 0.f;
	}*/

	if (Health <= 0.f)
	{
		return 0.f;
	}

	
	// Modify based on game rules.
	AGGameMode* const Game = GetWorld()->GetAuthGameMode<AGGameMode>();
	Damage = Game ? Game->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : 0.f;
	
	float ActualDamage = Damage;
	ActualDamage = Super::TakeDamage(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		/*
		Health -= ActualDamage;
		
		if (Health <= 0)
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(ActualDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : NULL, DamageCauser);
		}

		MakeNoise(1.0f, EventInstigator ? EventInstigator->GetPawn() : this);*/

		if (CurrentState())
		{
			ActualDamage = CurrentState()->TakeDamage(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}

		OnTakeDamage();
	}

	return ActualDamage;
#endif
}


bool AGActor::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const
{
	if (/*bIsDying										// already dying
		|| IsPendingKill()								// already destroyed
		|| */Role != ROLE_Authority						// not authority
		|| GetWorld()->GetAuthGameMode() == NULL
		|| GetWorld()->GetAuthGameMode()->GetMatchState() == MatchState::LeavingMap)	// level transition occurring
	{
		return false;
	}

	return true;
}


bool AGActor::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	if (!CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		return false;
	}

	Health = FMath::Min(0.0f, Health);

	// if this is an environmental death then refer to the previous killer so that they receive credit (knocked into lava pits, etc)
	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	Killer = GetDamageInstigator(Killer, *DamageType);

	AController* const KilledPlayer = (Controller != NULL) ? Controller : Cast<AController>(GetOwner());
	GetWorld()->GetAuthGameMode<AGGameMode>()->Killed(Killer, KilledPlayer, this, DamageType);

	NetUpdateFrequency = GetDefault<AGActor>()->NetUpdateFrequency;
#if 0
	GetCharacterMovement()->ForceReplicationUpdate();
#endif

	OnDeath(KillingDamage, DamageEvent, Killer ? Killer->GetPawn() : NULL, DamageCauser);
	return true;
}


void AGActor::OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	/*
	if (bIsDying)
	{
		return;
	}

	bIsDying = true;
	*/
	bReplicateMovement = false;
	bTearOff = true;
	

	if (Role == ROLE_Authority)
	{
		ReplicateHit(KillingDamage, DamageEvent, PawnInstigator, DamageCauser, true);

		// play the force feedback effect on the client player controller
		APlayerController* PC = Cast<APlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UGDamageType *DamageType = Cast<UGDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->KilledForceFeedback)
			{
				PC->ClientPlayForceFeedback(DamageType->KilledForceFeedback, false, "TakeDamage");
			}
		}
	}
#if 0
	// cannot use IsLocallyControlled here, because even local client's controller may be NULL here
	if (GetNetMode() != NM_DedicatedServer && DeathSound && Mesh1P && Mesh1P->IsVisible())
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}

	// remove all weapons
	DestroyInventory();

	// switch back to 3rd person view
	UpdatePawnMeshes();

	DetachFromControllerPendingDestroy();
	StopAllAnimMontages();

	if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	{
		LowHealthWarningPlayer->Stop();
	}

	if (RunLoopAC)
	{
		RunLoopAC->Stop();
	}

	// disable collisions on capsule
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	// Death anim
	float DeathAnimDuration = PlayAnimMontage(DeathAnim);

	// Ragdoll
	if (DeathAnimDuration > 0.f)
	{
		// Use a local timer handle as we don't need to store it for later but we don't need to look for something to clear
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AGActor::SetRagdollPhysics, FMath::Min(0.1f, DeathAnimDuration), false);
	}
	else
	{
		SetRagdollPhysics();
	}
#endif
}

void AGActor::PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (Role == ROLE_Authority)
	{
		ReplicateHit(DamageTaken, DamageEvent, PawnInstigator, DamageCauser, false);

		// play the force feedback effect on the client player controller
		APlayerController* PC = Cast<APlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UGDamageType *DamageType = Cast<UGDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->HitForceFeedback)
			{
				PC->ClientPlayForceFeedback(DamageType->HitForceFeedback, false, "TakeDamage");
			}
		}
	}
#if 0
	if (DamageTaken > 0.f)
	{
		ApplyDamageMomentum(DamageTaken, DamageEvent, PawnInstigator, DamageCauser);
	}
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	AShooterHUD* MyHUD = MyPC ? Cast<AShooterHUD>(MyPC->GetHUD()) : NULL;
	if (MyHUD)
	{
		MyHUD->NotifyHit(DamageTaken, DamageEvent, PawnInstigator);
	}

	if (PawnInstigator && PawnInstigator != this && PawnInstigator->IsLocallyControlled())
	{
		AShooterPlayerController* InstigatorPC = Cast<AShooterPlayerController>(PawnInstigator->Controller);
		AShooterHUD* InstigatorHUD = InstigatorPC ? Cast<AShooterHUD>(InstigatorPC->GetHUD()) : NULL;
		if (InstigatorHUD)
		{
			InstigatorHUD->NotifyEnemyHit();
		}
	}
#endif
}

void AGActor::ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser, bool bKilled)
{
	const float TimeoutTime = GetWorld()->GetTimeSeconds() + 0.5f;

	FDamageEvent const& LastDamageEvent = LastTakeHitInfo.GetDamageEvent();
	if ((PawnInstigator == LastTakeHitInfo.PawnInstigator.Get()) && (LastDamageEvent.DamageTypeClass == LastTakeHitInfo.DamageTypeClass) && (LastTakeHitTimeTimeout == TimeoutTime))
	{
		// same frame damage
		if (bKilled && LastTakeHitInfo.bKilled)
		{
			// Redundant death take hit, just ignore it
			return;
		}

		// otherwise, accumulate damage done this frame
		Damage += LastTakeHitInfo.ActualDamage;
	}

	LastTakeHitInfo.ActualDamage = Damage;
	LastTakeHitInfo.PawnInstigator = Cast<AGActor>(PawnInstigator);
	LastTakeHitInfo.DamageCauser = DamageCauser;
	LastTakeHitInfo.SetDamageEvent(DamageEvent);
	LastTakeHitInfo.bKilled = bKilled;
	LastTakeHitInfo.EnsureReplication();

	LastTakeHitTimeTimeout = TimeoutTime;
}

void AGActor::OnRep_LastTakeHitInfo()
{
	if (LastTakeHitInfo.bKilled)
	{
		OnDeath(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
	else
	{
		PlayHit(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
}

void AGActor::SetHealthPercent(float percent)
{
	Health = MaxHealth * percent;
}

UActorState* AGActor::FindState(UClass* staticClass)
{
	return StateMachine->FindState(staticClass);
}

void AGActor::SetState(UActorState* state)
{
	StateMachine->SetState(state);
}

bool AGActor::WrapAround(FVector& worldPosition) const
{
	AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
	if (!MyGameState)
		return false;

	return MyGameState->WrapAround(worldPosition, GetCollisionComponent()->GetScaledSphereRadius());
}

FQuat AGActor::ActorViewRotation() const
{
	AGGameState* const MyGameState = Cast<AGGameState>(GetWorld()->GameState);
	if (!MyGameState)
		return FQuat::Identity;

	return MyGameState->ActorViewRotation();
}

void AGActor::SetTeam(ATeamInfo* team)
{
	int nothing = 0;
	++nothing;

	/*
	// temporary till we know how to get the colour set properly
	if (team)
		SetColour(team->TeamColor);
		*/
}

bool AGActor::IsAlive() const
{
	return Health > 0.f; 
}

bool AGActor::IsDead() const
{
	return !IsAlive(); 
}

void AGActor::PostNetReceiveLocationAndRotation()
{
	Super::PostNetReceiveLocationAndRotation();
}

// https://udn.epicgames.com/Two/ReplicationRoles.html
void AGActor::OnRep_ReplicatedMovement()
{
	Super::OnRep_ReplicatedMovement();
}

void AGActor::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	FRepAttachment& AttachmentReplication = const_cast<FRepAttachment&>(GetAttachmentReplication()); // test this in 4.11
	if (bReplicateMovement || AttachmentReplication.AttachParent)
	{
		if (GatherUTMovement())
		{
			DOREPLIFETIME_ACTIVE_OVERRIDE(AGActor, UTReplicatedMovement, bReplicateMovement);
			DOREPLIFETIME_ACTIVE_OVERRIDE(AActor, ReplicatedMovement, false);
		}
		else
		{
			DOREPLIFETIME_ACTIVE_OVERRIDE(AGActor, UTReplicatedMovement, false);
			DOREPLIFETIME_ACTIVE_OVERRIDE(AActor, ReplicatedMovement, bReplicateMovement);
		}
	}
	else
	{
		DOREPLIFETIME_ACTIVE_OVERRIDE(AActor, ReplicatedMovement, false);
		DOREPLIFETIME_ACTIVE_OVERRIDE(AGActor, UTReplicatedMovement, false);
	}
#if 0
	const FAnimMontageInstance * RootMotionMontageInstance = GetRootMotionAnimMontageInstance();

	if (RootMotionMontageInstance)
	{
		// Is position stored in local space?
		RepRootMotion.bRelativePosition = BasedMovement.HasRelativeLocation();
		RepRootMotion.bRelativeRotation = BasedMovement.HasRelativeRotation();
		RepRootMotion.Location = RepRootMotion.bRelativePosition ? BasedMovement.Location : GetActorLocation();
		RepRootMotion.Rotation = RepRootMotion.bRelativeRotation ? BasedMovement.Rotation : GetActorRotation();
		RepRootMotion.MovementBase = BasedMovement.MovementBase;
		RepRootMotion.MovementBaseBoneName = BasedMovement.BoneName;
		RepRootMotion.AnimMontage = RootMotionMontageInstance->Montage;
		RepRootMotion.Position = RootMotionMontageInstance->GetPosition();

		DOREPLIFETIME_ACTIVE_OVERRIDE(ACharacter, RepRootMotion, true);
	}
	else
	{
		RepRootMotion.Clear();
		DOREPLIFETIME_ACTIVE_OVERRIDE(ACharacter, RepRootMotion, false);
	}

	ReplicatedMovementMode = GetCharacterMovement()->PackNetworkMovementMode();
	ReplicatedBasedMovement = BasedMovement;

	// Optimization: only update and replicate these values if they are actually going to be used.
	if (BasedMovement.HasRelativeLocation())
	{
		// When velocity becomes zero, force replication so the position is updated to match the server (it may have moved due to simulation on the client).
		ReplicatedBasedMovement.bServerHasVelocity = !GetCharacterMovement()->Velocity.IsZero();

		// Make sure absolute rotations are updated in case rotation occurred after the base info was saved.
		if (!BasedMovement.HasRelativeRotation())
		{
			ReplicatedBasedMovement.Rotation = GetActorRotation();
		}
	}

	DOREPLIFETIME_ACTIVE_OVERRIDE(AUTCharacter, LastTakeHitInfo, GetWorld()->TimeSeconds - LastTakeHitTime < 1.0f);
	DOREPLIFETIME_ACTIVE_OVERRIDE(AUTCharacter, HeadArmorFlashCount, GetWorld()->TimeSeconds - LastHeadArmorFlashTime < 1.0f);

	DOREPLIFETIME_ACTIVE_OVERRIDE(AUTCharacter, CosmeticFlashCount, GetWorld()->TimeSeconds - LastCosmeticFlashTime < 1.0f);

	// @TODO FIXMESTEVE - just don't want this ever replicated
	DOREPLIFETIME_ACTIVE_OVERRIDE(ACharacter, RemoteViewPitch, false);
#endif
}

bool AGActor::GatherUTMovement()
{
	UPrimitiveComponent* RootPrimComp = Cast<UPrimitiveComponent>(GetRootComponent());
	if (RootPrimComp && RootPrimComp->IsSimulatingPhysics())
	{
		FRigidBodyState RBState;
		RootPrimComp->GetRigidBodyState(RBState);
		ReplicatedMovement.FillFrom(RBState);
	}
	else if (RootComponent != NULL)
	{
		// If we are attached, don't replicate absolute position
		if (RootComponent->AttachParent != NULL)
		{
			// Networking for attachments assumes the RootComponent of the AttachParent actor. 
			// If that's not the case, we can't update this, as the client wouldn't be able to resolve the Component and would detach as a result.
			FRepAttachment& AttachmentReplication = const_cast<FRepAttachment&>(GetAttachmentReplication()); // test this in 4.11
			if (AttachmentReplication.AttachParent != NULL)
			{
				AttachmentReplication.LocationOffset = RootComponent->RelativeLocation;
				AttachmentReplication.RotationOffset = RootComponent->RelativeRotation;
			}
		}
		else
		{
			// @TODO FIXMESTEVE make sure not replicated to owning client!!!
			UTReplicatedMovement.Location = RootComponent->GetComponentLocation();
			UTReplicatedMovement.Rotation = RootComponent->GetComponentRotation();
			UTReplicatedMovement.Rotation.Pitch = GetControlRotation().Pitch;
			//UTReplicatedMovement.Acceleration = CharacterMovement->GetCurrentAcceleration();
			UTReplicatedMovement.LinearVelocity = GetVelocity();
			return true;
		}
	}
	return false;
}


void AGActor::OnRep_UTReplicatedMovement()
{
	// this gets called on the clients
	// if this is the client who is being controlled (AutoProxy?)
	// we want to allow its position to be set by the server so need to fake being a similated proxy for a moment
	ENetRole oldRole = Role;
	Role = ROLE_SimulatedProxy;

	/*
	ReplicatedMovement = ReplicatedMovement2;
	OnRep_ReplicatedMovement();
	*/
	if (Role == ROLE_SimulatedProxy)
	{		
		float dist = FVector::Dist(UTReplicatedMovement.Location, ReplicatedMovement.Location);

		ReplicatedMovement.Location = UTReplicatedMovement.Location;
		ReplicatedMovement.Rotation = UTReplicatedMovement.Rotation;
		RemoteViewPitch = (uint8)(ReplicatedMovement.Rotation.Pitch * 255.f / 360.f);
		ReplicatedMovement.Rotation.Pitch = 0.f;
		ReplicatedMovement.LinearVelocity = UTReplicatedMovement.LinearVelocity;
		ReplicatedMovement.AngularVelocity = FVector(0.f);
		ReplicatedMovement.bSimulatedPhysicSleep = false;
		ReplicatedMovement.bRepPhysics = false;

		OnRep_ReplicatedMovement();

		const float OnWarpMoveDistance = 200.f; // centimeters
		if (dist > OnWarpMoveDistance)
		{
			OnWarp();
		}
	}

	Role = oldRole;
}

void AGActor::GatherCurrentMovement()
{
	Super::GatherCurrentMovement();
#if 0
	// check the Super code is using ReplicatedMovement
	UPrimitiveComponent* RootPrimComp = Cast<UPrimitiveComponent>(GetRootComponent());
	check (!(RootPrimComp && RootPrimComp->IsSimulatingPhysics()))
	check(RootComponent != NULL);
	check(RootComponent->AttachParent == NULL);
	ReplicatedMovement2 = ReplicatedMovement;
#endif
}

void AGActor::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);

	// [server] as soon as PlayerState is assigned, set team colors of this pawn for local player
	UpdateColors();
}

void AGActor::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	// [client] as soon as PlayerState is assigned, set team colors of this pawn for local player
	if (PlayerState != NULL)
	{
		UpdateColors();
	}
}

void AGActor::NotifyTeamChanged()
{
	UpdateColors();
}

bool AGActor::IsEnemyFor(AController* TestPC) const
{
	if (TestPC == Controller || TestPC == NULL)
	{
		return false;
	}

	AGPlayerState* TestPlayerState = Cast<AGPlayerState>(TestPC->PlayerState);
	AGPlayerState* MyPlayerState = Cast<AGPlayerState>(PlayerState);

	bool bIsEnemy = true;
	if (GetWorld()->GameState && GetWorld()->GameState->GameModeClass)
	{
		const AGGameMode* DefGame = GetWorld()->GameState->GameModeClass->GetDefaultObject<AGGameMode>();
		if (DefGame && MyPlayerState && TestPlayerState)
		{
			bIsEnemy = DefGame->CanDealDamage(TestPlayerState, MyPlayerState);
		}
	}

	return bIsEnemy;
}


void AGActor::UpdateColors()
{
	AGPlayerState* PS = Cast<AGPlayerState>(PlayerState);
	if (PS != NULL)
	{
		SetColour(PS->GetPlayerColour());
	}

	OnUpdateColours();
}

AGPlayerState* AGActor::GetPlayerState() const
{
	return Cast<AGPlayerState>(PlayerState);
}

void AGActor::GetPlayerState(AGPlayerState*& APlayerState) const
{
	APlayerState = Cast<AGPlayerState>(PlayerState);
}


void AGActor::GetHealthPercent(float& HealthPercent)
{
	HealthPercent = GetHealthPercent(); 
}

FVector AGActor::GetVelocity() const
{
	if (CurrentState())
	{
		return CurrentState()->GetVelocity();
	}

	return Super::GetVelocity();
}

void AGActor::PickupShield_Implementation()
{
	OnPickupShield();
}

/*
void AGActor::ClientSetTeamTagVisible_Implementation(bool bVisible)
{
	SetTeamTagVisible(bVisible);
}*/

void AGActor::OnRep_Health()
{
	// allow effects to show up on clients
	OnTakeDamage();
}