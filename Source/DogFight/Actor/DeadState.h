// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "DeadState.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UDeadState : public UActorState
{
	GENERATED_BODY()
public:

	virtual void StateEnter() override;
};
