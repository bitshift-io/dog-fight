// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "HoverFlyState.generated.h"

/**
 * Specialised version of the fly state for UFO (and holicopter?) allows the vehicle to have dyamic speed!
 */
UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UHoverFlyState : public UFlyState
{
	GENERATED_BODY()
public:

	UHoverFlyState(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	virtual void StateInit();

	virtual void StateEnter() override;
	virtual void StateUpdate(float deltaTime) override;

	//virtual bool CanFire();

	void StopToFire();
	bool IsStopped();
	
	/** The maximum time before we try to change linear speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxTimeToChangeLinearSpeed;

	/** The minimum time before we try to change linear speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinTimeToChangeLinearSpeed;

	float TimeToChangeLinearSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ChanceToBeStationary;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bMustBeStationaryToFire;

	/** The rate (metres per second) at which LinearSpeed changes */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LinearSpeedChangeRate;

	UFUNCTION()
	void OnRep_TargetLinearSpeed();

	UPROPERTY(Transient, ReplicatedUsing=OnRep_TargetLinearSpeed)
	float TargetLinearSpeed;

	float MaxRollBackup;
	float RotationSpeedBackup;
	float MaxLinearSpeed;
};
