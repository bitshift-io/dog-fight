// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "ParachuteDyingState.h"
#include "FlyState.h"
#include "DeadState.h"
#include "GActor.h"
#include "UnrealNetwork.h"

#pragma optimize("", OPTIMISATION)

UParachuteDyingState::UParachuteDyingState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxTimeInState = 2.0f;
	Acceleration = 100.0f;
}


void UParachuteDyingState::StateEnter()
{
	Super::StateEnter();

	m_timeInState = 0.0f;
	m_linearSpeed = 0.0f;
}

void UParachuteDyingState::StateUpdate(float deltaTime)
{
	m_timeInState += deltaTime;
	if (!GetOwner())
		return;

	// accelerate move down
	m_linearSpeed += deltaTime * Acceleration * 100.0f;
	FVector position = GetOwner()->GetActorLocation();
	position = position - FVector::UpVector * m_linearSpeed * deltaTime;

	GetOwner()->SetActorLocation(position);

	Super::StateUpdate(deltaTime);

	if (m_timeInState >= MaxTimeInState)
	{
		UActorState* deadState = GetOwner()->FindState(UDeadState::StaticClass());
		GetOwner()->SetState(deadState);
	}
}
