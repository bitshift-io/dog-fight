
#pragma once

#include "Inventory.generated.h"

class AWeapon;

// Method of specifying polygons for a sprite's render or collision data
UENUM()
namespace EWeaponCycleMode
{
	enum Type
	{
		Independent,		// weapons fire independantly
		SingleCyclic,		// single cyclic - one weapon fires at a time in a cylic fashion
	};
}

USTRUCT()
struct FWeaponArcData
{
	GENERATED_USTRUCT_BODY()

	FWeaponArcData()
	{
		ConeAngle = 45.0f;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTransform	Transform;

	/** Degrees this is the half angle. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ConeAngle;

	/** centimeters. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinDistance;

	/** centimeters. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxDistance;
};

USTRUCT(BlueprintType)
struct FWeaponItem
{
	GENERATED_USTRUCT_BODY()

	FWeaponItem()
	{
		//Weapon = NULL;
		bAimAttachPointAtTarget = true;
		bSpawnWithWeapon = true;
	}

	/** weapon to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AWeapon>	WeaponClass;

	/** spawn the weapon when the actor spawns */
	UPROPERTY(EditDefaultsOnly)
	uint8 bSpawnWithWeapon : 1;

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly)
	FName WeaponAttachPoint;

	/** current firing state */
	UPROPERTY(EditDefaultsOnly)
	uint8 bAimAttachPointAtTarget : 1;

	/** list of acs this weapon can use */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FWeaponArcData>	WeaponArcs;

	//AWeapon* Weapon;
};

UCLASS(BlueprintType, Blueprintable, ClassGroup = ("Inventory"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UInventory : public USceneComponent
{
	GENERATED_BODY()
public:

	UInventory(const FObjectInitializer& ObjectInitializer);

	AGActor* GetOwner() const	{ return Cast<AGActor>(Super::GetOwner()); }

	//virtual void PostInitProperties() override;

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

	/** updates current weapon */
	void SetCurrentWeapon(AWeapon* NewWeapon, AWeapon* LastWeapon = NULL);

	/** get current weapon */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	AWeapon* GetCurrentWeapon() const { return CurrentWeapon;  }

	/** current weapon rep handler */
	UFUNCTION()
	void OnRep_CurrentWeapon(AWeapon* LastWeapon);

	/**
	* [server] add weapon to inventory
	*
	* @param Weapon	Weapon to add.
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddWeapon(AWeapon* Weapon);

	/**
	* [server] remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void RemoveWeapon(class AWeapon* Weapon);

	/**
	* Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	class AWeapon* FindWeapon(TSubclassOf<class AWeapon> WeaponClass);

	/**
	* [server + local] equips weapon from inventory
	*
	* @param Weapon	Weapon to equip
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void EquipWeapon(AWeapon* Weapon);

	/** [server] remove all weapons from inventory and destroy them */
	void DestroyInventory();

	/** equip weapon */
	UFUNCTION(reliable, server, WithValidation)
	void ServerEquipWeapon(AWeapon* NewWeapon);

	virtual bool ServerEquipWeapon_Validate(AWeapon* NewWeapon);
	virtual void ServerEquipWeapon_Implementation(AWeapon* NewWeapon);


	void DrawVisualization(const FSceneView* View, FPrimitiveDrawInterface* PDI) const;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** get list of available weapons that can attach the approrpriate target */
	TArray<AWeapon*> WeaponsThatCanAttackTarget(AGActor* Target);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<AWeapon*> WeaponList()	{ return Inventory;  }

	/** list of acs this weapon can use */
	UPROPERTY(EditAnywhere)
	TArray<FWeaponItem>	DefaultInventoryClasses;

	/** weapons in inventory */
	UPROPERTY(Transient, Replicated)
	TArray<AWeapon*> Inventory;

	/** how the AI operates the inventory items */
	UPROPERTY(EditDefaultsOnly)
	TEnumAsByte<EWeaponCycleMode::Type> WeaponCycleMode;

	/** currently equipped weapon */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	AWeapon* CurrentWeapon;

	/** current firing state */
	//uint8 bWantsToFire : 1;
};
