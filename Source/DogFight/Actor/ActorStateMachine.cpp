// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "ActorStateMachine.h"
#include "UnrealNetwork.h"

UActorStateMachine::UActorStateMachine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// https://wiki.unrealengine.com/Replication#Replication_of_Actor_Components_and_Subobjects
	SetIsReplicated(true);
	CurrentState = NULL;
}

/*
void UActorStateMachine::PostInitProperties()
{
	Super::PostInitProperties();
	StateInit();
}*/

void UActorStateMachine::StateInit(/*AGActor* owner*/)
{
	//m_owner = owner;
	Super::StateInit(/*owner*/);

	// setup and enter first state
	SetState(FindState(InitialState));

	//check(CurrentState);
}

void UActorStateMachine::StateUpdate(float deltaTime)
{
	if (CurrentState)
		CurrentState->StateUpdate(deltaTime);
}

UActorState* UActorStateMachine::FindState(UClass* staticClass)
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s] && ActorStateList[s]->IsA(staticClass))
			return ActorStateList[s];
	}

	return NULL;
}

void UActorStateMachine::SetState(UActorState* state)
{
	if (state == CurrentState)
		return;

	if (CurrentState)
		CurrentState->StateExit();

	state->StateEnter();
	CurrentState = state;
}

void UActorStateMachine::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UActorStateMachine, CurrentState);
}

void UActorStateMachine::OnRep_CurrentState(UActorState* state)
{
	// this is called once CurrentState is changed, so we need to "revert" the change
	// and apply the change via SetState
	UActorState* newState = CurrentState;
	CurrentState = state;
	SetState(newState);
}