// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "EventState.generated.h"

/*
	This just fires events so we can use blueprints to do things on state enter, exit, update etc...
*/
UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UEventState : public UActorState
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpeningEvent);
public:

	virtual void StateInit(/*AGActor* owner*/);
	virtual void StateEnter();
	virtual void StateUpdate(float deltaTime);
	virtual void StateExit();

	// this is only used so we can give the state some usable name to help identify it
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName	Name;

	/*
	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "On State Init"))
	virtual void OnStateInit();

	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "On State Enter"))
	virtual void OnStateEnter();

	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "On State Update"))
	virtual void OnStateUpdate(float deltaTime);

	UFUNCTION(BlueprintImplementableEvent, meta = (FriendlyName = "On State Exit"))
	virtual void OnStateExit();
	*/

public: // Events

	/** Called when a new item is selected in the combobox. */
	UPROPERTY(BlueprintAssignable, Category = Events)
	FOnOpeningEvent OnStateInit;

	/** Called when a new item is selected in the combobox. */
	UPROPERTY(BlueprintAssignable, Category = Events)
	FOnOpeningEvent OnStateEnter;

	/** Called when a new item is selected in the combobox. */
	UPROPERTY(BlueprintAssignable, Category = Events)
	FOnOpeningEvent OnStateUpdate;

	/** Called when a new item is selected in the combobox. */
	UPROPERTY(BlueprintAssignable, Category = Events)
	FOnOpeningEvent OnStateExit;
};
