// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "FlyState.h"
#include "ParachuteState.h"
#include "DeadState.h"
#include "GActor.h"
#include "ActorMovement.h"
#include "Controller/ControllerInterface.h"
#include "GameMode/GPlayerState.h"
#include "Weapon/GDamageType.h"
#include "Util/ComponentUtil.h"
#include "UnrealNetwork.h"

// input example:
// https://answers.unrealengine.com/questions/166084/check-keyboard-events-in-code.html

// https://wiki.unrealengine.com/First_Person_Shooter_C%2B%2B_Tutorial
//

#pragma optimize("", OPTIMISATION)

UFlyState::UFlyState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//Radius = 10.0f;
	LinearSpeed = 20.0f;
	RotationSpeed = 160.0f;
	MaxRoll = 60.0f;
	RollSpeed = 360.0f;

	SetIsReplicated(true);
}

void UFlyState::StateEnter()
{
	Super::StateEnter();
	
	// reset rotation
	// pick a random rotation
	FQuat randRot(FVector::UpVector, FMath::FRandRange(-PI, PI));
	GetOwner()->SetActorRotation(FRotator(randRot));
	m_roll = 0.0f;
}

// remember X is forward for ue4 engine!
void UFlyState::StateUpdate(float deltaTime)
{
	m_timeInState += deltaTime;
	if (!GetOwner())
		return;

	FVector fwd = GetOwner()->GetActorForwardVector();
	fwd.Z = 0.0f;
	fwd.Normalize();

	FVector position = GetOwner()->GetActorLocation();

	if (position != GetOwner()->ReplicatedMovement.Location)
	{
		int nothing = 0;
		++nothing;
	}

	float rotationInputValue = GetOwner()->HorizontalMovementAxis();

	float rollSpeedRad = FMath::DegreesToRadians(RollSpeed);
	float desiredRoll = rotationInputValue * FMath::DegreesToRadians(MaxRoll);
	float rollDiff = desiredRoll - m_roll;
	if (rollDiff < 0.0f)
		m_roll -= FMath::Min(rollSpeedRad * deltaTime, FMath::Abs(rollDiff));
	else
		m_roll += FMath::Min(rollSpeedRad * deltaTime, FMath::Abs(rollDiff));

	FQuat yaw(FVector::UpVector, rotationInputValue * FMath::DegreesToRadians(RotationSpeed) * deltaTime);
	FQuat roll(FVector::ForwardVector, -m_roll);

	// create transform with the roll removed
	FMatrix transform(fwd, FVector::CrossProduct(FVector::UpVector, fwd), FVector::UpVector, FVector::ZeroVector);
	FQuat absYaw = transform.ToQuat();

	// apply yaw and roll
	FQuat absYawNew = absYaw * roll;
	absYawNew = yaw * absYawNew;
	GetOwner()->SetActorRotation(FRotator(absYawNew));

	check(fwd.Z == 0.0f);
	position = position + (fwd * LinearSpeed * 100.0f * deltaTime); // 100 = cm to meters

	if (GetOwner()->Role == ROLE_Authority && GetOwner()->WrapAround(position))
	{
		// https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AActor/TeleportTo/index.html
		GetOwner()->TeleportTo(position, GetOwner()->GetActorRotation(), false, true);

		// would we ever want this actor state specific? no we can change that later if required
		GetOwner()->BroadcastWarp();
	}
	else
	{
		/*
		UActorMovement* MovementComponent = Cast<UActorMovement>(m_owner->GetMovementComponent());
		if (MovementComponent)
		{
			//MovementComponent->MoveSmooth(fwd * LinearSpeed * 100.0f * deltaTime, deltaTime);
			MovementComponent->RequestDirectMove(fwd * LinearSpeed * 100.0f * deltaTime, false);
		}*/

		//m_owner->AddMovementInput(fwd * LinearSpeed * 100.0f * deltaTime, 1.0f);
		//m_owner->SetActorLocation(position);
	}

	//position = position + (fwd * LinearSpeed * 100.0f * deltaTime); // TODO: BUG: done twice?!

	check(GetOwner()->GetActorLocation().Z == position.Z);


#if 0
	// network firendly movement code
	GetOwner()->AddMovementInput(fwd, LinearSpeed * 100.0f * deltaTime);
#endif

	// old movement code
	GetOwner()->SetActorLocation(position);
	//if (m_owner->GetMovementComponent())
	//	m_owner->GetMovementComponent()->SetVelocity(fwd); // does this fix lag?
	//m_owner->MoveSmooth(fwd * LinearSpeed * 100.0f * deltaTime, deltaTime);

	Super::StateUpdate(deltaTime);
}

void UFlyState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(UFlyState, m_roll);
	DOREPLIFETIME(UFlyState, RotationSpeed);	
	DOREPLIFETIME(UFlyState, LinearSpeed);	
}

void UFlyState::CheckStateTransitions()
{
	// if health less than or equal 50% go to parachute state if we have a parachute state
	UActorState* parachuteState = GetOwner()->FindState(UParachuteState::StaticClass());
	if (parachuteState && parachuteState->bIsEnabled && GetOwner()->GetHealthPercent() <= 0.5f)
	{
		GetOwner()->SetState(parachuteState);
	}

	if (GetOwner()->GetHealth() <= 0)
	{
		UActorState* flyDyingState = GetOwner()->FindState(UFlyDyingState::StaticClass());
		//if ((!parachuteState || (parachuteState && !parachuteState->bIsEnabled)) && flyDyingState && flyDyingState->bIsEnabled)
		if (flyDyingState)
		{
			GetOwner()->SetState(flyDyingState);
		}
		else
		{
			// this shuold be replaced with DeadState in metadata
			UActorState* deadState = GetOwner()->FindState(UDeadState::StaticClass());
			GetOwner()->SetState(deadState);
		}
	}
}

bool UFlyState::CanFire()
{
	return true;
}

FVector UFlyState::GetVelocity() const
{
	return GetOwner()->GetActorForwardVector() * LinearSpeed * 100.0f;
}
