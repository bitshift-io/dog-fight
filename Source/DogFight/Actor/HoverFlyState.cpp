// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "HoverFlyState.h"
#include "UnrealNetwork.h"

// input example:
// https://answers.unrealengine.com/questions/166084/check-keyboard-events-in-code.html

// https://wiki.unrealengine.com/First_Person_Shooter_C%2B%2B_Tutorial
//

#pragma optimize("", OPTIMISATION)

UHoverFlyState::UHoverFlyState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bMustBeStationaryToFire = true;
	TimeToChangeLinearSpeed = 0.f;
	ChanceToBeStationary = 0.1f;
	LinearSpeedChangeRate = 10.0f;
	MaxTimeToChangeLinearSpeed = 2.0f;
	MinTimeToChangeLinearSpeed = 1.0f;
}

void UHoverFlyState::StateInit()
{
	Super::StateInit();
	MaxLinearSpeed = LinearSpeed; // back up the linear speed
	MaxRollBackup = MaxRoll; // backup max roll
	RotationSpeedBackup = RotationSpeed;
}

void UHoverFlyState::StateEnter()
{
	Super::StateEnter();
}

// remember X is forward for ue4 engine!
void UHoverFlyState::StateUpdate(float deltaTime)
{
	Super::StateUpdate(deltaTime);

	// only run this on the server, then replicate data to clients
	if (GetOwner()->Role == ROLE_Authority)
	{
		TimeToChangeLinearSpeed -= deltaTime;
		if (TimeToChangeLinearSpeed <= 0)
		{
			TimeToChangeLinearSpeed = FMath::FRandRange(MinTimeToChangeLinearSpeed, MaxTimeToChangeLinearSpeed);

			TargetLinearSpeed = FMath::FRandRange(0.0f, MaxLinearSpeed);
			RotationSpeed = RotationSpeedBackup; // restore rotation speed when we change speed incase we were asked to stop and shoot, we dont want to rotate

			// chance to go stationary so we can shoot needs to be forced on occasion
			// so we can shoot!
			if (bMustBeStationaryToFire)
			{
				if (GetOwner()->bWantsToFire || (FMath::FRandRange(0.0f, 1.0f) < ChanceToBeStationary))
				{
					TargetLinearSpeed = 0.f;
				}
			}
		}

		// wants to shoot, so just go stationary
		if (bMustBeStationaryToFire && GetOwner()->bWantsToFire)
		{
			TargetLinearSpeed = 0.f;
		}
	}


	// move LinearSpeed towards TargetLinearSpeed
	float LinearSpeedDifference = (TargetLinearSpeed - LinearSpeed);
	float sign = FMath::Sign(LinearSpeedDifference);
	LinearSpeed += FMath::Min(FMath::Abs(LinearSpeedDifference), LinearSpeedChangeRate * deltaTime) * sign;

	// adjust max roll to be linear compared to LinearSpeed
	// this means, the slower we go, the less roll there is
	float LinearSpeedPercent = LinearSpeed / MaxLinearSpeed;
	MaxRoll = MaxRollBackup * LinearSpeedPercent;
	//m_roll = FMath::Clamp(m_roll, -MaxRoll, MaxRoll);	// adjust roll to ensure it doesnt go above MaxRoll
}

void UHoverFlyState::StopToFire()
{
	TargetLinearSpeed = 0.f;
	RotationSpeed = 0.f; // dont want to rotate to keep the enemy in our weapon view
}

bool UHoverFlyState::IsStopped()
{
	return LinearSpeed <= 0.01f;
}

/*
bool UHoverFlyState::CanFire()
{
	if (bMustBeStationaryToFire)
		return LinearSpeed <= 0.01f;

	return true;
}*/

void UHoverFlyState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(UHoverFlyState, TargetLinearSpeed);
}

void UHoverFlyState::OnRep_TargetLinearSpeed()
{
	int nothing = 0;
	++nothing;
}