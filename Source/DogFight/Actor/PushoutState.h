// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "PushoutState.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UPushoutState : public UActorState
{
	GENERATED_BODY()
public:

	virtual void StateUpdate(float deltaTime) override;

	/** Does this state cause damage to the hit actor? default is no */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CauseDamage;
};
