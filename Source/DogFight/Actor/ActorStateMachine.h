// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "ActorStateMachine.generated.h"

class AGActor;

/**
 *   FBlueprintDetails::AddEventsCategory
 */
UCLASS(BlueprintType, Blueprintable, ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UActorStateMachine : public UActorState
{
	GENERATED_BODY()
public:

	UActorStateMachine(const FObjectInitializer& ObjectInitializer);

	//virtual void PostInitProperties() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	virtual void StateInit(/*AGActor* owner*/);
	virtual void StateUpdate(float deltaTime);

	UPROPERTY(ReplicatedUsing = OnRep_CurrentState)
	UActorState*	CurrentState;

	UActorState* FindState(UClass* staticClass);
	void SetState(UActorState* state);

protected:
	
	UFUNCTION()
	virtual void OnRep_CurrentState(UActorState* state);

	/** Start state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UActorState> InitialState;
};
