#include "DogFight.h"
#include "Inventory.h"
#include "Util/MathUtil.h"

/*
#if WITH_EDITOR
#include "UnrealEd.h"
#include "ComponentVisualizer.h"
#endif
*/

#pragma optimize("", OPTIMISATION)


/*
#if WITH_EDITOR
class FUInventoryVisualizer : public FComponentVisualizer
{
public:
	// Begin FComponentVisualizer interface
	virtual void DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI) override;
	// End FComponentVisualizer interface
};

void FUInventoryVisualizer::DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	const UInventory* SpotLightComp = Cast<const UInventory>(Component);
	if (!SpotLightComp)
		return;

	SpotLightComp->DrawVisualization(View, PDI);
}
#endif
*/


UInventory::UInventory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// TODO: check this works in release
	// https://wiki.unrealengine.com/An_Introduction_to_UE4_Plugins
	//FModuleManager::LoadModuleChecked<FComponentVisualizersModule>("ComponentVisualizers").RegisterComponentVisualizer(UConeComponent::StaticClass()->GetFName(), MakeShareable(new FConeComponentVisualizer));

	/*
#if WITH_EDITOR
	if (GUnrealEd != NULL)
		GUnrealEd->RegisterComponentVisualizer(UInventory::StaticClass()->GetFName(), MakeShareable(new FUInventoryVisualizer));
#endif
		*/

	// for debugging
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

/*
void UInventory::PostInitProperties()
{
	Super::PostInitProperties();

	if (GetOwnerRole() == ROLE_Authority)
	{
		SpawnDefaultInventory();
	}
}*/


void UInventory::SpawnDefaultInventory()
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		return;
	}

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i].WeaponClass && DefaultInventoryClasses[i].bSpawnWithWeapon)
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AWeapon* NewWeapon = GetWorld()->SpawnActor<AWeapon>(DefaultInventoryClasses[i].WeaponClass, SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		EquipWeapon(Inventory[0]);
	}
}

void UInventory::DestroyInventory()
{
	if (GetOwner()->Role < ROLE_Authority)
	{
		return;
	}

	// remove all weapons from inventory and destroy them
	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AWeapon* Weapon = Inventory[i];
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
		}
	}
}

void UInventory::AddWeapon(AWeapon* Weapon)
{
	if (Weapon && GetOwner()->Role == ROLE_Authority)
	{
		// look for an appropriate weapon item data and assign to the weapon
		// this setsup firing arcs, and mount points etc...
		int32 NumWeaponClasses = DefaultInventoryClasses.Num();
		for (int32 i = 0; i < NumWeaponClasses; i++)
		{
			if (Weapon->IsA(DefaultInventoryClasses[i].WeaponClass)) //StaticClass)
			{
				Weapon->SetWeaponItemData(DefaultInventoryClasses[i]);
			}
		}

		Weapon->OnEnterInventory(GetOwner());
		Inventory.AddUnique(Weapon);
	}
}

void UInventory::RemoveWeapon(AWeapon* Weapon)
{
	if (Weapon && GetOwner()->Role == ROLE_Authority)
	{
		Weapon->OnLeaveInventory();
		Inventory.RemoveSingle(Weapon);

		// if we remove the equiped weapon, equip the first weapon
		if (CurrentWeapon == Weapon)
		{
			CurrentWeapon = NULL;
			if (Inventory.Num())
				EquipWeapon(Inventory[0]);
		}
	}
}

AWeapon* UInventory::FindWeapon(TSubclassOf<AWeapon> WeaponClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(WeaponClass))
		{
			return Inventory[i];
		}
	}

	return NULL;
}

void UInventory::EquipWeapon(AWeapon* Weapon)
{
	if (Weapon)
	{
		if (GetOwner()->Role == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon);
		}
		else
		{
			ServerEquipWeapon(Weapon);
		}
	}
}

bool UInventory::ServerEquipWeapon_Validate(AWeapon* Weapon)
{
	return true;
}

void UInventory::ServerEquipWeapon_Implementation(AWeapon* Weapon)
{
	EquipWeapon(Weapon);
}

void UInventory::OnRep_CurrentWeapon(AWeapon* LastWeapon)
{
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void UInventory::SetCurrentWeapon(class AWeapon* NewWeapon, class AWeapon* LastWeapon)
{
	AWeapon* LocalLastWeapon = NULL;

	if (LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// unequip previous
	if (LocalLastWeapon)
	{
		LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		NewWeapon->SetOwningPawn(GetOwner());	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		NewWeapon->OnEquip();
	}
}

void UInventory::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(UInventory, Inventory, COND_OwnerOnly);

	DOREPLIFETIME(UInventory, CurrentWeapon);
}

void UInventory::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// debug
	//DrawVisualization(NULL, NULL);
}

void UInventory::DrawVisualization(const FSceneView* View, FPrimitiveDrawInterface* PDI) const
{
	FTransform TransformNoScale = ComponentToWorld;
	TransformNoScale.RemoveScaling();

	for (int i = 0; i < DefaultInventoryClasses.Num(); ++i)
	{
		for (int a = 0; a < (DefaultInventoryClasses[i].WeaponArcs.Num()); ++a)
		{
			const FWeaponArcData& arcData = DefaultInventoryClasses[i].WeaponArcs[a];

			float maxDist = arcData.MaxDistance;
			if (maxDist == 0.0f)
			{
				maxDist = 1000000.0f;	// something suitable large to indicate infinite range
			}

			FTransform FinalTransform = TransformNoScale * arcData.Transform;
			DrawWireSphereCappedCone(PDI, FinalTransform, maxDist, arcData.ConeAngle, 32, 8, 10, FColor(200, 255, 255), SDPG_World);
			DrawWireSphereCappedCone(PDI, FinalTransform, arcData.MinDistance, arcData.ConeAngle, 32, 8, 10, FColor(150, 200, 255), SDPG_World);
		}
	}
}

TArray<AWeapon*> UInventory::WeaponsThatCanAttackTarget(AGActor* Target)
{
	TArray<AWeapon*> WeaponList;

	for (int i = 0; i < Inventory.Num(); ++i)
	{
		AWeapon* Weapon = Inventory[i];
		if (!Weapon)
			continue;

		if (Weapon->InFiringArc(Target))
			WeaponList.Add(Weapon);
#if 0
		// NOTE: This only accounts for rotation, it ignores translation and scale
		// on the Transform

		FVector DirToTarget = Target->GetActorLocation() - Weapon->GetMuzzleLocation();
		float DistToTarget = DirToTarget.Size();
		DirToTarget.Normalize();

		FVector Forward = GetOwner()->GetActorForwardVector(); // Weapon->GetAdjustedAim();
		
		for (int a = 0; a < (DefaultInventoryClasses[i].WeaponArcs.Num()); ++a)
		{
			const FWeaponArcData& arcData = DefaultInventoryClasses[i].WeaponArcs[a];

			FVector ArcForward = arcData.Transform.TransformVector(Forward);
			float Angle = Math::AngleBetween(DirToTarget, ArcForward);

			// outside firing arc ...
			if (Angle > arcData.ConeAngle)
				continue;

			// target to close
			if (DistToTarget < arcData.MinDistance)
				continue;

			// target to far
			if (arcData.MaxDistance > 0.0f && DistToTarget > arcData.MaxDistance)
				continue;

			// weapon has the target in firing arc
			WeaponList.Add(Weapon);
			break;
		}
#endif
	}

#if 0
	FVector DirToTarget = Target->GetActorLocation() - GetMuzzleLocation();
	float DistToTarget = DirToTarget.Size();
	DirToTarget.Normalize();

	for (int i = 0; i < DefaultInventoryClasses.Num(); ++i)
	{
		for (int a = 0; a < (DefaultInventoryClasses[i].WeaponArcs.Num()); ++a)
		{
			const FWeaponArcData& arcData = DefaultInventoryClasses[i].WeaponArcs[a];

			float maxDist = arcData.MaxDistance;
			if (maxDist == 0.0f)
			{
				maxDist = 1000000.0f;	// something suitable large to indicate infinite range
			}

			FTransform FinalTransform = TransformNoScale * arcData.Transform;
			DrawWireSphereCappedCone(PDI, FinalTransform, maxDist, arcData.ConeAngle, 32, 8, 10, FColor(200, 255, 255), SDPG_World);
			DrawWireSphereCappedCone(PDI, FinalTransform, arcData.MinDistance, arcData.ConeAngle, 32, 8, 10, FColor(150, 200, 255), SDPG_World);
		}
	}

	

	FVector AdjustedAim = GetAdjustedAim();

	float Angle = Math::AngleBetween(DirToTarget, AdjustedAim);
	bool InArc = Angle <= (WeaponConfig.FiringArc * 0.5f);
	if (!InArc)
		return false;

	// target to close
	if (DistToTarget < WeaponConfig.MinFiringRange)
		return false;

	// target to far
	if (WeaponConfig.MaxFiringRange > 0.0f && DistToTarget > WeaponConfig.MaxFiringRange)
		return false;

	return true;
#endif

	return WeaponList;
}
