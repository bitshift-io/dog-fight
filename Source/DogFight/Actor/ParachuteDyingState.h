// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "ParachuteDyingState.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UParachuteDyingState : public UActorState
{
	GENERATED_BODY()
public:

	UParachuteDyingState(const FObjectInitializer& ObjectInitializer);

	virtual void StateEnter() override;
	virtual void StateUpdate(float deltaTime) override;

protected:
	
	/** time in state before going to dead state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				MaxTimeInState;

	/** gravity */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				Acceleration;

	float 						m_timeInState;
	float						m_linearSpeed;
};
