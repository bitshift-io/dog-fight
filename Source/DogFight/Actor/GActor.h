// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ActorStateMachine.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Inventory.h"
#include "GActor.generated.h"

class UActorState;
class ATeam;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDamageAvertedSignature, AActor*, OtherActor);

/** Replicated movement data of our RootComponent.
* More efficient than engine's FRepMovement
*/
USTRUCT()
struct FRepUTMovement
{
	GENERATED_USTRUCT_BODY()

		/** @TODO FIXMESTEVE version that just replicates XY components, plus could quantize to tens easily */
		UPROPERTY()
		FVector_NetQuantize LinearVelocity;

	UPROPERTY()
		FVector_NetQuantize Location;

	/** @TODO FIXMESTEVE only need a few bits for this - maybe hide in Rotation.Roll */
	//UPROPERTY()
	//FVector_NetQuantize Acceleration;

	UPROPERTY()
		FRotator Rotation;

	FRepUTMovement()
		: LinearVelocity(ForceInit)
		, Location(ForceInit)
		//, Acceleration(ForceInit)
		, Rotation(ForceInit)
	{}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		bOutSuccess = true;

		bool bOutSuccessLocal = true;

		// update location, linear velocity
		Location.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;
		Rotation.SerializeCompressed(Ar);
		LinearVelocity.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;
		//Acceleration.NetSerialize(Ar, Map, bOutSuccessLocal);
		//bOutSuccess &= bOutSuccessLocal;

		return true;
	}

	bool operator==(const FRepUTMovement& Other) const
	{
		if (LinearVelocity != Other.LinearVelocity)
		{
			return false;
		}

		if (Location != Other.Location)
		{
			return false;
		}

		if (Rotation != Other.Rotation)
		{
			return false;
		}
		/*
		if (Acceleration != Other.Acceleration)
		{
		return false;
		}
		*/
		return true;
	}

	bool operator!=(const FRepUTMovement& Other) const
	{
		return !(*this == Other);
	}
};

/** replicated information on a hit we've taken */
USTRUCT()
struct FTakeHitInfo
{
	GENERATED_USTRUCT_BODY()

		/** The amount of damage actually applied */
		UPROPERTY()
		float ActualDamage;

	/** The damage type we were hit with. */
	UPROPERTY()
		UClass* DamageTypeClass;

	/** Who hit us */
	UPROPERTY()
		TWeakObjectPtr<class AGActor> PawnInstigator;

	/** Who actually caused the damage */
	UPROPERTY()
		TWeakObjectPtr<class AActor> DamageCauser;

	/** Specifies which DamageEvent below describes the damage received. */
	UPROPERTY()
		int32 DamageEventClassID;

	/** Rather this was a kill */
	UPROPERTY()
		uint32 bKilled : 1;

private:

	/** A rolling counter used to ensure the struct is dirty and will replicate. */
	UPROPERTY()
		uint8 EnsureReplicationByte;

	/** Describes general damage. */
	UPROPERTY()
		FDamageEvent GeneralDamageEvent;

	/** Describes point damage, if that is what was received. */
	UPROPERTY()
		FPointDamageEvent PointDamageEvent;

	/** Describes radial damage, if that is what was received. */
	UPROPERTY()
		FRadialDamageEvent RadialDamageEvent;

public:
	FTakeHitInfo()
		: ActualDamage(0)
		, DamageTypeClass(NULL)
		, PawnInstigator(NULL)
		, DamageCauser(NULL)
		, DamageEventClassID(0)
		, bKilled(false)
		, EnsureReplicationByte(0)
	{}

	FDamageEvent& GetDamageEvent()
	{
		switch (DamageEventClassID)
		{
		case FPointDamageEvent::ClassID:
			if (PointDamageEvent.DamageTypeClass == NULL)
			{
				PointDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return PointDamageEvent;

		case FRadialDamageEvent::ClassID:
			if (RadialDamageEvent.DamageTypeClass == NULL)
			{
				RadialDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return RadialDamageEvent;

		default:
			if (GeneralDamageEvent.DamageTypeClass == NULL)
			{
				GeneralDamageEvent.DamageTypeClass = DamageTypeClass ? DamageTypeClass : UDamageType::StaticClass();
			}
			return GeneralDamageEvent;
		}
	}

	void SetDamageEvent(const FDamageEvent& DamageEvent)
	{
		DamageEventClassID = DamageEvent.GetTypeID();
		switch (DamageEventClassID)
		{
		case FPointDamageEvent::ClassID:
			PointDamageEvent = *((FPointDamageEvent const*)(&DamageEvent));
			break;
		case FRadialDamageEvent::ClassID:
			RadialDamageEvent = *((FRadialDamageEvent const*)(&DamageEvent));
			break;
		default:
			GeneralDamageEvent = DamageEvent;
		}

		DamageTypeClass = DamageEvent.DamageTypeClass;
	}

	void EnsureReplication()
	{
		EnsureReplicationByte++;
	}
};

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWarpEventSignature);

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPhysicsVolumeChanged, class APhysicsVolume*, NewVolume);

/**
 
 https://docs.unrealengine.com/latest/INT/Gameplay/ClassCreation/CodeAndBlueprints/index.html
 https://docs.unrealengine.com/latest/INT/Engine/Blueprints/TechnicalGuide/ExtendingBlueprints/index.html

 https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Properties/index.html
 https://www.unrealengine.com/blog/unreal-property-system-reflection

 */
UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API AGActor : public APawn //ACharacter
{
	GENERATED_BODY()
public:

	AGActor(const FObjectInitializer& ObjectInitializer);
	/*
	// Begin Pawn overrides
	virtual UPawnMovementComponent* GetMovementComponent() const override;
	*/

	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

	void SetHorizontalMovementAxis(float value);
	void SetVerticalMovementAxis(float value);

	/** update movement axis */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetHorizontalMovementAxis(float value);

	bool ServerSetHorizontalMovementAxis_Validate(float value);
	void ServerSetHorizontalMovementAxis_Implementation(float value);

	UFUNCTION(reliable, server, WithValidation)
	void ServerSetVerticalMovementAxis(float value);

	bool ServerSetVerticalMovementAxis_Validate(float value);
	void ServerSetVerticalMovementAxis_Implementation(float value);


	float HorizontalMovementAxis() const { return m_horizontalMovementAxis;  }
	float VerticalMovementAxis() const { return m_verticalMovementAxis; }

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent);

	virtual void PostInitializeComponents() override;
	virtual void Tick(float deltaTime) override;

	virtual void BroadcastWarp();

	//UFUNCTION(NetMulticast, Reliable)
	//void ClientSetTeamTagVisible(bool bVisible);

	/** Called when the player state changes/replicated across network either team or colour */
	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Warp", DisplayName = "Set Team Tag Visible"))
	void SetTeamTagVisible(bool bVisible);

	/** pick up the shield */
	UFUNCTION(BlueprintCallable, Category = "Pickup|Shield", reliable, NetMulticast)
	void PickupShield();

	/** Called when the player state changes/replicated across network either team or colour */
	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Warp", DisplayName = "On Pickup Shield"))
	void OnPickupShield();

	/** Called when the player state changes/replicated across network either team or colour */
	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Warp", DisplayName = "On Update Colours"))
	void OnUpdateColours();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Warp", DisplayName = "On Warp"))
	void OnWarp();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Rescue", DisplayName = "On Rescue"))
	void OnRescue();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Spawn", DisplayName = "On Spawn"))
	void OnSpawn();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Take Damage", DisplayName = "On Take Damage"))
	void OnTakeDamage();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Colour", DisplayName = "On Set Colour"))
	void OnSetColour();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Colour", DisplayName = "Notify Out Of Ammo"))
	void NotifyOutOfAmmo();

	UFUNCTION(BlueprintImplementableEvent, meta = (Keywords = "Weapon", DisplayName = "On Fire"))
	void OnFire();

	/** Called when the actor is damaged in any way. */
	UPROPERTY(BlueprintAssignable, Category = "Game|Damage")
	FOnDamageAvertedSignature OnDamageAverted;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	bool WrapAround(FVector& worldPosition) const;
	FQuat ActorViewRotation() const;

	void SetTeam(class ATeamInfo* team);

	// health / death / damage
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	void GetHealthPercent(float& HealthPercent);

	float				GetHealthPercent()					{ return Health / MaxHealth; }

	float				GetHealth()							{ return Health; }
	void				SetHealth(float health)				{ Health = FMath::Max(health, 0.0f); }
	void				SetHealthPercent(float percent);

	bool IsAlive() const;
	bool IsDead() const;	// this should be isDying

	float				GetTimeAlive() const				{ return m_timeAlive; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	float				SpawnProtectionTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	uint32				bCanTakeDamage : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	float				DamageProtectionTime;

	float				m_timeAlive;
	float				m_damageTime;

#if 0
	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** updates current weapon */
	void SetCurrentWeapon(class AWeapon* NewWeapon, class AWeapon* LastWeapon = NULL);

	/** current weapon rep handler */
	UFUNCTION()
	void OnRep_CurrentWeapon(class AWeapon* LastWeapon);

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

#endif

	/**
	* Check if pawn is enemy if given controller.
	*
	* @param	TestPC	Controller to check against.
	*/
	bool IsEnemyFor(AController* TestPC) const;

#if 0
	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/**
	* [server] add weapon to inventory
	*
	* @param Weapon	Weapon to add.
	*/
	void AddWeapon(class AWeapon* Weapon);

	/**
	* [server] remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	void RemoveWeapon(class AWeapon* Weapon);

	/**
	* Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	class AWeapon* FindWeapon(TSubclassOf<class AWeapon> WeaponClass);

	/**
	* [server + local] equips weapon from inventory
	*
	* @param Weapon	Weapon to equip
	*/
	void EquipWeapon(class AWeapon* Weapon);

#endif

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] set weapon target */
	void SetWeaponTarget(AGActor* Target);

	/** [local] starts weapon fire */
	void StartWeaponFire();

	/** [local] stops weapon fire */
	void StopWeaponFire();

	/** check if pawn can fire weapon */
	bool CanFire() const;

	/** check if pawn can reload weapon */
	bool CanReload() const;

	/** [server + local] change targeting state */
	void SetTargeting(bool bNewTargeting);
#if 0
	/** [server] remove all weapons from inventory and destroy them */
	void DestroyInventory();

	/** equip weapon */
	UFUNCTION(reliable, server, WithValidation)
	void ServerEquipWeapon(class AWeapon* NewWeapon);

	virtual bool ServerEquipWeapon_Validate(class AWeapon* NewWeapon);
	virtual void ServerEquipWeapon_Implementation(class AWeapon* NewWeapon);

#endif
	/** player pressed start fire action */
	void OnStartFire();

	/** player released start fire action */
	void OnStopFire();


	/** player pressed next weapon action */
	void OnNextWeapon();

	/** player pressed prev weapon action */
	void OnPrevWeapon();


	bool IsFirstPerson() const	{ return false;  }

	/** get mesh component */
	UFUNCTION(BlueprintCallable, Category = "Game|SkinnedMesh")
	USkeletalMeshComponent* GetPawnMesh() const;

	UFUNCTION(BlueprintCallable, Category = "Game|SkinnedMesh")
	virtual void SetPawnMesh(class USkeletalMesh* NewMesh);

	/** get currently equipped weapon */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AWeapon* GetWeapon() const;

#if 0
	/** get weapon attach point */
	FName GetWeaponAttachPoint() const;
#endif

	UActorState* CurrentState() const { return StateMachine->CurrentState;  }
	UActorState* FindState(UClass* staticClass);
	void SetState(UActorState* state);
	
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UActorStateMachine* StateMachine; 

	class AWeapon* GetCurrentWeapon() const { return GetInventory()->CurrentWeapon;  }

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	UInventory* GetInventory() const	{ return Inventory; }

	/** Name of the CollisionComponent. */
	static FName InventoryComponentName;

	/** Pawn inventory component */
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventory* Inventory;

#if 0
	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AWeapon> > DefaultInventoryClasses;

	/** weapons in inventory */
	UPROPERTY(Transient, Replicated)
	TArray<class AWeapon*> Inventory;

	/** currently equipped weapon */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	class AWeapon* CurrentWeapon;

#endif

	/** current firing state */
	uint8 bWantsToFire : 1;



	UPROPERTY(Transient, Replicated)
	float m_horizontalMovementAxis;

	UPROPERTY(Transient, Replicated)
	float m_verticalMovementAxis;


	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	float	MaxHealth;

	UFUNCTION()
	void OnRep_Health();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, ReplicatedUsing = OnRep_Health)
	float	Health;

	/** Take damage, handle death */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	virtual float TakeDamageInternal(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);


	/** Pawn suicide */
	virtual void Suicide();

	/** Kill this pawn */
	virtual void KilledBy(class APawn* EventInstigator);

	/** Returns True if the pawn can die in the current state */
	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	/**
	* Kills pawn.  Server/authority only.
	* @param KillingDamage - Damage amount of the killing blow
	* @param DamageEvent - Damage event of the killing blow
	* @param Killer - Who killed this pawn
	* @param DamageCauser - the Actor that directly caused the damage (i.e. the Projectile that exploded, the Weapon that fired, etc)
	* @returns true if allowed
	*/
	virtual bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser);

	/** notification when killed, for both the server and client. */
	virtual void OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser);

	/** play effects on hit */
	virtual void PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser);

	/** sets up the replication for taking a hit */
	void ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser, bool bKilled);

	/** play hit or death on client */
	UFUNCTION()
	void OnRep_LastTakeHitInfo();

	UFUNCTION()
	void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


	/** Replicate where this pawn was last hit and damaged */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_LastTakeHitInfo)
	struct FTakeHitInfo LastTakeHitInfo;

	/** Time at which point the last take hit info for the actor times out and won't be replicated; Used to stop join-in-progress effects all over the screen */
	float LastTakeHitTimeTimeout;

	/** collisions */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComp;


	/** Delegate that will be called when PhysicsVolume has been changed **/
	UPROPERTY(BlueprintAssignable, Category = PhysicsVolume, meta = (DisplayName = "Physics Volume Changed"))
	FPhysicsVolumeChanged PhysicsVolumeChangedDelegate;

	/**  Override colour of all materials on the supplied component, used to change ammunition colour **/
	UFUNCTION(BlueprintCallable, Category = "Material")
	void OverrideColourOnAllMaterials(USceneComponent* component);

	/**  Override colour of all materials on the supplied component with the supplied colour **/
	UFUNCTION(BlueprintCallable, Category = "Material")
	void OverrideColourOnAllMaterialsWithColour(USceneComponent* component, const FLinearColor& colour);

	void SetColour(const FLinearColor& colour);

	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;

	/** [client] perform PlayerState related setup */
	virtual void OnRep_PlayerState() override;
	virtual void NotifyTeamChanged();

	/** Update the team color of all player meshes. */
	void UpdateColors();

#if 0
	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;
#endif	

	/*
		Colour of this actor
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Colour)
	FLinearColor Colour;

	/*
		The name of the parameter in the material to apply Colour too
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Colour)
	FName ColourParameterName;


	void Rescue(AGActor* rescuer);

	// from DefaultPawn.h/cpp

	/** Returns CollisionComponent subobject **/
	USphereComponent* GetCollisionComponent() const;

	/** Name of the CollisionComponent. */
	static FName CollisionComponentName;

	/** DefaultPawn collision component */
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionComponent;

	/** Name of the MovementComponent.  Use this name if you want to use a different class (with ObjectInitializer.SetDefaultSubobjectClass). */
	//static FName MovementComponentName;

	/** DefaultPawn movement component */
	//UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	//UFloatingPawnMovement* MovementComponent;

	virtual FVector GetVelocity() const;

	virtual void PostNetReceiveLocationAndRotation() override;

	virtual void OnRep_ReplicatedMovement() override;

	virtual void GatherCurrentMovement() override;

	UFUNCTION()
	virtual void OnRep_UTReplicatedMovement();

	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;

	/** UTCharacter version of GatherMovement(), gathers into UTReplicatedMovement.  Return true if using UTReplicatedMovement rather than ReplicatedMovement */
	virtual bool GatherUTMovement();

	/** Used for replication of our RootComponent's position and velocity */
	UPROPERTY(ReplicatedUsing = OnRep_UTReplicatedMovement)
	struct FRepUTMovement UTReplicatedMovement;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	void GetPlayerState(class AGPlayerState*& APlayerState) const;

	class AGPlayerState* GetPlayerState() const;

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;


	/** pawn mesh */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh;
};
