// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "DeadState.h"
#include "Actor/GActor.h"

void UDeadState::StateEnter()
{
	Super::StateEnter();

	// code from OnDeath

	// remove all weapons
	GetOwner()->GetInventory()->DestroyInventory();

	// cause this actor to be destroyed
	GetOwner()->DetachFromControllerPendingDestroy();

	/* - done in blueprints
	// disable collisions on capsule
	m_owner->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	m_owner->GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);*/
}