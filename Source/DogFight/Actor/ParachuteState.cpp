// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "ParachuteState.h"
#include "ParachuteDyingState.h"
#include "FlyState.h"
#include "DeadState.h"
#include "GActor.h"
#include "UnrealNetwork.h"

#pragma optimize("", OPTIMISATION)

UParachuteState::UParachuteState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Radius = 8.0f;
	LinearSpeed = 15.0f;
	TimeTillFly = 4;
	HorizontalMovementSpeed = 20.0f;
	VerticalMovementSpeed = 20.0f;
	MaxRoll = 45.0f;
	RollSpeed = 360.0f;
	WobbleAmount = 45.0f;
	WobbleSpeed = 3.0;

	SetIsReplicated(true);
}


void UParachuteState::StateEnter()
{
	Super::StateEnter();

	GetOwner()->SetActorRotation(FRotator(GetOwner()->ActorViewRotation()));
	m_timeInState = 0.0f;
	m_roll = 0.0f;
}

void UParachuteState::StateUpdate(float deltaTime)
{
	m_timeInState += deltaTime;
	if (!GetOwner())
		return;

	if (m_timeInState > TimeTillFly)
	{
		GetOwner()->SetState(GetOwner()->FindState(UFlyState::StaticClass()));
		GetOwner()->SetHealthPercent(1.0f);
		return;
	}

	FQuat actorViewRotation = GetOwner()->ActorViewRotation();
	FVector downDir = actorViewRotation.GetAxisX();
	FVector rightDir = actorViewRotation.GetAxisY();

	FVector fwd = GetOwner()->GetActorForwardVector();
	FVector position = GetOwner()->GetActorLocation();

	float horizMovementInputValue = GetOwner()->HorizontalMovementAxis();
	float verticalMovementInputValue = GetOwner()->VerticalMovementAxis();

	FVector horizMovement = -rightDir * (HorizontalMovementSpeed * 100.0f) * horizMovementInputValue;
	FVector verticalMovement = -downDir * (VerticalMovementSpeed * 100.0f) * verticalMovementInputValue;

	check(horizMovement.Z == 0.f);
	check(verticalMovement.Z == 0.f);
	position = position - ((downDir * LinearSpeed * 100.0f) + horizMovement + verticalMovement) * deltaTime;

	// add some wave/wobble affect for visual candies
	float wobble = ((float)FMath::Sin(m_timeInState * WobbleSpeed) - 0.5f) * (FMath::DegreesToRadians(WobbleAmount));
	
	float rollSpeedRad = (FMath::DegreesToRadians(RollSpeed));
	float desiredRoll = horizMovementInputValue * (FMath::DegreesToRadians(MaxRoll));
	float rollDiff = desiredRoll - m_roll;
	if (rollDiff < 0.0f)
		m_roll -= FMath::Min(rollSpeedRad * deltaTime, FMath::Abs(rollDiff));
	else
		m_roll += FMath::Min(rollSpeedRad * deltaTime, FMath::Abs(rollDiff));

	FQuat yaw(FVector::UpVector, wobble);
	FQuat roll(FVector::ForwardVector, -m_roll);

	FQuat absYaw = actorViewRotation;

	// apply yaw and roll
	FQuat absYawNew = absYaw *roll;
	absYawNew = yaw * absYawNew;
	GetOwner()->SetActorRotation(FRotator(absYawNew));

	// TODO: wrap around
	if (GetOwner()->WrapAround(position))
	{
		// https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AActor/TeleportTo/index.html
		GetOwner()->TeleportTo(position, GetOwner()->GetActorRotation(), false, true);

		// would we ever want this actor state specific? no we can change that later if required
		GetOwner()->BroadcastWarp();
	}
	else
	{

	}

	check(GetOwner()->GetActorLocation().Z == position.Z);
	GetOwner()->SetActorLocation(position);

	Super::StateUpdate(deltaTime);
}



void UParachuteState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(UParachuteState, m_roll);
}

void UParachuteState::CheckStateTransitions()
{
	if (GetOwner()->GetHealth() <= 0)
	{
		// this should be replaced with DeadState in metadata
		UActorState* deadState = GetOwner()->FindState(UParachuteDyingState::StaticClass());
		GetOwner()->SetState(deadState);
	}
}

float UParachuteState::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser, bool bRoot)
{
	// damage from same team? I got revived!
	if (EventInstigator && EventInstigator->PlayerState && GetOwner() && GetOwner()->PlayerState)
	{
		if (Cast<AGPlayerState>(GetOwner()->PlayerState)->GetTeam() == Cast<AGPlayerState>(EventInstigator->PlayerState)->GetTeam())
		{
			GetOwner()->Rescue(Cast<AGActor>(DamageCauser));
			return 0.0f;
		}
	}

	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser, bRoot);
}

#if 0
float UParachuteState::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	Damage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	// same team? I got revived!
	if (EventInstigator && Cast<AGPlayerState>(m_owner->PlayerState)->GetTeam() == Cast<AGPlayerState>(EventInstigator->PlayerState)->GetTeam())
	{
		m_owner->Rescue(Cast<AGActor>(DamageCauser));
		return false;
	}

	if (m_owner->GetHealth() <= 0 || m_owner->GetTimeAlive() <= m_owner->SpawnProtectionTime
		|| m_owner->m_damageTime < m_owner->DamageProtectionTime)
		return false;

	if (m_owner->Role == ROLE_Authority)
	{
		// play the force feedback effect on the client player controller
		APlayerController* PC = Cast<APlayerController>(m_owner->Controller);
		if (PC && DamageForceFeedbackEffect)
		{
			PC->ClientPlayForceFeedback(DamageForceFeedbackEffect, false, "Damage");
		}
	}

	m_owner->m_damageTime = 0.f;

	//msg->mDamageTaken = Math::Min(msg->mDamage, mOwner->GetHealth());
	m_owner->SetHealth(m_owner->GetHealth() - Damage);

	if (m_owner->GetHealth() <= 0)
	{
		//msg->mKilled = true;

		// this suold be replaced with DeadState in metadata
		UActorState* deadState = m_owner->FindState(UParachuteDyingState::StaticClass());
		m_owner->SetState(deadState);
	}

	return Damage;
}
#endif

FVector UParachuteState::GetVelocity() const
{
	return GetOwner()->GetActorForwardVector() * LinearSpeed * 100.0f;;
}
