// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "ActorState.h"


UActorState::UActorState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIsEnabled = true;
}

void UActorState::StateInit(/*AGActor* owner*/)
{
	//m_owner = owner;

	// compile a list of children
	TArray<USceneComponent*> childComp;
	GetChildrenComponents(false, childComp);
	for (int32 s = 0; s < childComp.Num(); ++s)
	{
		UActorState* state = Cast<UActorState>(childComp[s]);
		ActorStateList.Add(state);
	}

	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->StateInit();
	}
}

void UActorState::StateEnter()
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->StateEnter();
	}
}

void UActorState::StateUpdate(float deltaTime)
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->StateUpdate(deltaTime);
	}
}

void UActorState::StateExit()
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->StateExit();
	}
}

void UActorState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// This should not be required, as actor states are components
	/*
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->GetLifetimeReplicatedProps(OutLifetimeProps);
	}*/
}

float UActorState::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser, bool bRoot)
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			Damage = ActorStateList[s]->TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser, false);
	}

	// bRoot = true on first call, so it means this is a root state
	if (bRoot)
	{
		Damage = GetOwner()->TakeDamageInternal(Damage, DamageEvent, EventInstigator, DamageCauser);
		CheckStateTransitions();
	}

	return Damage;
}

void UActorState::CheckStateTransitions()
{
	for (int32 s = 0; s < ActorStateList.Num(); ++s)
	{
		if (ActorStateList[s])
			ActorStateList[s]->CheckStateTransitions();
	}
}

bool UActorState::CanFire()
{
	return false;
}

FVector UActorState::GetVelocity() const
{
	return FVector::ZeroVector;
}
