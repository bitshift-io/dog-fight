// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActorState.h"
#include "ParachuteState.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = ("Actor State Machine"), meta = (BlueprintSpawnableComponent))
class DOGFIGHT_API UParachuteState : public UActorState
{
	GENERATED_BODY()
public:

	UParachuteState(const FObjectInitializer& ObjectInitializer);

	virtual void StateEnter() override;
	virtual void StateUpdate(float deltaTime) override;

	virtual void CheckStateTransitions() override;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser, bool bRoot = true) override;

	virtual FVector GetVelocity() const;

protected:

	/** force feedback effect to play on a player hit */
	UPROPERTY(EditDefaultsOnly)
	UForceFeedbackEffect* DamageForceFeedbackEffect;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				Radius;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				LinearSpeed;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				TimeTillFly;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				HorizontalMovementSpeed;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				VerticalMovementSpeed;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				MaxRoll;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				RollSpeed;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				WobbleAmount;

	/** the desired intensity for the light */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float				WobbleSpeed;


	float 						m_timeInState;

	UPROPERTY(Replicated)
	float						m_roll;
};
