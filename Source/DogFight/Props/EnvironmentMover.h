// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EnvironmentMover.generated.h"


UCLASS(BlueprintType, Blueprintable)
class DOGFIGHT_API AEnvironmentMover : public AActor
{
	GENERATED_BODY()
public:

	AEnvironmentMover(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float deltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float VelocityScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector MaxMovement;
};
