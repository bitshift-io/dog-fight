// Fill out your copyright notice in the Description page of Project Settings.

#include "DogFight.h"
#include "EnvironmentMover.h"
#include "Controller/GPlayerController.h"
#include "Actor/GActor.h"
#include "Actor/FlyState.h"

AEnvironmentMover::AEnvironmentMover(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	USceneComponent* SceneComponent = ObjectInitializer.CreateOptionalDefaultSubobject<USceneComponent>(this, TEXT("SceneRoot"));
	RootComponent = SceneComponent;

	VelocityScale = 0.2f;
	MaxMovement = FVector(1.0f, 1.0f, 0.0f) * 10000.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	PrimaryActorTick.bStartWithTickEnabled = true;
	//PrimaryActorTick.bAllowTickOnDedicatedServer = true;
}

void AEnvironmentMover::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	// get controller/pawn list and move according to average velocity
	// we cant use this as we get a pop when player wraps
	FVector netVelocity = FVector::ZeroVector;
	for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
	{
		AController* Controller = *Iterator;
		AGPlayerController* playerController = Cast<AGPlayerController>(Controller);
		if (!playerController)
			continue;

		AGActor* actor = playerController->GetActor();
		if (!actor)
			continue;

		UFlyState* flyState = Cast<UFlyState>(actor->CurrentState());
		if (!flyState)
			continue;

		netVelocity += actor->GetActorForwardVector() * flyState->LinearSpeed * deltaTime;
	}

	FVector position = GetActorLocation();
	position = position - (netVelocity * VelocityScale) * 100.0f; // m to cm

	position.X = FMath::Clamp(position.X, -MaxMovement.X, MaxMovement.X);
	position.Y = FMath::Clamp(position.Y, -MaxMovement.Y, MaxMovement.Y);
	position.Z = FMath::Clamp(position.Z, -MaxMovement.Z, MaxMovement.Z);

	SetActorLocation(position);
}