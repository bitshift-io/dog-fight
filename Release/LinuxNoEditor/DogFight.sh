#!/bin/bash
#
# This is a script which runs programs in the Steam runtime

# Note that we put the Steam runtime first
# If ldd on a program shows any library in the system path, then that program
# may not run in the Steam runtime.
export STEAM_RUNTIME="${PWD}"

exec "DogFight/Binaries/Linux/DogFight-Linux-Shipping"

# vi: ts=4 sw=4 expandtab
