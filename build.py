#!/usr/bin/env python3
# need the 'shebang' for linux/Users/osx/Projects/DogFight/launch_editor.py

#imports
import os.path
from os import walk
import fnmatch
import shutil
import subprocess
import sys

# setup some variables
mOS = os.name
mProjectName='DogFight'
if mOS == 'nt': # windows
    os.chdir("C:\\Apps\\Epic Games\\4.11\\")
    mUrealEditor='C:\\Apps\\Epic Games\\4.11\\Engine\\Binaries\\Win64\\UE4Editor.exe'
    mBuild = "C:\\Apps\\Epic Games\\4.11\\Engine\\Build\\BatchFiles\\build.bat"
    mRunUAT = "C:\\Apps\\Epic Games\\4.11\\Engine\\Build\\BatchFiles\\RunUAT.bat"
    mProject='C:\\Projects\\DogFight\\DogFight.uproject'
    mTargetPlatform = "Win64"
    mBuildOutputDir = "C:/Projects/DogFight/Release/"
    mGenerateProjectFiles = "C:\\Apps\\Epic Games\\4.11\\Engine\\Build\\BatchFiles\\RocketGenerateProjectFiles.bat"
else:
	if sys.platform == "darwin": # yes, mac users have the IQ of an ape....
		os.chdir("/Users/osx/UnrealEngine")
		mUrealEditor='/Users/osx/UnrealEngine/Engine/Binaries/Mac/UE4Editor'
		mBuild = "/Users/osx/UnrealEngine/Engine/Build/BatchFiles/Mac/Build.sh"
		mRunUAT = "/Users/osx/UnrealEngine/Engine/Build/BatchFiles/RunUAT.sh"
		mProject='/Users/osx/Projects/DogFight/DogFight.uproject'
		mTargetPlatform = "Mac"
		mBuildOutputDir = "/Users/osx/Projects/DogFight/Release/"
	else:
		#home=str(os.path.expanduser)
		home = '/home/l'
		print(home)
		mUrealEditor = '{}/UnrealEngine/4.11/Engine/Binaries/Linux/UE4Editor'.format(home)
		mBuild = "{}/UnrealEngine/4.11/Engine/Build/BatchFiles/Linux/Build.sh".format(home)
		mRunUAT = "{}/UnrealEngine/4.11/Engine/Build/BatchFiles/RunUAT.sh".format(home)
		mProject= '{}/Projects/DogFight/DogFight.uproject'.format(home)
		mTargetPlatform = "Linux"
		mBuildOutputDir = "{}/Projects/DogFight/Release/".format(home)


def GenerateSourceProjectFiles():
    args = mGenerateProjectFiles + " -project=" + mProject + " -game"
    print(args)
    sp = subprocess.call(args.split(' '))
    return	
		
# build only code
def BuildEditorSource():
    args = mBuild + " " + mProjectName + "Editor Development " + mTargetPlatform + " " + mProject
    print(args)
    sp = subprocess.call(args.split(' '))
    return

# do a release(development) build
def BuildReleaseSource():
    args = mBuild + " " + mProjectName + " " + mTargetPlatform + " Shipping " + mProject
    sp = subprocess.call(args.split(' '))
    return

# do a release(shipping) build + cook
def PackageRelease():
    args = mRunUAT + ' BuildCookRun -project="' + mProject + '" -noP4 -targetplatform=' + mTargetPlatform + ' -clientconfig=Shipping -serverconfig=Shipping -cook -package -allmaps -build -stage -stage -pak -archive -archivedirectory="' + mBuildOutputDir + '"'
    sp = subprocess.call(args.split(' '))
    print('\n')
    print("Output will be in " + mBuildOutputDir)
    return
	
def UploadProjectToSteam():
	if mOS != 'nt':
		print('Can only run on windows')
		return
		
	print ('\nsteam username:')
	u = input(' -> ')
	print ('\nsteam password:')
	p = input(' -> ')
	cmd = "cd C:/Projects/DogFight/Steamworks/tools/ContentBuilder/builder & steamcmd.exe +login " + u + " " + p + " +run_app_build_http ../scripts/app_build_362370.vdf +quit" 
	print(cmd)
	os.system(cmd)
	
# clears the terminal/Users/osx/Projects/DogFight/Source/DogFight/DogFight.Build.cs
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
    return
	
# display a user menu
def DisplayMenu():
    print ('\nGAME BUILD SCRIPT:')
	
    print(' 1) Compile source for editor')
    print(' 2) Compile source for release')
    print(' 3) Package project for release')
    print(' 4) Upload project to steam')
    print(" 5) Generate source project files")
    print(' 6) Quit')
    x = input(' -> ')
    print('\n')

    # clear screen
    #cls()

    # no switch in python, this looks similar :)
    if x == '1':
        BuildEditorSource()
        DisplayMenu()
    elif x == '2':
        BuildReleaseSource()
        DisplayMenu()
    elif x == '3':
        PackageRelease()
        DisplayMenu()
    elif x == '4':
        UploadProjectToSteam()
        DisplayMenu()
    elif x == '5':
        GenerateSourceProjectFiles()
        DisplayMenu()
    else:
        print('exiting')
        return
		
# main
if __name__ == '__main__':
    DisplayMenu()

	
	
	
    # /home/linux/UnrealEngine/4.11/Engine/Build/BatchFiles/RunUAT.sh BuildCookRun -project="/home/linux/Projects/DogFight/DogFight".uproject -noP4 -platform=Linux -clientconfig=Development -serverconfig=Development -cook -maps=AllMaps -compile -stage -pak -archive -archivedirectory="Output Directory"

    # /home/linux/UnrealEngine/4.11/Engine/Build/BatchFiles/Linux/Build.sh DogFight Linux Development "/home/linux/Projects/DogFight/DogFight.uproject"
