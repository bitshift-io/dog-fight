#!/usr/bin/env python3
# need the 'shebang' for linux

#imports
import os.path
from os import walk
import fnmatch
import shutil
import subprocess
import sys

# setup some variables
mOS = os.name
if mOS == 'nt': # windows
    mUrealEditor='C:\\Apps\\Epic Games\\4.11\\Engine\\Binaries\\Win64\\UE4Editor.exe'
    mProject='C:\\Projects\\DogFight\\DogFight.uproject'
else:
    if sys.platform == "darwin": # yes, mac users have the IQ of an ape....
        mUrealEditor='/Users/Shared/UnrealEngine/4.11/Engine/Binaries/Mac/UE4Editor.app/Contents/MacOS/UE4Editor'
        mProject='/Users/osx/Projects/DogFight/DogFight.uproject'
    else:
        mUrealEditor='/home/l/UnrealEngine/4.11/Engine/Binaries/Linux/UE4Editor'
        mProject='/home/l/Projects/DogFight/DogFight.uproject'


# main
if __name__ == '__main__':
    # run the editor with the project
    args = '{} {}'.format(mUrealEditor, mProject)
    sp = subprocess.call(args.split(' '))
