﻿# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


bl_info = {
    'name': "FBX To UE4",
    'author': "FM",
    'version': (1, 0, 0),
    'blender': (2, 7, 2),
    'location': "VIEW_3D > TAB > TOOLS",
    'description': "FBX TO UE4",
    'category': 'EXPORT'}


import bpy, bmesh, mathutils, time
from bpy.props import BoolProperty, IntProperty, FloatProperty, EnumProperty
from collections import defaultdict
from math import radians, hypot
import os
import sys

# https://github.com/Braffe/blender-lazy-export/blob/master/exportfbx.py



class FBXtoUE4(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'Tools'
    bl_label = "FBX To UE4"
    
  
    def draw(self, context):
        scn = context.scene
        layout = self.layout
        
        col = layout.column(align=True) 
        row = col.row(align=True)
        row.operator("fbx.exportscene", text="Export Scene")
        
        row = col.row(align=True)
        row.operator("fbx.exportselected", text="Export Selected")

		
def getPath():
	directory = os.path.dirname(bpy.path.abspath("//"))
	name = bpy.path.display_name_from_filepath(bpy.data.filepath)
	#fbx_type = int(sys.argv[5])

	linux_filepath = str(directory + "/" + name + '.fbx')
	windows_filepath = str(directory + "\\" + name + '.fbx')

	if sys.platform.startswith('win'):
		path = windows_filepath
		
	elif sys.platform.startswith('linux'):
		path = linux_filepath 
		
	return path
	
class exportSelected(bpy.types.Operator):
    """Export Selected"""
    bl_label = "Export Selected"
    bl_idname = "fbx.exportselected"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):

        print("Export Selected FBX")
        bpy.ops.export_scene.fbx(check_existing=False, filepath=getPath(), filter_glob="*.fbx", use_selection=True, global_scale=1, axis_forward='-X', axis_up='Z', object_types={'MESH','ARMATURE'}, use_mesh_modifiers=True, mesh_smooth_type='EDGE',add_leaf_bones=False)
        return {"FINISHED"}
		
class exportScene(bpy.types.Operator):
    """Export All"""
    bl_label = "Export All"
    bl_idname = "fbx.exportscene"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):

        print("Export All FBX")
        bpy.ops.export_scene.fbx(check_existing=False, filepath=getPath(), filter_glob="*.fbx", use_selection=False, global_scale=1, axis_forward='-X', axis_up='Z', object_types={'MESH','ARMATURE'}, use_mesh_modifiers=True, mesh_smooth_type='EDGE', add_leaf_bones=False)
        return {"FINISHED"}
		
class exportSkeletalMesh(bpy.types.Operator):
    """Export Skeletal Mesh"""
    bl_label = "Export Skeletal Mesh"
    bl_idname = "fbx.exportskeletalmesh"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):

        print("Skeletal mesh export")
        
        bpy.context.area.spaces[0].pivot_point='CURSOR'
        bpy.context.area.spaces[0].cursor_location = (0.0, 0.0, 0.0)

        #bpy.ops.transform.rotate(value=-1.57079633, axis=(-0, -0, -1))
        bpy.ops.transform.resize(value=(100.0, 100.0, 100.0))
        
        bpy.ops.export_scene.fbx(check_existing=False, filepath=getPath(), 
        filter_glob="*.fbx", use_selection=False, global_scale=1, axis_forward='-X', 
        axis_up='Z', object_types={'MESH','ARMATURE'}, 
        use_mesh_modifiers=True, mesh_smooth_type='FACE',
        add_leaf_bones=False)
        
        bpy.ops.transform.resize(value=(0.01, 0.01, 0.01))
        #bpy.ops.transform.rotate(value=1.57079633, axis=(-0, -0, -1))
        
        return {"FINISHED"}
    

def register():

    bpy.utils.register_module(__name__)    
    pass

def unregister():
   
    bpy.utils.unregister_module(__name__)    
    pass    
    try:
        del bpy.types.WindowManager.retopowindowtool
    except:
        pass

if __name__ == "__main__":
    register()
